# Description
Presenting the design I had previously served at my only other blog back in 2015 for exhibition

## Please visit
You can see [the project](https://artavia.gitlab.io/blog-2015/ "the project at gitlab") for yourself and decide whether it is really for you or not. 

## Dedication to Master Jhoon Goo Rhee (1932 &#45; 2018)
I want to dedicate this work to one of my teachers as I was growing up in the eighties, Master Jhoon Goo Rhee. And, if you were here today I would tell you that I am very thankful to you. You were a strict man of principle who led by example and by decency. As a former student, there will always be a place in my heart for what you have graciously shown me. Even the student creed will always be a part of my being. As a teenager, you changed my life. May Christ be with you.

## A Personal Story
Three years ago my younger sister was diagnosed with terminal brain cancer. 

I built this project with God in mind and the fellowship that he has shown the world in offering his only son who rose from the dead after three (3) days; the name above all other names that is Christ Jesus. I fasted and prayed to the Lord for months that he protect my &quot;baby&quot; sister. 

So fast forward to today in 2018. I am still without a job for speaking the truth on God&rsquo;s foundation. The world is still spiraling out of control and gets more depraved by the day. But my sister is alive and without defect thanks be to the power of the son of the living God, Christ Jesus. Even her doctors could not explain it three years ago when it all went down.

I give glory to God in the name of his son Christ Jesus. I do so through obedience and he will continue to protect me in his bosom. God gives me power during any day of the week over Satan and his children of a lesser light who are in rebellion. In fact, as long as I continue to obey the Lord, my enemies become the Lord&rsquo;s enemies so that I may obtain mercy and find help and grace in a time of need. 

I have always been bullied and probably always will be. I can objectively say that there is the type of [confidence that I learned](https://www.youtube.com/watch?v=n7PEMGuA6tw "link of Master Rhee from the seventies on Youtube") as a youngster that is useful to possess (although it is of the flesh); I have had to execute it even in recent years to shut bullies up very suddenly **if a fight could not be avoided** like I have been taught. But, that type of confidence is no comparison to the other type of spiritual confidence that I have refined as an adult which is of the spirit through Christ Jesus. That fire and resolve in me which I have to this day is on a whole other level entirely and is what I tried to communicate through this project. 

That is all!

## A more brief description
This is beyond being merely a test. I have published all of the flaws and strengths which are present on purpose for the benefit of the reading public. No reorganization would take place. For the purpose of this exact presentation, however, I did have to insert five new lines into a javascript in order to accomodate for the hosting at io side of github (and gitlab). Thank you, b.t.w., because without gitlab it would not have been possible to publish at all. I am thrilled even if the technology is deprecated and could plausibly have an audience.

### A disclaimer on the lesson of practicality
The overriding offline technology called app&#45;cache that is contained in this version of the project is presently deprecated. Therefore, the next incarnation of this project will likely have service worker, then, gracefully degrade to the aforementioned deprecated piece of technology. 

It is fools play to latch on to an operating system that only supports a deprecated technology. It is only a matter of opportunity before I program in the new tech. I have previously rehearsed moving all of the pieces involved in service worker and for different contingencies involving caching setup. After a few decadent pepperoni pizza pies I should have sufficient motivation to produce it! And, obviously all of the javascript will be further consolidated and organized. The former will take place by eliminating a lot of the Internet Explorer less than version 8 functionality that is baked into a lot of the different expressions. Similar to JQuery, as the javascript specification has matured a lot of the justification for using abstraction layering just seems to have disappeared.


#### Some concessions
I did not want to invest any more time to a dead technology. But in order to publish I have to make some choices that ultimately do not end up distracting from the main message&hellip; which is to showcase html5 apis used in a progressively enhanced manner. 

In the end, there are two branches. The first is the master branch from which you can obtain a copy of the source repo. Then, there is the gh&#45;pages branch which is specific to everything specific to the facade that applies to this project. The latter is limited in scope but powerful. These are some of those more significant constraints I had to deal with in order to get this presentation to function offline at all. Those are:

  - I consciously decided to cull any use of font-face. There was one instance called Vanilla and it was embedded in each the application cache document itself (from lines 134 to 149) and in both dynamicVars.scss, then, layout.scss;
  - In terms of the wonky url() expressions that are contained in the stylesheets, I found that the easiest thing to do in this case is to maintain two versions of lines of code because presently the virtual path at IO includes the name of the project in the gh-pages branch. Those addresses are relative to 'blog-2015' which is relative to the ROOT. But when I developed this project, it was tested in ROOT and not in a subdirectory as a principle of deploy anywhere;
  - The pain point I felt the most was in 'complete.js' when I had to insert four new lines as they relate to 'var _nm_lenThresh';
  - Generally, anything having to do with the javascript variable called AssetURLs has five (and possibly up to seven) new points of adjustment where the name of the project folder-- in this case 'blog-2015' --is included in the path, as well;

### Setup
This was not developed in localhost. I developed and tested this project on my desktop. Therefore, I consider this project of type &laquo;deploy anywhere &raquo;. It works anywhere! I make it so that it runs on localhost or in another domain under your full control if you choose. Github or Gitlab do not qualify in this sense nor should they.

### To run it
You need a few programs on your machine to launch this project (reviewing said installation each are not within scope of this lesson). Those, btw can include:
  - Ruby gems for your computer
  - SASS (Syntactically Awesome Stylesheets)
  - Compass for Ruby (I have version 1.0.3 (Polaris))
  - git scm
  - npm with nodejs (this will include minor commentary shortly)

To at least check if the items are installed you can try separate commands to confirm with each of the following directives

Terminal App:
```sh
$ ruby --version
$ gem --version
$ compass --version
$ sass --version
```
### The down-low!
I am including all of the defects and flaws. I was frazzled at some moments and still am two years later. Here are some highlights in no particular order. 
  - What I want to communicate is offline first capabilities in a pre-serviceworker age. How it was done in 2015 (in this case with existing support for IE8 and less) in terms of progressive enhancement versus graceful degradation. The latter was a large focus of this project. 
  - Cookies versus web storage was an issue again because IE8 had not yet been decomissioned at the time;
  - Default language for me would be English so call it a command decision (it should not distract anyways)
  - Content is scarce in source on purpose and to said author privacy will always be considered golden; 
  - XML was the language I chose and I am ready to ditch it in favor of JSON. responseXml is more difficult than json. With responseXml, the object that will present the actual responseXml will have yet to be built in order to publish the responseXml. The same does not go for json since the response is merely caught, shuffled, and presented with half of the code required to present the data from the exact same source. 
  - I hard coded non&#45;compliant prefixed code into project rather than be boxed in with COMPASS&rsquo;s own built&#45;ins for handling transitions, animations, and CSS3 in general. The former can be dealt with but the latter is limited to Compass and code written for Compass cannot be hustled off to a SASS project. I am just pointing this out for everybody to see.

### Lets audit npm GLOBALLY
There are several programs that need to be installed globally on your machine. You will need to confirm the existence of each grunt-cli, grunt-init, and http-server. In your terminal type the following:

Terminal App:
```sh
$ npm ls -g --depth=0
```
If they are not present, install them like so:

Terminal App:
```sh
$ npm install -g grunt-cli grunt-init http-server
```

After downloading a copy of your project which I will call ROOT in lieu of blog-2015, you will want to change directory into the ROOT folder. We are going to download the development assets necessary to complete this blog with Grunt and with Compass eventually. These constitute the development programs and are qualified in the included package with the &quot;--save dev&quot; squeezed in, too.

Terminal App:
```sh
$ npm install grunt grunt-contrib-sass grunt-contrib-cssmin grunt-contrib-uglify grunt-contrib-compass grunt-contrib-watch grunt-contrib-imagemin --save-dev
```

Next, you will want to visit the site but you need to compile the project. Here is what is contained within so you can do so:
	- &laquo; grunt imgmini &raquo; This statement takes existing images from a source folder and copies a minified version to a destination folder.
	- &laquo; grunt compapa &raquo; This will compile all scss documents into their equivalent css versions.
	- &laquo; grunt buildcss &raquo; This statement will round up the code into a final document called Mangleton. I chose Mangleton not because I elected to mangle my output in my gruntfile.js. Rather, I had the movie Young Guns going in the background and Billy the Kid scolds the Texas governor&rsquo;s servant named Pendleton (Mangle + Pendleton = Mangleton).
	- &laquo; grunt buildjs &raquo; This declaration takes an uglification to all javascript and corrals it all into one document in the end.
	- &laquo; grunt default &raquo; This is merely the watch function working in the background.

In my experience, I can only get everything to run decently with two (2) simultaneous sessions. To fire off the above&#45;mentioned directives we could roll with one of these like so:

Terminal App #1:
```sh
$ grunt compapa && grunt buildcss && grunt buildjs && grunt
```
The code above will run everything without the image minification process since there are no new images to look after.

#### For the occasional hiccup
&hellip;or for trying cases of severe heartburn, too, remind me to bring that up some other time! 

Because right now, a good portion of you are probably wondering why did I see errors about &quot;DEPRECATION WARNING&quot;s or &quot;deprecated-support.scss&quot;? If you CANNOT launch and want to fix it with a well documented hack and here is how. You need to locate your copy of Compass on your machine. After finding the Compass root folder, look for &quot;stylesheets/compass/css3/_deprecated-support.scss&quot;. In that sass file, you will find a mixin&#45;function called &quot;debug-support-matrix&quot;

You will need to change this function&hellip;:
```sh
@mixin debug-support-matrix($experimental: true, $ie: true) {
  @debug  #{'$moz-'}$experimental-support-for-mozilla
          #{'$webkit-'}$experimental-support-for-webkit
          #{'$opera-'}$experimental-support-for-opera
          #{'$microsoft-'}$experimental-support-for-microsoft
          #{'$khtml-'}$experimental-support-for-khtml;
  @debug  #{'$ie6-'}$legacy-support-for-ie6
          #{'$ie7-'}$legacy-support-for-ie7
          #{'$ie8-'}$legacy-support-for-ie8;
}
```

&hellip;to look like this instead!
```sh
@mixin debug-support-matrix($experimental: true, $ie: true) {
  @debug  '#{"$moz-"}$experimental-support-for-mozilla'
          '#{"$webkit-"}$experimental-support-for-webkit'
          '#{"$opera-"}$experimental-support-for-opera'
          '#{"$microsoft-"}$experimental-support-for-microsoft'
          '#{"$khtml-"}$experimental-support-for-khtml';
  @debug  '#{"$ie6-"}$legacy-support-for-ie6'
          '#{"$ie7-"}$legacy-support-for-ie7'
          '#{"$ie8-"}$legacy-support-for-ie8';
} 
```

Now you should be able to run the steps that will initiate the grunt mini&#45;tasks I have spelled out above.

##### One last footnote
If on the other hand, you need to cull the output that was generated previously at anytime, you can simply run the following command to do so:
Terminal App #1:
```sh
$ compass clean
```
And, perhaps you are just downright having problems in launching your npm session at all. If you are having trouble, put the search engine away before you hurt yourself, and type what I am about to show you. This is the code that I run after I have been informed out of the blue of some new ERROR. 

Terminal App #1:
```sh
$ npm cache ls
$ npm cache clean
```
The first line lists your objects while the second line is obviously the secret sauce. Your nvm or npm installations do not need to have their limits raised and nothing of the sort should ever take place. Instead, objects that have accumulated to the point of annoyance need to be erased in the CLI. You can then restart your computer and your npm will be as good as new!


### Picking up where we left off&hellip;

Then, in my second terminal, I launch the server so that I can visit it in the default browser if I choose. Running &quot;http-server --help&quot; is hugely helpful. You can serve your content over either default, toy, or even real ip addresses served in seconds right to you by using any of the following:

  - Using default settings:
    Terminal App #2 ( default settings ):
    ```sh
    $ http-server
    ```
  - I like the mighty Van Halen so I use a toy address to launch sometimes like so:
    Terminal App #2 ( toy address ):
    ```sh
    $ http-server -p 5150 -a 127.1.2.8 -e -o
    ```

  - Finally, to test on mobile devices over default port 8080, I would use something similar to this after running &quot;ifconfig&quot; on +nix or &quot;ipconfig&quot; on Microsoft:
    Terminal App #2 ( local ip address ):
    ```sh
    $ http-server -p 8080 -a YOUR.REAL.IP.ADDRESS -e -o 
    ```

I was hooked on http-server the moment I tried it. It totally facilitated my development process and without it I would never been able to test on real devices. 


### Brainstorming on differences in the next version
My predictions state that PWA is next. I need time during a Saturday. I have all the pieces. I would purge anything that is attachEvent or IE related, and present the service worker as the star of the show while application cache takes &quot;second&#45;fiddle.&quot; And, I would bring back the left hand column that I intentionally made disappear. Now that I have tried it I can safely say that is kind of underwhelming to the whole experience and could distract. I will change and more this real soon. Also, the History API working with hash-bangs could satisfy the Single Page Application criteria.


### May God bless you all
