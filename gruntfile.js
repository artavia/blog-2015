module.exports = function ( grunt ){ 
	
	grunt.initConfig({
	
		pkg : grunt.file.readJSON( "package.json" ) , 
		
		banner : "/*This file was GRUNTULATED courtesy of file named <%= pkg.name %> on <%= grunt.template.today(\"yyyy-mm-dd\") %>. Yeah-yea! */\n" , 
		
		uglify: {
			
			options: {
				mangle: false
			} , 
			
			my_target: {
				files: {
					'js/projects/index/min/mangleton-min.js': ['js/projects/index/*.js'] 
				}
			} 
		},
		
		cssmin : {
		
			options : {
				banner : '<%= banner %>'
			} , 
			
			combine: {
				files: {
					
					'css/index/min/mangleton-min.css' : [ "css/index/complete.css" ]
					
				}
			} 
		} , 
		
		imagemin: {
		
			dist: {
				
				options: {
					optimizationLevel: 3
				},
				
				files: [ 
					{
						expand: true,
						cwd: 'img/index/',
						src: ['**/*.jpg'],
						dest: 'img/index/min/',
						ext: '.jpg'
					} ,
					{
						expand: true,
						cwd: 'img/index/',
						src: ['**/*.png'],
						dest: 'img/index/min/',
						ext: '.png'
					}
				] 
			}
			
		} , 
		
		// sass: {
		// 	dist: {
		// 		files: [{
		// 			expand: true,
		// 			cwd: 'scss/index/',
		// 			src: ['**/*.scss'], 
		// 			dest: 'css/index/',
		// 			ext: '.css'
		// 		}]
		// 	}
		// } ,
		
		compass: {
			dist: {
				options: {
					config: 'config.rb'
				}
			}
		} , 		
		watch: {
			
			// CSS, then, JAVASCRIPT
			// http://stackoverflow.com/questions/21859082/how-to-setup-gruntfile-js-to-watch-for-sass-compass-and-js
			
			// CSS
			css: {
				files: [ 'scss/index/**/*.scss' ], 
				/* tasks: [ 'compass' ], */  /* 'sass' is dead */
				tasks: [ 'compass', 'cssmin' ], 
				options: {
					spawn: false
				}
			} 
			
			// , 			
			// original LKGC one
			// JAVASCRIPT
			// scripts: {
			// 	files: [ 'js/projects/index/**/*.js' ] 
			// } 
			
			, 
			// JAVASCRIPT
			scripts: {
				
				// files: [ 'js/projects/index/**/*.js' ], 
				
				files: [ 'js/projects/index/*.js' ],
				tasks: [ 'uglify' ]
				
				/* eh...? ~ tbd */
				
				/* 
				, 
				options: {
					spawn: false
				} */
				
			} 
		} 
		/* , */
		
	}); // END grunt.initConfig({})
	
	// grunt.event.on( 'watch' , function( action, filepath, target ) {
	// 	grunt.log.writeln( target + ': ' + filepath + ' has ' + action );
	// } ); // END grunt.event.on( evt, cb )
	
	grunt.loadNpmTasks( "grunt-contrib-sass" );
	grunt.loadNpmTasks( "grunt-contrib-cssmin" );
	grunt.loadNpmTasks( "grunt-contrib-uglify" ); 
	grunt.loadNpmTasks( "grunt-contrib-imagemin" );	
	grunt.loadNpmTasks( "grunt-contrib-compass" );
	grunt.loadNpmTasks( "grunt-contrib-watch" );
	
	grunt.registerTask( "default", [ "watch" ] );
	//grunt.registerTask( "default" , [ "cssmin" ] );
	//grunt.registerTask( "default" , [ "cssmin" , "sass" ] ); 
	//grunt.registerTask( "sassy" , [ "sass" ] ); // KO
	
	grunt.registerTask( "compapa", [ "compass" ] );
	grunt.registerTask( "buildcss" , [ "cssmin" ] );
	grunt.registerTask( "buildjs" , [ "uglify" ] );
	grunt.registerTask( "imgmini" , [ "imagemin" ] );
	grunt.registerTask( "watchit", [ "watch" ] );
	
}; // END grunt MODULE