var donLuchoHAKI = (function () {
	
	'use strict';

	var _bpms = 15.46875;

	var _window = window; // GLOBAL element	
	var _document = window.document; // DOM Document Object
	var _viewport = window.document.documentElement; // <html> Element
	var _body = window.document.body;// <body> element
	var _navigator = window.navigator;


	var _getBodyHeight = window.document.body.offsetHeight;
	var _getBodyWidth = window.document.body.offsetWidth;
	var _getScreenWidth = window.screen.width;
	var _getScreenHeight = window.screen.height;
	var _getWindowWidth = window.innerWidth;
	var _getWindowHeight = window.innerHeight;
	var _getViewportWidth = window.document.documentElement.clientWidth;
	var _getViewportHeight = window.document.documentElement.clientHeight;


	var _arVendorPREs = [ "moz", "ms", "webkit", "o" ];

	var _visibilitychange = [ "visibilitychange", "mozvisibilitychange", "webkitvisibilitychange" , "msvisibilitychange" ]; 

	var _retDevicePixelRatio = window.devicePixelRatio; 
	
	function _AddEventoHandler( nodeFlanders, type, callback ) {
		if( type !== "DOMContentLoaded") { 
			if( nodeFlanders.addEventListener ) { 
				// W3C browser implementation 
				nodeFlanders.addEventListener( type, callback, false);
			}		
			else	
			if( nodeFlanders.attachEvent ) { 
				// IE8-- browser implementation 
				nodeFlanders.attachEvent( "on" + type, callback );
			} 		
			else { 
				// Classical Event model
				nodeFlanders["on" + type] = callback; 
			}
		}
		else 
		if( type === "DOMContentLoaded" ) { 
			if( nodeFlanders.addEventListener ) { 
				// W3C browser implementation 
				nodeFlanders.addEventListener( type, callback, false);
			}
			else 
			if( nodeFlanders.attachEvent ) { 
				if( nodeFlanders.readyState === "loading" ) {
					nodeFlanders.onreadystatechange = callback;
				}
			}
			else { 
				// Classical Event model
				nodeFlanders["on" + type] = callback; 
			}
		}
	}

	function _RetEVTsrcEL_evtTarget( leEvt ) { 
		if( typeof leEvt !== "undefined") { 
			var _EREF = leEvt; 		// w3c
		}
		else {
			var _EREF = window.event; // IE8--
		}
		if( typeof _EREF.target !== "undefined") {
			var evtTrgt = _EREF.target;	// w3c 
		}
		else {
			var evtTrgt = _EREF.srcElement; // IE8--
		}
		return evtTrgt;
	}

	function _RetValFalprevDef( leEvt ) { 
		if( typeof leEvt !== "undefined") {
			var _EREF = leEvt;  // W3C
		} 
		else {
			var _EREF = window.event; // IE8--
		}		
		if( _EREF.preventDefault) {
			_EREF.preventDefault(); // W3C // return true // --> polar opposite
		}
		else {
			_EREF.returnValue = false; // IE8 // _EREF.returnValue = true // --> polar opposite			
		}
	}

	function _RetPropertyRoutine( pm1, pm2 ) { 
		
		if( pm1 === "BlobBuilder" ) {
			var ar_vendorPreez = [ "webkit" , "WebKit" , "moz" , "Moz" , "o" , "O" , "ms" , "MS" ]; // --> // object
		}
		else {
			var ar_vendorPreez = _arVendorPREs; // [ "moz", "ms", "webkit", "o" ]  --> // object
		} 
		
		var clCharMax = ar_vendorPreez.length; // 3 			
		var leProp;
		var dL;
		var param = pm1; // getUserMedia
		var paramEl = pm2; // Navigator
		var len = param.length; 
		var nc = param.slice( 0,1 ); // g
		var Uc = param.slice( 0,1 ).toUpperCase(); // G
		var Param = param.replace( nc, Uc ); // GetUserMedia	
		if ( param in paramEl ) { 
			leProp = param; 
		} 
		for ( dL = 0; dL < clCharMax; dL = dL + 1) { 
			if ( ar_vendorPreez[ dL ] + Param in paramEl ) { 
				leProp = ar_vendorPreez[ dL ] + Param; 
			} 
		} 
		return leProp;
	}

	function _ReturnJSProperty( pm1 ) { // ( pm1, pm2 ) 
		var pm2 = document.createElement("div").style; // 
		var ar_vendorPreez = _arVendorPREs; // -->  object
		// [ "moz", "ms", "webkit", "o" ] 
		var clCharMax = ar_vendorPreez.length; // 3 
		var leProp;
		var dL;
		
		var param = pm1; // " transform "
		var paramEl = pm2; // " document.createElement("div").style "
		var len = param.length; 
		var nc = param.slice( 0,1 ); // t
		var Uc = param.slice( 0,1 ).toUpperCase(); // T
		
		var Param = param.replace( nc, Uc ); // transform	
		if ( param in paramEl ) { 
			leProp = param; 
		} 
		for ( dL = 0; dL < clCharMax; dL = dL + 1) { 
			if ( ar_vendorPreez[ dL ] + Param in paramEl ) { 
		// " transform " --> msTransform, webkitTransform, oTransform, mozTransform
				leProp = ar_vendorPreez[ dL ] + Param; 
			} 
		} 
		return leProp;
	}
	
	function _ReturnCSSProperty( pm1 ) { // ( pm1, pm2 ) 
		var dashChar = "-";
		var pm2 = document.createElement("div").style;  
		var ar_vendorPreez = _arVendorPREs; // -->  object
		// [ "moz", "ms", "webkit", "o" ] 
		var clCharMax = ar_vendorPreez.length; // 3 
		var leProp;
		var dL;
		
		var param = pm1; // " transform "
		var paramEl = pm2; // " document.createElement("div").style "
		var len = param.length; 
		var nc = param.slice( 0,1 ); // t
		var Uc = param.slice( 0,1 ).toUpperCase(); // T
		
		var Param = param.replace( nc, Uc ); // transform	
		if ( param in paramEl ) { 
			leProp = param; 
		} 
		for ( dL = 0; dL < clCharMax; dL = dL + 1) { 
			if ( ar_vendorPreez[ dL ] + Param in paramEl ) { 
		// " transform " --> -ms-transform, -webkit-transform, -o-transform, -moz-transform
				leProp = dashChar + ar_vendorPreez[ dL ] + dashChar + param; 
			} 
		} 
		return leProp;
	}
	
	function _Returnkeyframes( pm1 ) { // ( pm1 ) 
		var dashChar = "-";
		var ampChar = "@";
		
		var pm2 = document.createElement("div").style; // 
		var ar_vendorPreez = [ "moz", "ms", "webkit", "o" ]; // -->  object
		// [ "moz", "ms", "webkit", "o" ] 
		var clCharMax = ar_vendorPreez.length; // 3 
		
		var keyframes; // var leProp;
		var dL;
		
		var param = pm1; // "animationName" 
		var paramEl = pm2; // " document.createElement("div").style "
		var len = param.length; 
		var nc = param.slice( 0,1 ); // a // t
		var Uc = param.slice( 0,1 ).toUpperCase(); // A // T
		
		var Param = param.replace( nc, Uc ); // animationName 
		if ( param in paramEl ) { 
			// leProp = param; 
			keyframes = ampChar + "keyframes"; 
		} 
		
		for ( dL = 0; dL < clCharMax; dL = dL + 1) { 
			if ( ar_vendorPreez[ dL ] + Param in paramEl ) { 
				// leProp = dashChar + ar_vendorPreez[ dL ] + dashChar + param; 
				
		// " @keyframes " --> @-ms-keyframes, @-webkit-keyframes, @-o-keyframes, @-moz-keyframes
				keyframes = ampChar + dashChar + ar_vendorPreez[ dL ] + dashChar + "keyframes"; //
			} 
		} 
		//return leProp;
		return keyframes;
	}

	function _If_GeolocationAPI() {
		return !!( window.navigator.geolocation );
	}
	
	function _If_WebStorageAPI() {
		return !!( window.localStorage || window.localStorage );
	}

	function _Get_Element( elObj ) {
		return document.getElementById( elObj );
	}

	function _Get_Computed_Value( elObj, pmPropToGet ) {
		if(document.documentElement.addEventListener ) {
			return document.defaultView.getComputedStyle( elObj, "" ).getPropertyValue( pmPropToGet ).slice( 0 , document.defaultView.getComputedStyle( elObj, "" ).getPropertyValue( pmPropToGet ).length - 2 );
		}	
	}

	function _Get_OffsetWidth( elObj ) {
		return elObj.offsetWidth;
	}		
	
	function _Get_OffsetHeight( elObj ) {
		return elObj.offsetHeight;
	}
	
	function _Get_OffsetLeft( elObj ) {
		return elObj.offsetLeft;
	}
	
	function _Get_OffsetTop( elObj ) {
		return elObj.offsetTop;
	}

	function _If_Touch() {
		return !!( window.Touch );
	}

	function _If_MoveListen() {
		return !!( "listenForDeviceMovement" in window );
	}

	function _If_Drag_n_Drop() {
		
		if(document.documentElement.addEventListener) {
			return !!( "draggable" in document.createElement("span") );
		}
		else
		if(document.documentElement.attachEvent) { 
			return 1;
		}
	}

	function _If_devorient() {
		return !!( window.deviceorientation );
	}
	
	function _If_DevOrientEvent() {
		return !!( window.DeviceOrientationEvent ); // .Device ! .device
	}

	function _If_DevMotion() {
		return !!( window.DeviceMotionEvent ); // window.deviceMotionEvent
	}

	function _If_compassCalibrate() {
		return !!( "oncompassneedscalibration" in window );
	}


	// END of _private properties
	return {

		DeviceCapabilities : {
			
			html5_If_Touch : function() { 
				return _If_Touch();	
			} ,

			html5_if_listenForDeviceMovement : function() {
				return _If_MoveListen(); 
			} , 

			html5_If_Drag_n_Drop: function() {
				return _If_Drag_n_Drop();
			} , 

			html5_if_DeviceOrientationEvent : function() {
				return _If_DevOrientEvent();
			} , 

			html5_if_deviceorientation : function() {
				return _If_devorient();
			} , 

			html5_if_DeviceMotion : function() {
				return _If_DevMotion();
			} , 

			html5_if_compassCalibrate : function() {
				return _If_compassCalibrate();
			} , 

			html5_Actual_DevicePixelRatio : function() {
				return _retDevicePixelRatio;
			} 

		},

		PixelsArePixelsArePixels : { 
			bodHeight : function() {
				return _getBodyHeight;
			} , 

			bodWidth : function() {
				return _getBodyWidth;
			} , 
			
			screenHeight : function() {
				return _getScreenHeight;
			} , 
			screenWidth : function() {
				return _getScreenWidth;
			} ,
			
			glowHeight : function() {
				if(document.documentElement.addEventListener) {
					return _getWindowHeight;
				}
				else
				if(document.documentElement.attachEvent) {
					return _getViewportHeight;
				}
			} , 
			glowWidth : function() { 
				if(document.documentElement.addEventListener) {
					return _getWindowWidth;
				}
				else
				if(document.documentElement.attachEvent) {
					return _getViewportWidth;
				}
			} , 
			
			GetOffsetLeft : function( elObj ) {
				return _Get_OffsetLeft( elObj );
			} , 
			GetOffsetTop : function( elObj ) {
				return _Get_OffsetTop( elObj );
			} , 
			GetOffsetWidth : function( elObj ) {
				return _Get_OffsetWidth( elObj );
			} , 
			GetOffsetHeight : function( elObj ) {
				return _Get_OffsetHeight( elObj );
			} , 
			
			Get_Element_By_Id : function( elObj ) {
				return _Get_Element( elObj ); 
			} , 
			
			GetComputedValue : function( elObj, pmPropToGet ) {
				return _Get_Computed_Value( elObj, pmPropToGet );
			} 
			
		} , // END donLuchoHAKI.PixelsArePixelsArePixels

		Utils : { 

			SnifferREGEX : function() { 
				if(navigator.userAgent.match(/iPad/)) {
					return "iPad";
				}
				else 
				if(navigator.userAgent.match(/iPod/)) { 
					return "iPod";
				}
				else 
				if(navigator.userAgent.match(/iPhone/)) { 
					return "iPhone";
				}
				else 
				if(navigator.userAgent.match(/\) Gecko\//i)) { 
					return "Firefox";
				}
				else 
				if(navigator.userAgent.match(/Opera\/9.80/i)) { 
					return "Opera_12";
				}
				else 
				if( navigator.userAgent.match( /Safari\/537.36 OPR\/1/i ) ) { 
					return "Blink_Opera_Chrome"; 
				}
				else 
				if(navigator.userAgent.match(/\) Chrome\//i)) { 
					return "Blink_Chrome_Opera"; 
				}  
				else 
				if(navigator.userAgent.match(/MSIE 6.0/i)) { 
					return "ie6";
				}
				else 
				if(navigator.userAgent.match(/MSIE 7.0/i)) { 
					return "ie7";
				}
				else 
				if(navigator.userAgent.match(/MSIE 8.0/i)) { 
					return "ie8";
				}
				else 
				if(navigator.userAgent.match(/Version\/5/i)) { 
					return "Safari_5";
				}
				else 
				if(navigator.userAgent.match(/Linux; Android /)) { 
					return "Newer_Android_Device";
				}
				else
				if(navigator.userAgent.match(/Android.*Mobile Safari/i)) { 
					return "Android_Phone";
				}
				else 
				if(navigator.userAgent.match(/Android.*Safari/i)) { 
					return "Android_Tablet";
				}
				
				return "Sorry_Charlie";
			} , 

			SnifferOS : function() {
				
				if(navigator.userAgent.match(/iPhone OS 7_/)) { 
					return "iOS7";
				}
				else
				if(navigator.userAgent.match(/iPhone OS 6_/)) { 
					return "iOS6";
				}
				else
				if(navigator.userAgent.match(/iPhone OS 5_/)) { 
					return "iOS5";
				}
				else
				if(navigator.userAgent.match(/iPhone OS 4_/)) { 
					return "iOS4";
				} 
				else
				if(navigator.userAgent.match(/Linux; Android /)) { 
					return "Newer_Android_Device";
				}
				 
				return "No_Dice";
			} , 

			evt_u : { 
				AddEventoHandler : function( nodeFlanders, type, callback ) {
					return _AddEventoHandler( nodeFlanders, type, callback );
				} , 
				
				RetEVTsrcEL_evtTarget : function( leEvt ) {
					return _RetEVTsrcEL_evtTarget( leEvt );
				}	, 

				RetValFalprevDef : function( leEvt ) { 
					return _RetValFalprevDef( leEvt );
				} 
			} , 

			aR_u : {
				htmlFive : {
					visibilitychange : _visibilitychange
				}
			}

		} ,
		
		rafUtils : { 
			get_bpms: function() {
				return _bpms; 
			} 
		} , 

		RetELs : {
			glow : function() { 
				return _window; 
			} ,
			docObj : function() { 
				return _document; 
			} ,
			htmlEL : function() { 
				return _viewport; 
			} ,
			bodyEL : function() { 
				return _body; 
			} , 
			navEL : function() {
				return _navigator; 
			} 
		} , // END donLuchoHAKI.RetELs

		ReturnProps : { 
			RetPropertyRoutine : function( pm1, pm2 ) {
				return _RetPropertyRoutine( pm1, pm2 );
			} 
		} , // END donLuchoHAKI.ReturnProps 

		ApplyPrefixes : { 
			ReturnJSProperty : function( pm1 ) {
				return _ReturnJSProperty( pm1 );
			} , 
			ReturnCSSProperty : function( pm1 ) {
				return _ReturnCSSProperty( pm1 );
			} , 
			Returnkeyframes : function( pm1 ) {
				return _Returnkeyframes( pm1 );
			} 
		} , // END donLuchoHAKI.ApplyPrefixes 

		html5_Geolocation_API : {
			If_GeolocationAPI : function() {
				return _If_GeolocationAPI();
			}
		} , 
		
		html5_WebStorage_API : {
			If_WebStorageAPI : function() {
				return _If_WebStorageAPI();
			}
		} 

	}; // END public properties	
}()); 