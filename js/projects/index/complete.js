/* ###################################################### */
var Codified = ( function () { 
	
	/*################  ES5 pragma  ######################*/
	'use strict';	
	
	/*################  _SwtchRooVLU  ######################*/
	var _SwtchRooVLU;
	/*################  CONFIG Object  ######################*/
	
	var _obj_ConfigAll = {
		CannedStrings : {
			dvcO_labelOps : {
				assets : {
					formattedString : {
						readOut_alpha : {
							EN : {
								fullText : "<strong>alpha </strong>"
							} , 
							SP : {
								fullText : "<strong>alpha </strong>"
							} 
						} , 
						readOut_beta : {
							EN : {
								fullText : "<strong>beta </strong>"
							} , 
							SP : {
								fullText : "<strong>beta </strong>"
							} 
						} , 
						readOut_gamma : {
							EN : {
								fullText : "<strong>gamma </strong>"
							} , 
							SP : {
								fullText : "<strong>gamma </strong>"
							} 
						} ,
						absolute : {
							EN : {
								fullText : "<strong>Absolute </strong>"
							} , 
							SP : {
								fullText : "<strong>Absolute </strong>"
							} 
						} ,
						wch : {
							EN : {
								fullText : "<strong>Webkit Compass Heading: </strong>"
							} , 
							SP : {
								fullText : "<strong>Webkit Compass Heading: </strong>"
							} 
						} ,
						compass_func : {
							EN : {
								fullText : "<strong>Compass functionality: </strong>"
							} , 
							SP : {
								fullText : "<strong>Funcionalidad para br\u00FCjula: </strong>"
							} 
						} ,
						current_heading : {
							EN : {
								fullText : "<strong>Current Heading: </strong>"
							} , 
							SP : {
								fullText : "<strong>Current Heading: </strong>"
							} 
						} ,
						wca : {
							EN : {
								fullText : "<strong>Webkit Compass Accuracy: </strong>"
							} , 
							SP : {
								fullText : "<strong>Webkit Compass Accuracy: </strong>"
							} 
						} ,
						current_accuracy : {
							EN : {
								fullText : "<strong>Current Accuracy: </strong>"
							} , 
							SP : {
								fullText : "<strong>Current Accuracy: </strong>"
							} 
						} ,
						compass_accuracy : {
							EN : {
								fullText : "<strong>Compass Accuracy: </strong>"
							} , 
							SP : {
								fullText : "<strong>Compass Accuracy: </strong>"
							} 
						}
					}
				} 
			} , 
			dvcM_labelOps : {
				assets : {
					formattedString : {
						interval : {
							EN : {
								fullText : "<strong>Interval: </strong>"
							} , 
							SP : {
								fullText : "<strong>Interval: </strong>"
							} 
						} , 
						rotation_rate : {
							EN : {
								fullText : "<strong>Rotation Rate: </strong>"
							} , 
							SP : {
								fullText : "<strong>Tasa de rotaci\u00F3n: </strong>"
							} 
						} , 
						readOut_alpha : {
							EN : {
								fullText : "<strong>alpha: </strong>"
							} , 
							SP : {
								fullText : "<strong>alpha: </strong>"
							} 
						} ,
						readOut_beta : {
							EN : {
								fullText : "<strong>beta: </strong>"
							} , 
							SP : {
								fullText : "<strong>beta: </strong>"
							} 
						} ,
						readOut_gamma : {
							EN : {
								fullText : "<strong>gamma: </strong>"
							} , 
							SP : {
								fullText : "<strong>gamma: </strong>"
							} 
						} ,
						supp_A_sansG : {
							EN : {
								fullText : "<strong>Supports acceleration without gravity: </strong>"
							} , 
							SP : {
								fullText : "<strong>Acceleraci\u00F3n SIN gravedad: </strong>"
							} 
						} , 
						supp_A_inclG : {
							EN : {
								fullText : "<strong>Supports acceleration with gravity: </strong>"
							} , 
							SP : {
								fullText : "<strong>Acceleraci\u00F3n con gravedad: </strong>"
							} 
						} ,
						xRoll : {
							EN : {
								fullText : "<strong>X ~ roll: </strong>"
							} , 
							SP : {
								fullText : "<strong>X ~ roll: </strong>"
							} 
						} ,
						yPitch : {
							EN : {
								fullText : "<strong>Y ~ pitch: </strong>"
							} , 
							SP : {
								fullText : "<strong>Y ~ pitch: </strong>"
							} 
						} ,
						zYaw : {
							EN : {
								fullText : "<strong>Z ~ yaw: </strong>"
							} , 
							SP : {
								fullText : "<strong>Z ~ yaw: </strong>"
							} 
						} 
					}
				} 
			} ,
			dragNdropOps : {
				assets : {
					formattedString : {
						proAlt : {
							EN : {
								fullText : "<p><strong>Drag and Drop </strong> is <strong>supported</strong> given that initially this API was a Microsoft pet project.</p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>Drag and Drop </strong> s\u00ED <strong>deber\u00E1 servirle</strong> dado que \'D y D\' comenz\u00F3 c\u00F3mo un proyecto de Microsoft.</p>"
							}
						} , 
						pro : {
							EN : {
								fullText : "<p><strong>Drag and Drop </strong> is <strong>supported</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>Drag and Drop </strong> s\u00ED <strong>le funcionar\u00E1</strong></p>"
							}
						} , 
						anti : {
							EN : {
								fullText : "<p>It seems that <strong>Drag and Drop </strong>&#45;&#45; among other things&hellip; &#45;&#45;is <strong>not supported</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>Drag and Drop</strong>-- entre otras cosas\u2026 -- n\u00F3 <strong>le funcionar\u00E1</strong></p>"
							}
						}
					}
				}
			} , 
			touchOps : {
				assets : {
					formattedString : {
						pro : {
							EN : {
								fullText : "<p><strong>Touch</strong> interactivity is <strong>supported</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>Touch</strong> s\u00ED <strong>le funcionar\u00E1</strong></p>"
							} 
						} , 
						anti : {
							EN : {
								fullText : "<p>It seems that <strong>Touch </strong> interactivity is <strong>not supported</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que la cap\u00E1cidad asociado con <strong>Touch</strong> n\u00F3 <strong>le funcionar\u00E1</strong></p>"
							} 
						}
					}
				} 
			} , 
			devicemotionOps : {
				assets : {
					formattedString : {
						notFullySuppd : {
							EN : { 
								fullText : "<p><strong>device motion</strong> is <strong>not fully</strong> supported</p>"
							} , 
							SP : {
								fullText : "<p>Al <strong>device motion</strong> seguramente <strong>no se</strong> apoya de manera suficiente</p>"
							} 
						} , 
						pro : { 
							EN : {
								fullText : "<p class=\"caution\"> <strong>device motion</strong> may <strong>not be</strong> fully supported</p>"
							} , 
							SP : {
								fullText : "<p class=\"caution\"> con buenas probabilidades <strong>device motion</strong> es cap\u00E1z <strong>de no ser</strong> apoyado de manera completo</p>"
							} 
						} , 
						antiAlt : { 
							EN : {
								fullText : "<p>It seems that <strong>\'listenForDeviceMovement\' in window</strong> is <strong>not supported</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>\'listenForDeviceMovement\' in window</strong> n\u00F3 <strong>le funcionar\u00E1</strong></p>"
							} 
						} , 
						anti : { 
							EN : {
								fullText : "<p>It seems that <strong>DeviceMotionEvent</strong> is <strong>not supported</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>DeviceMotionEvent</strong> n\u00F3 <strong>le funcionar\u00E1</strong></p>"
							} 
						} 
					} 
				} 
			} , 
			deviceorientationOps : {
				assets : {
					nonFormatString : {
						totalChoke : {
							EN : {
								fullText : "not an object expression"
							} , 
							SP : {
								fullText : "no existe una expresi\u00F3n de objeto"
							}
						} , 
						noFullSupport : {
							EN : {
								fullText : "Not fully supported"
							} , 
							SP : {
								fullText : "No se apoya de manera suficiente"
							} 
						} , 
						noSignal : {
							EN : {
								fullText : "No signal"
							} , 
							SP : {
								fullText : "No le llega se\u00F1al"
							} 
						} , 
						noSupport : {
							EN : {
								fullText : "No support"
							} , 
							SP : {
								fullText : "No incluye apoyo"
							} 
						} , 
						noData : {
							EN : {
								fullText : "No data is being received"
							} , 
							SP : {
								fullText : "No se recibe informaci\u00F3n"
							} 
						} 
					} , 
					formattedString : {
						noFullSupport : {
							EN : {
								fullText : "<p>Not fully supported</p>"
							} , 
							SP : {
								fullText : "<p>No se apoya de manera suficiente</p>"
							} 
						} , 
						notFullySuppd : {
							EN : {
								fullText : "<p><strong>device orientation</strong> is <strong>not fully</strong> supported</p>"
							} , 
							SP : {
								fullText : "<p>Al <strong>device orientation</strong> seguramente <strong>no se</strong> apoya de manera suficiente</p>"
							} 
						} , 
						pro : {
							EN : {
								fullText : "<p class=\"caution\"> <strong>device orientation</strong> may <strong>not be</strong> fully supported</p>"
							} , 
							SP : {
								fullText : "<p class=\"caution\"> con buenas probabilidades <strong>device orientation</strong> es cap\u00E1z <strong>de no ser</strong> apoyado de manera completo</p>"
							} 
						} , 
						antiAlt : {
							EN : {
								fullText : "<p>It seems that <strong>DeviceOrientationEvent</strong> is <strong>not supported</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>DeviceOrientationEvent</strong> n\u00F3 <strong>le funcionar\u00E1</strong></p>"
							} 
						} , 
						anti : { 
							EN : {
								fullText : "<p>It seems that <strong>deviceorientation</strong> is <strong>not supported</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>deviceorientation</strong> n\u00F3 <strong>le funcionar\u00E1</strong></p>"
							} 
						}
						
					} 
				} 
			} 
		} 
	};
	/*################  private variables  ######################*/
	// ~ Universal functionality 
	var _docObj = window.document;
	var _docEL = donLuchoHAKI.RetELs.htmlEL();
	
	var _chopper = donLuchoHAKI.Utils.SnifferREGEX();
	var _chopperPoint = donLuchoHAKI.Utils.SnifferOS();

	
	// UNIVERSAL functionality ~ device.html
	var _str_url = window.location.pathname; 
	var _actv_Page = _str_url.substring( _str_url.lastIndexOf('/') + 1 ); // ~ cherry.html
	
	// ~ _RAF\_CAF functionality 
	var _mrBeepersMS;
	var _RAF;
	var _CAF;
	
	// ~ Universal AND Drag and Drop functionality ~ undry
	var _el_pro  = _docObj.getElementById( "pro" ); 
	var _el_anti = _docObj.getElementById( "anti" ); 
	
	// ~ Logo Anchor Behaviors functionality ~ text extrusion
	var _tgtEl_extrude;
	
	// ~ SELECTED LI functionality ~ 'custom_select'
	var _el_uniformNavigation = _docObj.getElementById("uniformNavigation"); 
	
	// ~ unordered list Behaviors in footer ~ RESPONSIVE dependent open close func.
	var _loopTimer_opnClo;
	var _tgtELId_opnClo = null;
	var _tgtEl_opnClo = null;	
	var _ar_TrgrAs_opnClo = null;
	var _trgrMax_opnClo = null;
	var _ar_TgtULs_opnClo = null;
	var _tgtMax_opnClo = null;
	
	// ~ all Touch functionality
	var _wboolyIf_Touch = donLuchoHAKI.DeviceCapabilities.html5_If_Touch();
	
	// ~ Drag and Drop functionality
	var _wboolyIf_D_n_D = donLuchoHAKI.DeviceCapabilities.html5_If_Drag_n_Drop();
	
	// ~ Drag and Drop functionality
	var _el_dz1 = _docObj.getElementById( "dz1" );
	var _el_dz2 = _docObj.getElementById( "dz2" );
	
	// ~ Drag and Drop Functionality
	var _allImgs = _docObj.getElementsByTagName( "img" );
	var _N;
	// ~ Drag and Drop Functionality
	var _allDivs = _docObj.getElementsByTagName( "div" );
	var _O;
	// ~ Drag and Drop Functionality
	var _ar_DZs = [];
	var _P;
	var _ar_DZsMax;
	
	// ~ Drag and Drop Functionality
	var _Q;
	var _R; 
	var _imgObj_dNd;
	var _evtTrgt_dNd;
	var _data_dNd;
	
	// ~ FAUX Drag and Drop Functionality
	var _el_faux_dz = _docObj.getElementById( "faux_dz" );
	var _offset_DDfo = false;
				
	// ~ FAUX Drag and Drop Functionality
	var _ar_fauxDraggables = [];
	var _Z;
	var _ar_fauxDZMax;
	
	// ~ FAUX Drag and Drop Functionality
	var _el_fauxMemb = _docObj.getElementById( "fauxMemb" );
	
	// ~ FAUX Drag and Drop Functionality
	var _allDraggableDivs = _docObj.getElementsByTagName( "div" );
	var _Y;
	
	// ~ FAUX Drag and Drop Functionality
	var _ar_CHGDtouches;
	var _fingerMoveCount;
	
	// ~ FAUX Drag and Drop Functionality
	var _fingerCount;
	var _W;
	
	// ~ FAUX Drag and Drop Functionality
	var _startX; 
	var _startY; 
	var _lastX; 
	var _lastY;
	
	// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html
	var _wboolyIf_Orientation1 = donLuchoHAKI.DeviceCapabilities.html5_if_DeviceOrientationEvent();
	var _wboolyIf_Orientation2 = donLuchoHAKI.DeviceCapabilities.html5_if_deviceorientation();	
	var _wboolyIf_Motion1 = donLuchoHAKI.DeviceCapabilities.html5_if_DeviceMotion();
	var _wboolyIf_Motion2 = donLuchoHAKI.DeviceCapabilities.html5_if_listenForDeviceMovement();
	var _wboolyIf_Calibration = donLuchoHAKI.DeviceCapabilities.html5_if_compassCalibrate();
	
	// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html
	// ~ Logo Anchor Behaviors functionality ~ text extrusion // TRANSITION ~ ZOOM card
	
	// CSS
	var css_transform = donLuchoHAKI.ApplyPrefixes.ReturnCSSProperty( "transform" ); 
	var css_transition = donLuchoHAKI.ApplyPrefixes.ReturnCSSProperty( "transition" ); 
	
	// JS
	var perspective = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "perspective" ); 
	var perspectiveOrigin = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "perspective-origin" );
	
	var transform = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transform" ); 
	var transformOrigin = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transform-origin" ); 
	var transformStyle = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transform-style" ); 
	var backfaceVisibility = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "backface-visibility" ); 
	
	var transition = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transition" ); 
	var transitionTimingFunction = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transition-timing-function" ); 
	var transitionProperty = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transition-property" ); 
	var transitionDuration = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transition-duration" ); 
	var transitionDelay = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transition-delay" ); 
	
	// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html
	if( _actv_Page === "device.html" ) {
		
		var _el_dvc_garden = _docObj.getElementById( "dvc_garden" );
		var _el_dvc_ball = _docObj.getElementById( "dvc_ball" );
		
		var _playWidth = parseInt( _el_dvc_ball.offsetWidth , 10 );
		var _playHeight = parseInt( _el_dvc_ball.offsetHeight , 10 );
		
		var _PNWidth = parseInt( _el_dvc_ball.parentElement.offsetWidth , 10 );
		var _PNHeight = parseInt( _el_dvc_ball.parentElement.offsetHeight , 10 );
		
		var _NuMaxX = _PNWidth - _playWidth;
		var _NuMaxY = _PNHeight - _playHeight;
		
		var _gar_maxX = _el_dvc_garden.offsetWidth - _el_dvc_ball.offsetWidth; // proposed
		// var _gar_maxX = _el_dvc_garden.clientWidth - _el_dvc_ball.clientWidth; // OLD
		
		var _gar_maxY = _el_dvc_garden.offsetHeight - _el_dvc_ball.offsetHeight; // proposed
		// var _gar_maxY = _el_dvc_garden.clientHeight - _el_dvc_ball.clientHeight; // OLD
		
		// ~ .getBoundingClientRect() ~ the amount of scrolling ( window.scrollX and window.scrollY )
		// that has been done of the VP area is taken into account when computing the TR. If they're 
		// not absolute, then, the top and left properties change as the scrolling positions change.
		
		var _gardenRect = _el_dvc_garden.getBoundingClientRect();
		var _garBallRect = _el_dvc_ball.getBoundingClientRect();
		
		var _TRgardenX = parseInt( _gardenRect.left, 10); 
		var _TRgardenY = parseInt( _gardenRect.top, 10); 
		var _TRendgardenX = parseInt( _gardenRect.right, 10);
		var _TRendgardenY = parseInt( _gardenRect.bottom, 10);
		var _gardenWidthTR = _TRendgardenX - _TRgardenX; 
		var _gardenHeightTR = _TRendgardenY - _TRgardenY;	
		
		var _TRgarBallX = parseInt( _garBallRect.left, 10); 
		var _TRgarBallY = parseInt( _garBallRect.top, 10); 
		var _TRendballX = parseInt( _garBallRect.right, 10);
		var _TRendballY = parseInt( _garBallRect.bottom, 10);
		var _garBallWidthTR = _TRendballX - _TRgarBallX; 
		var _garBallHeightTR = _TRendballY - _TRgarBallY;
		
		// If this is not the desired behavior add the crolling position to the top and left properties
		// to obtain constant values indefpendent from the current scrolling position.
		
		// use ~>> w3c window.pageXOffset || IE window.document.documentElement.scrollLeft
		// donLuchoHAKI.PixelsArePixelsArePixels.glowXScroll();
		// use ~>> w3c window.pageYOffset || IE  window.document.documentElement.scrollTop
		// donLuchoHAKI.PixelsArePixelsArePixels.glowYScroll();
		
		var _nmMultiplier = 0.01;
		var _curX = 0; // _el_dvc_ball.style.left; //  = null; // _TRgarBallX; // _el_dvc_ball.style.left;
		var _curY = 0; // _el_dvc_ball.style.top; //  = null; // _TRgarBallY; // _el_dvc_ball.style.top;
		var _vXbeta = 0; //  = null; // 0;
		var _vYgamma = 0; //  = null; // 0;
		var _pmBetaX_yesG = 0;
		var _pmGammaY_yesG = 0;
		
		// deviceorientation
		var _el_dvc_two_one_half = _docObj.getElementById( "dvc_two_one_half" );
		// deviceorientation
		var _el_deviceOrient = _docObj.getElementById( "deviceOrient" );
		var _el_svgpose = _docObj.getElementById( "svgpose" );
		// deviceorientation
		var _el_deviceOrientTwo = _docObj.getElementById( "deviceOrientTwo" );
		var _el_pngposeTwo = _docObj.getElementById( "pngposeTwo" );	
		// deviceorientation
		var _el_dvc_one = _docObj.getElementById( "dvc_one" ); 
		var _el_dvc_oneB = _docObj.getElementById( "dvc_oneB" ); 
		var _el_dvc_oneC = _docObj.getElementById( "dvc_oneC" ); 
		
		var _el_dvc_oneD = _docObj.getElementById( "dvc_oneD" ); 
		var _el_dvc_oneE = _docObj.getElementById( "dvc_oneE" ); 
		var _el_dvc_oneF = _docObj.getElementById( "dvc_oneF" ); 
		var _el_dvc_oneG = _docObj.getElementById( "dvc_oneG" ); 
		var _el_dvc_oneH = _docObj.getElementById( "dvc_oneH" ); 
		var _el_dvc_oneI = _docObj.getElementById( "dvc_oneI" ); 
		var _el_dvc_oneJ = _docObj.getElementById( "dvc_oneJ" ); 
		var _el_dvc_two = _docObj.getElementById( "dvc_two" );	
		var _el_descrdo = _docObj.getElementById( "descrdo" );
		var _el_descrdoTwo = _docObj.getElementById( "descrdoTwo" );	
		var _lastHeading;
		var _arr_DIR = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"];
		
		
		var _el_DVCninetynine = _docObj.getElementById("DVCninetynine"); // ~ COMPASS ~ DO
		var _el_dvcMembTwo = _docObj.getElementById( "dvcMembTwo" ); // ~ COMPASS ~ DO
		var _membTwoWdt = _el_dvcMembTwo.offsetWidth; // _GetComputedStyle( _el_dvcMembTwo, "width" )
		_el_dvcMembTwo.style.height = _membTwoWdt + "px";
		_el_DVCninetynine.style.height = (_membTwoWdt * 1.2) + "px";
		
		var _el_compass = _docObj.getElementById( "compass" ); 
		if( document.documentElement.addEventListener ){
			var _compassWdt = _el_compass.offsetWidth; // _GetComputedStyle( _el_compass , "width" );
			_el_compass.style.height = _compassWdt + "px";
		}
		
		var _el_spinner = _docObj.getElementById( "spinner" ); 
		if( document.documentElement.addEventListener ){
			var _spinnerWdt = _el_spinner.offsetWidth; // _GetComputedStyle( _el_spinner , "width" );
			_el_spinner.style.height = _spinnerWdt + "px";
		}
		
		var _el_pin = _docObj.getElementById( "pin" ); 
		if( document.documentElement.addEventListener ){
			var _pinWdt = _el_pin.offsetWidth; // _GetComputedStyle( _el_pin , "width" );
			_el_pin.style.height = _pinWdt + "px";	
		}
		
		var _frag_lbl_degree = _docObj.createDocumentFragment();
		var _degrees = 0;
		var _el_degree;
		var _degTInstance;
		_lastHeading = 0;
		for ( _degrees; _degrees < 360; _degrees = _degrees + 30 ) {
			_el_degree = _docObj.createElement( "label" );
			_degTInstance = _docObj.createTextNode( _degrees );
			_el_degree.appendChild( _degTInstance );
			_el_degree.setAttribute( "class" , "degree" );
			var _degRotation = "rotateZ(" + _degrees + "deg)";
			_el_degree.style[ transform ] = _degRotation; 
			_frag_lbl_degree.appendChild( _el_degree ); 
			_el_spinner.appendChild( _frag_lbl_degree ); // INSERTED DEGREE
		} 
		if( document.documentElement.addEventListener ){
			_arr_DIR.forEach( _DisplayDial );
		}
		else
		if( document.documentElement.attachEvent ){
			
			_el_dvcMembTwo.style.display = "none";
			_el_compass.style.display = "none";
			_el_spinner.style.display = "none";
			_el_pin.style.display = "none";
			_docObj.getElementById( "AccelCon" ).style.display = "none";
			_docObj.getElementById( "AccelConTwo" ).style.display = "none";
			
			_el_DVCninetynine.innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport.EN.fullText;
			
		}
		
		// END luchocioso
		
		// devicemotion
		var _el_descrdmThree = _docObj.getElementById( "descrdmThree" );
		var _el_pngposeThree = _docObj.getElementById( "pngposeThree" );
		// devicemotion
		var _el_descrdmFour = _docObj.getElementById( "descrdmFour" );
		var _el_pngposeFour = _docObj.getElementById( "pngposeFour" );
		// devicemotion
		var _el_dvc_three = _docObj.getElementById( "dvc_three" );
		var _el_dvc_threeB = _docObj.getElementById( "dvc_threeB" );
		var _el_dvc_threeC = _docObj.getElementById( "dvc_threeC" );
		var _el_dvc_threeD = _docObj.getElementById( "dvc_threeD" );	
		var _el_dvc_four = _docObj.getElementById( "dvc_four" );
		var _el_dvc_fourB = _docObj.getElementById( "dvc_fourB" );
		var _el_dvc_fourC = _docObj.getElementById( "dvc_fourC" );
		var _el_dvc_fourD = _docObj.getElementById( "dvc_fourD" );
		var _el_dvc_five = _docObj.getElementById( "dvc_five" );
		var _el_dvc_fiveB = _docObj.getElementById( "dvc_fiveB" );
		var _el_dvc_fiveC = _docObj.getElementById( "dvc_fiveC" );
		var _el_dvc_fiveD = _docObj.getElementById( "dvc_fiveD" );
		var _el_dvc_fiveE = _docObj.getElementById( "dvc_fiveE" );
		// ~ DM
		var _el_dvcMemb = _docObj.getElementById( "dvcMemb" ); // dvcMemb ~ LIGHTSABER demo ~ DM
		var _el_DVCninetyeight = _docObj.getElementById("DVCninetyeight"); // dvcMemb ~ LIGHTSABER demo ~ DM
		
	} // END if (_actv_Page) control statement
	
	// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html
	var _ck_space = " ";	// all types of events
	var _nm_posOne = +1; 
	var _nm_negOne = -1; 
	var _nm_zero = 0; 
	var _facingUp = _nm_negOne; // -1 // devicemotion ~ current
	var _gravityUnit = 9.81; // devicemotion ~ current
	var _nmHardThreeSixty = 360;
	var _nm_DM_Interval = 300;
	var _nm_DM_Event_last = _nm_zero;
	var _nm_DMlastX_Beta = _nm_zero;
	var _nm_DMlastY_Gamma = _nm_zero;
	var _nm_DO_Interval = 300;
	var _nm_DO_Event_last = _nm_zero;
	var _nm_DOlastX_Beta = _nm_zero;
	var _nm_DOlastY_Gamma = _nm_zero;
	var _nmChokeAdjustment = 0.3; // 0.7
	var _wboolyIf_NoGrav = false;
	var _wboolyIf_Grav = false;	
	var _wboolyIf_Compass = false;
	var _wboolyIf_Accuracy = false;
	var _currentHeading;
	var _currentAccuracy;
	
	if( _chopper === "Firefox" ){ // MOZILLA
		// X		[  180  ...  -180  ]
		var _xRoll_ballAdj = 180;	// -90
		var _xRoll_maxQty = -90;		
		var _xRoll_adj = 90; // -90
		var _xRoll_qtr = -90;
		var _xRoll_qtrDecr = -90; // 90;// -90 // devicemotion ~ current
		var _xRoll_adj_neg = -90;	// 90
		var _xRoll_minQty = 90;
		var _xRoll_absXrange = Math.abs( _xRoll_minQty + _xRoll_maxQty );
		// Y		[  90  ...  -90  ]
		var _yPitch_adj = 90; // -90
		var _yPitch_maxQty = 90; // -90; // 90 // devicemotion ~ current
		var _yPitch_qtr = -45;
		var _yPitch_qtrDecr = 45; 
		var _yPitch_minQty = 90; // -90 
		var _yPitch_ballAdj = 180; // Math.abs( _yPitch_minQty + _yPitch_maxQty )
	}
	else
	if( _chopper === "iPhone" || _chopper === "iPod" || _chopper === "iPad" ){ // iOS	
		// X		[  -90  ...  90  ]
		var _xRoll_ballAdj = 180;	// 90
		var _xRoll_maxQty = 90;
		var _xRoll_adj = 90; //45
		var _xRoll_qtr = 45; 
		var _xRoll_qtrDecr = -90; // -45; // -90 // devicemotion ~ current
		var _xRoll_adj_neg = -90;	// -45
		var _xRoll_minQty = -90;
		var _xRoll_absXrange = Math.abs( _xRoll_minQty + _xRoll_maxQty );
		// Y		[  -180  ...  180  ]
		var _yPitch_adj = 90; // 180 
		var _yPitch_maxQty = 90; // 180;// 90 // devicemotion ~ current
		var _yPitch_qtr = 90;
		var _yPitch_qtrDecr = -90; 
		var _yPitch_minQty = -180; // -90 
		var _yPitch_ballAdj = 180; // Math.abs( _yPitch_minQty + _yPitch_maxQty ) // 360
	}
	else { // normal interpretations
		// X		[  -180  ... 180  ]
		var _xRoll_ballAdj = 180;	
		var _xRoll_maxQty = 180;
		var _xRoll_adj = 90; //
		var _xRoll_qtr = 90; 
		var _xRoll_qtrDecr = -90; // devicemotion ~ current
		var _xRoll_adj_neg = -90;	// 
		var _xRoll_minQty = -180;	
		var _xRoll_absXrange = Math.abs( _xRoll_minQty + _xRoll_maxQty );
		// Y		[  -90  ... 90  ]
		var _yPitch_adj = 90; 
		var _yPitch_maxQty = 90; // devicemotion ~ current
		var _yPitch_qtr = 45;
		var _yPitch_qtrDecr = -45; 
		var _yPitch_minQty = -90;	
		var _yPitch_ballAdj = 180; // Math.abs( _yPitch_minQty + _yPitch_maxQty )
	}
	
	// Z ~ all interpretations	[ 0 ... 360 ]
	var _zHeadTail_midQty = 180;
	
	function _DisplayDial( ar_EL, ar_IDX, ar_PerSe ) { 
		/*FOR EACH*/ // console.log( "hk[" + ar_IDX + "] = " + ar_EL );
		if ( ar_IDX % 2 ) {
			var main = "";
		}
		else {
			var main = " main";
		} 
		var frag_lbl_point = _docObj.createDocumentFragment();
		var ar_pointTXT = [ "POINT" ];
		var el_point;
		var J;
		var pTInstance;
		for ( J = 0; J < ar_pointTXT.length; J = J + 1 ) {
			el_point = _docObj.createElement( "label" );
			pTInstance = _docObj.createTextNode( ar_EL );
			el_point.appendChild( pTInstance );
			el_point.setAttribute( "class" , "point" + main );
			var pointRotation = "rotateZ(" + ( ar_IDX * 45 ) + "deg)";
			el_point.style[ transform ]= pointRotation; 
			frag_lbl_point.appendChild( el_point ); 
			_el_spinner.appendChild( frag_lbl_point ); // INSERTED DEGREE
		}
		
		var frag_p_arrow = _docObj.createDocumentFragment();
		var ar_arrowTXT = [ "ARROW" ];
		var el_arrow;
		var K;
		for ( K = 0; K < ar_arrowTXT.length; K = K + 1) {
			el_arrow = _docObj.createElement( "p" );
			el_arrow.setAttribute( "class" , "arrow" + main ); 
			var arrowRotation = "rotateZ(" + ( ar_IDX * 45 ) + "deg)";
			el_arrow.style[ transform ] = arrowRotation; 
			frag_p_arrow.appendChild( el_arrow ); 
			_el_spinner.appendChild( frag_p_arrow ); // INSERTED ARROW
		} 
		
	} // END function _DisplayDial 	
	/* END FOR EACH*/
	
	// UNASSIGNED ROTATION COMPOSITES ~ PROPOSED #1 ~ noCompensation ~ nComp
	// var nCompGrav = _roX_BetaX_prpr( tbd ) + _ck_space + _roY_GammaY_neg( tbd ) + _ck_space + _roZ_AlphaZ_nComp( tbd ); 
	
	// UNASSIGNED ROTATION COMPOSITES ~ PROPOSED #2 ~ orientationCompensation ~ yComp
	// var yCompGrav = _roX_BetaX_prpr( tbd ) + _ck_space + _roY_GammaY_neg( tbd ) + _ck_space + _roZ_AlphaZ_yComp( tbd ); 
	
	/* ########################  start BETAs  ############################## */
	// ~ "THE ROLL" ~ X AXIS: "the roll" rotation around the vertical (y) axis represents the axis from West to East
	// X ~ BETA ~ NORMAL ~ DO  [ -180 ... +180 ]
	// X ~ BETA ~ MOZ ~ DO	  [ +180 ... -180 ]
	// X ~ BETA ~ iOS ~ DO	  [ -90 ... +90 ]
	
	/* -- device motion---------- */
	// RR.pmBeta ~= the amount of rotation around the X axis in degrees/second ( ??/s )
	// leEvt.x ~= the amount of device acceleration in meters/second^2 ( m/s^2 )
	// CCW = negative territory ~ acceleration to the left
	// CW = positive territory ~ acceleration to the right	
	
	// X ~ BETA ~ iOS~ DM 	  [   (9pm)  ( 12pm )  ( 3pm )    ( 6pm ) ]
	// X ~ BETA ~ iOS~ DM 	  [ ( -9.8 )  ( 0 )    ( +9.8 )   ( 0 ) ]	
	
	var _roX_BetaX_prpr = function( pmBta ){
		return "rotateX(" + pmBta + "deg)"; // ~ // deviceorientation ~ current ~ universal with | without Compensation
	};
	var _TreD_BetaX_GRV_fu = function( pmBta ){
		return "rotate3d(1, 0, 0, " + pmBta + "deg)"; // deviceorientation ~ current
	};
	var _TreD_BetaX_GRV_fu_neg = function( pmBta ){
		return "rotate3d(1, 0, 0, " + ( pmBta * _nm_negOne ) + "deg)"; // deviceorientation ~ current
	};
	var _ro_BetaX_Grv = function( pmBta ){ // "rotate(" + Math.round() + "deg)"
		return "rotate(" + ( ( pmBta ) / _gravityUnit * _xRoll_qtrDecr ) + "deg)"; 
	};
	/* ########################  end BETAs  ############################## */
	/* ########################  start GAMMAs  ############################## */
	// ~ "THE PITCH" ~ Y AXIS: "the pitch" rotation around the horizontal (x) axis represents the axis from South to North
	// Y ~ GAMMA ~ NORMAL ~ DO	 [ -90 ... +90 ]
	// Y ~ GAMMA ~ MOZ ~ DO		 [ +90 ... -90 ]
	// Y ~ GAMMA ~ iOS ~ DO		 [ -180 ... +180 ]	
	
	/* -- device motion---------- */
	// ~ The Y axis of the coordinate system is inverted such that translateY(100px) will move an 
	// 	element 100px downwards direction so the rotateY value should be [negative]GAMMA as in -GAMMA
	
	// RR.pmGamma ~= the amount of rotation around the Y axis in degrees/second ( ??/s )
	// leEvt.y ~= the amount of device acceleration in meters/second^2 ( m/s^2 )	
	// TIP DOWN ~ gas pedal ex. ~ Acceleration = negative territory ~ accel ftwds
	// TIP UP   ~ gas pedal ex. ~ Deceleration = positive territory ~ decel bkwds
	
	// Y ~ GAMMA ~ iOS~ DM 	  	 [ ( flat ) ( rightside~up ) ( flat ) (upside~down) ] 
	// Y ~ GAMMA ~ iOS~ DM		 [ (  0  ) (    -9.8    )   (   0   ) (   +9.8   ) ] 
	
	var _roY_GammaY_prpr = function( pmGma ){ // PROPOSED #s 1 & 2 ~ universal with | without Compensation
		return "rotateY(" + ( pmGma ) + "deg)"; 
	};
	var _roY_GammaY_neg = function( pmGma ){ // PROPOSED #s 1 & 2 ~ universal with | without Compensation
		return "rotateY(" + ( -pmGma ) + "deg)"; 
	};
	var _TreD_GammaY_GRV_fu = function( pmGma ){ // "rotate3d(1,0,0, " + ( Math.round( ) + "deg)"
		return "rotate3d(1,0,0, " + ( ( ( pmGma + _gravityUnit )/_gravityUnit ) * _yPitch_maxQty * _facingUp ) + "deg)"; 
	};
	var _ro_GammaY = function( pmGma ){ // deviceorientation ~ current
		return "rotate(" + pmGma + "deg)"; 
	};
	/* ########################  end GAMMAs  ############################## */	
	/* ########################  start ALPHAs  ############################## */
	// ~ "HEADS \ TAILS" ~ Z AXIS: not "the yaw," just "heads or tails" property represents the axis from PERPENDICULAR to the ground
	// Z ~ ALPHA ~ n/c ~ NORMAL ~ DO	  [ 0 ... +360 ]
	
	/* -- device motion---------- */
	// ~ The initial ALPHA value is 180 (device flat on back & top pointing to 12:00h ) 
	// so the rotateZ value should be ALPHA ~= -180	
	
	// RR.pmAlpha ~= the amount of rotation around the Z axis in degrees/second ( ??/s )
	// leEvt.z ~= the amount of device acceleration in meters/second^2 ( m/s^2 )	
	// UPWARDS   ~ no ex. ~ "upwards" = positive territory ~ 
	// DOWNWARDS ~ no ex. ~ "downwards" = negative territory ~ 	
	
	// Z ~ ALPHA ~ iOS ~ DM [ -->   --> ( 0900h ~ butt to me on back ~ -9.8)-->   -->( 1200h ~ stand rightside~up ~ 0 ) -->    --> ]
	// Z ~ ALPHA ~ iOS ~ DM [ -->   --> ( 0300h ~ head to me on face ~ +9.8)-->   -->( 0600h ~ stand upside~down ~ 0) -->      --> ]
	
	var _roZ_AlphaZ_prpr = function( pmAlp ){
		return "rotateZ(" + pmAlp + "deg)"; 
	};
	var _roZ_AlphaZ_nComp = function( pmAlp ){
		return "rotateZ(" + ( pmAlp - _zHeadTail_midQty ) + "deg)"; // ~ PROPOSED #1 ~ noCompensation ~ nComp
	};	
	var _roZ_AlphaZ_yComp = function( pmAlp ){
		return "rotateZ(" + ( -pmAlp - _zHeadTail_midQty ) + "deg)"; // ~ PROPOSED #2 ~ orientationCompensation ~ yComp
		// return "rotateZ(" + ( _nm_negOne * ( pmAlp - _zHeadTail_midQty ) ) + "deg)"; // ~ PROPOSED #2 ~ orientationCompensation ~ yComp
	};
	var _roZ_AlphaZ_neg = function( pmAlp ){
		return "rotateZ(-" + pmAlp + "deg)"
	};
	/* ########################  end ALPHAs  ############################## */
	
	
	// var DECLARATIONs...
	if( _actv_Page === "transition.html" ) { 
		
		// TRANSITION ~ ZOOM card 
		var _el_sectionContent = _docObj.getElementById( "sectionContent" );
		var _el_zoomWrapper = _docObj.getElementById( "zoomWrapper" );
		var _el_zoomCardOne = _docObj.getElementById( "zoomCardOne" );
		var _el_zBOne = _docObj.getElementById( "zBOne" );
		var _el_pBOne = _docObj.getElementById( "pBOne" );
		var _el_zFOne = _docObj.getElementById( "zFOne" );
		var _el_pFOne = _docObj.getElementById( "pFOne" );
		
		// TRANSITION ~ ZOOM card 
		var _fBWidth = _el_pBOne.offsetWidth;
		var _fBHeight = _el_pBOne.offsetHeight;	
		var _fFWidth = _el_pFOne.offsetWidth;
		var _fFHeight = _el_pFOne.offsetHeight;	
		
		// TRANSITION ~ ZOOM card 
		var _flipClassDIVs = _el_sectionContent.getElementsByTagName("div");
		var _flipMax = _flipClassDIVs.length;
		var x;
		var _zoomTrgt; 
		var _angle_zoom = 0;
		
		_DeployZoomFlipper();
		
		// DOMCONLO~SIDE ONLY ~ !LOAD~SIDE... EVER!
		// TRANSITION ~ three in one ~ 
		var _thrInOneTrgt; 
	}
	
	
	
	// TRANSFORM ~ two sided business card 
	if( _actv_Page === "transform.html" ){ 	
		
		// TRANSFORM ~ two sided business card
		var _el_bizWrapper = _docObj.getElementById( "bizWrapper" ); 
		var _el_bizCardOne = _docObj.getElementById( "bizCardOne" ); 
		var _el_frontBiz = _docObj.getElementById( "frontBiz" );
		var _el_backBiz = _docObj.getElementById( "backBiz" );		
		// TRANSFORM ~ two sided business card
		var _bizCardDIVs = _el_bizCardOne.getElementsByTagName("div");
		var _bizMax = _bizCardDIVs.length;
		var w;
		var _evtBizTrgt;		
		// TRANSFORM ~ two sided business card
		var _angle_biz = 0; 
		var _negScale_biz = -1;	
		var _scaleBizPM;
		
		
		// TRANSFORM ~ law
		var _el_lawWrapper = _docObj.getElementById( "lawWrapper" ); 
		var _el_lawCardOne = _docObj.getElementById( "lawCardOne" );
		var _el_frontLaw = _docObj.getElementById( "frontLaw" );		
		var _el_backLaw = _docObj.getElementById( "backLaw" ); 
		
		// TRANSFORM ~ law
		var _GrafsLaw = _el_lawCardOne.getElementsByTagName( "p" );
		var _pGmax_law = _GrafsLaw.length;
		var j;
		
		// TRANSFORM ~ law
		var _angle_law = 0; 
		var _nm_negScale_law = -1;
		var _scaleLawPM;
		var _lawTrgt;
		// TRANSFORM ~ law
		_DeployLawFlipper(); 
	} 
	
	// ~ NEXT...
	
	if(document.documentElement.addEventListener) {
		
		// ~ Logo Anchor Behaviors functionality ~ text extrusion
		/*var _kicker = _docObj.querySelector( "#kicker.gear" );*/
		var _el_anchorX = _docObj.querySelector( "#anchorX.gear" );
		var _anchorXChildren = _el_anchorX.children; // purposely repeated
		var _XchildMax = _anchorXChildren.length; // purposely repeated
		var _X; // purposely repeated
		var _el_childXSpan; // purposely repeated
		
		// ~ Logo Anchor Behaviors functionality ~ text extrusion
		_el_anchorX.style[ transitionTimingFunction ] = "ease-out";
		_el_anchorX.style[ transitionDuration ] = "0.703125s";
		_el_anchorX.style[ transitionProperty ] = "all"; 
		// ~ Logo Anchor Behaviors functionality ~ text extrusion
		for( _X = 0; _X < _XchildMax; _X = _X + 1 ) {  
			_el_childXSpan = _anchorXChildren[ _X ]; // console.log( "_el_childXSpan" , _el_childXSpan );
			_el_childXSpan.style[ transitionTimingFunction ] = "ease-out";
			_el_childXSpan.style[ transitionDuration ] = "0.703125s";
			_el_childXSpan.style[ transitionProperty ] = "all"; 
		} 
		
		// ~ Logo Anchor Behaviors functionality ~ text extrusion
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_anchorX, "mousedown", _Extrude ); // winner or "mouseup"
		// donLuchoHAKI.Utils.evt_u.AddEventoHandler( _kicker, "mousedown", _Extrude ); // winner or "mouseup"
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_anchorX, "click", _PrevDeffo ); // winner 
		
		// modal ops ~ 86 110 128 305 316
		/* margin-top: -450px; */ /* ~= margin-top @150px and height 300px ~= #specialBox offset */
		var _el_apparent = _docObj.querySelector("#apparent");
		var _el_overlay = _docObj.querySelector("#overlay");
		var _el_specialBox = _docObj.querySelector("#specialBox");
		var _el_retexpUL4 = _docObj.querySelector("#retexpUL4");
		
		// ~ unordered list Behaviors in footer ~ RESPONSIVE dependent open close func.
		var _el_footrow = _docObj.querySelector("#footrow");
	}
	else
	if(document.documentElement.attachEvent) {
		
		// ~ Logo Anchor Behaviors functionality ~ text extrusion
		/*var _kicker = _docObj.getElementById( "kicker" );*/
		var _el_anchorX = _docObj.getElementById( "anchorX" );
		var _anchorXChildren = _el_anchorX.children; // purposely repeated
		var _XchildMax = _anchorXChildren.length; // purposely repeated
		var _X; // purposely repeated
		var _el_childXSpan; // purposely repeated
		
		// ~ Logo Anchor Behaviors functionality ~ text extrusion
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_anchorX, "click", _PrevDeffo ); // winner 
		
		// modal ops ~ 86 110 128 305 316
		/* margin-top: -450px; */ /* ~= margin-top @150px and height 300px ~= #specialBox offset */ 
		var _el_apparent = _docObj.getElementById("apparent");
		var _el_overlay = _docObj.getElementById("overlay");
		var _el_specialBox = _docObj.getElementById("specialBox");
		var _el_retexpUL4 = _docObj.getElementById("retexpUL4");
		
		// ~ unordered list Behaviors in footer ~ RESPONSIVE dependent open close func.
		var _el_footrow = _docObj.getElementById("footrow");
		
	} // END control addEvtLs vs atcEvt
	
	
	/*################  utility functions  ######################*/
	function _PrevDeffo( leEvt ) { 
		donLuchoHAKI.Utils.evt_u.RetValFalprevDef( leEvt ); 
		//console.log( "yeah, baby 2 -- leEvt" , leEvt );
	} // END function
	
	function _GetOffsetLeft( elObj ) {
		return donLuchoHAKI.PixelsArePixelsArePixels.GetOffsetLeft( elObj );
	} // END function
	
	function _GetComputedStyle( elObj, pmPropToGet ) {
		return donLuchoHAKI.PixelsArePixelsArePixels.GetComputedValue( elObj, pmPropToGet );
	} // END function
	
	function _GetOffsetTop( elObj ) {
		return donLuchoHAKI.PixelsArePixelsArePixels.GetOffsetTop( elObj );
	} // END function
	
	
	/*################  private functions  ######################*/
	// modal ops ~ 86 110 128 305 316
	/* margin-top: -450px; */ /* ~= margin-top @150px and height 300px ~= #specialBox offset */
	function _ToggleOverlay() {	
		
		_el_overlay.style.opacity = .83; // ORIG
		_el_overlay.style.filter = "alpha(opacity=83)"; // ORIG
		
		if(document.documentElement.addEventListener){ 
			if( _chopperPoint === "iOS4" ){
				if(_el_overlay.style.display === "block") {
					_el_apparent.style.marginTop = "0px"; 
					_el_overlay.style.display = "none"; // ORIG
					_el_specialBox.style.display = "none"; // ORIG
				}
				else
				if(_el_overlay.style.display === "none") {
					window.scroll(0,0);	// ORIG
					
					_el_apparent.style.marginTop = "-450px"; // ORIG
					_el_overlay.style.display = "block"; // ORIG
					_el_specialBox.style.display = "block"; // ORIG
				}
				else {
					_el_apparent.style.marginTop = "0px"; // ORIG
					_el_overlay.style.display = "none"; // ORIG
					_el_specialBox.style.display = "none"; // ORIG
				}
			}
			else
			if( _chopperPoint !== "iOS4" ){
				if(_el_overlay.style.display === "block") {
					_el_apparent.style.marginTop = "0px"; 
					_el_overlay.style.display = "none"; // ORIG
					_el_specialBox.style.display = "none"; // ORIG
					
					if( !!(window.matchMedia !== "undefined" && _chopper === "iPad" && window.matchMedia( "only screen and (max-width: 768px) and (max-device-width: 768px) and (orientation: portrait)" ).matches ) ) {
						
						_el_apparent.style.marginTop = "0px"; 
						_el_overlay.style.display = "none"; // ORIG
						_el_specialBox.style.display = "none"; // ORIG
					}
				}
				else
				if(_el_overlay.style.display === "none") {
					
					if(_el_overlay.style.display === "none" && ( window.matchMedia( "only screen and (max-width: 768px) and (max-device-width: 768px) and (orientation: portrait)" ).matches ) ) {
						
						// window.scroll(0,0);	// ORIG -- TOASTED 
						
						_el_apparent.style.marginTop = "-550px"; // ORIG // "-450px"
						_el_overlay.style.display = "block"; // ORIG
						_el_specialBox.style.display = "block"; // ORIG
						
					}
					
					else
					if(_el_overlay.style.display === "none" && !( window.matchMedia( "only screen and (max-width: 768px) and (max-device-width: 768px) and (orientation: portrait)" ).matches ) ) {
						
						window.scroll(0,0);	// ORIG
						
						_el_apparent.style.marginTop = "-450px"; // ORIG
						_el_overlay.style.display = "block"; // ORIG
						_el_specialBox.style.display = "block"; // ORIG
					}
					
				}
				else { 
					_el_apparent.style.marginTop = "0px"; // ORIG
					_el_overlay.style.display = "none"; // ORIG
					_el_specialBox.style.display = "none"; // ORIG
					
					if( !!(window.matchMedia !== "undefined" && _chopper === "iPad" && window.matchMedia( "only screen and (max-width: 768px) and (max-device-width: 768px) and (orientation: portrait)" ).matches ) ) {
						_el_apparent.style.marginTop = "0px"; // ORIG
						_el_overlay.style.display = "none"; // ORIG
						_el_specialBox.style.display = "none"; // ORIG
						
					}
				}
			}
		} 
		else 
		if(document.documentElement.attachEvent){
			// start chopper IE6
			if(_el_overlay.style.display === "block") {
				_el_apparent.style.marginTop = "0px"; // ORIG
				_el_overlay.style.display = "none"; // ORIG
				_el_overlay.style.position = "static";
				_el_specialBox.style.display = "none"; // ORIG
			}
			else 
			if(_el_overlay.style.display === "none") { 
				window.scroll(0,0);	// ORIG
				_el_apparent.style.marginTop = "-330px"; // ORIG // -1200px -800px -600px -300px -280px  !-200px
				_el_overlay.style.display = "block"; // ORIG
				/**/_el_overlay.style.height = "290%";
				_el_overlay.style.width = "100%";
				_el_overlay.style.position = "absolute";
				_el_overlay.style.top = "50px";
				_el_overlay.style.left = "0px";
				_el_overlay.style.marginTop = "-50px"; /* -150px -50px 0px*/
				_el_overlay.style.paddingTop = "80px"; /**/
				_el_specialBox.style.display = "block"; // ORIG	
			}
			else {
				_el_apparent.style.marginTop = "0px"; // ORIG	
				_el_overlay.style.display = "none"; // ORIG	
				/**/_el_overlay.style.position = "static";
				_el_overlay.style.top = "0px";
				_el_overlay.style.left = "0px";
				_el_overlay.style.marginTop = "0px"; 
				_el_overlay.style.paddingTop = "0px"; /**/
				_el_specialBox.style.display = "none"; // ORIG	
			}
			// end chopper IE6 
		}
	} // END function
	
	// ~ Logo Anchor Behaviors functionality ~ text extrusion
	function _Extrude( leEvt ) { 
		
		_tgtEl_extrude = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); // ULTIMATE TARGET SRC EL
		
		/* console.log( "_tgtEl_extrude:...   " , _tgtEl_extrude ); 
		console.log( "_tgtEl_extrude.className:...   " , _tgtEl_extrude.className ); 
		console.log("_tgtEl_extrude.parentNode:...   ", _tgtEl_extrude.parentNode);
		console.log("_tgtEl_extrude.parentNode.parentNode:...   ", _tgtEl_extrude.parentNode.parentNode); */
		// console.log("_tgtEl_extrude.parentNode.previousSibling:...   ", _tgtEl_extrude.parentNode.previousSibling);	
		// console.log("_tgtEl_extrude.parentNode.parentNode.previousSibling:...   ", _tgtEl_extrude.parentNode.parentNode.previousSibling);
		// console.log("_tgtEl_extrude.parentNode.nextSibling:...   ", _tgtEl_extrude.parentNode.nextSibling); 
		// console.log("_tgtEl_extrude.parentNode.parentNode.nextSibling:...   ", _tgtEl_extrude.parentNode.parentNode.nextSibling); 
		
		var tgtEl_ID = _tgtEl_extrude.id; // EXAMPLE 3a * NEW
		
		//console.log( "tgtEl_ID", tgtEl_ID );
		
		// .parentNode.nodeName.toLowerCase() === "p" 
		if (_docObj.getElementById( tgtEl_ID ).className === "gear halfsies") {
			_docObj.getElementById( tgtEl_ID ).className = "gear wholesies sagondo";	;
		}
		else {
			_docObj.getElementById( tgtEl_ID ).className = "gear halfsies";
		}
	} // END function
	
	// ~ unordered list Behaviors in footer ~ RESPONSIVE dependent open close func.
	function _SunnyOps( trgrElEvt ) { 
		var triggeringAnchor = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( trgrElEvt );
		// _ar_TrgrAs_opnClo = _docObj.getElementsByTagName( triggeringAnchor.tagName );
		
		_tgtELId_opnClo = triggeringAnchor.id.slice( 1 , triggeringAnchor.id.length - 1 ); 
		_tgtEl_opnClo = _docObj.getElementById( _tgtELId_opnClo ); 
		
		_OpenShow( _tgtELId_opnClo ); // NewOpenShow( _tgtEl_opnClo );
	} // END function()
	
	function _OpenShow( pmTgtdvID ) { 
		// console.log( "pmTgtdvID" , pmTgtdvID); 
		// target 
		// _tgtELId_opnClo = triggeringAnchor.id.slice( 1 , triggeringAnchor.id.length - 1 ); //divX.id
		// console.log( "thousand  _tgtELId_opnClo", _tgtELId_opnClo ); 
		
		_tgtELId_opnClo = pmTgtdvID; 
			//		console.log( "_tgtELId_opnClo = pmTgtdvID" , _tgtELId_opnClo ); //divX
		
		_tgtEl_opnClo = _docObj.getElementById(_tgtELId_opnClo); 
		// console.log( "_tgtEl_opnClo" , _tgtEl_opnClo );	
		
		_mrBeepersMS = donLuchoHAKI.rafUtils.get_bpms();
		_RAF = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "requestAnimationFrame" , donLuchoHAKI.RetELs.glow() );
		_CAF = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "CancelAnimationFrame" , donLuchoHAKI.RetELs.glow() );
		
		// var tgtEL_OffsetHeight = _tgtEl_opnClo.offsetHeight;
		var tgtEL_OffsetHeight = donLuchoHAKI.PixelsArePixelsArePixels.GetOffsetHeight( _tgtEl_opnClo ); 
		var tgtEL_CapableHeight = _tgtEl_opnClo.scrollHeight;
		
		if( tgtEL_CapableHeight > tgtEL_OffsetHeight ) {
			
			tgtEL_OffsetHeight = parseInt( tgtEL_OffsetHeight, 10 ) + 5;
			_tgtEl_opnClo.style.height = parseInt( tgtEL_OffsetHeight, 10 ) + "px";
			
			if(self.mozRequestAnimationFrame || self.webkitRequestAnimationFrame) {
				_loopTimer_opnClo = window[ _RAF ]( function() { 
					return _OpenShow( _tgtELId_opnClo );
				} );
			}
			else { 
				_loopTimer_opnClo = setTimeout( function() { 
					return _OpenShow( _tgtELId_opnClo );
				} , _mrBeepersMS );
			}
			
		} // if( tgtEL_OffsetHeight < tgtEL_CapableHeight) {}
		
		if( tgtEL_CapableHeight < tgtEL_OffsetHeight ) {
			if(self.mozCancelAnimationFrame || self.webkitCancelAnimationFrame) {
				// return window[ _CAF ]( _loopTimer_opnClo );
				_loopTimer_opnClo = (function(self) {
					return window[ _CAF ]( _loopTimer_opnClo );
				})(_loopTimer_opnClo); 
			}
			else {
				_loopTimer_opnClo = (function(self) {
					return clearTimeout(_loopTimer_opnClo);
				})(_loopTimer_opnClo); 
			} 
		} // _tgtEl_opnClo.style.height = parseInt( tgtEL_OffsetHeight, 10 ) + "px";
		
	} // END function()
	
	function ThousandOps( trgrElEvt ) { 
		var triggeringAnchor = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( trgrElEvt ); 					
		// _ar_TrgrAs_opnClo = _docObj.getElementsByTagName( triggeringAnchor.tagName );
		
		_tgtELId_opnClo = triggeringAnchor.id.slice( 1 , triggeringAnchor.id.length - 1 ); 
		_tgtEl_opnClo = _docObj.getElementById( _tgtELId_opnClo ); 
		
		_CloseHide( _tgtELId_opnClo ); // NewCloseHide( _tgtEl_opnClo );
	} // END function()
	
	function _CloseHide( pmTgtdvID ) { // console.log( "pmTgtdvID" , pmTgtdvID); 
		// target 
		// _tgtELId_opnClo = triggeringAnchor.id.slice( 1 , triggeringAnchor.id.length - 1 ); //divX.id
		// console.log( "thousand  _tgtELId_opnClo", _tgtELId_opnClo ); 
		_tgtELId_opnClo = pmTgtdvID; 
			//		console.log( "_tgtELId_opnClo = pmTgtdvID" , _tgtELId_opnClo ); //divX
		_tgtEl_opnClo = _docObj.getElementById( _tgtELId_opnClo ); 
		// var tgtEL_CapableHeight = _tgtEl_opnClo.scrollHeight;
		
		// console.log( "_tgtEl_opnClo" , _tgtEl_opnClo );	
		
		_mrBeepersMS = donLuchoHAKI.rafUtils.get_bpms();
		_RAF = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "requestAnimationFrame" , donLuchoHAKI.RetELs.glow() );
		_CAF = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "CancelAnimationFrame" , donLuchoHAKI.RetELs.glow() );
		
		// var tgtEL_OffsetHeight = _tgtEl_opnClo.offsetHeight;
		var tgtEL_OffsetHeight = donLuchoHAKI.PixelsArePixelsArePixels.GetOffsetHeight( _tgtEl_opnClo ); 
		
		if( 0 < tgtEL_OffsetHeight ) {
			
			tgtEL_OffsetHeight = parseInt( tgtEL_OffsetHeight, 10 ) - 5;
			_tgtEl_opnClo.style.height = parseInt( tgtEL_OffsetHeight, 10 ) + "px";
			
			if(self.mozRequestAnimationFrame || self.webkitRequestAnimationFrame) {
				_loopTimer_opnClo = window[ _RAF ]( function() { 
					return _CloseHide( _tgtELId_opnClo );
				} );	
			}
			else {
				_loopTimer_opnClo = setTimeout( function() { 
					return _CloseHide( _tgtELId_opnClo );
				} , _mrBeepersMS );	
			} 
			
		} // if( tgtEL_OffsetHeight > 0) { }
		
		if( 0 > tgtEL_OffsetHeight ) { 
			_tgtEl_opnClo.style.height = "0px";
			if(self.mozCancelAnimationFrame || self.webkitCancelAnimationFrame) {
				
				_loopTimer_opnClo = (function(self) {
					return window[ _CAF ]( _loopTimer_opnClo );
				})(_loopTimer_opnClo); 
				
			}
			else {
				_loopTimer_opnClo = (function(self) {
					return clearTimeout(_loopTimer_opnClo);
				})(_loopTimer_opnClo); 
			} 
		} // _tgtEl_opnClo.style.height = parseInt( tgtEL_OffsetHeight, 10 ) + "px";
		
	} // END function()	
	
	
	// ~ Drag and Drop Functionality
	function _Dd_Profile() { 
	
		_N = 0;
		_O = 0;
		_P = 0;
		
		for (_N; _N < _allImgs.length; _N = _N + 1 ) {
			if( _allImgs[ _N ].className === "back_n_forth" ) { 
				_imgObj_dNd = _allImgs[ _N ];
				_imgObj_dNd.draggable = true; 
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _imgObj_dNd, "dragstart", _Dd_Drag ); 
			}
		}
		for (_O; _O < _allDivs.length; _O = _O + 1 ) {
			if( _allDivs[ _O ].className === "dz" ) { 
				_ar_DZs.push( _allDivs[ _O ] );
				_ar_DZsMax = _ar_DZs.length;
			}
		}
		for( _P; _P < _ar_DZsMax; _P = _P + 1 ){ 
			donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_DZs[ _P ], "drop", _Dd_Drop ); 
			donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_DZs[ _P ], "dragover", _Dd_Allow ); 
			if(_docObj.documentElement.attachEvent) { 
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_DZs[ _P ], "dragenter", _Dd_Allow ); 
			}
		} 
		
	} // END function
	
	// ~ Drag and Drop Functionality
	function _Dd_Drag(leEvt) { 
	
		leEvt.dataTransfer.setData( "Text", donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget(leEvt).id );
		
	} // END function
	
	// ~ Drag and Drop Functionality
	function _Dd_Drop(leEvt) { 
		
		_Q = 0;
		for( _Q; _Q < _ar_DZsMax; _Q = _Q + 1 ){
			if( donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget(leEvt).nodeName.toLowerCase() === "div" && _ar_DZs[ _Q ].id === donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget(leEvt).id ) { 
				donLuchoHAKI.Utils.evt_u.RetValFalprevDef(leEvt); 
				_evtTrgt_dNd = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget(leEvt);
				_data_dNd = leEvt.dataTransfer.getData( "Text" );	
				_evtTrgt_dNd.appendChild( _docObj.getElementById( _data_dNd ) );
			}
		}
		
	} // END function
	
	// ~ Drag and Drop Functionality
	function _Dd_Allow(leEvt){ 
		
		_R = 0;
		for( _R; _R < _ar_DZsMax; _R = _R + 1 ){
			if( donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget(leEvt).nodeName.toLowerCase() === "div" && _ar_DZs[ _R ].id === donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget(leEvt).id ) {
				donLuchoHAKI.Utils.evt_u.RetValFalprevDef(leEvt);  
			} 
		}
		
	} // END function
	
	
	// ~ FAUX Drag and Drop Functionality
	function _Dd_faux_Profile() { 
	
		_Y = 0;
		_Z = 0;					
		for (_Y; _Y < _allDraggableDivs.length; _Y = _Y + 1 ) {
			if( _allDraggableDivs[ _Y ].className === "faux_drag" ) { 
				_ar_fauxDraggables.push( _allDraggableDivs[ _Y ] );
				_ar_fauxDZMax = _ar_fauxDraggables.length;
				_ar_fauxDraggables.draggable = true; 
			}
		} 					
		for( _Z; _Z < _ar_fauxDZMax; _Z = _Z + 1 ){ 
			donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_fauxDraggables[ _Z ], "touchstart" , _TS_Dd_faux ); 
			donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_fauxDraggables[ _Z ], "touchmove" , _TM_Dd_faux ); 
			donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_fauxDraggables[ _Z ], "touchend" , _TE_Dd_faux ); 
		}					
	} // END function
	
	// ~ FAUX Drag and Drop Functionality
	function _TS_Dd_faux( leEvt ) { 
	
		var evtTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		_ar_CHGDtouches = leEvt.changedTouches; 
		_fingerCount = _ar_CHGDtouches.length;
		_W = 0;
		
		if ( _fingerCount === 1 && !_offset_DDfo ) {		
			for( _W; _W < _fingerCount; _W = _W + 1 ) { 
			
				if ( evtTrgt.tagName.toLowerCase() === "div" ) { 
					
					_startX = _ar_CHGDtouches[ _W ].pageX; 
					
					_startY = _ar_CHGDtouches[ _W ].pageY; 
					
					//console.log( "START _startX" , _startX );
					//console.log( "START _startY" , _startY );
					
					var touchTgtZero = _ar_CHGDtouches[ _W ].target; 
					//console.log("touchTgtZero" , touchTgtZero );					
					
					var GetOffsetTop = _GetOffsetTop( touchTgtZero );
					//console.log( "GetOffsetTop" , GetOffsetTop );
					
					var GetOffsetLeft = _GetOffsetLeft( touchTgtZero ); 
					//console.log( "GetOffsetLeft" , GetOffsetLeft );
					
					var ComputeMargTop = _GetComputedStyle( touchTgtZero, "margin-top" ); 
					//console.log("ComputeMargTop" , ComputeMargTop);
					
					var ComputeMargLeft = _GetComputedStyle( touchTgtZero, "margin-left" ); 
					//console.log("ComputeMargLeft", ComputeMargLeft );
					
					var TopQty = GetOffsetTop - ComputeMargTop; 
					//console.log("TopQty" , TopQty );
					
					var LeftQty = GetOffsetLeft - ComputeMargLeft; 
					//console.log("LeftQty" , LeftQty);
					
					_offset_DDfo = {
						x : ( _startX ) - GetOffsetLeft , 
						y : ( _startY ) - GetOffsetTop
					}; 
					
					//console.log( "_offset_DDfo.x" , _offset_DDfo.x );
					//console.log( "_offset_DDfo.y" , _offset_DDfo.y );
				

				}
			}
		}
		_PrevDeffo(leEvt); 
		
	} // END function
	
	// ~ FAUX Drag and Drop Functionality
	function _TM_Dd_faux( leEvt ) {
		
		var evtTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		_ar_CHGDtouches = leEvt.changedTouches; 
		_fingerMoveCount = _ar_CHGDtouches.length;
		_W = 0;
		
		if ( _fingerMoveCount === 1 ) { 
			for( _W; _W < _fingerMoveCount; _W = _W + 1 ) { 
				if ( _fingerMoveCount === 1 && evtTrgt.tagName.toLowerCase() === "div" && !!_offset_DDfo ) { 
					
					var touchTgtZero = _ar_CHGDtouches[ _W ].target; 
					//console.log( "touchTgtZero" ,  touchTgtZero );
					
					//touchTgtZero.style.cssText = "left: " + ( (_ar_CHGDtouches[ _W ].pageX) ) + "px; "; 
					touchTgtZero.style.cssText = "left: " + ( (_ar_CHGDtouches[ _W ].pageX) - (_offset_DDfo.x ) ) + "px; "; // * 0.4375 * 0.5625 * 0.625
					
					//touchTgtZero.style.cssText += "top: " + ( (_ar_CHGDtouches[ _W ].pageY) ) + "px; "; 
					touchTgtZero.style.cssText += "top: " + ( (_ar_CHGDtouches[ _W ].pageY) - (_offset_DDfo.y ) ) + "px; "; 
					
					//console.log( "MOVE _ar_CHGDtouches[ _W ].pageX" , _ar_CHGDtouches[ _W ].pageX );	
					//console.log( "MOVE _ar_CHGDtouches[ _W ].pageY" , _ar_CHGDtouches[ _W ].pageY );
					
				}
			}
		}
		
		if ( !_fingerMoveCount === 1) { 
		// if ( !_fingerCount === 1) { 
			_offset_DDfo = false;
		}
		_PrevDeffo(leEvt); 
		
	} // END function
	
	// ~ FAUX Drag and Drop Functionality
	function _TE_Dd_faux( leEvt ) { 
		// _offset_DDfo
		
		var evtTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		_ar_CHGDtouches = leEvt.changedTouches; 
		_fingerCount = _ar_CHGDtouches.length;
		_W = 0;
		
		if ( _fingerCount === 1 ) { 
			for( _W; _W < _fingerCount; _W = _W + 1 ) { 
				
				if ( _fingerCount === 1 && evtTrgt.tagName.toLowerCase() === "div" && !!_offset_DDfo ) { 
				
					_lastX = _ar_CHGDtouches[ _W ].pageX - (_offset_DDfo.x ); // * 0.4375 * 0.5625 * 0.625
					_lastY = _ar_CHGDtouches[ _W ].pageY - (_offset_DDfo.y ); 
					//console.log("END _lastX" , _lastX );
					//console.log("END _lastY" , _lastY);
					
					var touchTgtZero = _ar_CHGDtouches[ _W ].target; 
					//console.log("touchTgtZero" , touchTgtZero);
					
					touchTgtZero.style.cssText = "left: " + ( _lastX ) + "px; "; // _offset_DDfo.x
					// touchTgtZero.style.cssText = "left: " + ( _lastX - _offset_DDfo.x ) + "px; "; 
					
					touchTgtZero.style.cssText += "top: " + ( _lastY ) + "px; "; // _offset_DDfo.y	
					// touchTgtZero.style.cssText += "top: " + ( _lastY - _offset_DDfo.y ) + "px; "; 
					
					_offset_DDfo = false; 
				}
			}
		}
		if ( !_fingerCount === 1) {
			_offset_DDfo = false;
		}
		_PrevDeffo(leEvt); 
		
	} // END function
	
	// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html
	function _DeviceOrientationOps( leEvt ) {
		if( leEvt.type === "deviceorientation" ){ 
			
			var today = new Date();
			
			// CONTINGENCY PHASES
			// ABSOLUTE - absolute - specifies whether the device is providing orientation
			// data that's relative to the Earth's coordinate system, in which case its value is true or to an arbitrary coordinate system.
			if( leEvt.absolute !== undefined ){
				var If_absolute_earthCoord = leEvt.absolute;
			}
			else { 
				var If_absolute_earthCoord = false;
			} 
			
			// WEBKIT COMPASS HEADING - webkitCompassHeading - a property EVIDENTLY unique to the mobile iOS platform that is used in lieu of alpha property
			// webkitCompassHeading Property - A direction that is measured in degrees relative to magnetic north. 
			// + Declaration ~ JavaScript ~ readonly attribute double webkitCompassAccuracy
			// + Discussion ~ If this property value is 10, the heading is off by plus or minus 10 degrees. A value of -1 means that the compass is not calibrated and not giving usable readings. 
			// + Availability ~ Available in iOS 5.0 and later.
			if( leEvt.webkitCompassHeading || leEvt.compassHeading ) {
				
				var If_WCH = leEvt.webkitCompassHeading || leEvt.compassHeading;
				_wboolyIf_Compass = true;
				_currentHeading = _nmHardThreeSixty - If_WCH;
			}
			else {
				var If_WCH = false;
				_wboolyIf_Compass = false;
			} 
			
			// WEBKIT COMPASS ACCURACY - webkitCompassAccuracy - a property EVIDENTLY unique to the mobile iOS platform
			// webkitCompassAccuracy Property - The accuracy of the compass data in degrees. 
			// + Declaration ~ JavaScript ~ readonly attribute double webkitCompassAccuracy			
			// + Discussion ~ If this property value is 10, the heading is off by plus or minus 10 degrees. A value of -1 means that the compass is not calibrated and not giving usable readings.			
			// + Availability ~ Available in iOS 5.0 and later.
			if( leEvt.webkitCompassAccuracy || leEvt.compassAccuracy ) { 
				var If_WCA = leEvt.webkitCompassAccuracy || leEvt.compassAccuracy;
				_wboolyIf_Accuracy = true;
				_currentAccuracy = If_WCA;
			}
			else {
				var If_WCA = false;
				_wboolyIf_Accuracy = false;
			} 
			
			// Z AXIS ~ ALPHA - "rotation around z-axis" // deviceorientation
			// alpha - the angle around the z-axis. Its value ranges from 0 to 360 degrees. 
			// When the top of the device points to the True North, the value of this property is 0.
			// Alpha: The amount of rotation around the Z axis is known as alpha. To better understand it, consider a set
			// of rotating helicopter blades. They are not going up or down nor do they go side to side. They merely are
			// rotating along the Z-axis by units of alpha degrees per microsecond. The range is from 0 to 360 degrees.
			var zAxisYaw_alpha_dir = leEvt.alpha; // In degree in the range [0, 360]  // Heads or Tails
			
			
			// X AXIS ~ BETA - "front back motion" // deviceorientation
			// beta - the angle around the x-axis. Its value range from -180 to 180 degrees. When the device is parallel to surface of the Earth, 
			// the value of this property is 0.
			// Beta: The amount of rotation around the X axis is known as beta. To better understand it, consider an airplane
			// taking off from the runway. It will go in a straight line but in an upward direction. Hence, "it is turning along the X axis."
			// The range is from -180 to 180 degrees.
			var xAxisRoll_beta_tiltFB = leEvt.beta; // In degree in the range [-180,180] 
			// # Mobile iOS ~ DeviceOrientationEvent.beta has values between -90 and 90.
			// # Firefox ~ DeviceOrientationEvent.beta has values between 180 and -180.
			
			
			// Y AXIS ~ GAMMA - "left to right" // deviceorientation
			// gamma - the angle around the y-axis. Its values ranges from -90 to 90 degrees. 
			// When the device is parallel to the surface of the Earth, the value of this property is 0.
			// Gamma: The amount of rotation around the Y axis is known as gamma. To better understand it, consider an airplane
			// already in the air just flying straight in a single direction with wings parallel to the ground. The pilot will want to turn
			// so he will rotate one of the wings pointing closer to the ground while the opposite wing is pointing farther from the ground.
			// The range is from -90 to 90 degrees.
			var yAxisPitch_gamma_tiltLR = leEvt.gamma; // In degree in the range [-90,90] 
			// # Mobile iOS ~ DeviceOrientationEvent.gamma has values between -180 and 180.
			// # Firefox ~ DeviceOrientationEvent.gamma has values between 90 and -90.
			// fin. CONTINGENCY PHASES
			
			if( _actv_Page === "device.html" ) {
				if( !!zAxisYaw_alpha_dir && !!xAxisRoll_beta_tiltFB && !!yAxisPitch_gamma_tiltLR ){	
					if( ( today.getTime() -_nm_DO_Event_last) > _nm_DO_Interval ){
						var goTime = _DOcheckMotion( xAxisRoll_beta_tiltFB, yAxisPitch_gamma_tiltLR, zAxisYaw_alpha_dir );
						_nm_DO_Event_last = today.getTime();
					}
					if ( goTime ) {
						_DOHandler( zAxisYaw_alpha_dir , xAxisRoll_beta_tiltFB , yAxisPitch_gamma_tiltLR, If_absolute_earthCoord, If_WCH, If_WCA, _wboolyIf_Compass, _wboolyIf_Accuracy, _currentHeading, _currentAccuracy );
					}
				} // END if properties exist control stmt.
			} // END active page
		} // END typeof event
	} // END function
	
	// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html
	function _DOcheckMotion( pmBetaX, pmGammaY, pmAlphaZ ){ 
		var inMotion = false;
		// full throttle ~= 0 \ choke = 1
		var threshhold = _nmChokeAdjustment; // "The lower the more sensitive..." 
		var betaChange = Math.abs( pmBetaX - _nm_DOlastX_Beta );
		var gammaChange = Math.abs( pmGammaY - _nm_DOlastY_Gamma );	
		inMotion = betaChange >= threshhold || gammaChange >= threshhold;
		if ( !!inMotion ) {
			return inMotion; // true
		}
		_nm_DOlastX_Beta = pmBetaX;
		_nm_DOlastY_Gamma = pmGammaY;
	}
	
	 
	
	// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html
	function _DOHandler( pmAlphaZ , pmBetaX , pmGammaY, pmAbsolute, pm_ifWCH, pm_ifWCA, pm_ifCompass, pm_ifAccuracy, pm_curHeading, pm_curAccuracy ) {
		
		if( ( document.documentElement.addEventListener ) ) {
			
			if( ( ( document.documentElement.addEventListener ) && ( !!(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				// READOUT AND FEEDBACK PHASE 
				_el_dvc_one.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.readOut_alpha[window.localStorage.getItem("defaultLanguage")].fullText + pmAlphaZ.toFixed(2) + " ";
				_el_dvc_oneB.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.readOut_beta[window.localStorage.getItem("defaultLanguage")].fullText + pmBetaX.toFixed(2) + " ";
				_el_dvc_oneC.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.readOut_gamma[window.localStorage.getItem("defaultLanguage")].fullText + pmGammaY.toFixed(2) + " ";
				
				if( !!(pmAbsolute !== undefined ) ){
					_el_dvc_oneD.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.absolute[window.localStorage.getItem("defaultLanguage")].fullText + pmAbsolute + " ";
				} 
				
				if( !!( pm_ifWCH !== undefined ) ){ 
					_el_dvc_oneF.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.wch[window.localStorage.getItem("defaultLanguage")].fullText + pm_ifWCH + " "; 
					
					if ( typeof(pm_ifWCH) === "number" ) {
						_el_dvc_oneE.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.compass_func[window.localStorage.getItem("defaultLanguage")].fullText + pm_ifCompass + " ";
						_el_dvc_oneF.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.wch[window.localStorage.getItem("defaultLanguage")].fullText + pm_ifWCH.toFixed(2) + " "; 
						_el_dvc_oneH.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.current_heading[window.localStorage.getItem("defaultLanguage")].fullText + pm_curHeading.toFixed(2) + " ";
						
						_el_dvc_oneI.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.wca[window.localStorage.getItem("defaultLanguage")].fullText + pm_ifWCA.toFixed(2) + " "; 
						_el_dvc_oneJ.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.current_accuracy[window.localStorage.getItem("defaultLanguage")].fullText + pm_curAccuracy + " "; 
						
						// _el_dvcMembTwo.innerHTML = "U R not skerruud after all";
						var heading = pm_ifWCH.toFixed(2) + window.orientation; // leEvt.webkitCompassHeading
						if ( Math.abs( heading - _lastHeading ) < 180 ) { _el_spinner.style[ transition ] = "all 0.17578125s ease-in-out"; }
						else { _el_spinner.style[ transition ] = "none"; }	
						var spinnerRotation = _roZ_AlphaZ_neg( heading ); 
						//// var spinnerRotation = _roZ_AlphaZ_neg( pm_ifWCH ); 
						_el_spinner.style[ transform ] = spinnerRotation;
						_lastHeading = heading; 
					}
					if( pm_ifWCH === false ){ 
						_el_dvc_oneF.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.wch[window.localStorage.getItem("defaultLanguage")].fullText + _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport[window.localStorage.getItem("defaultLanguage")].fullText + " ";
						_el_dvc_oneH.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.current_heading[window.localStorage.getItem("defaultLanguage")].fullText + _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport[window.localStorage.getItem("defaultLanguage")].fullText + " ";
						
						_el_dvc_oneI.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.wca[window.localStorage.getItem("defaultLanguage")].fullText + _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport[window.localStorage.getItem("defaultLanguage")].fullText + " ";
						_el_dvc_oneJ.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.current_accuracy[window.localStorage.getItem("defaultLanguage")].fullText + _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport[window.localStorage.getItem("defaultLanguage")].fullText + " ";	
					}
					if( _chopperPoint === "Newer_Android_Device" ){ 
						_el_dvcMembTwo.style.display = "none";
						_el_DVCninetynine.innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport[window.localStorage.getItem("defaultLanguage")].fullText;
					}
				}
				
				if ( !!( !pm_curHeading ) ) {
					_el_dvc_oneE.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.compass_accuracy[window.localStorage.getItem("defaultLanguage")].fullText + _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noData[window.localStorage.getItem("defaultLanguage")].fullText + " "; 
				}
				
				if( !!( pm_ifWCA !== undefined ) ){ 
					if ( typeof(pm_ifWCA) === "number" ) {
						_el_dvc_oneG.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.compass_accuracy[window.localStorage.getItem("defaultLanguage")].fullText + pm_ifWCA.toFixed(2) + " "; 
					}
					if( pm_ifWCA === false ){ 
						_el_dvc_oneG.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.compass_accuracy[window.localStorage.getItem("defaultLanguage")].fullText + _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noData[window.localStorage.getItem("defaultLanguage")].fullText + " ";
					}
				}
				
				if ( !!( !pm_curAccuracy ) ) {
					_el_dvc_oneH.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.compass_accuracy[window.localStorage.getItem("defaultLanguage")].fullText + _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport[window.localStorage.getItem("defaultLanguage")].fullText + " "; 
				}
			}
			
			else
			if( ( ( document.documentElement.addEventListener ) && ( !(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				// READOUT AND FEEDBACK PHASE 
				_el_dvc_one.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.readOut_alpha.EN.fullText + pmAlphaZ.toFixed(2) + " ";
				_el_dvc_oneB.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.readOut_beta.EN.fullText + pmBetaX.toFixed(2) + " ";
				_el_dvc_oneC.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.readOut_gamma.EN.fullText + pmGammaY.toFixed(2) + " ";
				
				if( !!(pmAbsolute !== undefined ) ){
					_el_dvc_oneD.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.absolute.EN.fullText + pmAbsolute + " ";
				} 
				
				if( !!( pm_ifWCH !== undefined ) ){ 
					_el_dvc_oneF.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.wch.EN.fullText + pm_ifWCH + " "; 
					
					if ( typeof(pm_ifWCH) === "number" ) {
						_el_dvc_oneE.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.compass_func.EN.fullText + pm_ifCompass + " ";
						_el_dvc_oneF.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.wch.EN.fullText + pm_ifWCH.toFixed(2) + " "; 
						_el_dvc_oneH.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.current_heading.EN.fullText + pm_curHeading.toFixed(2) + " ";
						
						_el_dvc_oneI.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.wca.EN.fullText + pm_ifWCA.toFixed(2) + " "; 
						_el_dvc_oneJ.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.current_accuracy.EN.fullText + pm_curAccuracy + " "; 
						
						// _el_dvcMembTwo.innerHTML = "U R not skerruud after all";
						var heading = pm_ifWCH.toFixed(2) + window.orientation; // leEvt.webkitCompassHeading
						if ( Math.abs( heading - _lastHeading ) < 180 ) { _el_spinner.style[ transition ] = "all 0.17578125s ease-in-out"; }
						else { _el_spinner.style[ transition ] = "none"; }	
						var spinnerRotation = _roZ_AlphaZ_neg( heading ); 
						//// var spinnerRotation = _roZ_AlphaZ_neg( pm_ifWCH ); 
						_el_spinner.style[ transform ] = spinnerRotation;
						_lastHeading = heading; 
					}
					if( pm_ifWCH === false ){ 
						_el_dvc_oneF.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.wch.EN.fullText + _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport.EN.fullText + " ";
						_el_dvc_oneH.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.current_heading.EN.fullText + _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport.EN.fullText + " ";
						
						_el_dvc_oneI.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.wca.EN.fullText + _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport.EN.fullText + " ";
						_el_dvc_oneJ.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.current_accuracy.EN.fullText + _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport.EN.fullText + " ";	
					}
					if( _chopperPoint === "Newer_Android_Device" ){ 
						_el_dvcMembTwo.style.display = "none";
						_el_DVCninetynine.innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport.EN.fullText;
					}
				}
				
				if ( !!( !pm_curHeading ) ) {
					_el_dvc_oneE.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.compass_accuracy.EN.fullText + _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noData.EN.fullText + " "; 
				}
				
				if( !!( pm_ifWCA !== undefined ) ){ 
					if ( typeof(pm_ifWCA) === "number" ) {
						_el_dvc_oneG.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.compass_accuracy.EN.fullText + pm_ifWCA.toFixed(2) + " "; 
					}
					if( pm_ifWCA === false ){ 
						_el_dvc_oneG.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.compass_accuracy.EN.fullText + _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noData.EN.fullText + " ";
					}
				}
				
				if ( !!( !pm_curAccuracy ) ) {
					_el_dvc_oneH.innerHTML = _obj_ConfigAll.CannedStrings.dvcO_labelOps.assets.formattedString.compass_accuracy.EN.fullText + _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport.EN.fullText + " "; 
				}
			}
		
		}
		
		
		
		// fin. READOUT AND FEEDBACK PHASE
		
		
		
		// EXECUTION PHASE 
		
		// _el_svgpose
		_el_svgpose.style[ perspective ] = "768px"; //300px
		// ORIGINAL ROTATION ~ poserotation
		var poserotation = _ro_GammaY( pmGammaY ) + _ck_space + _TreD_BetaX_GRV_fu( pmBetaX ); 
		// UNASSIGNED ROTATION COMPOSITES ~ PROPOSED #1 ~ noCompensation ~ nComp
		var nCompsvg = _roX_BetaX_prpr( pmBetaX ) + _ck_space + _roY_GammaY_neg( pmGammaY ) + _ck_space + _roZ_AlphaZ_nComp( pmAlphaZ ); 
		// UNASSIGNED ROTATION COMPOSITES ~ PROPOSED #2 ~ orientationCompensation ~ yComp
		var yCompsvg = _roX_BetaX_prpr( pmBetaX ) + _ck_space + _roY_GammaY_neg( pmGammaY ) + _ck_space + _roZ_AlphaZ_yComp( pmAlphaZ ); 
		_el_svgpose.style[ transform ] = nCompsvg; // poserotation
		
		// _el_pngposeTwo
		_el_pngposeTwo.style[ perspective ] = "768px"; //300px
		// ORIGINAL ROTATION ~ posetworotation
		var posetworotation = _ro_GammaY( pmGammaY ) + _ck_space + _TreD_BetaX_GRV_fu_neg( pmBetaX ); 
		// UNASSIGNED ROTATION COMPOSITES ~ PROPOSED #1 ~ noCompensation ~ nComp
		var nCompPPTwo = _roX_BetaX_prpr( pmBetaX ) + _ck_space + _roY_GammaY_neg( pmGammaY ) + _ck_space + _roZ_AlphaZ_nComp( pmAlphaZ ); 		
		// UNASSIGNED ROTATION COMPOSITES ~ PROPOSED #2 ~ orientationCompensation ~ yComp
		var yCompPPTwo = _roX_BetaX_prpr( pmBetaX ) + _ck_space + _roY_GammaY_neg( pmGammaY ) + _ck_space + _roZ_AlphaZ_yComp( pmAlphaZ ); 
		_el_pngposeTwo.style[ transform ] = yCompPPTwo; // posetworotation
		
		// _el_dvc_two
		_el_dvc_two.style[ perspective ] = "300px"; 
		// UNASSIGNED ROTATION COMPOSITES ~ PROPOSED #1 ~ noCompensation ~ nComp
		var nCompDvcTwo = _roX_BetaX_prpr( pmBetaX ) + _ck_space + _roY_GammaY_neg( pmGammaY ) + _ck_space + _roZ_AlphaZ_nComp( pmAlphaZ ); 
		// UNASSIGNED ROTATION COMPOSITES ~ PROPOSED #2 ~ orientationCompensation ~ yComp
		var yCompDvcTwo = _roX_BetaX_prpr( pmBetaX ) + _ck_space + _roY_GammaY_neg( pmGammaY ) + _ck_space + _roZ_AlphaZ_yComp( pmAlphaZ ); 
		// ORIGINAL ROTATION ~ tworotation ~ NO CHANGE
		var tworotation = _roZ_AlphaZ_prpr( pmAlphaZ ) + _ck_space + _roX_BetaX_prpr( pmBetaX ); 
		_el_dvc_two.style[ transform ] = tworotation;
		
		/**/
		// _el_dvc_ball
		// Because we don't want to have the device upside down so we constrain the pmBetaX value to the range [-90,90]
		if( pmBetaX > _xRoll_adj ){
			pmBetaX = _xRoll_adj;
		};
		if( pmBetaX < _xRoll_adj_neg ){ 
			pmBetaX = _xRoll_adj_neg;
		}; 
		
		// To make computation easier we shift the range of pmBetaX and pmGammaY to [0,180]
		pmBetaX += _xRoll_adj;
		// To make computation easier we shift the range of pmBetaX and pmGammaY to [0,180]
		pmGammaY += _yPitch_adj;
		// 10 is half the size of the ball and will center the positioning point to the center of the ball
		
		// _el_dvc_ball.style.top  = ( _gar_maxX * pmBetaX / _xRoll_ballAdj - _garBallWidthTR ) + "px";
		// _el_dvc_ball.style.left = ( _gar_maxY * pmGammaY / _yPitch_ballAdj - _garBallWidthTR ) + "px";
		
		_el_dvc_ball.style.left = ( _NuMaxY * pmGammaY / _xRoll_ballAdj - _playHeight ) + "px";
		_el_dvc_ball.style.top = ( _NuMaxX  * pmBetaX / _yPitch_ballAdj - _playWidth ) + "px"; 		
		// fin. EXECUTION PHASE 
	} // END function 
	
	
	// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html	
	function _DeviceMotionOps( leEvt ) {
		if( leEvt.type === "devicemotion" ){ // CONTINGENCY PHASES
			
			
			var today = new Date();
			
			// ~ INTERVAL
			if ( leEvt.interval !== undefined ){
				// ~ INTERVAL ~ interval - It is expressed in milliseconds and provides the interval at which data is obtained. // devicemove
				var interval_ms_sq = leEvt.interval;
			}
			else {
				var interval_ms_sq = false;
			}
			
			// ~ ROTATION RATE
			if( leEvt.rotationRate !== null ){ // leEvt.rotationRate !== undefined 
				// ~ RR properties ~~~ ALPHA | BETA | GAMMA - rotationRate - specifies rate at which the device is rotating around each of its axes in degrees per second. // devicemove 
				var If_RR = true;				
				var rr = {};
				var rr = leEvt.rotationRate; 
				// ~ RR properties  ~ ALPHA ~ represents a rotation rate along the axis PERPENDICULAR to the screen [ ... ]
				var zAxisYaw_rrAlpha_Perp = rr.alpha;				
				// ~ RR properties ~ BETA ~ represents a rotation rate along the axis going from the LEFT to the RIGHT [ ... ]
				var xAxisRoll_rrBeta_LftRt = rr.beta;				
				// ~ RR properties ~ GAMMA ~ represents a rotation rate along the axis going from the BOTTOM to the TOP [ ... ]
				var yAxisPitch_rrGamma_BotTop = rr.gamma;
			} 
			else {
				var If_RR = false;
			}
			
			// ACCELERATION // devicemove 
			if( leEvt.acceleration !== null ){ 
				var AsansG = {};
				var AsansG = leEvt.acceleration;				
				// ~ "THE ROLL" ~ X AXIS: "the roll" rotation around the vertical (y) axis represents the axis from West to East
				var xAxisRoll_noG = AsansG.x;
				// ~ "THE PITCH" ~ Y AXIS: "the pitch" rotation around the horizontal (x) axis represents the axis from South to North
				var yAxisPitch_noG = AsansG.y;
				// ~ "HEADS \ TAILS" ~ Z AXIS: not "the yaw," just "heads or tails" property represents the axis from PERPENDICULAR to the ground
				var zAxisYaw_noG = AsansG.z;
				_wboolyIf_NoGrav = true; 
			}
			else { 
				var AsansG = null;
				if( ( ( document.documentElement.addEventListener ) && ( !!(window.localStorage.getItem("defaultLanguage") ) ) ) ){
					var xAxisRoll_noG = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[window.localStorage.getItem("defaultLanguage")].fullText;
					var yAxisPitch_noG = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[window.localStorage.getItem("defaultLanguage")].fullText;
					var zAxisYaw_noG = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[window.localStorage.getItem("defaultLanguage")].fullText; 
				}
				else
				if( ( ( document.documentElement.addEventListener ) && ( !(window.localStorage.getItem("defaultLanguage") ) ) ) ){
					var xAxisRoll_noG = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
					var yAxisPitch_noG = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
					var zAxisYaw_noG = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText; 
				}
			} 
			
			// ACCELERATION INCLUDING GRAVITY // devicemove
			if( leEvt.accelerationIncludingGravity !== null ){ 
				var AinclG = {};
				var AinclG = leEvt.accelerationIncludingGravity;
				// ~ "THE ROLL" ~ X AXIS: "the roll" rotation around the vertical (y) axis represents the axis from West to East
				var xAxisRoll_yesG = AinclG.x;				
				// ~ "THE PITCH" ~ Y AXIS: "the pitch" rotation around the horizontal (x) axis represents the axis from South to North
				var yAxisPitch_yesG = AinclG.y;				
				// ~ "HEADS \ TAILS" ~ Z AXIS: not "the yaw," just "heads or tails" property represents the axis from PERPENDICULAR to the ground
				var zAxisYaw_yesG = AinclG.z;
				_wboolyIf_Grav = true;
			}
			else {
				var AinclG = null;
				if( ( ( document.documentElement.addEventListener ) && ( !!(window.localStorage.getItem("defaultLanguage") ) ) ) ){ 
					var xAxisRoll_yesG = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[window.localStorage.getItem("defaultLanguage")].fullText;
					var yAxisPitch_yesG = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[window.localStorage.getItem("defaultLanguage")].fullText;
					var zAxisYaw_yesG = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[window.localStorage.getItem("defaultLanguage")].fullText;
				}
				else
				if( ( ( document.documentElement.addEventListener ) && ( !(window.localStorage.getItem("defaultLanguage") ) ) ) ){ 
					var xAxisRoll_yesG = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
					var yAxisPitch_yesG = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
					var zAxisYaw_yesG = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText; 
				}
			} 
			
			
			// fin. CONTINGENCY PHASES
			if( _actv_Page === "device.html" ) { 
				if( ( today.getTime() - _nm_DM_Event_last ) > _nm_DM_Interval ){
					var goTime = _DMcheckMotion( xAxisRoll_yesG, yAxisPitch_yesG );
					_nm_DM_Event_last = today.getTime();
				}
				if( goTime ){
					_DMHandler( interval_ms_sq, If_RR, rr, zAxisYaw_rrAlpha_Perp, xAxisRoll_rrBeta_LftRt, yAxisPitch_rrGamma_BotTop, AsansG, xAxisRoll_noG, yAxisPitch_noG, zAxisYaw_noG, AinclG, xAxisRoll_yesG, yAxisPitch_yesG, zAxisYaw_yesG );	
				}
			}
		}
	} // END function	
	
	// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html
	function _DMcheckMotion( pmBetaX, pmGammaY ){
		var inMotion = false;
		// full throttle ~= 0 \ choke = 1
		var threshhold = _nmChokeAdjustment; // "The lower the more sensitive..." 
		var betaChange = Math.abs( pmBetaX - _nm_DMlastX_Beta );
		var gammaChange = Math.abs( pmGammaY - _nm_DMlastY_Gamma );
		inMotion = betaChange >= threshhold || gammaChange >= threshhold;
		if ( !!inMotion ) {
			return inMotion; // true
		} 
		_nm_DMlastX_Beta = pmBetaX;
		_nm_DMlastY_Gamma = pmGammaY;
	} // END function
	
	
	// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html
	function _DMHandler( pmIntvl, pmIfRR, pmRr, pmAlphaZ_RR, pmBetaX_RR, pmGammaY_RR, pmAsansG, pmBetaX_noG, pmGammaY_noG, pmAlphaZ_noG, pmAinclG, _pmBetaX_yesG, _pmGammaY_yesG, pmAlphaZ_yesG ) {
		
		if( ( document.documentElement.addEventListener ) ) {
			
			if( ( ( document.documentElement.addEventListener ) && ( !!(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				// READOUT AND FEEDBACK PHASE  // EXECUTION PHASE
				if( pmIntvl === false ){
					if( _actv_Page === "device.html" ) {
						_el_dvc_fiveB.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.interval[window.localStorage.getItem("defaultLanguage")].fullText + pmIntvl + " ";
					}
				}
				else{
					if( _actv_Page === "device.html" ) {
						_el_dvc_fiveB.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.interval[window.localStorage.getItem("defaultLanguage")].fullText + pmIntvl.toFixed(2) + " ";
					}
				}
				
				if( pmIfRR === false ){
					if( _actv_Page === "device.html" ) {
						_el_dvc_five.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.rotation_rate[window.localStorage.getItem("defaultLanguage")].fullText + pmIfRR + " ";
						_el_dvc_fiveC.style.display = "none";
						_el_dvc_fiveD.style.display = "none";
						_el_dvc_fiveE.style.display = "none";
					}
				}
				else{
					if( _actv_Page === "device.html" ) {
						_el_dvc_five.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.rotation_rate[window.localStorage.getItem("defaultLanguage")].fullText + pmIfRR + " ";
						_el_dvc_fiveC.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.readOut_alpha[window.localStorage.getItem("defaultLanguage")].fullText + pmAlphaZ_RR.toFixed(2) + " ";
						_el_dvc_fiveD.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.readOut_beta[window.localStorage.getItem("defaultLanguage")].fullText + pmBetaX_RR.toFixed(2) + " ";
						_el_dvc_fiveE.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.readOut_gamma[window.localStorage.getItem("defaultLanguage")].fullText + pmGammaY_RR.toFixed(2) + " ";
					}
				}
				
				if( pmAsansG === null ){
					if( _actv_Page === "device.html" ) {
						_el_dvc_three.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.supp_A_sansG[window.localStorage.getItem("defaultLanguage")].fullText + _wboolyIf_NoGrav + " ";
						_el_dvc_threeB.style.display = "none";
						_el_dvc_threeC.style.display = "none";
						_el_dvc_threeD.style.display = "none";
					} // END active page control stmt.
				}
				else{
					if( _actv_Page === "device.html" ) {
						_el_dvc_three.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.supp_A_sansG[window.localStorage.getItem("defaultLanguage")].fullText + _wboolyIf_NoGrav + " ";
						_el_dvc_threeB.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.xRoll[window.localStorage.getItem("defaultLanguage")].fullText + pmBetaX_noG.toFixed(2) + " ";
						_el_dvc_threeC.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.yPitch[window.localStorage.getItem("defaultLanguage")].fullText + pmGammaY_noG.toFixed(2) + " ";
						_el_dvc_threeD.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.zYaw[window.localStorage.getItem("defaultLanguage")].fullText + pmAlphaZ_noG.toFixed(2) + " ";
					} // END active page control stmt.
				}
				
				if( pmAinclG === null ){
					if( _actv_Page === "device.html" ) {
						_el_dvc_four.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.supp_A_inclG[window.localStorage.getItem("defaultLanguage")].fullText + _wboolyIf_Grav + " ";
						_el_dvc_fourB.style.display = "none";
						_el_dvc_fourC.style.display = "none";
						_el_dvc_fourD.style.display = "none";
					} // END active page control stmt.
				}
				else{
					if( _actv_Page === "device.html" ) { 
						
						_el_dvc_four.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.supp_A_inclG[window.localStorage.getItem("defaultLanguage")].fullText + _wboolyIf_Grav + " ";
						_el_dvc_fourB.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.xRoll[window.localStorage.getItem("defaultLanguage")].fullText + _pmBetaX_yesG.toFixed(2) + " ";
						_el_dvc_fourC.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.yPitch[window.localStorage.getItem("defaultLanguage")].fullText + _pmGammaY_yesG.toFixed(2) + " ";
						_el_dvc_fourD.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.zYaw[window.localStorage.getItem("defaultLanguage")].fullText + pmAlphaZ_yesG.toFixed(2) + " "; 
						_el_pngposeThree.style[ perspective ] = "1024px"; //300px //768px 
						_el_pngposeFour.style[ perspective ] = "1024px"; //300px //768px
						
						// ORIGINAL ROTATION 
						if( pmAlphaZ_yesG > _nm_zero ) { // ~ current
							_facingUp = _nm_posOne; // ~ current
						}
						// ORIGINAL ROTATION 
						var AigRotation = _ro_BetaX_Grv( _pmBetaX_yesG ) + _ck_space + _TreD_GammaY_GRV_fu( _pmGammaY_yesG ); 
						_el_pngposeThree.style[ transform ] = AigRotation; // nComp
						_el_pngposeFour.style[ transform ] = AigRotation; // yComp
						
						// UNASSIGNED ROTATION COMPOSITES ~ PROPOSED #1 ~ noCompensation ~ nComp
						// var nComp = _roX_BetaX_prpr( _pmBetaX_yesG ) + _ck_space + _roY_GammaY_neg( _pmGammaY_yesG ) + _ck_space + _roZ_AlphaZ_nComp( pmAlphaZ_yesG ); 
						
						// UNASSIGNED ROTATION COMPOSITES ~ PROPOSED #2 ~ orientationCompensation ~ yComp
						// var yComp = _roX_BetaX_prpr( _pmBetaX_yesG ) + _ck_space + _roY_GammaY_neg( _pmGammaY_yesG ) + _ck_space + _roZ_AlphaZ_yComp( pmAlphaZ_yesG );
						
					} // END active page control stmt.
				}
			}
			
			else
			if( ( ( document.documentElement.addEventListener ) && ( !(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				// READOUT AND FEEDBACK PHASE  // EXECUTION PHASE
				if( pmIntvl === false ){
					if( _actv_Page === "device.html" ) {
						_el_dvc_fiveB.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.interval.EN.fullText + pmIntvl + " ";
					}
				}
				else{
					if( _actv_Page === "device.html" ) {
						_el_dvc_fiveB.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.interval.EN.fullText + pmIntvl.toFixed(2) + " ";
					}
				}
				
				if( pmIfRR === false ){
					if( _actv_Page === "device.html" ) {
						_el_dvc_five.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.rotation_rate.EN.fullText + pmIfRR + " ";
						_el_dvc_fiveC.style.display = "none";
						_el_dvc_fiveD.style.display = "none";
						_el_dvc_fiveE.style.display = "none";
					}
				}
				else{
					if( _actv_Page === "device.html" ) {
						_el_dvc_five.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.rotation_rate.EN.fullText + pmIfRR + " ";
						_el_dvc_fiveC.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.readOut_alpha.EN.fullText + pmAlphaZ_RR.toFixed(2) + " ";
						_el_dvc_fiveD.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.readOut_beta.EN.fullText + pmBetaX_RR.toFixed(2) + " ";
						_el_dvc_fiveE.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.readOut_gamma.EN.fullText + pmGammaY_RR.toFixed(2) + " ";
					}
				}
				
				if( pmAsansG === null ){
					if( _actv_Page === "device.html" ) {
						_el_dvc_three.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.supp_A_sansG.EN.fullText + _wboolyIf_NoGrav + " ";
						_el_dvc_threeB.style.display = "none";
						_el_dvc_threeC.style.display = "none";
						_el_dvc_threeD.style.display = "none";
					} // END active page control stmt.
				}
				else{
					if( _actv_Page === "device.html" ) {
						_el_dvc_three.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.supp_A_sansG.EN.fullText + _wboolyIf_NoGrav + " ";
						_el_dvc_threeB.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.xRoll.EN.fullText + pmBetaX_noG.toFixed(2) + " ";
						_el_dvc_threeC.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.yPitch.EN.fullText + pmGammaY_noG.toFixed(2) + " ";
						_el_dvc_threeD.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.zYaw.EN.fullText + pmAlphaZ_noG.toFixed(2) + " ";
					} // END active page control stmt.
				}
				
				if( pmAinclG === null ){
					if( _actv_Page === "device.html" ) {
						_el_dvc_four.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.supp_A_inclG.EN.fullText + _wboolyIf_Grav + " ";
						_el_dvc_fourB.style.display = "none";
						_el_dvc_fourC.style.display = "none";
						_el_dvc_fourD.style.display = "none";
					} // END active page control stmt.
				}
				else{
					if( _actv_Page === "device.html" ) { 
						
						_el_dvc_four.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.supp_A_inclG.EN.fullText + _wboolyIf_Grav + " ";
						_el_dvc_fourB.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.xRoll.EN.fullText + _pmBetaX_yesG.toFixed(2) + " ";
						_el_dvc_fourC.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.yPitch.EN.fullText + _pmGammaY_yesG.toFixed(2) + " ";
						_el_dvc_fourD.innerHTML = _obj_ConfigAll.CannedStrings.dvcM_labelOps.assets.formattedString.zYaw.EN.fullText + pmAlphaZ_yesG.toFixed(2) + " "; 
						_el_pngposeThree.style[ perspective ] = "768px"; //300px 
						_el_pngposeFour.style[ perspective ] = "768px"; //300px
						
						// ORIGINAL ROTATION 
						if( pmAlphaZ_yesG > _nm_zero ) { // ~ current
							_facingUp = _nm_posOne; // ~ current
						}
						// ORIGINAL ROTATION 
						var AigRotation = _ro_BetaX_Grv( _pmBetaX_yesG ) + _ck_space + _TreD_GammaY_GRV_fu( _pmGammaY_yesG ); 
						_el_pngposeThree.style[ transform ] = AigRotation; // nComp
						_el_pngposeFour.style[ transform ] = AigRotation; // yComp
						
						// UNASSIGNED ROTATION COMPOSITES ~ PROPOSED #1 ~ noCompensation ~ nComp
						// var nComp = _roX_BetaX_prpr( _pmBetaX_yesG ) + _ck_space + _roY_GammaY_neg( _pmGammaY_yesG ) + _ck_space + _roZ_AlphaZ_nComp( pmAlphaZ_yesG ); 
						
						// UNASSIGNED ROTATION COMPOSITES ~ PROPOSED #2 ~ orientationCompensation ~ yComp
						// var yComp = _roX_BetaX_prpr( _pmBetaX_yesG ) + _ck_space + _roY_GammaY_neg( _pmGammaY_yesG ) + _ck_space + _roZ_AlphaZ_yComp( pmAlphaZ_yesG );
						
					} // END active page control stmt.
				}
			}
			
		}
		
	} // END function
	
	
	// TRANSITION ~ three in one
	function _ThereAndBack_thInon( leEvt ) {		
		
		_thrInOneTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		
		if( ( _thrInOneTrgt.nodeName.toLowerCase() === "p" ) && ( _thrInOneTrgt.parentNode.className === "bidirect") ) { 
		
			if( _thrInOneTrgt.parentNode.style.left === "65%" ) { 
				_thrInOneTrgt.parentNode.style.left = "0%";
				_thrInOneTrgt.parentNode.style[ transitionProperty ]  = "left";
				_thrInOneTrgt.parentNode.style[ transitionDuration ]  = "5.625s"; 
			}
			else {
				_thrInOneTrgt.parentNode.style.left = "65%"; 
				_thrInOneTrgt.parentNode.style[ transitionProperty ]  = "left";
				_thrInOneTrgt.parentNode.style[ transitionDuration ]  = "5.625s"; 
			} 
		} 
		
		if( ( _thrInOneTrgt.nodeName.toLowerCase() === "strong" ) && ( _thrInOneTrgt.parentNode.parentNode.className === "bidirect") ) { 
		
			if( _thrInOneTrgt.parentNode.parentNode.style.left === "65%" ) { 
				_thrInOneTrgt.parentNode.parentNode.style.left = "0%";
				_thrInOneTrgt.parentNode.parentNode.style[ transitionProperty ]  = "left";
				_thrInOneTrgt.parentNode.parentNode.style[ transitionDuration ]  = "5.625s"; 
			}
			else {
				_thrInOneTrgt.parentNode.parentNode.style.left = "65%"; 
				_thrInOneTrgt.parentNode.parentNode.style[ transitionProperty ]  = "left";
				_thrInOneTrgt.parentNode.parentNode.style[ transitionDuration ]  = "5.625s"; 
			} 
		} 
		
	} // END function
	
	// TRANSITION ~ three in one
	function _ExpandRetract_thInon( leEvt ) { 
	
		_thrInOneTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		if( ( _thrInOneTrgt.nodeName.toLowerCase() === "div" ) && ( _thrInOneTrgt.className === "turtleHead") ) { 
			if( _thrInOneTrgt.style.width === "100%"){ 
				_thrInOneTrgt.style.width = "3em";
				_thrInOneTrgt.style.backgroundColor = "#00CCCC";
				_thrInOneTrgt.style[ transitionProperty ] = "all";
				_thrInOneTrgt.style[ transitionDuration ] = "3.69140625s";
				_thrInOneTrgt.style[ transitionTimingFunction ] = "cubic-bezier(0.250, 0.100, 0.250, 0.703)"; 
				_thrInOneTrgt.style[ transitionDelay ] = "1.0546875s"; 
			}
			else {
				_thrInOneTrgt.style.width = "100%";
				_thrInOneTrgt.style.backgroundColor = "#904B30";
				_thrInOneTrgt.style[ transitionProperty ] = "all";
				_thrInOneTrgt.style[ transitionDuration ] = "3.69140625s";
				_thrInOneTrgt.style[ transitionTimingFunction ] = "cubic-bezier(0.250, 0.100, 0.250, 0.703)"; 
				_thrInOneTrgt.style[ transitionDelay ] = "1.0546875s";
			} 
		} // END if( ( _thrInOneTrgt.nodeName.toLowerCase() === "div" ) && ( _thrInOneTrgt.className === "turtleHead") ) 
	} // END function
	
	// TRANSITION ~ three in one
	function _OvniHover_thInon( leEvt ) { 
	
		_thrInOneTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		if( ( _thrInOneTrgt.nodeName.toLowerCase() === "div" ) && ( _thrInOneTrgt.className === "ovni") ) { 
			if( _thrInOneTrgt.style.top === "14em"){ // "14em" // "-7.5em"
				_thrInOneTrgt.style[ transitionProperty ]= "left, background-color, top";
				_thrInOneTrgt.style[ transitionDuration ] = "1.0546875s, 1.0546875s, 1.0546875s";
				_thrInOneTrgt.style[ transitionTimingFunction ] = "linear, linear, linear"; // "linear, ease-in-out, ease-in-out" 
				_thrInOneTrgt.style[ transitionDelay ] = "0s, 0s, 0s"; 					
				_thrInOneTrgt.style.left = "0"; // _thrInOneTrgt.style.left = "60%";
				_thrInOneTrgt.style.backgroundColor = "#00CCCC";
				_thrInOneTrgt.style.top = "0";					
			}
			else { 					
				_thrInOneTrgt.style[ transitionProperty ] = "left, background-color, top";
				_thrInOneTrgt.style[ transitionDuration ] = "1.0546875s, 1.0546875s, 1.0546875s";
				_thrInOneTrgt.style[ transitionTimingFunction ] = "cubic-bezier(0.250, 0.100, 0.250, 0.703),cubic-bezier(0.250, 0.100, 0.250, 0.703), cubic-bezier(0.250, 0.100, 0.250, 0.703)";
				_thrInOneTrgt.style[ transitionDelay ] = "0s, 0s, 0s"; 
				_thrInOneTrgt.style.left = "30%";
				_thrInOneTrgt.style.backgroundColor = "#EBC793";
				_thrInOneTrgt.style.top = "14em"; // "14em" // "-7.5em"
			} 
		}
	} // END function
	
	
	// TRANSITION ~ ZOOM card 
	function _ZoomZoneSetUp(){
		
		_el_zoomWrapper.style.height = (parseInt( _fFHeight , 10 ) + "px"); // 1.375 1.125
		_el_zoomWrapper.style.width = (parseInt( _fFWidth , 10 ) + "px");
		
		_el_zoomCardOne.style.marginTop = ( .125 * (parseInt( _fFHeight , 10 ) + "px") ) + "px";
		_el_zoomCardOne.style.height = (parseInt( _fFHeight , 10 ) + "px"); // 1.375 1.125
		_el_zoomCardOne.style.width = (parseInt( _fFWidth , 10 ) + "px");
		
		_el_pBOne.setAttribute( "width" , (parseInt( _fFWidth , 10 ) + "px") );
		_el_pBOne.setAttribute( "height" , (parseInt( _fFHeight , 10 ) + "px") );
		
		_el_pFOne.setAttribute( "width" , (parseInt( _fFWidth , 10 ) + "px") );
		_el_pFOne.setAttribute( "height" , (parseInt( _fFHeight , 10 ) + "px") );	
		
		_el_zFOne.style.height = (parseInt( _fFHeight , 10 ) + "px");
		_el_zFOne.style.width = (parseInt( _fFWidth , 10 ) + "px");
		
		_el_zBOne.style.height = (parseInt( _fFHeight , 10 ) + "px");////
		_el_zBOne.style.width = (parseInt( _fFWidth , 10 ) + "px");
		
		
	} // END function
	
	
	// TRANSITION ~ ZOOM card 
	function _DeployZoomFlipper() {
		
		if( _actv_Page === "transition.html" ) {
			
			if( _docObj.documentElement.addEventListener ){
				
				_el_zBOne.style.display = "block";
				_el_zFOne.style.display = "block";
				
				_el_zoomWrapper.style[ perspective ] = "875px"; // "575px"
				_el_zoomWrapper.style[ perspectiveOrigin ] = "50% 50%"; // "0% 0%"
				
				_el_zoomCardOne.style[ transformStyle ] = "preserve-3d"; // "flat";
				_el_zoomCardOne.style[ backfaceVisibility ] = "visible"; // "hidden";
				_el_zoomCardOne.style[ transitionProperty ] = "" + css_transform + ""; // "-webkit-transform"
				_el_zoomCardOne.style[ transitionDuration ] = "1.546875s"; 
				_el_zoomCardOne.style[ transitionTimingFunction ] = "cubic-bezier(0,.75,.25,1)";
				
				_el_zBOne.style[ transform ] = "translateZ(-180px) scaleX(-1)"; // This is the only negative companion...
				_el_zFOne.style[ transform ] = "translateZ(1px) scaleX(1)"; // << correction settled "neg" to "positive..."
				
				_ZoomZoneSetUp();

			}
		}
		
		
	} // END function
	
	// TRANSITION ~ ZOOM card 
	function _SpinZoom( leEvt ) {
		_zoomTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); // ULTIMATE TARGET SRC EL ~ img.zoomPic
		
		_angle_zoom = _angle_zoom + 180;
		
		_zoomTrgt.parentNode.parentNode.style[ transform ] = "rotateY(" + _angle_zoom + "deg)";  // ~div.zoomCardOne
	} // END function
	
	
	// TRANSFORM ~ two sided business card
	function _DeployBizCard() {
		
		_el_frontBiz.style.display = "block"; 
		_el_backBiz.style.display = "block";
		
		_el_bizCardOne.style[ transition ] = "1.40625s all";
		_el_bizCardOne.style[ transformStyle ] = "preserve-3d";
		
		_el_frontBiz.style[ backfaceVisibility ] = "hidden";
		_el_frontBiz.style[ transform ] = "rotateX(180deg) scaleY(-1)"; 
		_el_frontBiz.style[ backfaceVisibility ] = "hidden";
		_el_frontBiz.style[ transform ] = "rotateX(0deg)";
		
		//// _el_backBiz.style.top = -( _el_frontBiz.offsetHeight ) + "px";
		_el_backBiz.style.height = _el_frontBiz.offsetHeight + "px";
		_el_backBiz.style.width = _el_frontBiz.offsetWidth + "px";
		
		if( !!(_chopperPoint === "iOS4") ){
			// know moar....................
		}
		
		else 
		if( !(_chopperPoint === "iOS4") ){
			if( ( _docObj.documentElement.clientWidth > 1024 ) || ( _docObj.documentElement.clientWidth <= 800 ) ){
				_el_backBiz.style.left = ( parseInt( ( _GetComputedStyle( _el_backBiz, "right" ) / 2 ), 10 ) ) + "px";
				//// _el_backBiz.style.left = ( parseInt( ( _GetComputedStyle( _el_backBiz, "right" ) / 1 ), 10 ) ) + "px";
			} 
			else
			if( ( _docObj.documentElement.clientWidth > 800 ) && ( _docObj.documentElement.clientWidth <= 1024 )){
				_el_backBiz.style.left = ( parseInt( ( _GetComputedStyle( _el_backBiz, "right" ) ), 10 ) ) + "px"; 
			}
		}
		
	} // END function
	
	
	
	// TRANSFORM ~ two sided business card
	function _SpinBizCard( leEvt ) { 
		
		_evtBizTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		
		if( _angle_biz === 0 ) {
			_angle_biz = _angle_biz + 180;
			_scaleBizPM = _negScale_biz;
		}
		else 
		if( _angle_biz === 180 ) {
			_angle_biz = 0;
			_scaleBizPM = -_negScale_biz;
		}
		
		if( ( _evtBizTrgt.nodeName.toLowerCase() === "div" ) && ( _evtBizTrgt.id === "frontBiz" ) ){
			_evtBizTrgt.parentElement.style[ transform ] = "rotateX("+ _angle_biz +"deg) scaleY("+ _scaleBizPM +")";
		}
		else
		if( ( _evtBizTrgt.nodeName.toLowerCase() === "h1" ) && ( _evtBizTrgt.className === "hotelOneBiz" ) ){
			_evtBizTrgt.parentElement.parentElement.style[ transform ] = "rotateX("+ _angle_biz +"deg) scaleY("+ _scaleBizPM +")";
		}
		else
		if( ( _evtBizTrgt.nodeName.toLowerCase() === "h2" ) && ( _evtBizTrgt.className === "hotelTwoBiz" ) ){
			_evtBizTrgt.parentElement.parentElement.style[ transform ] = "rotateX("+ _angle_biz +"deg) scaleY("+ _scaleBizPM +")";
		}
		else
		if( ( _evtBizTrgt.nodeName.toLowerCase() === "p" ) && ( _evtBizTrgt.className === "genericBiz" ) ){
			_evtBizTrgt.parentElement.parentElement.style[ transform ] = "rotateX("+ _angle_biz +"deg) scaleY("+ _scaleBizPM +")";
		}
		else
		if( ( _evtBizTrgt.nodeName.toLowerCase() === "div" ) && ( _evtBizTrgt.id === "backBiz" ) ){
			_evtBizTrgt.parentElement.style[ transform ] = "rotateX("+ _angle_biz +"deg) scaleY("+ _scaleBizPM +")";
		}
		else
		if( ( _evtBizTrgt.nodeName.toLowerCase() === "ul" ) && ( _evtBizTrgt.className === "unlimabiz" ) ){
			_evtBizTrgt.parentElement.parentElement.style[ transform ] = "rotateX("+ _angle_biz +"deg) scaleY("+ _scaleBizPM +")";
		}
		else
		if( ( _evtBizTrgt.nodeName.toLowerCase() === "li" ) && ( _evtBizTrgt.className === "limabiz" ) ){
			_evtBizTrgt.parentElement.parentElement.parentElement.style[ transform ] = "rotateX("+ _angle_biz +"deg) scaleY("+ _scaleBizPM +")";
		}
		else
		if( ( _evtBizTrgt.nodeName.toLowerCase() === "strong" ) && ( _evtBizTrgt.className === "strongbiz" ) ){
			_evtBizTrgt.parentElement.parentElement.parentElement.parentElement.style[ transform ] = "rotateX("+ _angle_biz +"deg) scaleY("+ _scaleBizPM +")";
		} 
		
	} // END function
	
	// TRANSFORM ~ law
	function _DeployLawFlipper() { 
		
		_el_backLaw.style.display = "block";
		_el_frontLaw.style.display = "block";
		
		for( j = 0; j < _pGmax_law; j = j + 1 ) { 
			if( _GrafsLaw[ j ].className === "LawLogo" ) { // p.LawLogo	
				_GrafsLaw[ j ].style.left = (_GrafsLaw[ j ].parentElement.offsetWidth) + (_GrafsLaw[ j ].offsetWidth/(1/.5625)) + "px";
			}
			else
			if( _GrafsLaw[ j ].className === "shadowLaw" ) { // p.shadowLaw 
				_GrafsLaw[ j ].style.left = (_GrafsLaw[ j ].parentElement.offsetWidth) + (_GrafsLaw[ j ].offsetWidth/(1/.5625)) + "px";
			}
			else
			if( _GrafsLaw[ j ].className === "LawCallsign" ) { // p.LawCallsign
				// _GrafsLaw[ j ].style.left = (_GrafsLaw[ j ].parentElement.offsetWidth) + (_GrafsLaw[ j ].offsetWidth/(1/.5625)) + "px";
				_GrafsLaw[ j ].style.left = (_GrafsLaw[ j ].parentElement.offsetWidth) + (_GrafsLaw[ j ].offsetWidth) + "px";
			} 
			else
			if( _GrafsLaw[ j ].className === "LawBubble" ) { // p.LawBubble	
				_GrafsLaw[ j ].style[ transform ] = "rotate(-20deg)";	
			}
		} 
		
		///// if( ( _docObj.documentElement.clientWidth > 1024 ) || ( _docObj.documentElement.clientWidth <= 800 ) ){} else if( ( _docObj.documentElement.clientWidth > 800 ) && ( _docObj.documentElement.clientWidth <= 1024 )) {}
		
		// TRANSFORM ~ law
		_el_lawWrapper.style[ perspective ] = "1000px";
		
		// TRANSFORM ~ law
		// _el_lawCardOne.style[ transformOrigin ] = "37% 50% 0"; // "50% 50% 0"
		// TRANSFORM ~ law
		_el_lawCardOne.style[ transition ] = "0.703125s all";
		_el_lawCardOne.style[ transformStyle ] = "preserve-3d"; 
		// TRANSFORM ~ law
		// _el_lawCardOne.style[ transformOrigin ] = "50% 50% 0"; // "50% 50% 0"
		
		// TRANSFORM ~ law
		_el_frontLaw.style[ backfaceVisibility ] = "hidden";
		_el_frontLaw.style[ transform ] = "rotateY(0deg)"; 
		
		// TRANSFORM ~ law
		_el_backLaw.style[ backfaceVisibility ] = "hidden";
		_el_backLaw.style[ transform ] = "rotateY(180deg)";
		
		// TRANSFORM ~ law
		_el_backLaw.style.width = _el_frontLaw.offsetWidth + "px";
		_el_backLaw.style.height = _el_frontLaw.offsetHeight + "px";
		
		// TRANSFORM ~ law
		// if VP GT 1ST mq...
		// _el_backLaw.style.left = ( ( _GetComputedStyle( _el_frontLaw.parentElement, "padding-left" ) + _GetComputedStyle( _el_frontLaw.parentElement, "margin-left" ) ) + ( _el_frontLaw.offsetWidth / 2 ) ) + "px";
		
		// TRANSFORM ~ law
		// UNSURE OF PURPOSE
		// _el_backLaw.style.left = ( parseInt( ( _GetComputedStyle( _el_backLaw, "right" ) / 2 ), 10 ) ) + "px";
		
	} // END function
	
	// TRANSFORM ~ law
	function _RotateLaw( leEvt ) { 
		
		_lawTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		
		if( _angle_law === 0 ) {
			_angle_law = _angle_law + 180;
			_scaleLawPM = -_nm_negScale_law;
		}
		else 
		if( _angle_law === 180 ) {
			_angle_law = 0;
			_scaleLawPM = -_nm_negScale_law;
		} 
		
		if( ( _lawTrgt.nodeName.toLowerCase() === "div" ) && ( _lawTrgt.id === "lawCardOne" ) ){
			_lawTrgt.style[ transform ] = "rotateY("+ _angle_law +"deg) scaleX("+ _scaleLawPM +")";
		} 
		else
		if( ( _lawTrgt.nodeName.toLowerCase() === "div" ) && ( _lawTrgt.id === "frontLaw" ) ){
			_lawTrgt.parentElement.style[ transform ] = "rotateY("+ _angle_law +"deg) scaleX("+ _scaleLawPM +")";
		} 
		else
		if( ( _lawTrgt.nodeName.toLowerCase() === "p" ) && ( _lawTrgt.className === "LawBubble" ) ){
			_lawTrgt.parentElement.parentElement.style[ transform ] = "rotateY("+ _angle_law +"deg) scaleX("+ _scaleLawPM +")";
		} 
		else
		if( ( _lawTrgt.nodeName.toLowerCase() === "div" ) && ( _lawTrgt.id === "backLaw" ) ){
			_lawTrgt.parentElement.style[ transform ] = "rotateY("+ _angle_law +"deg) scaleX("+ _scaleLawPM +")";
		} 
		else
		if( ( _lawTrgt.nodeName.toLowerCase() === "p" ) && ( _lawTrgt.className === "LawLogo" ) ){
			_lawTrgt.parentElement.parentElement.style[ transform ] = "rotateY("+ _angle_law +"deg) scaleX("+ _scaleLawPM +")";
		} 
		else
		if( ( _lawTrgt.nodeName.toLowerCase() === "p" ) && ( _lawTrgt.className === "shadowLaw" ) ){
			_lawTrgt.parentElement.parentElement.style[ transform ] = "rotateY("+ _angle_law +"deg) scaleX("+ _scaleLawPM +")";
		} 
		else
		if( ( _lawTrgt.nodeName.toLowerCase() === "p" ) && ( _lawTrgt.className === "LawCallsign" ) ){
			_lawTrgt.parentElement.parentElement.style[ transform ] = "rotateY("+ _angle_law +"deg) scaleX("+ _scaleLawPM +")";
		} 
	} // END function
	
	
	/*################  INIT function  ######################*/
	function _DOMCONLO() { 
		
		// _SwtchRooVLU;
		if( !!( "localStorage" in window ) ) {
			// WEB STORAGE || COOKIE FUNC.
			if( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) { _SwtchRooVLU = "EN"; }
			else
			if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {_SwtchRooVLU = window.localStorage.getItem("defaultLanguage") }
		}
		
		// ALL BROWSERS
		// modal ops ~ 86 110 128 305 316
		/* margin-top: -450px; */ /* ~= margin-top @150px and height 300px ~= #specialBox offset */ 
		var _ar_As_tog_1_MDL = _el_specialBox.getElementsByTagName("a");
		var _As_tog1Max_MDL = _ar_As_tog_1_MDL.length; 
		var A;		
		for( A = 0; A < _As_tog1Max_MDL; A = A + 1) {
			if(_ar_As_tog_1_MDL[A].className === "modalOps") {
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_As_tog_1_MDL[A], "mousedown", _ToggleOverlay ); 
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_As_tog_1_MDL[A], "click", _PrevDeffo );
			} 
		}
		
		// modal ops ~ 86 110 128 305 316
		/* margin-top: -450px; */ /* ~= margin-top @150px and height 300px ~= #specialBox offset */ 
		var _ar_As_tog_2_MDL = _el_retexpUL4.getElementsByTagName("a");
		var _As_tog2Max_MDL = _ar_As_tog_2_MDL.length; 
		var C;		
		for( C = 0; C < _As_tog2Max_MDL; C = C + 1) {
			if(_ar_As_tog_2_MDL[C].className === "modalOps") {
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_As_tog_2_MDL[C], "mousedown", _ToggleOverlay ); 
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_As_tog_2_MDL[C], "click", _PrevDeffo );
			} 
		}
		
		if( _actv_Page === "transform.html" ) { 
			// TRANSFORM ~ two sided business card
			_DeployBizCard(); 
		}
		
		// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html
		
		// if DEVICE ORIENTATION === undefined || false || null
		if( ( document.documentElement.addEventListener ) && ( _wboolyIf_Orientation1 === "false" ) ){
			
			_el_anti.style.display = "block";
			
			if( ( ( document.documentElement.addEventListener ) && ( !!(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.antiAlt[window.localStorage.getItem("defaultLanguage")].fullText;
			}
			else
			if( ( ( document.documentElement.addEventListener ) && ( !(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.antiAlt.EN.fullText;
			}
			
		}
		else
		if( ( document.documentElement.addEventListener ) && ( _wboolyIf_Orientation2 === "false" ) ){
			
			_el_anti.style.display = "block";
			
			if( ( ( document.documentElement.addEventListener ) && ( !!(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.anti[window.localStorage.getItem("defaultLanguage")].fullText;
			}
			else
			if( ( ( document.documentElement.addEventListener ) && ( !(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.anti.EN.fullText;
			}
			
		}
		else
		if( ( document.documentElement.attachEvent ) && ( !(_wboolyIf_Orientation1) ) ){
			
			_el_anti.style.display = "block";
			
			if( ( document.documentElement.attachEvent ) && ( !!( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" ) ) )) {
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.antiAlt[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
			}
			else
			if( ( document.documentElement.attachEvent ) && ( !( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" ) ) )) {
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.antiAlt.EN.fullText;
			}
		}
		
		else
		if( ( document.documentElement.attachEvent ) && ( _wboolyIf_Orientation2 === "false" ) ){
			
			_el_anti.style.display = "block";
			
			if( ( document.documentElement.attachEvent ) && ( !!( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" ) ) )) {
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.anti[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
			}
			else
			if( ( document.documentElement.attachEvent ) && ( !( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" ) ) )) {
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.anti.EN.fullText;
			}
		}
		
		// if DEVICE ORIENTATION
		// if( ( document.documentElement.addEventListener ) && ( ( _wboolyIf_Orientation1 === true ) && ( _wboolyIf_Orientation2 === true ) ) ){
		if( ( document.documentElement.addEventListener ) && ( ( _wboolyIf_Orientation1 !== "false" ) && ( _wboolyIf_Orientation2 !== "false" ) ) ){
			
			if( ( ( document.documentElement.addEventListener ) && ( !!(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				if( _chopperPoint !== "iOS4" ){
					
					if( _actv_Page === "device.html" ) { 
						// noAction
					}
					_el_pro.style.display = "block";
					_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.pro[window.localStorage.getItem("defaultLanguage")].fullText; 
					donLuchoHAKI.Utils.evt_u.AddEventoHandler( donLuchoHAKI.RetELs.glow() , "deviceorientation" , _DeviceOrientationOps ); 
				}
				
				else
				
				if( _chopperPoint === "iOS4" ){
					
					_el_anti.style.display = "block";
					_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.notFullySuppd[window.localStorage.getItem("defaultLanguage")].fullText; 
					
					if( _actv_Page === "device.html" ) { 
					
						_el_dvc_one.innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport[window.localStorage.getItem("defaultLanguage")].fullText;
						_el_dvc_oneB.style.display = "none";
						_el_dvc_oneC.style.display = "none";
						_el_dvc_oneD.style.display = "none";
						_el_dvc_oneE.style.display = "none";
						_el_dvc_oneF.style.display = "none";
						_el_dvc_oneG.style.display = "none";
						_el_dvc_oneH.style.display = "none";
						_el_dvc_oneI.style.display = "none";
						_el_dvc_oneJ.style.display = "none";
						_el_dvc_two.innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport[window.localStorage.getItem("defaultLanguage")].fullText;	
						
						_el_svgpose.style.display = "none";
						_el_descrdo.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport[window.localStorage.getItem("defaultLanguage")].fullText;
						
						_el_pngposeTwo.style.display = "none";
						_el_descrdoTwo.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport[window.localStorage.getItem("defaultLanguage")].fullText; 
						
						_el_dvc_two_one_half.innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport[window.localStorage.getItem("defaultLanguage")].fullText; 
						_el_dvc_garden.style.display = "none";
						
						if( _chopperPoint !== "Newer_Android_Device" ){ 
							_el_DVCninetynine.innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport[window.localStorage.getItem("defaultLanguage")].fullText;
							_el_dvcMembTwo.style.display = "none";
						}
						
					} // END if( _actv_Page === "device.html" )
					
				} 
			}
			else
			if( ( ( document.documentElement.addEventListener ) && ( !(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				
				if( _chopperPoint !== "iOS4" ){
					
					if( _actv_Page === "device.html" ) { 
						// noAction
					}
					_el_pro.style.display = "block";
					_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.pro.EN.fullText; 
					donLuchoHAKI.Utils.evt_u.AddEventoHandler( donLuchoHAKI.RetELs.glow() , "deviceorientation" , _DeviceOrientationOps ); 
				}
				else
				if( _chopperPoint === "iOS4" ){
					
					_el_anti.style.display = "block";
					_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.notFullySuppd.EN.fullText; 
					
					if( _actv_Page === "device.html" ) { 
					
						_el_dvc_one.innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport.EN.fullText;
						_el_dvc_oneB.style.display = "none";
						_el_dvc_oneC.style.display = "none";
						_el_dvc_oneD.style.display = "none";
						_el_dvc_oneE.style.display = "none";
						_el_dvc_oneF.style.display = "none";
						_el_dvc_oneG.style.display = "none";
						_el_dvc_oneH.style.display = "none";
						_el_dvc_oneI.style.display = "none";
						_el_dvc_oneJ.style.display = "none";
						_el_dvc_two.innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport.EN.fullText;	
						
						_el_svgpose.style.display = "none";
						_el_descrdo.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport.EN.fullText;
						
						_el_pngposeTwo.style.display = "none";
						_el_descrdoTwo.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport.EN.fullText; 
						
						_el_dvc_two_one_half.innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport.EN.fullText; 
						_el_dvc_garden.style.display = "none";
						
						if( _chopperPoint !== "Newer_Android_Device" ){ 
							_el_DVCninetynine.innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport.EN.fullText;
							_el_dvcMembTwo.style.display = "none";
						}
						
					} // END if( _actv_Page === "device.html" )
					
				} 
			}
			
		}
		
		
		else
			
		if( ( document.documentElement.attachEvent ) && ( _wboolyIf_Orientation1 ) ){
			_el_anti.style.display = "block";
			_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.notFullySuppd.EN.fullText; 
			
			if( _actv_Page === "device.html" ) { 
				
				if( ( document.documentElement.attachEvent ) && ( !!( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" ) ) )) {
					_docObj.getElementById("dvc_one").innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
					_docObj.getElementById("dvc_two").innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
					_docObj.getElementById("descrdo").innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
					_docObj.getElementById("descrdoTwo").innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
					_docObj.getElementById("dvc_two_one_half").innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
					_docObj.getElementById("DVCninetynine").innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
				}
				else
				if( ( document.documentElement.attachEvent ) && ( !( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" ) ) )) {
					_docObj.getElementById("dvc_one").innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport.EN.fullText;
					_docObj.getElementById("dvc_two").innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport.EN.fullText;
					_docObj.getElementById("descrdo").innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport.EN.fullText;
					_docObj.getElementById("descrdoTwo").innerHTML += _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport.EN.fullText;
					_docObj.getElementById("dvc_two_one_half").innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.nonFormatString.noFullSupport.EN.fullText;
					_docObj.getElementById("DVCninetynine").innerHTML = _obj_ConfigAll.CannedStrings.deviceorientationOps.assets.formattedString.noFullSupport.EN.fullText;
				}
				
				_docObj.getElementById("dvc_oneB").style.display = "none";
				_docObj.getElementById("dvc_oneC").style.display = "none";
				_docObj.getElementById("dvc_oneD").style.display = "none";
				_docObj.getElementById("dvc_oneE").style.display = "none";
				_docObj.getElementById("dvc_oneF").style.display = "none";
				_docObj.getElementById("dvc_oneG").style.display = "none";
				_docObj.getElementById("dvc_oneH").style.display = "none";
				_docObj.getElementById("dvc_oneI").style.display = "none";
				_docObj.getElementById("dvc_oneJ").style.display = "none";
				_docObj.getElementById("svgpose").style.display = "none";
				_docObj.getElementById("pngposeTwo").style.display = "none";
				_docObj.getElementById("dvc_garden").style.display = "none";
				_docObj.getElementById("dvcMembTwo").style.display = "none";
				
				
			} // END if( _actv_Page === "device.html" )
		}
		
		// NO ~ if DEVICE MOTION === undefined || false || null
		if( ( document.documentElement.addEventListener ) && ( _wboolyIf_Motion1 === "false" ) ){
			
			_el_anti.style.display = "block"; 
			
			if( ( ( document.documentElement.addEventListener ) && ( !!(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.anti[window.localStorage.getItem("defaultLanguage")].fullText;
			}
			else
			if( ( ( document.documentElement.addEventListener ) && ( !(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.anti.EN.fullText;
			}
		}
		
		else
		if( ( document.documentElement.addEventListener ) && ( _wboolyIf_Motion2 === "false" ) ){
			
			_el_anti.style.display = "block"; 
			
			if( ( ( document.documentElement.addEventListener ) && ( !!(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.antiAlt[window.localStorage.getItem("defaultLanguage")].fullText;
			}
			else
			if( ( ( document.documentElement.addEventListener ) && ( !(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.antiAlt.EN.fullText;
			}
		}
		
		else
		if( ( document.documentElement.attachEvent ) && ( !(_wboolyIf_Motion1) ) ){
			
			_el_anti.style.display = "block";
			
			if( ( document.documentElement.attachEvent ) && ( !!( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" ) ) )) {
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.anti[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
			}
			else
			if( ( document.documentElement.attachEvent ) && ( !( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" ) ) )) {
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.anti.EN.fullText;
			}
		}
		
		else
		if( ( document.documentElement.attachEvent ) && ( _wboolyIf_Motion2 === "false" ) ){
			
			_el_anti.style.display = "block"; 
			
			if( ( document.documentElement.attachEvent ) && ( !!( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" ) ) )) {
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.antiAlt[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
			}
			else
			if( ( document.documentElement.attachEvent ) && ( !( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" ) ) )) {
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.antiAlt.EN.fullText;
			}
		}
		
		// YES ~ if DEVICE MOTION
		if( ( document.documentElement.addEventListener ) && ( ( _wboolyIf_Motion1 !== "false" ) && ( _wboolyIf_Motion2 !== "false" ) ) ){ 
			
			if( ( ( document.documentElement.addEventListener ) && ( !!(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				
				if( _chopperPoint !== "Newer_Android_Device" ){
					_el_pro.style.display = "block";
					_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.pro[window.localStorage.getItem("defaultLanguage")].fullText;
					donLuchoHAKI.Utils.evt_u.AddEventoHandler( donLuchoHAKI.RetELs.glow() , "devicemotion" , _DeviceMotionOps ); 
				}
				else
				if( _chopperPoint === "Newer_Android_Device" ){
					
					_el_anti.style.display = "block";
					
					_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[window.localStorage.getItem("defaultLanguage")].fullText;
					
					if( _actv_Page === "device.html" ) { 
						
						_el_pngposeThree.style.display = "none";
						_el_descrdmThree.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[window.localStorage.getItem("defaultLanguage")].fullText;
						
						_el_pngposeFour.style.display = "none";
						_el_descrdmFour.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[window.localStorage.getItem("defaultLanguage")].fullText;
						
						_el_dvc_three.innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[window.localStorage.getItem("defaultLanguage")].fullText;
						_el_dvc_threeB.style.display = "none";
						_el_dvc_threeC.style.display = "none";
						_el_dvc_threeD.style.display = "none";
						
						_el_dvc_four.innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[window.localStorage.getItem("defaultLanguage")].fullText;
						_el_dvc_fourB.style.display = "none";
						_el_dvc_fourC.style.display = "none";
						_el_dvc_fourD.style.display = "none";
						
						_el_dvc_five.innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[window.localStorage.getItem("defaultLanguage")].fullText;
						_el_dvc_fiveB.style.display = "none";
						_el_dvc_fiveC.style.display = "none";
						_el_dvc_fiveD.style.display = "none";
						_el_dvc_fiveE.style.display = "none";
						
						_el_dvcMemb.innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[window.localStorage.getItem("defaultLanguage")].fullText;
						// 
					} // END if( _actv_Page === "device.html" )
					
				} 
				
			}
			
			else
			if( ( ( document.documentElement.addEventListener ) && ( !(window.localStorage.getItem("defaultLanguage") ) ) ) ){
				
				if( _chopperPoint !== "Newer_Android_Device" ){
					_el_pro.style.display = "block";
					_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.pro.EN.fullText;
					donLuchoHAKI.Utils.evt_u.AddEventoHandler( donLuchoHAKI.RetELs.glow() , "devicemotion" , _DeviceMotionOps ); 
				}
				else
				if( _chopperPoint === "Newer_Android_Device" ){
					
					_el_anti.style.display = "block";
					
					_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
					
					if( _actv_Page === "device.html" ) { 
						
						_el_pngposeThree.style.display = "none";
						_el_descrdmThree.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
						
						_el_pngposeFour.style.display = "none";
						_el_descrdmFour.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
						
						_el_dvc_three.innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
						_el_dvc_threeB.style.display = "none";
						_el_dvc_threeC.style.display = "none";
						_el_dvc_threeD.style.display = "none";
						
						_el_dvc_four.innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
						_el_dvc_fourB.style.display = "none";
						_el_dvc_fourC.style.display = "none";
						_el_dvc_fourD.style.display = "none";
						
						_el_dvc_five.innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
						_el_dvc_fiveB.style.display = "none";
						_el_dvc_fiveC.style.display = "none";
						_el_dvc_fiveD.style.display = "none";
						_el_dvc_fiveE.style.display = "none";
						
						_el_dvcMemb.innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
						// 
					} // END if( _actv_Page === "device.html" )
					
				} 
				
			}
			
		}
		
		else
		if( ( document.documentElement.attachEvent ) && ( _wboolyIf_Motion1 ) ){
			
			_el_anti.style.display = "block";
			
			if( ( document.documentElement.attachEvent ) && ( !!( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" ) ) )) {
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
			}
			else
			if( ( document.documentElement.attachEvent ) && ( !( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" ) ) )) {
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
			}
			
			if( _actv_Page === "device.html" ) { 
				
				_docObj.getElementById("pngposeThree").style.display = "none";
				_docObj.getElementById("pngposeFour").style.display = "none";
				
				_docObj.getElementById("dvc_threeB").style.display = "none";
				_docObj.getElementById("dvc_threeC").style.display = "none";
				_docObj.getElementById("dvc_threeD").style.display = "none";
				
				_docObj.getElementById("dvc_fourB").style.display = "none";
				_docObj.getElementById("dvc_fourC").style.display = "none";
				_docObj.getElementById("dvc_fourD").style.display = "none";
				
				_docObj.getElementById("dvc_fiveB").style.display = "none";
				_docObj.getElementById("dvc_fiveC").style.display = "none";
				_docObj.getElementById("dvc_fiveD").style.display = "none";
				_docObj.getElementById("dvc_fiveE").style.display = "none"; 
				
				
				if( ( document.documentElement.attachEvent ) && ( !!( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" ) ) )) {
					_docObj.getElementById("descrdmThree").innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
					_docObj.getElementById("descrdmFour").innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
					_docObj.getElementById("dvc_three").innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
					_docObj.getElementById("dvc_four").innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
					_docObj.getElementById("dvc_five").innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
					_docObj.getElementById("dvcMemb").innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" )].fullText;
				}
				else
				if( ( document.documentElement.attachEvent ) && ( !( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1" ) ) )) {
					_docObj.getElementById("descrdmThree").innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
					_docObj.getElementById("descrdmFour").innerHTML += _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
					_docObj.getElementById("dvc_three").innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
					_docObj.getElementById("dvc_four").innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
					_docObj.getElementById("dvc_five").innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
					_docObj.getElementById("dvcMemb").innerHTML = _obj_ConfigAll.CannedStrings.devicemotionOps.assets.formattedString.notFullySuppd.EN.fullText;
				}
				
			} // END if( _actv_Page === "device.html" )
		}
		
		// ~ all Touch functionality
		if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) || ( document.documentElement.attachEvent && !!( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1") ) ) ) { 
		
			if( document.documentElement.attachEvent && !!( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1") ) ) {
				
				if( !!_wboolyIf_Touch ){
					// GENERAL
					_el_pro.style.display = "block";
					_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.touchOps.assets.formattedString.pro[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1")].fullText;
					// ~ FAUX Drag and Drop Functionality
					_Dd_faux_Profile();	
					
				}
				else
				if( !_wboolyIf_Touch ){
					// GENERAL
					_el_anti.style.display = "block";
					_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.touchOps.assets.formattedString.anti[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1")].fullText;
					// ~ FAUX Drag and Drop Functionality
					_el_faux_dz.style.display = "none"; 
				}
				
			}
			else
			if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
				if( !!_wboolyIf_Touch ){
					// GENERAL
					_el_pro.style.display = "block";
					_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.touchOps.assets.formattedString.pro[window.localStorage.getItem("defaultLanguage")].fullText;
					// ~ FAUX Drag and Drop Functionality
					_Dd_faux_Profile();	
					
				}
				else
				if( !_wboolyIf_Touch ){
					// GENERAL
					_el_anti.style.display = "block";
					_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.touchOps.assets.formattedString.anti[window.localStorage.getItem("defaultLanguage")].fullText;
					// ~ FAUX Drag and Drop Functionality
					_el_faux_dz.style.display = "none"; 
				}
			}
		}
		
		else {
			
			if( ( document.documentElement.attachEvent && !( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1") ) ) || ( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) ) {
				if( !!_wboolyIf_Touch ){
					// GENERAL
					_el_pro.style.display = "block";
					_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.touchOps.assets.formattedString.pro.EN.fullText;
					// ~ FAUX Drag and Drop Functionality
					_Dd_faux_Profile();	
					
				}
				else
				if( !_wboolyIf_Touch ){
					// GENERAL
					_el_anti.style.display = "block";
					_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.touchOps.assets.formattedString.anti.EN.fullText;
					// ~ FAUX Drag and Drop Functionality
					_el_faux_dz.style.display = "none"; 
				}
			}
			
		}
		
		if( ( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) || ( document.documentElement.attachEvent && !!( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1") ) ) ) {
			if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ){
				if( !!_wboolyIf_D_n_D ){
					_el_pro.style.display = "block";
					if( document.documentElement.addEventListener ){
						_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.dragNdropOps.assets.formattedString.pro[window.localStorage.getItem("defaultLanguage")].fullText;
					}
					else
					if( document.documentElement.attachEvent ){
						_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.dragNdropOps.assets.formattedString.proAlt[window.localStorage.getItem("defaultLanguage")].fullText;
					}
				}
			}
			else
			if( document.documentElement.attachEvent && !!( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1") ) ){
				if( !!_wboolyIf_D_n_D ){
					_el_pro.style.display = "block";
					if( document.documentElement.addEventListener ){
						_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.dragNdropOps.assets.formattedString.pro[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1")].fullText;
					}
					else
					if( document.documentElement.attachEvent ){
						_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.dragNdropOps.assets.formattedString.proAlt[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1")].fullText;
					}
				}
			}
		}
		
		if( ( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) || ( document.documentElement.attachEvent && ! ( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1") ) ) ) {
			
			_el_pro.style.display = "block";
			
			if( document.documentElement.addEventListener ){
				_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.dragNdropOps.assets.formattedString.pro.EN.fullText;
			}
			else
			if( document.documentElement.attachEvent ){
				_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.dragNdropOps.assets.formattedString.proAlt.EN.fullText;
			}
		}
		else
		if( ( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) || ( document.documentElement.attachEvent && !!( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1") ) ) ) {
			if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ){
				if( !_wboolyIf_D_n_D ){
					_el_dz1.style.display = "none";
					_el_dz2.style.display = "none";
					_el_anti.style.display = "block";
					_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.dragNdropOps.assets.formattedString.anti[window.localStorage.getItem("defaultLanguage")].fullText;
				}
			}
			else
			if( document.documentElement.attachEvent && !!( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1") ) ){
				if( !_wboolyIf_D_n_D ){
					_el_dz1.style.display = "none";
					_el_dz2.style.display = "none";
					_el_anti.style.display = "block";
					_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.dragNdropOps.assets.formattedString.anti[document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1")].fullText;
				}
			}
		}
		
		if( ( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) || ( document.documentElement.attachEvent && ! ( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1") ) ) ) {
			
			if( !_wboolyIf_D_n_D ){
				_el_dz1.style.display = "none";
				_el_dz2.style.display = "none";
				_el_anti.style.display = "block";
				_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.dragNdropOps.assets.formattedString.anti.EN.fullText;
			}
		}
		
		// ~ Drag and Drop functionality ~ DRY
		if( !!_wboolyIf_D_n_D && !_wboolyIf_Touch ){
			
			// _el_dz1.style.display = "block";
			// _el_dz2.style.display = "block";
			// _Dd_Profile();
			 
			 if( !!_wboolyIf_D_n_D && !_wboolyIf_Touch && _chopper !== "Safari_5" ){
				_el_dz1.style.display = "block";
				_el_dz2.style.display = "block";
				_Dd_Profile();
			 }
			 
			 if( !!_wboolyIf_D_n_D && !_wboolyIf_Touch && _chopper === "Safari_5" ){
				_el_dz1.style.display = "none";
				_el_dz2.style.display = "none";
			 }
		}
		else
		if( !!_wboolyIf_D_n_D && !!_wboolyIf_Touch ){
			
			if( !!_wboolyIf_D_n_D && !!_wboolyIf_Touch && !!( _chopper === "iPad" || _chopper === "iPod" || _chopper === "iPhone" || _chopper === "Newer_Android_Device" ) ){
				_el_dz1.style.display = "none";
				_el_dz2.style.display = "none"; 
			}
			else
			if( !!_wboolyIf_D_n_D && !!_wboolyIf_Touch && !!( _chopper === "Blink_Chrome_Opera" ) ){
				_el_dz1.style.display = "block";
				_el_dz2.style.display = "block"; 
				_Dd_Profile();
			}
			else
			if( !!_wboolyIf_D_n_D && !!_wboolyIf_Touch && !( _chopper === "Blink_Chrome_Opera" ) ){
				_el_dz1.style.display = "block";
				_el_dz2.style.display = "block";
				_Dd_Profile();
			}
			
		}
		else
		if( !!_wboolyIf_D_n_D && !!_wboolyIf_Touch && ( _chopper === "iPod" || _chopper === "iPhone" ) ){ 
			_el_dz1.style.display = "none";
			_el_dz2.style.display = "none";
		}
		else
		if( !_wboolyIf_D_n_D && !!_wboolyIf_Touch ){
			_el_dz1.style.display = "none";
			_el_dz2.style.display = "none";
		}
		
		if( _actv_Page === "transition.html" ) { 
			
			for ( x = 0; x < _flipMax; x = x + 1 ) { 
				if( _flipClassDIVs[x].className === "zoomCard" ) { 
					donLuchoHAKI.Utils.evt_u.AddEventoHandler( _flipClassDIVs[x], "mousedown", _SpinZoom )
				}
			}
			
			// TRANSITION ~ three in one
			var allDIVsByTagName = _docObj.getElementsByTagName("div");
			var divsByTagMAX = allDIVsByTagName.length;
			var y; 	
			for( y = 0; y < divsByTagMAX; y = y + 1) {				
				
				if( allDIVsByTagName[y].className === "bidirect" ) {
					donLuchoHAKI.Utils.evt_u.AddEventoHandler( allDIVsByTagName[y] , "click", _ThereAndBack_thInon ); 
				}
				else 
				if( allDIVsByTagName[y].className === "turtleHead" ) {
					donLuchoHAKI.Utils.evt_u.AddEventoHandler( allDIVsByTagName[y] , "click", _ExpandRetract_thInon ); 
				}
				else 
				if( allDIVsByTagName[y].className === "ovni" ) {
					donLuchoHAKI.Utils.evt_u.AddEventoHandler( allDIVsByTagName[y] , "click", _OvniHover_thInon ); 
				} 
			} 
			
		} 
		
		if( _actv_Page === "transform.html" ) {	
			// TRANSFORM ~ law
			
			donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_lawCardOne, "mousedown", _RotateLaw ); 
			donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_lawCardOne, "click", _PrevDeffo ); 
			
			// TRANSFORM ~ two sided business card
			for ( w = 0; w < _bizMax; w = w + 1 ) { 
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _bizCardDIVs[w], "mousedown", _SpinBizCard )
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _bizCardDIVs[w], "click", _PrevDeffo ); 
			} 
			
		} 
		
		// END ~ ALL BROWSERS
		if(document.documentElement.addEventListener) {
			
			// ~ unordered list Behaviors in footer ~ RESPONSIVE dependent open close func.	
			var T;
			var U; 
			_ar_TgtULs_opnClo = _el_footrow.getElementsByTagName("ul");
			_tgtMax_opnClo = _ar_TgtULs_opnClo.length;
			_ar_TrgrAs_opnClo = _el_footrow.getElementsByTagName("a");
			_trgrMax_opnClo = _ar_TrgrAs_opnClo.length; 
			for( T = 0; T < _trgrMax_opnClo; T = T + 1 ) { // _ar_TrgrAs_opnClo[ T ]
				
				for( U = 0; U < _tgtMax_opnClo; U = U + 1 ) { // _ar_TgtULs_opnClo[ U ]
					if ( _ar_TgtULs_opnClo[ U ].className === "ulShoHid" ) { 
						_ar_TgtULs_opnClo[ U ].style.height = "0px";
					}
				}
				
				if( _ar_TrgrAs_opnClo[ T ].className === "hideoC" && _ar_TrgrAs_opnClo[ T ].nodeName.toLowerCase() === "a" ) { 
					donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_TrgrAs_opnClo[ T ] , "mousedown" , ThousandOps );
					donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_TrgrAs_opnClo[ T ] , "click" , _PrevDeffo ); 
				} // END if()
				
				if( _ar_TrgrAs_opnClo[ T ].className === "showoC" && _ar_TrgrAs_opnClo[ T ].nodeName.toLowerCase() === "a" ) { 
					donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_TrgrAs_opnClo[ T ] , "mousedown" , _SunnyOps ); 
					donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_TrgrAs_opnClo[ T ] , "click" , _PrevDeffo ); 
				} // END if()
				
			} // END for ... // _ar_TrgrAs_opnClo[ T ]
			
			// ~ next...
			
		}
		else
		if(document.documentElement.attachEvent) { 
			
		} 
		
		// alert("domconlo"); 
		// console.log( "It\'s on, bebe! ~ DCL ");
	} // END function
	
	// END private section
	return {
		Sinanju : function() {
			
			// ~ unordered list Behaviors in footer ~ RESPONSIVE dependent open close func.
			_loopTimer_opnClo = null;
			
			return _DOMCONLO();	
			
		} // FIN. Sinanju
	}; // END public section ~ return values
}) (); 

donLuchoHAKI.Utils.evt_u.AddEventoHandler( donLuchoHAKI.RetELs.glow() , "DOMContentLoaded" , donLuchoHAKI.RetELs.glow().Codified.Sinanju() ); // FIN. #1



/* ###################################################### */
var FinishTheGame = ( function () { 
	
	/*################  ES5 pragma  ######################*/
	'use strict'; 
	
	/*################  CONFIG Object  ######################*/
	var _obj_ConfigAll = {
		textOBJ : {
			a: "lang:[en\\es]" , 
			b: "You just switched to English" , 
			c: "Cambiaste su idioma a castellano" 
		} , 
		classOBJ : {
			a: "lingua", b: "EN", c: "SP", d: "linguaOps" 
		} , 
		AssetURLs : {
			toolbarOps : {
				assets : {
					refreshIco : {
						// normal ops...
						fullPath : "img/vanguard/app_toolbar/refresh-icon.png"
						// ... created, adjusted and inserted ONLY for gitlab.io gh-pages prezzo
						, fullPath_IO : "blog-2015/img/vanguard/app_toolbar/refresh-icon.png"
					} ,
					commentIco : {
						// normal ops...
						fullPath : "img/vanguard/app_toolbar/comment_icon.gif"
						// ... created, adjusted and inserted ONLY for gitlab.io gh-pages prezzo
						, fullPath_IO : "blog-2015/img/vanguard/app_toolbar/comment_icon.gif"
					} 
				}
			} , 		
			flipBookOps : {
				assets : {
					frame : {
						// normal ops...
						pathLVL0 : "../../../img/vanguard/jabez/" , 
						// ... created, adjusted and inserted ONLY for gitlab.io gh-pages prezzo
						pathLVL0_IO : "../../../blog-2015/img/vanguard/jabez/" , 
						asset : "frame.png"
					} ,
					threeD : {
						// normal ops...
						pathLVL0 : "../../../img/vanguard/jabez/" , 
						// ... created, adjusted and inserted ONLY for gitlab.io gh-pages prezzo
						pathLVL0_IO : "../../../blog-2015/img/vanguard/jabez/" , 
						asset : "3d_new.png"
					} ,
					tenPage : {
						// normal ops...
						pathLVL0 : "../../../img/vanguard/jabez/" , 
						// ... created, adjusted and inserted ONLY for gitlab.io gh-pages prezzo
						pathLVL0_IO : "../../../blog-2015/img/vanguard/jabez/" , 
						asset : "10page.png"
					} 
				}
			} , 	
			xhrOps : {
				assets : {
					content: {
						// normal ops...
						pathLVL0 : "../../zhml/idx/" , 
						// ... created, adjusted and inserted ONLY for gitlab.io gh-pages prezzo
						pathLVL0_IO : "../../blog-2015/zhml/idx/" , 
						asset : "data.xml"
					} , 
					truth: {
						// normal ops...
						pathLVL0 : "../../zhml/idx/" , 
						// ... created, adjusted and inserted ONLY for gitlab.io gh-pages prezzo
						pathLVL0_IO : "../../blog-2015/zhml/idx/" , 
						asset : "citationEnhancer.xml"
					}
				}
			}	
		}  , 
		CannedStrings : {
			linguaOps : {
				assets : { 		
					nonFormatString : { 
						adjustUrStng : { 
							intro : "Adjust device settings so it can permit cookies and so you can navigate properly"
						} , 
						deffoLingo : { 
							intro : "You can set up a default language in seconds!"
						} 
					}			
				} 
			} , 	
			genericPhrasesOps : {
				assets : { 		
					formattedString : { 
						whatIs_and : { 
							EN : {
								fullText : " <strong>and</strong> "
							} , 
							SP : {
								fullText : " <strong>y</strong> "
							}
						} 
					}
				} 
			} , 	
			geoloOps : {
				assets : { 		
					nonFormatString : { 
						gotcha : { 
							EN : {
								fullText : "Gotcha"
							} , 
							SP : {
								fullText : "A os tengo"
							} 
						} , 
						choke : { 
							EN : {
								fullText : "Choke!"
							} , 
							SP : {
								fullText : "\u00A1Se esfum\u00F3!"
							} 
						} 
					} , 			
					formattedString : { 
						pro : { 
							EN : {
								fullText : "<p><strong>Geolocation</strong> is <strong>supported</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>Geolocation</strong> s\u00ED <strong>le funcionar\u00E1</strong></p>"
							}
						} , 
						antiAlt : { 
							EN : {
								fullText : "<p>It seems that <strong>Geolocation</strong>-- among other things\u2026 --is <strong>not supported</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>Geolocation</strong>-- entre otras cosas\u2026 -- n\u00F3 <strong>le funcionar\u00E1</strong></p>"
							}
						} , 
						anti : { 
							EN : {
								fullText : "<p>It seems that <strong>Geolocation</strong> is <strong>not supported</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>Geolocation</strong> n\u00F3 <strong>le funcionar\u00E1</strong></p>"
							}
						} 
					} 			
				} 
			} , 	
			pageVisibilityOps : {
				assets : { 			
					formattedString : { 
						pro : { 
							EN : {
								fullText : "<p><strong>page visibility</strong> is <strong>supported.</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>page visibility</strong> s\u00ED <strong>le funcionar\u00E1</strong></p>"
							}
						} , 
						anti : { 
							EN : {
								fullText : "<p>It seems that <strong>page visibility</strong> is <strong>not supported</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>page visibility</strong> n\u00F3 <strong>le funcionar\u00E1</strong></p>"
							}
						} 
					} 			
				} 
			} , 	
			fullScreenOps : {
				assets : { 		
					formattedString : { 
						pro : { 
							EN : {
								fullText : "<strong>Fullscreen</strong> is <strong>supported: </strong>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>Fullscreen</strong> s\u00ED <strong>le funcionar\u00E1</strong></p>"
							}
						} , 
						anti : { 
							EN : {
								fullText : "<p>It seems that <strong>Fullscreen</strong> is <strong>not supported</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>Fullscreen</strong> n\u00F3 <strong>le funcionar\u00E1</strong></p>"
							}
						} , 
						phrase : { 
							EN : {
								fullText : "<strong>Fullscreen properties include: </strong>"
							} , 
							SP : {
								fullText : "<strong>Propiedades llevados por el interf\u00E1z de Fullscreen incluyen: </strong>"
							}
						} , 
						wcfse : {
							EN : {
								fullText : "<p><strong>Fullscreen properties include:</strong> webkitCurrentFullScreenElement</p>"
							} , 
							SP : {
								fullText : "<p><strong>Propiedades llevados por el interf\u00E1z de Fullscreen incluyen: </strong> webkitCurrentFullScreenElement</p>"
							}
						} , 
						WIFs : {
							EN : {
								fullText : "<p><strong>Fullscreen properties include:</strong> webkitIsFullScreen</p>"
							} , 
							SP : {
								fullText : "<p><strong>Propiedades llevados por el interf\u00E1z de Fullscreen incluyen: </strong> webkitIsFullScreen</p>"
							}
						} , 
						WDF : {
							EN : {
								fullText : "<p><strong>Fullscreen properties include:</strong> webkitDisplayingFullscreen</p>"
							} , 
							SP : {
								fullText : "<p><strong>Propiedades llevados por el interf\u00E1z de Fullscreen incluyen: </strong> webkitDisplayingFullscreen</p>"
							}
						} 
					} 			
				} 
			} , 	
			dateObjOps : {
				assets : { 
					nonFormatString : { 
						dtObjPrezzo : { 
							EN : {
								fullText : "Made with pride in Costa Rica "
							} , 
							SP : {
								fullText : "Hecho con orgullo en la Rep\u00FAblica de Costa Rica "
							} 
						} 
					} 
				} 
			} , 	
			localStorageOps : {
				assets : { 		
					formattedString : { 
						pro : { 
							EN : {
								fullText : "<p><strong>web storage</strong> is <strong>supported.</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>web storage</strong> s\u00ED <strong> le funcionar\u00E1</strong></p>"
							}
						} , 
						anti : { 
							EN : {
								fullText : "<p>It seems that <strong>web storage</strong> is <strong>not supported</strong></p>"
							} , 
							SP : {
								fullText : "<p>Parece que <strong>web storage</strong> n\u00F3 <strong> le funcionar\u00E1</strong></p>"
							}
						} 
					} 			
				} 
			} , 	
			sessionStorageOps : {
				assets : { 		
					nonFormatString : { 
						sessionTitle : {
							altTitle : { 
								EN : {
									fullText : "Don\u2019t forget\u2013 you are here"
								} , 
								SP : {
									fullText : "Aqu\u00ED se encontr\u00F3 antes"
								} 
							} 
						} 
					} 
				} 
			} , 	
			toolbarOps : {
				assets : { 		
					nonFormatString : { 
						refreshher : { 
							EN : {
								fullText : "Refresh Post"
							} , 
							SP : {
								fullText : "Refrescar al Post"
							} 
						} , 
						skipher : { 
							EN : {
								fullText : "Skip to Aside"
							} , 
							SP : {
								fullText : "Vayase hac\u00EDa el Aside"
							} 
						} 
					} 
				} 
			} , 	
			ajaxOps : {
				assets : {
					nonFormatString : { 
						lostConn : { 
							EN : {
								fullText : "You probably have lost your connection. Try later."
							} , 
							SP : {
								fullText : "Seguramente has perdido su se\u00F1al. Intentalo de nuevo."
							} 
						} , 
						upgradeUrApp : { 
							EN : {
								fullText : "Upgrade your app please. Are you ready now? \nThis takes less than one second to complete."
							} , 
							SP : {
								fullText : "Actualiza a su aplicaci\u00F3n por favor. \u00BFEst\u00E1s listo? \n\u00C9ste proceso requiere menos de un segundo para terminar de viaje."
							} 
						} , 
						dOAajaxReq : { 
							intro : "Sorry, but I couldn't create an XMLHttpRequest" , 
							EN : {
								fullText : "Sorry, but I couldn't create an XMLHttpRequest"
							} , 
							SP : {
								fullText : "Disculpa, aun n\u00F3 se cumpli\u00F3 la llamada-XMLHttpRequest"
							} 
						} , 
						problems : { 
							intro : "Uh-oh ~ problems! Request Status: " , 
							EN : {
								fullText : "Uh-oh ~ problems! Request Status: "
							} , 
							SP : {
								fullText : "Bu ~ \u00A1problemas! Su Pr\u00F3gnosis: "
							} 
						} 				
					}			
				} 
			} , 	
			deviceorientationOps : {
				assets : { 
					nonFormatString : { 
						device : { 
							EN : {
								fullText : "Device Orientation capabilities in this browser do not include acceleration with gravity."
							} , 
							SP : {
								fullText : "Las cap\u00E1cidades de \u00E9ste navegador no son suficientes para conducir experimentos cuales suelen de incluir manera de medir la gravedad terreno."
							} 
						} , 
						totalChoke : { 
							EN : {
								fullText : "not an object expression"
							} , 
							SP : {
								fullText : "no existe una expresi\u00F3n de objeto"
							} 
						} 
					} , 
					formattedString : { 
						noFullSupport : { 
							EN : {
								fullText : "<p>Not fully supported</p>" 
							} , 
							SP : {
								fullText : "<p>No se apoye de manera suficiente</p>" 
							}
						} 
					} 
				} 
			} 
			
		}
		
	};
	/*################  private variables  ######################*/
	// ~ Universal functionality 
	var _docObj = window.document;
	var _chopper = donLuchoHAKI.Utils.SnifferREGEX();
	var _chopperPoint = donLuchoHAKI.Utils.SnifferOS();
	
	// ~ _RAF\_CAF functionality 
	var _mrBeepersMS;
	var _RAF;
	var _CAF;
	
	// ~ DATE OBJECT(YEAR) functionality 
	var _dtObj = new Date();
	
	// ~ Universal AND Fullscreen AND SELECTED LI functionality 
	var _docEL = donLuchoHAKI.RetELs.htmlEL();
	
	// ~ Online Offline functionality
	var _navObj = donLuchoHAKI.RetELs.navEL();
	
	// TOUCH ~ untangle.html
	// ~ all Touch functionality
	var _wboolyIf_Touch = donLuchoHAKI.DeviceCapabilities.html5_If_Touch();
	
	// Fullscreen AND SELECTED LI AND _obj_ConfigAll AND universal functionalities
	var _str_host = window.location.host; // ~ "donluchoweb.com"
	
	var _str_LocalHo = "donluchoweb.com";
	var _str_LocalHo_test = "192.168.0.201"; // ~ "192.168.0.201" // ~ "127.1.2.8:5150"
	
	var _str_href = window.location.href; 
	// console.log( "_str_href" , _str_href ); // ~ http://donluchoweb.com/mmm/yummy/cherry.html
	
	var _str_url = window.location.pathname; 
	// console.log( "_str_url" , _str_url ); // ~ /mmm/yummy/cherry.html
	
	var _str_Urlstring = ""; // PUBLIC SERVER ~ // var _str_Urlstring = "index.html"; // AT home
	
	var _actv_Page = _str_url.substring( _str_url.lastIndexOf('/') + 1 ); 
	// console.log( "_actv_Page" , _actv_Page ); // ~ cherry.html
	
	var CNFG_SVR_IDX = _str_Urlstring;
	
	// WINDOW RESIZE METRICS functionality ~ TESTING PURPOSES
	var _ViewportWidth = "_ViewportWidth: " + donLuchoHAKI.PixelsArePixelsArePixels.glowWidth() + " | ";
	var _ViewportHeight = "_ViewportHeight: " + donLuchoHAKI.PixelsArePixelsArePixels.glowHeight() + " | ";
	var _DeviceWidth = "_DeviceWidth: " + donLuchoHAKI.PixelsArePixelsArePixels.screenWidth() + " | ";
	var _DeviceHeight = "_DeviceHeight: " + donLuchoHAKI.PixelsArePixelsArePixels.screenHeight() + " | ";
	var _PageWidth = "_PageWidth: " + donLuchoHAKI.PixelsArePixelsArePixels.bodWidth() +" ";
	var _PageHeight = "_PageHeight: " + donLuchoHAKI.PixelsArePixelsArePixels.bodHeight() +" ";
	
	// WINDOW RESIZE METRICS functionality ~ TESTING PURPOSES
	var _el_logDetails = _docObj.getElementById("logDetails");
	
	// ~ ELEVATOR LINK functionality ~ upTown\downTown
	var _scrollY_elev; 
	var _loopTimer_elev; 
	var _tgtELId_elev = null;
	var _tgtEl_elev = null;
	var _el_silky = _docObj.getElementById( "silky" );
	// ~ ELEVATOR LINK functionality ~ upTown\downTown
	var _E;
	// ~ ELEVATOR LINK functionality ~ upTown\downTown ~ // LANGUAGE SWITCHER FUNC.	
	var _all_anchors = _docObj.getElementsByTagName("a");
	
	// ~ DATE OBJECT(YEAR) functionality 
	var _el_placedLow = _docObj.getElementById( "placedLow" );
	
	// ~ Online Offline functionality
	var _el_colorBlock = _docObj.getElementById("colorBlock");
	var _el_toggleState = _docObj.getElementById("toggleState");
	
	// ~ Carousel functionality ~ image gallery
	var _el_caroLink = _docObj.getElementById( "caroLink" ); 
	
	// ~ Universal AND Carousel functionality ~ image gallery -- part i ~ undry
	var _el_pro  = _docObj.getElementById( "pro" ); 
	var _el_anti = _docObj.getElementById( "anti" ); 
	
	// ~ SELECTED LI functionality ~ 'custom_select'
	var _el_uniformNavigation = _docObj.getElementById("uniformNavigation"); 
	// ~ SELECTED LI functionality ~ 'custom_select'
	var ar_slctdCls_LIs = _el_uniformNavigation.getElementsByTagName("li");
	var ar_slctdClsLIsMAX = ar_slctdCls_LIs.length;
	var S;
	// ~ SELECTED LI functionality ~ 'custom_select'
	var ar_slctdCls_childAs = _el_uniformNavigation.getElementsByTagName("a");
	var ar_slctdClsAsMAX = ar_slctdCls_childAs.length;
	var H;
	
	// ~ Geolocation functionality
	var _wboolyIf_Geo;
	var _el_geolo = _docObj.getElementById( "geolo" );
	
	// ~ CANVAS functionality
	var _el_cnvMemb = _docObj.getElementById("cnvMemb");
	var _el_cnv = _docObj.getElementById("cnv");
	var _radiusSupported = false;
	var _nextCount = 0;
	var _obj_touchMap = {};
	var _pointMode = false;
	var _enableForce = false;
	var _mousePressed = false;
	var _scale; 
	var _startCircle = 0;
	var _fullCircle = 2 * Math.PI;
	
	// ~ PageFlip Functionality
	var _el_coverwrapper = _docObj.getElementById("coverwrapper");
	var _coverWidth;
	var _nm_BGcoverWidth = 306;
	var _nm_negator = -1;
	var _cvrWrprLeftqty; 
	// ~ PageFlip Functionality
	var _el_bookspine; // orig
	var _spineWidth;
	var _spineLeft;	
	// ~ PageFlip Functionality
	var _el_leftpage; // orig
	var _el_rightpage; // orig	
	// ~ PageFlip Functionality
	var _leftPageWidth;
	var _rightPageWidth;
	var _rightPageLeftX; 
	// ~ PageFlip Functionality
	var _leftConTop = 0; // 0 189 378 567 756 945
	var _leftConLeft; // = 5
	// ~ PageFlip Functionality
	var _rightConTop = 0; // 0 189 378 567 756 945
	var _rightConLeft; // = 179
	// ~ PageFlip Functionality
	var _nm_Height_JbzPg = 189; /* a single page height */ // orig
	var _nm_rowCt = 5;
	var _nm_Width_JbzPg = 146; /* a single page width */ // orig // ON ITS WAY OUT
	var _nm_bgYPOS_JbzPg = 0; /* current Y position of our bg-image (in both pages) */ // orig
	var _nm_el_bgMAX = _nm_Height_JbzPg * ( _nm_rowCt + 1 ); // 1134 // orig
	// ~ PageFlip Functionality
	var _evtTrgt_pflip; // orig
	var _loopTimer_pFlip; // orig
	
	// LANGUAGE SWITCHER FUNC.
	var _anchoMax = _all_anchors.length; 
	var r; 	
	var _allLIs = _docObj.getElementsByTagName("li");
	var _LIMax = _allLIs.length;
	var _oneShort = _LIMax - 1;
	// LANGUAGE SWITCHER FUNC.
	var _SwtchRooVLU;
	var _textOBJ = _obj_ConfigAll.textOBJ || {};
	var _classOBJ = _obj_ConfigAll.classOBJ || {};
	// LANGUAGE SWITCHER FUNC.
	var _trgrAncho_Lang;
	var _trgrLangClassAs;
	var _trgrLangMax;
	var q;
	
	// WEB STORAGE || COOKIE FUNC.
	var COOKIE_DEATH = -1;
	var COOKIE_LIFE = 365;	
	var DEFAULTLANGUAGE = "defaultLanguage";
	var _obj_locsto_lang;
	
	// INSPIRATION ~ XHR ~ COMBO ~ AC ~ "excluding "iOS4" ~ FIRST WAVE 
	var _topGLO = donLuchoHAKI.RetELs.glow();
	var _AC;
	var _bodyObj = donLuchoHAKI.RetELs.bodyEL();
	// INSPIRATION ~ XHR ~ COMBO ~ AC ~ "excluding "iOS4" ~ FIRST WAVE 
	var _el_loadOps = _docObj.getElementById("loadOps");
	var _el_dwnldAC = _docObj.getElementById("dwnldAC");
	// INSPIRATION ~ XHR ~ COMBO ~ AC ~ "excluding "iOS4" ~ FIRST WAVE 
	var _stringCheese;
	
	// DYNAMICCONTENT ~ XHR ~ Translatable CONTENT
	var _el_contento = _docObj.getElementById( "contento" );
	
	// DYNAMICCONTENT ~ XHR ~ Translatable CONTENT
	if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "enhance.html") || (_actv_Page === "transform.html") || (_actv_Page === "transition.html") || (_actv_Page === "device.html") || (_actv_Page === "interaction.html") || (_actv_Page === "untangle.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") || (_actv_Page === "sitemap.html") || (_actv_Page === "inspiration.html") ){
		
		if( (_actv_Page !== "sitemap.html") ){
			var _el_pubdate = _docObj.getElementById("pubdate");
		}
		if( (_actv_Page === "device.html") || (_actv_Page === "enhance.html") ){
			var _el_secHredCaps = _docObj.getElementById("secHredCaps");
		}
		if( (_actv_Page === "next.html") ){
			var _el_img1 = _docObj.getElementById("img1");
			var _el_img2 = _docObj.getElementById("img2");
			var _el_img3 = _docObj.getElementById("img3");
			var _el_img4 = _docObj.getElementById("img4");
			var _el_img5 = _docObj.getElementById("img5");
			var _el_img6 = _docObj.getElementById("img6");
			var _el_img7 = _docObj.getElementById("img7");
			var _el_img8 = _docObj.getElementById("img8");
		}
		if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "interaction.html") || (_actv_Page === "untangle.html") || (_actv_Page === "contact.html") || (_actv_Page === "inspiration.html") ){
			var _el_ctaImg = _docObj.getElementById("ctaImg");
		}
		if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "interaction.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") || (_actv_Page === "inspiration.html") ){
			var _el_secHthree = _docObj.getElementById("secHthree");
		}
		if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "enhance.html") || (_actv_Page === "interaction.html") || (_actv_Page === "untangle.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") ){
			var _el_secPmA = _docObj.getElementById("secPmA");
		}
		if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "enhance.html") || (_actv_Page === "interaction.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") ){
			var _el_secPsA = _docObj.getElementById("secPsA");
		}
		if( (_actv_Page !== "sitemap.html") || (_actv_Page !== "inspiration.html") ){
			var _el_secPdryZ = _docObj.getElementById("secPdryZ");
		}
		if( (_actv_Page !== "enhance.html") || (_actv_Page !== "device.html") || (_actv_Page !== "sitemap.html") ){
			var _el_asideA = _docObj.getElementById("asideA");
			var _el_asideB = _docObj.getElementById("asideB");
			var _el_asideC = _docObj.getElementById("asideC");
			var _el_asideimgA = _docObj.getElementById("asideimgA");
			var _el_asideD = _docObj.getElementById("asideD");
			var _el_asideE = _docObj.getElementById("asideE");
			var _el_asideF = _docObj.getElementById("asideF");
			var _el_asideimgB = _docObj.getElementById("asideimgB");
			var _el_asideG = _docObj.getElementById("asideG");
			var _el_asideH = _docObj.getElementById("asideH");
			var _el_asideI = _docObj.getElementById("asideI");
			var _el_asideimgC = _docObj.getElementById("asideimgC");
		}
	}
	
	// DYNAMICCONTENT ~ XHR ~ Translatable CONTENT
	if( (_actv_Page === "events.html") || (_actv_Page === "references.html") || (_actv_Page === "products.html") || (_actv_Page === "how.html") || (_actv_Page === "solutions.html") || (_actv_Page === "activities.html") || (_actv_Page === "community.html") || (_actv_Page === "downloads.html") || (_actv_Page === "about.html") || (_actv_Page === "services.html") ){
		
		var _el_pubdate = _docObj.getElementById("pubdate");
		var _el_figcaption = _docObj.getElementById("figcaption");
		var _el_captionImg = _docObj.getElementById("captionImg");
		var _el_secHthree = _docObj.getElementById("secHthree");
		var _el_secPmA = _docObj.getElementById("secPmA");
		var _el_secPmlrB = _docObj.getElementById("secPmlrB");
		var _el_secPmlrC = _docObj.getElementById("secPmlrC");
		if( (_actv_Page === "activities.html") || (_actv_Page === "community.html") || (_actv_Page === "downloads.html") || (_actv_Page === "about.html") || (_actv_Page === "services.html") ){ 
			var _el_secPmD = _docObj.getElementById("secPmD");
		}
		var _el_secPdryZ = _docObj.getElementById("secPdryZ");
		var _el_asideA = _docObj.getElementById("asideA");
		var _el_asideB = _docObj.getElementById("asideB");
		var _el_asideC = _docObj.getElementById("asideC");
		var _el_asideimgA = _docObj.getElementById("asideimgA");
		var _el_asideD = _docObj.getElementById("asideD");
		var _el_asideE = _docObj.getElementById("asideE");
		var _el_asideF = _docObj.getElementById("asideF");
		var _el_asideimgB = _docObj.getElementById("asideimgB");
		var _el_asideG = _docObj.getElementById("asideG");
		var _el_asideH = _docObj.getElementById("asideH");
		var _el_asideI = _docObj.getElementById("asideI");
		var _el_asideimgC = _docObj.getElementById("asideimgC");
		
	}
	// DYNAMICCONTENT ~ XHR ~ Translatable CONTENT
	var _el_likeA = _docObj.getElementById("likeA");
	var _el_likeB = _docObj.getElementById("likeB");
	var _el_likeC = _docObj.getElementById("likeC");
	var _el_workinProgress = _docObj.getElementById( "workinProgress" );
	
	// NONPAREILCONTENT ~ XHR ~ Translatable ~ device.html
	if( _actv_Page === "device.html" ){
		var _el_dvcPa = _docObj.getElementById( "dvcPa" );
		var _el_dvcPb = _docObj.getElementById( "dvcPb" );
		var _el_dvcPc = _docObj.getElementById( "dvcPc" );
		var _el_dvcPd = _docObj.getElementById( "dvcPd" );
		var _el_dvchxA = _docObj.getElementById( "dvchxA" );
		var _el_dvchxB = _docObj.getElementById( "dvchxB" );
		var _el_dvchxC = _docObj.getElementById( "dvchxC" );
		var _el_dvchxD = _docObj.getElementById( "dvchxD" );
		var _el_dvc_panoramic = _docObj.getElementById( "dvc_panoramic" );
		var _el_dvc_portrait = _docObj.getElementById( "dvc_portrait" );
		var _el_dvchxE = _docObj.getElementById( "dvchxE" );
		var _el_dvchxF = _docObj.getElementById( "dvchxF" );
		var _el_dvc_two_one_half = _docObj.getElementById( "dvc_two_one_half" );
		var _el_dvchxG = _docObj.getElementById( "dvchxG" );
		var _el_dvchxH = _docObj.getElementById( "dvchxH" );
		var _el_dvchxI = _docObj.getElementById( "dvchxI" );
		var _el_dvchxJ = _docObj.getElementById( "dvchxJ" );
		var _el_dvchxK = _docObj.getElementById( "dvchxK" );
		var _el_dvchxL = _docObj.getElementById( "dvchxL" );
		var _el_dvcM_h2 = _docObj.getElementById( "dvcM_h2" );
		var _el_dvcM_h3 = _docObj.getElementById( "dvcM_h3" );
		var _el_dvcM_readOut = _docObj.getElementById( "dvcM_readOut" );
		var _el_dvcO_h2 = _docObj.getElementById( "dvcO_h2" );
		var _el_dvcO_h3 = _docObj.getElementById( "dvcO_h3" );
		var _el_dvcO_readOut = _docObj.getElementById( "dvcO_readOut" );
	}
	// NONPAREILCONTENT ~ XHR ~ Translatable ~ untangle.html
	if( ( _actv_Page === "untangle.html" ) ){
		var _el_LO = _docObj.getElementById( "LO" );
		var _el_RG = _docObj.getElementById( "RG" );
		var _el_TM = _docObj.getElementById( "TM" );
		var _el_BP = _docObj.getElementById( "BP" );
	}
	// NONPAREILCONTENT ~ XHR ~ Translatable ~ transform.html
	if( ( _actv_Page === "transform.html" ) ){
		var _el_fbZhxA = _docObj.getElementById( "fbZhxA" );
		var _el_fbZhxB = _docObj.getElementById( "fbZhxB" );
		var _el_fbZp1 = _docObj.getElementById( "fbZp1" );
		var _el_fbZp2 = _docObj.getElementById( "fbZp2" );
		var _el_bbZli1 = _docObj.getElementById( "bbZli1" );
		var _el_bbZli2 = _docObj.getElementById( "bbZli2" );
		var _el_bbZli3 = _docObj.getElementById( "bbZli3" );
		var _el_bbZli4 = _docObj.getElementById( "bbZli4" );
		var _el_fLp1 = _docObj.getElementById( "fLp1" );
		var _el_fLp2 = _docObj.getElementById( "fLp2" );
		var _el_bLp3 = _docObj.getElementById( "bLp3" );
		var _el_bLp4 = _docObj.getElementById( "bLp4" );
		var _el_bLp5 = _docObj.getElementById( "bLp5" );
	}
	// NONPAREILCONTENT ~ XHR ~ Translatable ~ transition.html
	if( ( _actv_Page === "transition.html" ) ){
		var _el_tranHxA = _docObj.getElementById( "tranHxA" );
		var _el_tranLR = _docObj.getElementById( "tranLR" );
		var _el_tranHxB = _docObj.getElementById( "tranHxB" );
		var _el_tranPa = _docObj.getElementById( "tranPa" );
		var _el_tranHxC = _docObj.getElementById( "tranHxC" );
		var _el_tranOvni = _docObj.getElementById( "tranOvni" );
	}
	
	// NONPAREILCONTENT ~ XHR ~ Translatable ~ enhance.html
	if( _actv_Page === "enhance.html" ){
		var _el_lgdFSrox = _docObj.getElementById( "lgdFSrox" );
		var _el_lblFSrox = _docObj.getElementById( "lblFSrox" );
		var _el_lgdFSroy = _docObj.getElementById( "lgdFSroy" );
		var _el_lblFSroy = _docObj.getElementById( "lblFSroy" );
		var _el_lgdFSopnclo = _docObj.getElementById( "lgdFSopnclo" );
	}
	
	// STATICCONTENT ~ XHR
	var _el_sectionContent_dL = _docObj.getElementById("sectionContent_dL");
	var _el_asideContent_dL = _docObj.getElementById("asideContent_dL");
	var _el_colRight_dL = _docObj.getElementById("colRight_dL");
	var _el_footer_dL = _docObj.getElementById("footer_dL");
	// STATICCONTENT ~ XHR
	var _el_courtesyGreeting = _docObj.getElementById("courtesyGreeting");
	var _el_bCapable = _docObj.getElementById("bCapable");
	var _el_bIncapable = _docObj.getElementById("bIncapable");
	var _el_closeone = _docObj.getElementById("closeone");
	// STATICCONTENT ~ XHR
	var _el_silky_dL1 = _docObj.getElementById("silky_dL1");
	var _el_silky_dL2 = _docObj.getElementById("silky_dL2");
	var _el_silky_dL3 = _docObj.getElementById("silky_dL3");
	var _el_silky_dL4 = _docObj.getElementById("silky_dL4");	
	// STATICCONTENT ~ XHR
	var _el_dz1A = _docObj.getElementById("dz1A");
	var _el_dz1B= _docObj.getElementById("dz1B");
	var _el_dz1C= _docObj.getElementById("dz1C");
	var _el_dz1D = _docObj.getElementById("dz1D");
	// STATICCONTENT ~ XHR
	var _el_dz2A = _docObj.getElementById("dz2A");
	var _el_dz2B = _docObj.getElementById("dz2B");
	// STATICCONTENT ~ XHR
	var _el_jbzA = _docObj.getElementById("jbzA");
	var _el_jbzB = _docObj.getElementById("jbzB");
	var _el_jbzC = _docObj.getElementById("jbzC");
	var _el_jbzD = _docObj.getElementById("jbzD");
	// STATICCONTENT ~ XHR
	var _el_nav_home = _docObj.getElementById("nav_home");
	var _el_nav_inspire = _docObj.getElementById("nav_inspire");
	var _el_nav_enhance = _docObj.getElementById("nav_enhance");
	var _el_nav_transform = _docObj.getElementById("nav_transform");
	var _el_nav_transition = _docObj.getElementById("nav_transition");
	var _el_nav_next = _docObj.getElementById("nav_next");
	var _el_nav_device = _docObj.getElementById("nav_device");
	var _el_nav_untang = _docObj.getElementById("nav_untang");
	var _el_nav_interact = _docObj.getElementById("nav_interact");
	var _el_nav_activities = _docObj.getElementById("nav_activities");
	var _el_nav_events = _docObj.getElementById("nav_events");
	var _el_nav_community = _docObj.getElementById("nav_community");
	var _el_nav_downloads = _docObj.getElementById("nav_downloads");
	var _el_nav_references = _docObj.getElementById("nav_references");
	var _el_nav_about = _docObj.getElementById("nav_about");
	var _el_nav_products = _docObj.getElementById("nav_products");
	var _el_nav_services = _docObj.getElementById("nav_services");
	var _el_nav_how = _docObj.getElementById("nav_how");
	var _el_nav_solutions = _docObj.getElementById("nav_solutions");
	var _el_nav_contact = _docObj.getElementById("nav_contact");
	var _el_nav_sitemap = _docObj.getElementById("nav_sitemap");	
	// STATICCONTENT ~ XHR
	var _el_cnvA = _docObj.getElementById("cnvA");
	var _el_cnvB = _docObj.getElementById("cnvB");
	var _el_cnvC = _docObj.getElementById("cnvC");
	var _el_cnvD = _docObj.getElementById("cnvD");	
	// STATICCONTENT ~ XHR
	var _el_gloA = _docObj.getElementById("gloA");
	var _el_gloB = _docObj.getElementById("gloB");
	var _el_gloC = _docObj.getElementById("gloC");
	var _el_gloD = _docObj.getElementById("gloD");
	var _el_gloE = _docObj.getElementById("gloE");
	// STATICCONTENT ~ XHR
	var _el_fdA = _docObj.getElementById("fdA");
	var _el_fdB = _docObj.getElementById("fdB");
	var _el_fdC = _docObj.getElementById("fdC");
	var _el_fdD = _docObj.getElementById("fdD");
	var _el_Brooke_fxDrg = _docObj.getElementById("Brooke_fxDrg");
	var _el_Franky_fxDrg = _docObj.getElementById("Franky_fxDrg");
	var _el_Robin_fxDrg = _docObj.getElementById("Robin_fxDrg");
	var _el_Sanji_fxDrg = _docObj.getElementById("Sanji_fxDrg");
	var _el_Chopper_fxDrg = _docObj.getElementById("Chopper_fxDrg");
	var _el_Usopp_fxDrg = _docObj.getElementById("Usopp_fxDrg");
	var _el_Zoro_fxDrg = _docObj.getElementById("Zoro_fxDrg");
	var _el_Nami_fxDrg = _docObj.getElementById("Nami_fxDrg");
	var _el_Luffy_fxDrg = _docObj.getElementById("Luffy_fxDrg");
	// STATICCONTENT ~ XHR
	var _str_pita_1 = "#" + "retexpUL1A";
	var _el_retexpUL1A = _docObj.getElementById(_str_pita_1);
	var _str_pita_2 = "#" + "retexpUL1B";
	var _el_retexpUL1B = _docObj.getElementById(_str_pita_2);
	var _str_pita_3 = "#" + "retexpUL2A";
	var _el_retexpUL2A = _docObj.getElementById(_str_pita_3);
	var _str_pita_4 = "#" + "retexpUL2B";
	var _el_retexpUL2B = _docObj.getElementById(_str_pita_4);
	var _str_pita_5 = "#" + "retexpUL3A";
	var _el_retexpUL3A = _docObj.getElementById(_str_pita_5);
	var _str_pita_6 = "#" + "retexpUL3B";
	var _el_retexpUL3B = _docObj.getElementById(_str_pita_6);
	var _str_pita_7 = "#" + "retexpUL4A";
	var _el_retexpUL4A = _docObj.getElementById(_str_pita_7);
	var _str_pita_8 = "#" + "retexpUL4B";
	var _el_retexpUL4B = _docObj.getElementById(_str_pita_8);
	// STATICCONTENT ~ XHR
	var _el_fha = _docObj.getElementById("fha");
	var _el_fhb = _docObj.getElementById("fhb");
	var _el_fhc = _docObj.getElementById("fhc");
	var _el_fhd = _docObj.getElementById("fhd");
	var _el_retexpHotelFour4 = _docObj.getElementById("retexpHotelFour4");
	var _el_partners = _docObj.getElementById("partners");
	// STATICCONTENT ~ XHR
	var _el_foot_home = _docObj.getElementById("foot_home");
	var _el_foot_inspire = _docObj.getElementById("foot_inspire");
	var _el_foot_enhance = _docObj.getElementById("foot_enhance");
	var _el_foot_device = _docObj.getElementById("foot_device");
	var _el_foot_untang = _docObj.getElementById("foot_untang");
	var _el_foot_interact = _docObj.getElementById("foot_interact");
	var _el_foot_transform = _docObj.getElementById("foot_transform");
	var _el_foot_transition = _docObj.getElementById("foot_transition");
	var _el_foot_activities = _docObj.getElementById("foot_activities");
	var _el_foot_events = _docObj.getElementById("foot_events");
	var _el_foot_community = _docObj.getElementById("foot_community");
	var _el_foot_contact = _docObj.getElementById("foot_contact");
	var _el_foot_downloads = _docObj.getElementById("foot_downloads");
	var _el_foot_references = _docObj.getElementById("foot_references");
	var _el_foot_how = _docObj.getElementById("foot_how");
	var _el_foot_about = _docObj.getElementById("foot_about");
	var _el_foot_products = _docObj.getElementById("foot_products");
	var _el_foot_services = _docObj.getElementById("foot_services");
	var _el_foot_solutions = _docObj.getElementById("foot_solutions");
	var _el_foot_sitemap = _docObj.getElementById("foot_sitemap");
	
	
	// INSPIRATION ~ XHR ~ COMBO ~ AC ~ "excluding "iOS4" ~ FIRST WAVE 
	var _ar_RIGHTEOUSCONTENT = []; // INSPIRATION ~ XHR
	var _ar_STATICCONTENT = []; // STATICCONTENT ~ XHR
	var _ar_DYNAMICCONTENT = []; // DYNAMICCONTENT ~ XHR
	var _ar_NONPAREILCONTENT = []; // NONPAREILCONTENT ~ XHR
	var _url_content;
	var _xhr_Content = false; 
	var _actvX_albtrs_Content = false;	
	
	if( _actv_Page === "inspiration.html" ) {
		// INSPIRATION ~ XHR ~ COMBO ~ AC ~ "excluding "iOS4" ~ FIRST WAVE 
		var _ar_TRUTH = [];	
		var _url_truth = "../../zhml/idx/citationEnhancer.xml"; 
		var _xhr_truth = false; 
		var _actvX_albtrs_truth = false;
		var _el_righteousWords = _docObj.getElementById("righteousWords");
	}
	
	
	// RESIZE OPS ~ TRANSFORM ~ two sided business card 
	if( _actv_Page === "transform.html" ) {
		var _el_bizCardOne = _docObj.getElementById( "bizCardOne" );
		var _el_bizWrapper = _docObj.getElementById( "bizWrapper" );
		var _el_frontBiz = _docObj.getElementById( "frontBiz" );
		var _el_backBiz = _docObj.getElementById( "backBiz" );
		
		// TRANSFORM ~ law
		var _el_lawWrapper = _docObj.getElementById( "lawWrapper" ); 
		var _el_lawCardOne = _docObj.getElementById( "lawCardOne" );
		var _el_frontLaw = _docObj.getElementById( "frontLaw" );		
		var _el_backLaw = _docObj.getElementById( "backLaw" ); 
		
		// TRANSFORM ~ law
		var _GrafsLaw = _el_lawCardOne.getElementsByTagName( "p" );
		var _pGmax_law = _GrafsLaw.length;
		var p;
		
	}
	
	// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html
	if( _actv_Page === "device.html" ) {
		var _el_dvc_garden = _docObj.getElementById( "dvc_garden" );
		var _gardenRect = _el_dvc_garden.getBoundingClientRect();
		var _gardenX = parseInt( _gardenRect.left, 10); 
		var _gardenY = parseInt( _gardenRect.top, 10); 
		var _endgardenX = parseInt( _gardenRect.right, 10);
		var _endgardenY = parseInt( _gardenRect.bottom, 10);
		var _gardenWidth = _endgardenX - _gardenX; 
		var _gardenHeight = _endgardenY - _gardenY;
	} // END if (_actv_Page) control statement
	
	// ~ Toolbar functionality ~ Only for MozillaFF at the moment
	var _str_host = window.location.host; // ~ 127.1.2.8:5150
	var _regurgPath = _str_proto + _str_dblBar + _str_host + _str_url;
	var _ar_urlPath = _str_url.split( '/' ); 
	// ~ Toolbar functionality ~ Only for MozillaFF at the moment
	var _str_proto = window.location.protocol; // ~ http:
	var _str_dblBar = "//";
	var _str_sglBar = "/";
	var _str_Pullback = "../";
	// var _nm_lenThresh = 2;
	
	if( (_str_host === 'donlucho.github.io') || ( _str_host === 'artavia.gitlab.io') ){	
		var _nm_lenThresh = 3;
	}
	else {
		var _nm_lenThresh = 2;
	}
	
	// EXTRA ~ Axis functionality ~ just x\y poles... no Z //if( document.documentElement.addEventListener && linkhref_Page === "enhance.html" ){} 
	// JS ~ pt i
	var perspective = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "perspective" ); 
	var transformStyle = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transform-style" ); 
	var transform = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transform" ); 
	var transformOrigin = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transform-origin" ); 	
	var transition = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transition" ); 	
	
	// JS ~ pt ii	
	var perspectiveOrigin = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "perspective-origin" );
	var backfaceVisibility = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "backface-visibility" ); 
	var transitionTimingFunction = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transition-timing-function" ); 
	var transitionProperty = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transition-property" ); 
	var transitionDuration = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transition-duration" ); 
	var transitionDelay = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "transition-delay" ); 
	
	// CSS
	var css_transform = donLuchoHAKI.ApplyPrefixes.ReturnCSSProperty( "transform" ); 
	var css_transition = donLuchoHAKI.ApplyPrefixes.ReturnCSSProperty( "transition" ); 
	
	// EXTRA ~ Axis functionality ~ just x\y poles... no Z
	var _lcWidth;
	var _lcHeight;
	
	
	// TOUCH ~ interaction.html
	if( _actv_Page === "interaction.html" ) { 
		var _el_kuma = _docObj.getElementById( "kuma" );
		var _el_gesture = _docObj.getElementById( "gesture" );
		var _el_green = _docObj.getElementById( "green" );
		var _el_blue = _docObj.getElementById( "blue" );
		var _el_red = _docObj.getElementById( "red" );
		var _el_solid = _docObj.getElementById( "solid" );	
		var _el_dashed = _docObj.getElementById( "dashed" );	
		var _el_dotted = _docObj.getElementById( "dotted" );
		
		var _oneU = parseInt( ( 0.109375 * _el_kuma.offsetWidth ) , 10); 
		var _threeU = parseInt( 3 * _oneU , 10 ); 
		var _ninetyTwoPerU = parseInt( _oneU * 0.92 , 10 );
		
		var _sparDIVs = _docObj.getElementById( "kuma" ).getElementsByTagName( "div" ); // _el_kuma.getElementsByTagName( "div" )
		var _sparMax = _sparDIVs.length;
		var a;
	}
	
	// ~ all Touch functionality
	if( !!_wboolyIf_Touch ){
		// TOUCH ~ untangle.html
		if( _actv_Page === "untangle.html" ) {
			var _tch_fingerCount;
			var _tch_startX;
			var _tch_startY;
			var _tch_lastX;
			var _tch_lastY;
			var _tch_swipeLength;
			var _tch_swipeDegrees;
			var _tch_swipeDirection;
			var _hack1_ID;
			var _allDIVs = donLuchoHAKI.RetELs.docObj().getElementsByTagName( "div" );
			var _allDvMAX = _allDIVs.length;
			var d;
		}
		// TOUCH ~ interaction.html
		if( _actv_Page === "interaction.html" ) {
			var _tchDragging = false;
			var _tchSizing = false;
			var _tchRotating = 0; 
		} 
	}  // END if( !!_wboolyIf_Touch )
	else
	if( !_wboolyIf_Touch ) {	
		// TOUCH ~ untangle.html
		if( _actv_Page === "untangle.html" ) {
			// noAction
		}
		// TOUCH ~ interaction.html
		if( _actv_Page === "interaction.html" ) {
			// noAction
		}
	}  // END if( !_wboolyIf_Touch )
	
	if(document.documentElement.addEventListener){
		
		// ~ Loadicon Behaviors functionality ~ Inject Keyframe Rule
		var _leHead = _docObj.getElementsByTagName( "head" )[0];
		
		// ~ Loadicon Behaviors functionality ~ Inject Keyframe Rule
		var _el_loadicon = _docObj.querySelector( "#loadicon" );
		var _js_animation = donLuchoHAKI.ApplyPrefixes.ReturnJSProperty( "animation" ); // console.log("_js_animation", _js_animation ); 
		var _css_transform = donLuchoHAKI.ApplyPrefixes.ReturnCSSProperty( "transform" ); // console.log( "_css_transform" , _css_transform );	
		var _keyframes = donLuchoHAKI.ApplyPrefixes.Returnkeyframes( "animationName" ); // console.log( "_keyframes", _keyframes );
		
		// ~ Carousel functionality ~ image gallery
		/*###########  ~ Carousel functionality ~ image gallery ###########*/ 
		if ( _str_href.match(/next/)) {
			// ~ Carousel functionality ~ image gallery
			var _singleImgWidth; // CON // ORIGINAL GROUP
			var _totalWidthImgs; // CON // ORIGINAL GROUP
			var _arPush_ImgWidths = []; // CON // ORIGINAL GROUP
			var _arPush_ImgELs = []; // CON // ORIGINAL GROUP
			var _ar_ImgCt; // CON // ORIGINAL GROUP
			// ~ Carousel functionality ~ image gallery
			var _endVal; // 500px  // ORIGINAL GROUP
			var _startVal; // 0 -500 -1000 -1500 // ORIGINAL GROUP
			// ~ Carousel functionality ~ image gallery
			var _pmAvgMultiple; // ORIGINAL GROUP
			var _pmEndV; // ORIGINAL GROUP
			// ~ Carousel functionality ~ image gallery
			var _loopTimer_caro; // ORIGINAL GROUP
			var _curImg; // ORIGINAL GROUP
			// ~ Carousel functionality ~ image gallery	
			var _el_AvgBtnCon = _docObj.getElementById( "AvgBtnCon" ); // ORIGINAL GROUP
			var _el_AvgWideCan = _docObj.getElementById( "AvgWideCan" ); // ORIGINAL GROUP
			var _el_AverageIn = _docObj.getElementById( "AverageIn" ); // ORIGINAL GROUP
			var _el_AverageOut = _docObj.getElementById( "AverageOut" ); // ORIGINAL GROUP
			// ~ Carousel functionality ~ image gallery
			var _all_imgs = _docObj.getElementsByTagName( "img" ); // ORIGINAL GROUP
			var _I; // ORIGINAL GROUP
			// ~ Carousel functionality ~ image gallery
			var _all_btns = _docObj.getElementsByTagName( "button" ); // ORIGINAL GROUP
			var _btnCt = _all_btns.length; // ORIGINAL GROUP
			var _B; // ORIGINAL GROUP
			
			// ~ Carousel functionality ~ image gallery -- part ii
			var _el_crutch = _docObj.getElementById( "crutch" ); // newbie 
		} // END if _str_href.match(/next/)) 
		
		
		// var DECLARATIONs...
		if( _actv_Page === "transition.html" ) { 
			
			// TRANSITION ~ ZOOM card 
			var _el_sectionContent = _docObj.getElementById( "sectionContent" );
			var _el_zoomWrapper = _docObj.getElementById( "zoomWrapper" );
			var _el_zoomCardOne = _docObj.getElementById( "zoomCardOne" );
			var _el_zBOne = _docObj.getElementById( "zBOne" );
			var _el_pBOne = _docObj.getElementById( "pBOne" );
			var _el_zFOne = _docObj.getElementById( "zFOne" );
			var _el_pFOne = _docObj.getElementById( "pFOne" );
			
			// TRANSITION ~ ZOOM card 
			var _fBWidth = _el_pBOne.offsetWidth;
			var _fBHeight = _el_pBOne.offsetHeight;	
			var _fFWidth = _el_pFOne.offsetWidth;
			var _fFHeight = _el_pFOne.offsetHeight;
		}
		
		// ~ Page Visibility functionality
		var _firstProp = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "hidden" , donLuchoHAKI.RetELs.docObj() ); // hidden
		var _stoProp = donLuchoHAKI.html5_WebStorage_API.If_WebStorageAPI();
		
		if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
			var _peeVeeLite_configObject = { 
				thisTitle : _docObj.title , 
				altTitle : _obj_ConfigAll.CannedStrings.sessionStorageOps.assets.nonFormatString.sessionTitle.altTitle[ window.localStorage.getItem("defaultLanguage") ].fullText 
			};
			
		}
		else 
		if( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) {
			var _peeVeeLite_configObject = { 
				thisTitle : _docObj.title , 
				altTitle : _obj_ConfigAll.CannedStrings.sessionStorageOps.assets.nonFormatString.sessionTitle.altTitle.EN.fullText 
			};
			
		}
		
		var _title_sessto = window.sessionStorage || {};
		
		// ~ Toolbar functionality ~ Only for MozillaFF at the moment
		if ( _chopper === "Firefox" ) { 
		
			var _el_TBar_API = _docObj.querySelector( "#TBar_API" );
			
			if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
				
				if( (_str_host === 'donlucho.github.io') || ( _str_host === 'artavia.gitlab.io') ){
					var _ctxmnu_AR = [ 
						{ eL: "menuitem" , label : _obj_ConfigAll.CannedStrings.toolbarOps.assets.nonFormatString.refreshher[ window.localStorage.getItem("defaultLanguage") ].fullText , onclick : "window.location.reload();" , icon : _obj_ConfigAll.AssetURLs.toolbarOps.assets.refreshIco.fullPath_IO } , 
						{ eL: "menuitem" , label : _obj_ConfigAll.CannedStrings.toolbarOps.assets.nonFormatString.skipher[ window.localStorage.getItem("defaultLanguage") ].fullText , onclick : "window.location=\"#asideContent\";" , icon : _obj_ConfigAll.AssetURLs.toolbarOps.assets.commentIco.fullPath_IO } 
					];
				} 
				else {
					var _ctxmnu_AR = [ 
						{ eL: "menuitem" , label : _obj_ConfigAll.CannedStrings.toolbarOps.assets.nonFormatString.refreshher[ window.localStorage.getItem("defaultLanguage") ].fullText , onclick : "window.location.reload();" , icon : _obj_ConfigAll.AssetURLs.toolbarOps.assets.refreshIco.fullPath } , 
						{ eL: "menuitem" , label : _obj_ConfigAll.CannedStrings.toolbarOps.assets.nonFormatString.skipher[ window.localStorage.getItem("defaultLanguage") ].fullText , onclick : "window.location=\"#asideContent\";" , icon : _obj_ConfigAll.AssetURLs.toolbarOps.assets.commentIco.fullPath } 
					];
				}
				
			}
			else 
			if( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) {

				if( (_str_host === 'donlucho.github.io') || ( _str_host === 'artavia.gitlab.io') ){
					var _ctxmnu_AR = [ 
						{ eL: "menuitem" , label : _obj_ConfigAll.CannedStrings.toolbarOps.assets.nonFormatString.refreshher.EN.fullText , onclick : "window.location.reload();" , icon : _obj_ConfigAll.AssetURLs.toolbarOps.assets.refreshIco.fullPath_IO } , 
						{ eL: "menuitem" , label : _obj_ConfigAll.CannedStrings.toolbarOps.assets.nonFormatString.skipher.EN.fullText , onclick : "window.location=\"#asideContent\";" , icon : _obj_ConfigAll.AssetURLs.toolbarOps.assets.commentIco.fullPath_IO } 
					];
				} 
				else {
					var _ctxmnu_AR = [ 
						{ eL: "menuitem" , label : _obj_ConfigAll.CannedStrings.toolbarOps.assets.nonFormatString.refreshher.EN.fullText , onclick : "window.location.reload();" , icon : _obj_ConfigAll.AssetURLs.toolbarOps.assets.refreshIco.fullPath } , 
						{ eL: "menuitem" , label : _obj_ConfigAll.CannedStrings.toolbarOps.assets.nonFormatString.skipher.EN.fullText , onclick : "window.location=\"#asideContent\";" , icon : _obj_ConfigAll.AssetURLs.toolbarOps.assets.commentIco.fullPath } 
					];
				}
				
			}
			
		} // END if "chopper" condition ~ MozFF Toolbar functionality
		
		
		// ~ Fullscreen functionality
		if ( !_str_href.match(/next/) ) {
			// ~ Fullscreen functionality
			var _ar_TrgrAs_fs = null;
			var _trgrMax_fs = null;
			// ~ Fullscreen functionality
			var _el_fsBtnCon = _docObj.getElementById( "fsBtnCon" );
			var _el_fsLaunch = _docObj.getElementById( "fsLaunch" );
			var _el_fsRestore = _docObj.getElementById( "fsRestore" );		
			// ~ Fullscreen functionality
			var _requestFullScreen;
			var _cancelFullScreen;
			
		} // END if ( !_str_href.match(/next/) )
		
		// ~ ELEVATOR LINK functionality ~ upTown\downTown
		var _str_top_silky = document.defaultView.getComputedStyle( _el_silky, "").getPropertyValue( "padding-top" ); // 19.9167px
		// console.log( "_str_top_silky" , _str_top_silky );
		var _str_btm_silky = document.defaultView.getComputedStyle( _el_silky, "").getPropertyValue( "padding-bottom" ); // 19.9167px
		// console.log( "_str_btm_silky" , _str_btm_silky );
		var _nm_padTop_silky = parseInt( _str_top_silky.match(/[0-9.]/gi).join("") , 10 ); 
		var _nm_padBtm_silky = parseInt( _str_btm_silky.match(/[0-9.]/gi).join("") , 10 );
		var _silky_cmptdDist = _nm_padTop_silky + _nm_padBtm_silky ; // _silky_cmptdDist = 40; 
		// console.log( "_silky_cmptdDist" , _silky_cmptdDist );
		
		// ~ Geolocation functionality
		var _el_gCP_BTN = _docObj.getElementById( "gCP_BTN" ); 
		var _el_gCP_lat = _docObj.getElementById( "gCP_lat" ); 
		var _el_gCP_long = _docObj.getElementById( "gCP_long" ); 
		var _el_stat_gCP = _docObj.getElementById( "stat_gCP" );
		// ~ Geolocation functionality
		var _el_watPos_BTN = _docObj.getElementById( "watPos_BTN" ); 
		var _el_watPos_CLR_BTN = _docObj.getElementById( "watPos_CLR_BTN" );
		var _el_watPos_lat = _docObj.getElementById( "watPos_lat" ); 
		var _el_watPos_long = _docObj.getElementById( "watPos_long" ); 
		var _el_watPos_acc = _docObj.getElementById( "watPos_acc" ); 
		var _el_stat_watPos = _docObj.getElementById("stat_watPos");
		
		// EXTRA ~ Axis functionality ~ just x\y poles... no Z
		if( document.documentElement.addEventListener && _actv_Page === "enhance.html" ){ 
			var _el_LessonCon = _docObj.getElementById( "LessonCon" );
			var _el_View = _docObj.getElementById( "View" );
			var _el_Top = _docObj.getElementById( "Top" ); 
			var _el_Bottom = _docObj.getElementById( "Bottom" ); 
			
			var _el_xSlide = _docObj.getElementById( "xSlide" );
			var _el_ySlide = _docObj.getElementById( "ySlide" );
			var _xVal = _el_xSlide.value; //console.log( "_xVal" , _xVal ); 
			var _yVal = _el_ySlide.value; //console.log( "_yVal" , _yVal );
			
			var _el_YAxis = _docObj.getElementById( "YAxis" ); 
			var _el_ZAxis = _docObj.getElementById( "ZAxis" ); 	
			var _el_openIt = _docObj.getElementById( "openIt" ); 
			var _el_closeIt = _docObj.getElementById( "closeIt" );	
			var _el_Ball = _docObj.getElementById( "Ball" ); 
			var _el_Left = _docObj.getElementById( "Left" ); 
			var _el_Right = _docObj.getElementById( "Right" ); 
			var _el_Front = _docObj.getElementById( "Front" );
			var _el_Back = _docObj.getElementById( "Back" ); 
		}
		
	} // END if document.documentElement.addEventListener
	else
	if( document.documentElement.attachEvent ){
		
		// ~ ELEVATOR LINK functionality ~ upTown\downTown
		var _str_top_silky = _el_silky.currentStyle.paddingTop; // 0.83em
		// window.alert( "_str_top_silky: " + _str_top_silky + "." );
		var _str_btm_silky = _el_silky.currentStyle.paddingBottom; // 0.83em
		// window.alert( "_str_btm_silky: " + _str_btm_silky + "." );
		var _nm_padTop_silky = ( _str_top_silky.slice( 0, _str_top_silky.length - 2 ) * 16);
		// window.alert( "_nm_padTop_silky: " + _nm_padTop_silky + "." );
		var _nm_padBtm_silky = ( _str_btm_silky.slice( 0, _str_btm_silky.length - 2 ) * 16);
		// window.alert( "_nm_padBtm_silky: " + _nm_padBtm_silky + "." );
		var _silky_cmptdDist = Math.ceil( _nm_padTop_silky + _nm_padBtm_silky );
		// window.alert( "_silky_cmptdDist: " + _silky_cmptdDist +".");
		
		// ~ CANVAS functionality
		var _plusNewDate;
		var _CNV_startTime;
		
		// EXTRA ~ Axis functionality ~ just x\y poles... no Z
		if( document.documentElement.attachEvent && _actv_Page === "enhance.html" ){ 
			var _el_LessonCon = _docObj.getElementById( "LessonCon" );
			var _el_View = _docObj.getElementById( "View" );
			var _el_Top = _docObj.getElementById( "Top" ); 
			var _el_Bottom = _docObj.getElementById( "Bottom" ); 
			
			var _el_xSlide = _docObj.getElementById( "xSlide" );
			var _el_ySlide = _docObj.getElementById( "ySlide" );
			var _xVal = _el_xSlide.value; //console.log( "_xVal" , _xVal ); 
			var _yVal = _el_ySlide.value; //console.log( "_yVal" , _yVal );
			
			var _el_YAxis = _docObj.getElementById( "YAxis" ); 
			var _el_ZAxis = _docObj.getElementById( "ZAxis" ); 	
			var _el_openIt = _docObj.getElementById( "openIt" ); 
			var _el_closeIt = _docObj.getElementById( "closeIt" );	
			var _el_Ball = _docObj.getElementById( "Ball" ); 
			var _el_Left = _docObj.getElementById( "Left" ); 
			var _el_Right = _docObj.getElementById( "Right" ); 
			var _el_Front = _docObj.getElementById( "Front" );
			var _el_Back = _docObj.getElementById( "Back" ); 
		}
	}
	
	/*################  utility functions  ######################*/	
	function _HideURLbar() {
		window.scrollTo(0, 1);
	} // END function
	
	function _PrevDeffo( leEvt ) { 
		donLuchoHAKI.Utils.evt_u.RetValFalprevDef( leEvt ); 
		//console.log( "yeah, baby 2 -- leEvt" , leEvt );
	} // END function
	
	function _GetComputedStyle( elObj, pmPropToGet ) {
		return donLuchoHAKI.PixelsArePixelsArePixels.GetComputedValue( elObj, pmPropToGet );
	} // END function
	
	function _$xID( name ) { 
		return donLuchoHAKI.PixelsArePixelsArePixels.Get_Element_By_Id( name ); 
	} // END function ~ UNUSED untangle.html function
	
	/*################  private functions  ######################*/
	
	// WEB STORAGE || COOKIE FUNC.
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@  COOKIES ++ _ToastCookie  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	function _ToastCookie( pmNamecookie ) { 
		_SetCookie( pmNamecookie, "", COOKIE_DEATH );
	} // END function
	
	// WEB STORAGE || COOKIE FUNC.
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  COOKIES ++ _GetCookie  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	function _GetCookie( pmNamecookie ) {
		var c_stringWhole = pmNamecookie + "=";
		var _ar_cookies = _docObj.cookie.split(';');
		var i; 
		for( i = 0; i < _ar_cookies.length; i = i + 1 ) {
			var c_name = _ar_cookies[ i ];
			while (c_name.charAt( 0 ) === ' ' ) {
				c_name = c_name.substring( 1, c_name.length );
			}
			if ( c_name.indexOf( c_stringWhole ) === 0 ) {
				return c_name.substring( c_stringWhole.length, c_name.length );
			}
		}
		return null;
	} // END function
	
	// WEB STORAGE || COOKIE FUNC.
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  COOKIES ++ _SetCookie  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	function _SetCookie( pmNamecookie, pmValcookie, pmDayscookie ) {
		if ( pmDayscookie ) {
			var expDate = new Date();
			expDate.setTime( expDate.getTime() + ( pmDayscookie * 24*60*60*1000 ) );
			var str_Expires = "; expires=" + expDate.toGMTString();
			_docObj.cookie = pmNamecookie + "=" + pmValcookie + str_Expires + "; path=/";
			expDate = null;
		}
		else {
			var str_Expires = "";
			_docObj.cookie = pmNamecookie + "=" + pmValcookie + str_Expires +"; path=/";
		}
	} // END function
	
	// LANGUAGE SWITCHER FUNC.
	function _ChangeSrcLangAndBack(leEvt) { 
		
		// WEB STORAGE || COOKIE FUNC.
		if( !!( "localStorage" in window ) ) { 
			
			// WEB STORAGE || COOKIE FUNC. ~ LANGUAGE SWITCHER FUNC.
			_trgrAncho_Lang = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
			_trgrLangClassAs = _docObj.getElementsByTagName(_trgrAncho_Lang.tagName);
			_trgrLangMax = _trgrLangClassAs.length; 
			
			for ( q = 0 ; q < _trgrLangMax; q = q + 1 ) { 
				if (_trgrAncho_Lang.className === _classOBJ.a) { 
					_SwtchRooVLU = _classOBJ.b;
					_obj_locsto_lang.setItem("defaultLanguage", _SwtchRooVLU);
					_trgrAncho_Lang.className = _obj_locsto_lang.getItem("defaultLanguage");
				}
				else
				if (_trgrAncho_Lang.className === _classOBJ.b) {
					_SwtchRooVLU = _classOBJ.c;
					_obj_locsto_lang.setItem("defaultLanguage", _SwtchRooVLU);
					_trgrAncho_Lang.className = _obj_locsto_lang.getItem("defaultLanguage");
					_trgrAncho_Lang.innerHTML = _textOBJ.c;
				} 
				else {
					_SwtchRooVLU = _classOBJ.b; 
					_obj_locsto_lang.setItem("defaultLanguage", _SwtchRooVLU);
					_trgrAncho_Lang.className = _obj_locsto_lang.getItem("defaultLanguage");
					_trgrAncho_Lang.innerHTML = _textOBJ.b;
				} 
			} // END for loop 
			_el_logDetails.innerHTML = "<p>Web Storage Value: " + _obj_locsto_lang.getItem("defaultLanguage") + "</p>"; 
		}
		else
		if( !( "localStorage" in window ) ) { 
			
			// WEB STORAGE || COOKIE FUNC. ~ LANGUAGE SWITCHER FUNC.
			_trgrAncho_Lang = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
			_trgrLangClassAs = _docObj.getElementsByTagName(_trgrAncho_Lang.tagName);
			_trgrLangMax = _trgrLangClassAs.length; 
			
			for ( q = 0 ; q < _trgrLangMax; q = q + 1 ) { 
				if (_trgrAncho_Lang.className === _classOBJ.a) { 
					_SwtchRooVLU = _classOBJ.b;
					// >>> when assigning a DOM element value to a cookie string value USE >>> escape(str)
					_SetCookie( DEFAULTLANGUAGE, escape( _SwtchRooVLU ), COOKIE_LIFE );
					_trgrAncho_Lang.className = unescape( _GetCookie(DEFAULTLANGUAGE) );
				}
				else
				if (_trgrAncho_Lang.className === _classOBJ.b) {
					_SwtchRooVLU = _classOBJ.c;
					_SetCookie( DEFAULTLANGUAGE, escape( _SwtchRooVLU ), COOKIE_LIFE );
					_trgrAncho_Lang.className = unescape( _GetCookie(DEFAULTLANGUAGE) );
					_trgrAncho_Lang.innerHTML = _textOBJ.c;
				} 
				else {
					_SwtchRooVLU = _classOBJ.b; 
					_SetCookie( DEFAULTLANGUAGE, escape( _SwtchRooVLU ), COOKIE_LIFE );
					_trgrAncho_Lang.className = unescape( _GetCookie(DEFAULTLANGUAGE) );
					_trgrAncho_Lang.innerHTML = _textOBJ.b;
				} 
			} // END for loop 
			_el_logDetails.innerHTML = "<p>Cookie Value: " + unescape( _GetCookie(DEFAULTLANGUAGE) ) + "</p>"; 
		}
		
	} // END function
	
	// ACTIVE~X ~ Content REQUEST OBJ 
	function _ActvX_albTrsReqObj_Content_xml() { 
		
		if (window.XMLHttpRequest) {
			_actvX_albtrs_Content = new XMLHttpRequest();
		}
		else
		if (window.ActiveXObject) {
			try {
				_actvX_albtrs_Content = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e) {}
		}
		if (_actvX_albtrs_Content) {
			_actvX_albtrs_Content.onreadystatechange = _ActvX_albTrs_Content_xml_Resp;
			_actvX_albtrs_Content.open("GET", _url_content, true);
			_actvX_albtrs_Content.send(null);	
		}
		else {
			window.alert(_obj_ConfigAll.CannedStrings.ajaxOps.assets.nonFormatString.dOAajaxReq.intro);
		}
		
		_ActvX_albTrs_CitationDeploy();
		
	} // END function
	
	// XHR ~ Content REQUEST OBJ 
	function _XhrReqObj_Content_xml() { 
		if (window.XMLHttpRequest) {
			_xhr_Content = new XMLHttpRequest();
		}
		else
		if (window.ActiveXObject) {
			try {
				_xhr_Content = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e) {}
		}
		if (!!_xhr_Content) {
			_xhr_Content.onreadystatechange = _Xhr_Content_xml_Resp;
			_xhr_Content.open("GET", _url_content, true);
			_xhr_Content.send(null);
			
			_xhr_Content.onload = _Xhr_Content_xml_Load;
		}
		else
		if (!_xhr_Content) { 
			window.alert(_obj_ConfigAll.CannedStrings.ajaxOps.assets.nonFormatString.dOAajaxReq.intro);
		}
	} // END function
	
	
	// PRETZEL
	// ACTIVE~X ~ Content RESPONSE OBJ
	function _ActvX_albTrs_Content_xml_Resp() {
		if (_actvX_albtrs_Content.readyState === 4) {
			if (_actvX_albtrs_Content.status === 200) {
				if( _actvX_albtrs_Content.responseXML ){
					
					// DYNAMIC CONTENT
					var all_dynamiccontent = _actvX_albtrs_Content.responseXML.getElementsByTagName("dynamiccontent"); 
					var dynconLen = all_dynamiccontent.length; 
					var l;
					var dynamic_attr;
					var m;
					
					// RIGHTEOUS CONTENT
					var all_righteouscontent = _actvX_albtrs_Content.responseXML.getElementsByTagName("righteouscontent");
					var rytconLen = all_righteouscontent.length;
					var g;
					var focus_rightcon;
					var righteous_attr;
					var h;
					
					// STATIC CONTENT
					var all_staticcontent = _actvX_albtrs_Content.responseXML.getElementsByTagName("staticcontent");
					var statconLen = all_staticcontent.length;
					var o;
					var static_attr;
					var k;
					
					// NONPAREIL CONTENT
					var all_nonpareilcontent = _actvX_albtrs_Content.responseXML.getElementsByTagName("nonpareilcontent"); // 
					var nonpareilconLen = all_nonpareilcontent.length;
					var ZZ;
					var focus_nonpareilcon;
					var nonpareil_attr;
					var url_attr;
					var YY;
					
					// NONPAREIL CONTENT
					for ( YY = 0; YY < nonpareilconLen; YY = YY + 1) {
						
						nonpareil_attr = all_nonpareilcontent[YY].getAttribute("clientele");
						url_attr = all_nonpareilcontent[YY].getAttribute("url");
						
						if( !( _GetCookie(DEFAULTLANGUAGE) ) ){
							_stringCheese = "EN";
							if( ( nonpareil_attr === _stringCheese ) && ( url_attr === _actv_Page ) ){
								focus_nonpareilcon = all_nonpareilcontent[YY];
								for ( ZZ = 0; ZZ < nonpareilconLen; ZZ = ZZ + 1) {
									var tempObj_Nonpareilcon = {};
									if( all_nonpareilcontent[ZZ] === focus_nonpareilcon ){
										tempObj_Nonpareilcon.contentattribute = all_nonpareilcontent[ZZ].getAttribute("clientele");
										tempObj_Nonpareilcon.curlang = _stringCheese;
										tempObj_Nonpareilcon.urlattribute = all_nonpareilcontent[ZZ].getAttribute("url");
										
										// ENHANCE
										tempObj_Nonpareilcon.xaxis = GetVal(all_nonpareilcontent[ZZ], "xaxis");
										tempObj_Nonpareilcon.yaxis = GetVal(all_nonpareilcontent[ZZ], "yaxis");
										tempObj_Nonpareilcon.zaxis = GetVal(all_nonpareilcontent[ZZ], "zaxis");
										tempObj_Nonpareilcon.cubeback = GetVal(all_nonpareilcontent[ZZ], "cubeback");
										tempObj_Nonpareilcon.cubefront = GetVal(all_nonpareilcontent[ZZ], "cubefront");
										tempObj_Nonpareilcon.cuberight = GetVal(all_nonpareilcontent[ZZ], "cuberight");
										tempObj_Nonpareilcon.cubeleft = GetVal(all_nonpareilcontent[ZZ], "cubeleft");
										tempObj_Nonpareilcon.cubebottom = GetVal(all_nonpareilcontent[ZZ], "cubebottom");
										tempObj_Nonpareilcon.cubetop = GetVal(all_nonpareilcontent[ZZ], "cubetop");
										tempObj_Nonpareilcon.lgdfsrox = GetVal(all_nonpareilcontent[ZZ], "lgdfsrox");
										tempObj_Nonpareilcon.lblfsrox = GetVal(all_nonpareilcontent[ZZ], "lblfsrox");
										tempObj_Nonpareilcon.lgdfsroy = GetVal(all_nonpareilcontent[ZZ], "lgdfsroy");
										tempObj_Nonpareilcon.lblfsroy = GetVal(all_nonpareilcontent[ZZ], "lblfsroy");
										tempObj_Nonpareilcon.lgdfsopnclo = GetVal(all_nonpareilcontent[ZZ], "lgdfsopnclo");
										tempObj_Nonpareilcon.openitvalue = GetVal(all_nonpareilcontent[ZZ], "openitvalue");
										tempObj_Nonpareilcon.closeitvalue = GetVal(all_nonpareilcontent[ZZ], "closeitvalue");
										// TRANSFORM
										tempObj_Nonpareilcon.fbzhxa = GetVal(all_nonpareilcontent[ZZ], "fbzhxa");
										tempObj_Nonpareilcon.fbzhxb = GetVal(all_nonpareilcontent[ZZ], "fbzhxb");
										tempObj_Nonpareilcon.fbzp1 = GetVal(all_nonpareilcontent[ZZ], "fbzp1");
										tempObj_Nonpareilcon.fbzp2 = GetVal(all_nonpareilcontent[ZZ], "fbzp2");
										tempObj_Nonpareilcon.bbzli1 = GetVal(all_nonpareilcontent[ZZ], "bbzli1");
										tempObj_Nonpareilcon.bbzli2 = GetVal(all_nonpareilcontent[ZZ], "bbzli2");
										tempObj_Nonpareilcon.bbzli3 = GetVal(all_nonpareilcontent[ZZ], "bbzli3");
										tempObj_Nonpareilcon.bbzli4 = GetVal(all_nonpareilcontent[ZZ], "bbzli4");
										tempObj_Nonpareilcon.flp1 = GetVal(all_nonpareilcontent[ZZ], "flp1");
										tempObj_Nonpareilcon.flp2 = GetVal(all_nonpareilcontent[ZZ], "flp2");
										tempObj_Nonpareilcon.blp3 = GetVal(all_nonpareilcontent[ZZ], "blp3");
										tempObj_Nonpareilcon.blp4 = GetVal(all_nonpareilcontent[ZZ], "blp4");
										tempObj_Nonpareilcon.blp5 = GetVal(all_nonpareilcontent[ZZ], "blp5");
										// TRANSITION
										tempObj_Nonpareilcon.tranhxa = GetVal(all_nonpareilcontent[ZZ], "tranhxa");
										tempObj_Nonpareilcon.tranlr = GetVal(all_nonpareilcontent[ZZ], "tranlr");
										tempObj_Nonpareilcon.tranhxb = GetVal(all_nonpareilcontent[ZZ], "tranhxb");
										tempObj_Nonpareilcon.tranpa = GetVal(all_nonpareilcontent[ZZ], "tranpa");
										tempObj_Nonpareilcon.tranhxc = GetVal(all_nonpareilcontent[ZZ], "tranhxc");
										tempObj_Nonpareilcon.tranovni = GetVal(all_nonpareilcontent[ZZ], "tranovni");
										// DEVICE
										tempObj_Nonpareilcon.dvcpa = GetVal(all_nonpareilcontent[ZZ], "dvcpa");
										tempObj_Nonpareilcon.dvcpb = GetVal(all_nonpareilcontent[ZZ], "dvcpb");
										tempObj_Nonpareilcon.dvcpc = GetVal(all_nonpareilcontent[ZZ], "dvcpc");
										tempObj_Nonpareilcon.dvcpd = GetVal(all_nonpareilcontent[ZZ], "dvcpd");
										tempObj_Nonpareilcon.dvchxa = GetVal(all_nonpareilcontent[ZZ], "dvchxa");
										tempObj_Nonpareilcon.dvchxb = GetVal(all_nonpareilcontent[ZZ], "dvchxb");
										tempObj_Nonpareilcon.dvchxc = GetVal(all_nonpareilcontent[ZZ], "dvchxc");
										tempObj_Nonpareilcon.dvchxd = GetVal(all_nonpareilcontent[ZZ], "dvchxd");
										tempObj_Nonpareilcon.dvcpanoramic = GetVal(all_nonpareilcontent[ZZ], "dvcpanoramic");
										tempObj_Nonpareilcon.dvcportrait = GetVal(all_nonpareilcontent[ZZ], "dvcportrait");
										tempObj_Nonpareilcon.dvchxe = GetVal(all_nonpareilcontent[ZZ], "dvchxe");
										tempObj_Nonpareilcon.dvchxf = GetVal(all_nonpareilcontent[ZZ], "dvchxf");
										// tempObj_Nonpareilcon.twoandonehalf = GetVal(all_nonpareilcontent[ZZ], "twoandonehalf");
										tempObj_Nonpareilcon.dvchxg = GetVal(all_nonpareilcontent[ZZ], "dvchxg");
										tempObj_Nonpareilcon.dvchxh = GetVal(all_nonpareilcontent[ZZ], "dvchxh");
										tempObj_Nonpareilcon.dvchxi = GetVal(all_nonpareilcontent[ZZ], "dvchxi");
										tempObj_Nonpareilcon.dvchxj = GetVal(all_nonpareilcontent[ZZ], "dvchxj");
										tempObj_Nonpareilcon.dvchxk = GetVal(all_nonpareilcontent[ZZ], "dvchxk");
										tempObj_Nonpareilcon.dvchxl = GetVal(all_nonpareilcontent[ZZ], "dvchxl");
										tempObj_Nonpareilcon.dvcmh2 = GetVal(all_nonpareilcontent[ZZ], "dvcmh2");
										tempObj_Nonpareilcon.dvcmh3 = GetVal(all_nonpareilcontent[ZZ], "dvcmh3");
										// tempObj_Nonpareilcon.dvcmro= GetVal(all_nonpareilcontent[ZZ], "dvcmro");
										tempObj_Nonpareilcon.dvcoh2 = GetVal(all_nonpareilcontent[ZZ], "dvcoh2");
										tempObj_Nonpareilcon.dvcoh3 = GetVal(all_nonpareilcontent[ZZ], "dvcoh3");
										// tempObj_Nonpareilcon.dvcoro = GetVal(all_nonpareilcontent[ZZ], "dvcoro");
										// UNTANGLE
										tempObj_Nonpareilcon.mvlo = GetVal(all_nonpareilcontent[ZZ], "mvlo");
										tempObj_Nonpareilcon.mvrg = GetVal(all_nonpareilcontent[ZZ], "mvrg");
										tempObj_Nonpareilcon.mvtm = GetVal(all_nonpareilcontent[ZZ], "mvtm");
										tempObj_Nonpareilcon.mvbp = GetVal(all_nonpareilcontent[ZZ], "mvbp");
									}
									_ar_NONPAREILCONTENT[ZZ] = tempObj_Nonpareilcon;
								}
								
							}
						}
						else
						if( !!( _GetCookie(DEFAULTLANGUAGE) ) ){
							if( !!( _stringCheese ) ){
								_stringCheese = undefined;
							}
							if( ( nonpareil_attr === _GetCookie(DEFAULTLANGUAGE) ) && ( url_attr === _actv_Page ) ){
								focus_nonpareilcon = all_nonpareilcontent[YY];
								for ( ZZ = 0; ZZ < nonpareilconLen; ZZ = ZZ + 1) {
									var tempObj_Nonpareilcon = {};
									if( all_nonpareilcontent[ZZ] === focus_nonpareilcon ){
										tempObj_Nonpareilcon.contentattribute = all_nonpareilcontent[ZZ].getAttribute("clientele");
										tempObj_Nonpareilcon.curlang = _GetCookie(DEFAULTLANGUAGE);
										tempObj_Nonpareilcon.urlattribute = all_nonpareilcontent[ZZ].getAttribute("url");
										
										// ENHANCE
										tempObj_Nonpareilcon.xaxis = GetVal(all_nonpareilcontent[ZZ], "xaxis");
										tempObj_Nonpareilcon.yaxis = GetVal(all_nonpareilcontent[ZZ], "yaxis");
										tempObj_Nonpareilcon.zaxis = GetVal(all_nonpareilcontent[ZZ], "zaxis");
										tempObj_Nonpareilcon.cubeback = GetVal(all_nonpareilcontent[ZZ], "cubeback");
										tempObj_Nonpareilcon.cubefront = GetVal(all_nonpareilcontent[ZZ], "cubefront");
										tempObj_Nonpareilcon.cuberight = GetVal(all_nonpareilcontent[ZZ], "cuberight");
										tempObj_Nonpareilcon.cubeleft = GetVal(all_nonpareilcontent[ZZ], "cubeleft");
										tempObj_Nonpareilcon.cubebottom = GetVal(all_nonpareilcontent[ZZ], "cubebottom");
										tempObj_Nonpareilcon.cubetop = GetVal(all_nonpareilcontent[ZZ], "cubetop");
										tempObj_Nonpareilcon.lgdfsrox = GetVal(all_nonpareilcontent[ZZ], "lgdfsrox");
										tempObj_Nonpareilcon.lblfsrox = GetVal(all_nonpareilcontent[ZZ], "lblfsrox");
										tempObj_Nonpareilcon.lgdfsroy = GetVal(all_nonpareilcontent[ZZ], "lgdfsroy");
										tempObj_Nonpareilcon.lblfsroy = GetVal(all_nonpareilcontent[ZZ], "lblfsroy");
										tempObj_Nonpareilcon.lgdfsopnclo = GetVal(all_nonpareilcontent[ZZ], "lgdfsopnclo");
										tempObj_Nonpareilcon.openitvalue = GetVal(all_nonpareilcontent[ZZ], "openitvalue");
										tempObj_Nonpareilcon.closeitvalue = GetVal(all_nonpareilcontent[ZZ], "closeitvalue");
										// TRANSFORM
										tempObj_Nonpareilcon.fbzhxa = GetVal(all_nonpareilcontent[ZZ], "fbzhxa");
										tempObj_Nonpareilcon.fbzhxb = GetVal(all_nonpareilcontent[ZZ], "fbzhxb");
										tempObj_Nonpareilcon.fbzp1 = GetVal(all_nonpareilcontent[ZZ], "fbzp1");
										tempObj_Nonpareilcon.fbzp2 = GetVal(all_nonpareilcontent[ZZ], "fbzp2");
										tempObj_Nonpareilcon.bbzli1 = GetVal(all_nonpareilcontent[ZZ], "bbzli1");
										tempObj_Nonpareilcon.bbzli2 = GetVal(all_nonpareilcontent[ZZ], "bbzli2");
										tempObj_Nonpareilcon.bbzli3 = GetVal(all_nonpareilcontent[ZZ], "bbzli3");
										tempObj_Nonpareilcon.bbzli4 = GetVal(all_nonpareilcontent[ZZ], "bbzli4");
										tempObj_Nonpareilcon.flp1 = GetVal(all_nonpareilcontent[ZZ], "flp1");
										tempObj_Nonpareilcon.flp2 = GetVal(all_nonpareilcontent[ZZ], "flp2");
										tempObj_Nonpareilcon.blp3 = GetVal(all_nonpareilcontent[ZZ], "blp3");
										tempObj_Nonpareilcon.blp4 = GetVal(all_nonpareilcontent[ZZ], "blp4");
										tempObj_Nonpareilcon.blp5 = GetVal(all_nonpareilcontent[ZZ], "blp5");
										// TRANSITION
										tempObj_Nonpareilcon.tranhxa = GetVal(all_nonpareilcontent[ZZ], "tranhxa");
										tempObj_Nonpareilcon.tranlr = GetVal(all_nonpareilcontent[ZZ], "tranlr");
										tempObj_Nonpareilcon.tranhxb = GetVal(all_nonpareilcontent[ZZ], "tranhxb");
										tempObj_Nonpareilcon.tranpa = GetVal(all_nonpareilcontent[ZZ], "tranpa");
										tempObj_Nonpareilcon.tranhxc = GetVal(all_nonpareilcontent[ZZ], "tranhxc");
										tempObj_Nonpareilcon.tranovni = GetVal(all_nonpareilcontent[ZZ], "tranovni");
										// DEVICE
										tempObj_Nonpareilcon.dvcpa = GetVal(all_nonpareilcontent[ZZ], "dvcpa");
										tempObj_Nonpareilcon.dvcpb = GetVal(all_nonpareilcontent[ZZ], "dvcpb");
										tempObj_Nonpareilcon.dvcpc = GetVal(all_nonpareilcontent[ZZ], "dvcpc");
										tempObj_Nonpareilcon.dvcpd = GetVal(all_nonpareilcontent[ZZ], "dvcpd");
										tempObj_Nonpareilcon.dvchxa = GetVal(all_nonpareilcontent[ZZ], "dvchxa");
										tempObj_Nonpareilcon.dvchxb = GetVal(all_nonpareilcontent[ZZ], "dvchxb");
										tempObj_Nonpareilcon.dvchxc = GetVal(all_nonpareilcontent[ZZ], "dvchxc");
										tempObj_Nonpareilcon.dvchxd = GetVal(all_nonpareilcontent[ZZ], "dvchxd");
										tempObj_Nonpareilcon.dvcpanoramic = GetVal(all_nonpareilcontent[ZZ], "dvcpanoramic");
										tempObj_Nonpareilcon.dvcportrait = GetVal(all_nonpareilcontent[ZZ], "dvcportrait");
										tempObj_Nonpareilcon.dvchxe = GetVal(all_nonpareilcontent[ZZ], "dvchxe");
										tempObj_Nonpareilcon.dvchxf = GetVal(all_nonpareilcontent[ZZ], "dvchxf");
										// tempObj_Nonpareilcon.twoandonehalf = GetVal(all_nonpareilcontent[ZZ], "twoandonehalf");
										tempObj_Nonpareilcon.dvchxg = GetVal(all_nonpareilcontent[ZZ], "dvchxg");
										tempObj_Nonpareilcon.dvchxh = GetVal(all_nonpareilcontent[ZZ], "dvchxh");
										tempObj_Nonpareilcon.dvchxi = GetVal(all_nonpareilcontent[ZZ], "dvchxi");
										tempObj_Nonpareilcon.dvchxj = GetVal(all_nonpareilcontent[ZZ], "dvchxj");
										tempObj_Nonpareilcon.dvchxk = GetVal(all_nonpareilcontent[ZZ], "dvchxk");
										tempObj_Nonpareilcon.dvchxl = GetVal(all_nonpareilcontent[ZZ], "dvchxl");
										tempObj_Nonpareilcon.dvcmh2 = GetVal(all_nonpareilcontent[ZZ], "dvcmh2");
										tempObj_Nonpareilcon.dvcmh3 = GetVal(all_nonpareilcontent[ZZ], "dvcmh3");
										// tempObj_Nonpareilcon.dvcmro= GetVal(all_nonpareilcontent[ZZ], "dvcmro");
										tempObj_Nonpareilcon.dvcoh2 = GetVal(all_nonpareilcontent[ZZ], "dvcoh2");
										tempObj_Nonpareilcon.dvcoh3 = GetVal(all_nonpareilcontent[ZZ], "dvcoh3");
										// tempObj_Nonpareilcon.dvcoro = GetVal(all_nonpareilcontent[ZZ], "dvcoro");
										// UNTANGLE
										tempObj_Nonpareilcon.mvlo = GetVal(all_nonpareilcontent[ZZ], "mvlo");
										tempObj_Nonpareilcon.mvrg = GetVal(all_nonpareilcontent[ZZ], "mvrg");
										tempObj_Nonpareilcon.mvtm = GetVal(all_nonpareilcontent[ZZ], "mvtm");
										tempObj_Nonpareilcon.mvbp = GetVal(all_nonpareilcontent[ZZ], "mvbp");
									}
									_ar_NONPAREILCONTENT[ZZ] = tempObj_Nonpareilcon; 
								}
							}
						}
					}
					
					// DYNAMIC CONTENT
					for ( m = 0; m < dynconLen; m = m + 1 ) {
						dynamic_attr = all_dynamiccontent[m].getAttribute("clientele");
						if( !( _GetCookie(DEFAULTLANGUAGE) ) ){
							_stringCheese = "EN";
							if( dynamic_attr === _stringCheese ){
								
								for ( l = 0; l < dynconLen; l = l + 1) {
									var tempObj_Dyncon = {};
									tempObj_Dyncon.contentattribute = all_dynamiccontent[l].getAttribute("clientele");
									tempObj_Dyncon.urlstringattribute = all_dynamiccontent[l].getAttribute("urlstring");
									tempObj_Dyncon.curlang = _stringCheese;
									tempObj_Dyncon.activepage = GetVal(all_dynamiccontent[l], "activepage");
									tempObj_Dyncon.contento = GetVal(all_dynamiccontent[l], "contento");
									tempObj_Dyncon.pubdate = GetVal(all_dynamiccontent[l], "pubdate");
									tempObj_Dyncon.sechredcaps = GetVal(all_dynamiccontent[l], "sechredcaps");
									tempObj_Dyncon.todolistalt = GetVal(all_dynamiccontent[l], "todolistalt");
									tempObj_Dyncon.ctaimgalt = GetVal(all_dynamiccontent[l], "ctaimgalt");
									tempObj_Dyncon.ctaimg = GetVal(all_dynamiccontent[l], "ctaimg");
									tempObj_Dyncon.figcaption = GetVal(all_dynamiccontent[l], "figcaption");
									tempObj_Dyncon.captionimgalt = GetVal(all_dynamiccontent[l], "captionimgalt");
									tempObj_Dyncon.captionimg = GetVal(all_dynamiccontent[l], "captionimg");
									tempObj_Dyncon.sechthree = GetVal(all_dynamiccontent[l], "sechthree");
									tempObj_Dyncon.secpma = GetVal(all_dynamiccontent[l], "secpma");
									tempObj_Dyncon.secpsa = GetVal(all_dynamiccontent[l], "secpsa");
									tempObj_Dyncon.secpmlrb = GetVal(all_dynamiccontent[l], "secpmlrb");
									tempObj_Dyncon.secpmlrc = GetVal(all_dynamiccontent[l], "secpmlrc");
									tempObj_Dyncon.secpmd = GetVal(all_dynamiccontent[l], "secpmd");
									tempObj_Dyncon.secpdryz = GetVal(all_dynamiccontent[l], "secpdryz");
									tempObj_Dyncon.asidea = GetVal(all_dynamiccontent[l], "asidea");
									tempObj_Dyncon.asideb = GetVal(all_dynamiccontent[l], "asideb");
									tempObj_Dyncon.asidec = GetVal(all_dynamiccontent[l], "asidec");
									tempObj_Dyncon.asideimgaalt = GetVal(all_dynamiccontent[l], "asideimgaalt");
									tempObj_Dyncon.asideimga = GetVal(all_dynamiccontent[l], "asideimga");
									tempObj_Dyncon.asided = GetVal(all_dynamiccontent[l], "asided");
									tempObj_Dyncon.asidee = GetVal(all_dynamiccontent[l], "asidee");
									tempObj_Dyncon.asidef = GetVal(all_dynamiccontent[l], "asidef");
									tempObj_Dyncon.asideimgbalt = GetVal(all_dynamiccontent[l], "asideimgbalt");
									tempObj_Dyncon.asideimgb = GetVal(all_dynamiccontent[l], "asideimgb");
									tempObj_Dyncon.asideg = GetVal(all_dynamiccontent[l], "asideg");
									tempObj_Dyncon.asideh = GetVal(all_dynamiccontent[l], "asideh");
									tempObj_Dyncon.asidei = GetVal(all_dynamiccontent[l], "asidei");
									tempObj_Dyncon.asideimgcalt = GetVal(all_dynamiccontent[l], "asideimgcalt");
									tempObj_Dyncon.asideimgc = GetVal(all_dynamiccontent[l], "asideimgc");
									tempObj_Dyncon.likea = GetVal(all_dynamiccontent[l], "likea");
									tempObj_Dyncon.likeb = GetVal(all_dynamiccontent[l], "likeb");
									tempObj_Dyncon.likec = GetVal(all_dynamiccontent[l], "likec");
									tempObj_Dyncon.workinprogress = GetVal(all_dynamiccontent[l], "workinprogress");
									
									_ar_DYNAMICCONTENT[l] = tempObj_Dyncon;
								}
							}
						} // end if locsto not initiated
						else
						if( !!( _GetCookie(DEFAULTLANGUAGE) ) ){
							if( !!( _stringCheese ) ){
								_stringCheese = undefined;
							}
							if( dynamic_attr === _GetCookie(DEFAULTLANGUAGE) ){
								
								for ( l = 0; l < dynconLen; l = l + 1) {
									var tempObj_Dyncon = {};
									tempObj_Dyncon.contentattribute = all_dynamiccontent[l].getAttribute("clientele");
									tempObj_Dyncon.urlstringattribute = all_dynamiccontent[l].getAttribute("urlstring");
									tempObj_Dyncon.curlang = _GetCookie(DEFAULTLANGUAGE);
									tempObj_Dyncon.activepage = GetVal(all_dynamiccontent[l], "activepage");
									tempObj_Dyncon.contento = GetVal(all_dynamiccontent[l], "contento");
									tempObj_Dyncon.pubdate = GetVal(all_dynamiccontent[l], "pubdate");
									tempObj_Dyncon.sechredcaps = GetVal(all_dynamiccontent[l], "sechredcaps");
									tempObj_Dyncon.todolistalt = GetVal(all_dynamiccontent[l], "todolistalt");
									tempObj_Dyncon.ctaimgalt = GetVal(all_dynamiccontent[l], "ctaimgalt");
									tempObj_Dyncon.ctaimg = GetVal(all_dynamiccontent[l], "ctaimg");
									tempObj_Dyncon.figcaption = GetVal(all_dynamiccontent[l], "figcaption");
									tempObj_Dyncon.captionimgalt = GetVal(all_dynamiccontent[l], "captionimgalt");
									tempObj_Dyncon.captionimg = GetVal(all_dynamiccontent[l], "captionimg");
									tempObj_Dyncon.sechthree = GetVal(all_dynamiccontent[l], "sechthree");
									tempObj_Dyncon.secpma = GetVal(all_dynamiccontent[l], "secpma");
									tempObj_Dyncon.secpsa = GetVal(all_dynamiccontent[l], "secpsa");
									tempObj_Dyncon.secpmlrb = GetVal(all_dynamiccontent[l], "secpmlrb");
									tempObj_Dyncon.secpmlrc = GetVal(all_dynamiccontent[l], "secpmlrc");
									tempObj_Dyncon.secpmd = GetVal(all_dynamiccontent[l], "secpmd");
									tempObj_Dyncon.secpdryz = GetVal(all_dynamiccontent[l], "secpdryz");
									tempObj_Dyncon.asidea = GetVal(all_dynamiccontent[l], "asidea");
									tempObj_Dyncon.asideb = GetVal(all_dynamiccontent[l], "asideb");
									tempObj_Dyncon.asidec = GetVal(all_dynamiccontent[l], "asidec");
									tempObj_Dyncon.asideimgaalt = GetVal(all_dynamiccontent[l], "asideimgaalt");
									tempObj_Dyncon.asideimga = GetVal(all_dynamiccontent[l], "asideimga");
									tempObj_Dyncon.asided = GetVal(all_dynamiccontent[l], "asided");
									tempObj_Dyncon.asidee = GetVal(all_dynamiccontent[l], "asidee");
									tempObj_Dyncon.asidef = GetVal(all_dynamiccontent[l], "asidef");
									tempObj_Dyncon.asideimgbalt = GetVal(all_dynamiccontent[l], "asideimgbalt");
									tempObj_Dyncon.asideimgb = GetVal(all_dynamiccontent[l], "asideimgb");
									tempObj_Dyncon.asideg = GetVal(all_dynamiccontent[l], "asideg");
									tempObj_Dyncon.asideh = GetVal(all_dynamiccontent[l], "asideh");
									tempObj_Dyncon.asidei = GetVal(all_dynamiccontent[l], "asidei");
									tempObj_Dyncon.asideimgcalt = GetVal(all_dynamiccontent[l], "asideimgcalt");
									tempObj_Dyncon.asideimgc = GetVal(all_dynamiccontent[l], "asideimgc");
									tempObj_Dyncon.likea = GetVal(all_dynamiccontent[l], "likea");
									tempObj_Dyncon.likeb = GetVal(all_dynamiccontent[l], "likeb");
									tempObj_Dyncon.likec = GetVal(all_dynamiccontent[l], "likec");
									tempObj_Dyncon.workinprogress = GetVal(all_dynamiccontent[l], "workinprogress");
									
									_ar_DYNAMICCONTENT[l] = tempObj_Dyncon;
								}
							}
						} // end if locsto initiated
					} // END for ( m = 0; m < dynconLen; m = m + 1 )
					
					// RIGHTEOUS CONTENT
					for ( h = 0; h < rytconLen; h = h + 1) {
						righteous_attr = all_righteouscontent[h].getAttribute("clientele");
						if( !( _GetCookie(DEFAULTLANGUAGE) ) ){
							_stringCheese = "EN";
							if( righteous_attr === _stringCheese ){
								
								focus_rightcon = all_righteouscontent[h];
								for ( g = 0; g < rytconLen; g = g + 1) {
									var tempObj_Righteouscon = {};
									if( all_righteouscontent[g] === focus_rightcon ){
										tempObj_Righteouscon.contentattribute = all_righteouscontent[g].getAttribute("clientele");
										tempObj_Righteouscon.curlang = _stringCheese;
										tempObj_Righteouscon.activepage = GetVal(all_righteouscontent[g], "activepage");
										tempObj_Righteouscon.righteouswords = GetVal(all_righteouscontent[g], "righteouswords");
									}
									_ar_RIGHTEOUSCONTENT[g] = tempObj_Righteouscon;
								}
								
							}
						} // end if not cookie
						else
						if( !!( _GetCookie(DEFAULTLANGUAGE) ) ){
							if( !!( _stringCheese ) ){
								_stringCheese = undefined;
							}
							if( righteous_attr === _GetCookie(DEFAULTLANGUAGE) ){
								focus_rightcon = all_righteouscontent[h];
								for ( g = 0; g < rytconLen; g = g + 1) {
									var tempObj_Righteouscon = {};
									if( all_righteouscontent[g] === focus_rightcon ){
										tempObj_Righteouscon.contentattribute = all_righteouscontent[g].getAttribute("clientele");
										tempObj_Righteouscon.curlang = _GetCookie(DEFAULTLANGUAGE);
										tempObj_Righteouscon.activepage = GetVal(all_righteouscontent[g], "activepage");
										tempObj_Righteouscon.righteouswords = GetVal(all_righteouscontent[g], "righteouswords");
									}
									_ar_RIGHTEOUSCONTENT[g] = tempObj_Righteouscon;
								}
							}
						} // end if cookie
					} // END RIGHTEOUS CONTENT
					
					// STATIC CONTENT
					for ( k = 0; k < statconLen; k = k + 1 ) {
						static_attr = all_staticcontent[k].getAttribute("clientele");
						
						if( !( _GetCookie(DEFAULTLANGUAGE) ) ){
							_stringCheese = "EN";
							if( static_attr === _stringCheese ){
								
								for ( o = 0; o < statconLen; o = o + 1) {
									var tempObj_Statcon = {};
									tempObj_Statcon.contentattribute = all_staticcontent[o].getAttribute("clientele");
									tempObj_Statcon.curlang = _stringCheese;
									tempObj_Statcon.courtesygreeting = GetVal(all_staticcontent[o], "courtesygreeting");
									tempObj_Statcon.bcapable = GetVal(all_staticcontent[o], "bcapable");
									tempObj_Statcon.bincapable = GetVal(all_staticcontent[o], "bincapable");
									tempObj_Statcon.closeone = GetVal(all_staticcontent[o], "closeone");
									tempObj_Statcon.silkylinks = GetVal(all_staticcontent[o], "silkylinks");
									tempObj_Statcon.labelhome = GetVal(all_staticcontent[o], "labelhome");
									tempObj_Statcon.labelinspire = GetVal(all_staticcontent[o], "labelinspire");
									tempObj_Statcon.labelenhance = GetVal(all_staticcontent[o], "labelenhance");
									tempObj_Statcon.labeltransform = GetVal(all_staticcontent[o], "labeltransform");
									tempObj_Statcon.labeltransition = GetVal(all_staticcontent[o], "labeltransition");
									tempObj_Statcon.labelgallery = GetVal(all_staticcontent[o], "labelgallery");
									tempObj_Statcon.labeldevice = GetVal(all_staticcontent[o], "labeldevice");
									tempObj_Statcon.labeluntang = GetVal(all_staticcontent[o], "labeluntang");
									tempObj_Statcon.labelinteract = GetVal(all_staticcontent[o], "labelinteract");
									tempObj_Statcon.labelactivities = GetVal(all_staticcontent[o], "labelactivities");
									tempObj_Statcon.labelevents = GetVal(all_staticcontent[o], "labelevents");
									tempObj_Statcon.labelcommunity = GetVal(all_staticcontent[o], "labelcommunity");
									tempObj_Statcon.labeldownloads = GetVal(all_staticcontent[o], "labeldownloads");
									tempObj_Statcon.labelreferences = GetVal(all_staticcontent[o], "labelreferences");
									tempObj_Statcon.labelabout = GetVal(all_staticcontent[o], "labelabout");
									tempObj_Statcon.labelproducts = GetVal(all_staticcontent[o], "labelproducts");
									tempObj_Statcon.labelservices = GetVal(all_staticcontent[o], "labelservices");
									tempObj_Statcon.labelhow = GetVal(all_staticcontent[o], "labelhow");
									tempObj_Statcon.labelsolutions = GetVal(all_staticcontent[o], "labelsolutions");
									tempObj_Statcon.labelcontact = GetVal(all_staticcontent[o], "labelcontact");
									tempObj_Statcon.labelsitemap = GetVal(all_staticcontent[o], "labelsitemap");
									tempObj_Statcon.labelsection = GetVal(all_staticcontent[o], "labelsection");
									tempObj_Statcon.labelaside = GetVal(all_staticcontent[o], "labelaside");
									tempObj_Statcon.labelrightcolumn = GetVal(all_staticcontent[o], "labelrightcolumn");
									tempObj_Statcon.labelfooter = GetVal(all_staticcontent[o], "labelfooter");
									tempObj_Statcon.dz1a = GetVal(all_staticcontent[o], "dz1a");
									tempObj_Statcon.dz1b = GetVal(all_staticcontent[o], "dz1b");
									tempObj_Statcon.dz1c = GetVal(all_staticcontent[o], "dz1c");
									tempObj_Statcon.dz1d = GetVal(all_staticcontent[o], "dz1d");
									tempObj_Statcon.dz2a = GetVal(all_staticcontent[o], "dz2a");
									tempObj_Statcon.dz2b = GetVal(all_staticcontent[o], "dz2b");
									tempObj_Statcon.jbza = GetVal(all_staticcontent[o], "jbza");
									tempObj_Statcon.jbzb = GetVal(all_staticcontent[o], "jbzb");
									tempObj_Statcon.jbzc = GetVal(all_staticcontent[o], "jbzc");
									tempObj_Statcon.jbzd = GetVal(all_staticcontent[o], "jbzd");
									tempObj_Statcon.fslaunch = GetVal(all_staticcontent[o], "fslaunch");
									tempObj_Statcon.fsrestore = GetVal(all_staticcontent[o], "fsrestore");
									tempObj_Statcon.cnva = GetVal(all_staticcontent[o], "cnva");
									tempObj_Statcon.cnvb = GetVal(all_staticcontent[o], "cnvb");
									tempObj_Statcon.cnvc = GetVal(all_staticcontent[o], "cnvc");
									tempObj_Statcon.cnvd = GetVal(all_staticcontent[o], "cnvd");
									tempObj_Statcon.findlabel = GetVal(all_staticcontent[o], "findlabel");
									tempObj_Statcon.watchlabel = GetVal(all_staticcontent[o], "watchlabel");
									tempObj_Statcon.clearlabel = GetVal(all_staticcontent[o], "clearlabel");
									tempObj_Statcon.trylabel = GetVal(all_staticcontent[o], "trylabel");
									tempObj_Statcon.gloe = GetVal(all_staticcontent[o], "gloe");
									tempObj_Statcon.gloa = GetVal(all_staticcontent[o], "gloa");
									tempObj_Statcon.glob = GetVal(all_staticcontent[o], "glob");
									tempObj_Statcon.gloc = GetVal(all_staticcontent[o], "gloc");
									tempObj_Statcon.glod = GetVal(all_staticcontent[o], "glod");
									tempObj_Statcon.draglabel = GetVal(all_staticcontent[o], "draglabel");
									tempObj_Statcon.fda = GetVal(all_staticcontent[o], "fda");
									tempObj_Statcon.fdb = GetVal(all_staticcontent[o], "fdb");
									tempObj_Statcon.fdc = GetVal(all_staticcontent[o], "fdc");
									tempObj_Statcon.fdd = GetVal(all_staticcontent[o], "fdd");
									tempObj_Statcon.opnlabel = GetVal(all_staticcontent[o], "opnlabel");
									tempObj_Statcon.clslabel = GetVal(all_staticcontent[o], "clslabel");
									tempObj_Statcon.fha = GetVal(all_staticcontent[o], "fha");
									tempObj_Statcon.fhb = GetVal(all_staticcontent[o], "fhb");
									tempObj_Statcon.fhc = GetVal(all_staticcontent[o], "fhc");
									tempObj_Statcon.fhd = GetVal(all_staticcontent[o], "fhd");
									tempObj_Statcon.retexphotelfour4 = GetVal(all_staticcontent[o], "retexphotelfour4");
									tempObj_Statcon.partners = GetVal(all_staticcontent[o], "partners");
									_ar_STATICCONTENT[o] = tempObj_Statcon;
								}
							}
						} // end if local storage not initiated
						else
						if( !!( _GetCookie(DEFAULTLANGUAGE) ) ){
							if( !!( _stringCheese ) ){
								_stringCheese = undefined;
							}
							if( static_attr === _GetCookie(DEFAULTLANGUAGE) ){
								
								for ( o = 0; o < statconLen; o = o + 1) {
									var tempObj_Statcon = {};
									tempObj_Statcon.contentattribute = all_staticcontent[o].getAttribute("clientele");
									tempObj_Statcon.curlang = _GetCookie(DEFAULTLANGUAGE);
									tempObj_Statcon.courtesygreeting = GetVal(all_staticcontent[o], "courtesygreeting");
									tempObj_Statcon.bcapable = GetVal(all_staticcontent[o], "bcapable");
									tempObj_Statcon.bincapable = GetVal(all_staticcontent[o], "bincapable");
									tempObj_Statcon.closeone = GetVal(all_staticcontent[o], "closeone");
									tempObj_Statcon.silkylinks = GetVal(all_staticcontent[o], "silkylinks");
									tempObj_Statcon.labelhome = GetVal(all_staticcontent[o], "labelhome");
									tempObj_Statcon.labelinspire = GetVal(all_staticcontent[o], "labelinspire");
									tempObj_Statcon.labelenhance = GetVal(all_staticcontent[o], "labelenhance");
									tempObj_Statcon.labeltransform = GetVal(all_staticcontent[o], "labeltransform");
									tempObj_Statcon.labeltransition = GetVal(all_staticcontent[o], "labeltransition");
									tempObj_Statcon.labelgallery = GetVal(all_staticcontent[o], "labelgallery");
									tempObj_Statcon.labeldevice = GetVal(all_staticcontent[o], "labeldevice");
									tempObj_Statcon.labeluntang = GetVal(all_staticcontent[o], "labeluntang");
									tempObj_Statcon.labelinteract = GetVal(all_staticcontent[o], "labelinteract");
									tempObj_Statcon.labelactivities = GetVal(all_staticcontent[o], "labelactivities");
									tempObj_Statcon.labelevents = GetVal(all_staticcontent[o], "labelevents");
									tempObj_Statcon.labelcommunity = GetVal(all_staticcontent[o], "labelcommunity");
									tempObj_Statcon.labeldownloads = GetVal(all_staticcontent[o], "labeldownloads");
									tempObj_Statcon.labelreferences = GetVal(all_staticcontent[o], "labelreferences");
									tempObj_Statcon.labelabout = GetVal(all_staticcontent[o], "labelabout");
									tempObj_Statcon.labelproducts = GetVal(all_staticcontent[o], "labelproducts");
									tempObj_Statcon.labelservices = GetVal(all_staticcontent[o], "labelservices");
									tempObj_Statcon.labelhow = GetVal(all_staticcontent[o], "labelhow");
									tempObj_Statcon.labelsolutions = GetVal(all_staticcontent[o], "labelsolutions");
									tempObj_Statcon.labelcontact = GetVal(all_staticcontent[o], "labelcontact");
									tempObj_Statcon.labelsitemap = GetVal(all_staticcontent[o], "labelsitemap");
									tempObj_Statcon.labelsection = GetVal(all_staticcontent[o], "labelsection");
									tempObj_Statcon.labelaside = GetVal(all_staticcontent[o], "labelaside");
									tempObj_Statcon.labelrightcolumn = GetVal(all_staticcontent[o], "labelrightcolumn");
									tempObj_Statcon.labelfooter = GetVal(all_staticcontent[o], "labelfooter");
									tempObj_Statcon.dz1a = GetVal(all_staticcontent[o], "dz1a");
									tempObj_Statcon.dz1b = GetVal(all_staticcontent[o], "dz1b");
									tempObj_Statcon.dz1c = GetVal(all_staticcontent[o], "dz1c");
									tempObj_Statcon.dz1d = GetVal(all_staticcontent[o], "dz1d");
									tempObj_Statcon.dz2a = GetVal(all_staticcontent[o], "dz2a");
									tempObj_Statcon.dz2b = GetVal(all_staticcontent[o], "dz2b");
									tempObj_Statcon.jbza = GetVal(all_staticcontent[o], "jbza");
									tempObj_Statcon.jbzb = GetVal(all_staticcontent[o], "jbzb");
									tempObj_Statcon.jbzc = GetVal(all_staticcontent[o], "jbzc");
									tempObj_Statcon.jbzd = GetVal(all_staticcontent[o], "jbzd");
									tempObj_Statcon.fslaunch = GetVal(all_staticcontent[o], "fslaunch");
									tempObj_Statcon.fsrestore = GetVal(all_staticcontent[o], "fsrestore");
									tempObj_Statcon.cnva = GetVal(all_staticcontent[o], "cnva");
									tempObj_Statcon.cnvb = GetVal(all_staticcontent[o], "cnvb");
									tempObj_Statcon.cnvc = GetVal(all_staticcontent[o], "cnvc");
									tempObj_Statcon.cnvd = GetVal(all_staticcontent[o], "cnvd");
									tempObj_Statcon.findlabel = GetVal(all_staticcontent[o], "findlabel");
									tempObj_Statcon.watchlabel = GetVal(all_staticcontent[o], "watchlabel");
									tempObj_Statcon.clearlabel = GetVal(all_staticcontent[o], "clearlabel");
									tempObj_Statcon.trylabel = GetVal(all_staticcontent[o], "trylabel");
									tempObj_Statcon.gloe = GetVal(all_staticcontent[o], "gloe");
									tempObj_Statcon.gloa = GetVal(all_staticcontent[o], "gloa");
									tempObj_Statcon.glob = GetVal(all_staticcontent[o], "glob");
									tempObj_Statcon.gloc = GetVal(all_staticcontent[o], "gloc");
									tempObj_Statcon.glod = GetVal(all_staticcontent[o], "glod");
									tempObj_Statcon.draglabel = GetVal(all_staticcontent[o], "draglabel");
									tempObj_Statcon.fda = GetVal(all_staticcontent[o], "fda");
									tempObj_Statcon.fdb = GetVal(all_staticcontent[o], "fdb");
									tempObj_Statcon.fdc = GetVal(all_staticcontent[o], "fdc");
									tempObj_Statcon.fdd = GetVal(all_staticcontent[o], "fdd");
									tempObj_Statcon.opnlabel = GetVal(all_staticcontent[o], "opnlabel");
									tempObj_Statcon.clslabel = GetVal(all_staticcontent[o], "clslabel");
									tempObj_Statcon.fha = GetVal(all_staticcontent[o], "fha");
									tempObj_Statcon.fhb = GetVal(all_staticcontent[o], "fhb");
									tempObj_Statcon.fhc = GetVal(all_staticcontent[o], "fhc");
									tempObj_Statcon.fhd = GetVal(all_staticcontent[o], "fhd");
									tempObj_Statcon.retexphotelfour4 = GetVal(all_staticcontent[o], "retexphotelfour4");
									tempObj_Statcon.partners = GetVal(all_staticcontent[o], "partners");
									_ar_STATICCONTENT[o] = tempObj_Statcon;
								}
							}
						} // end if local storage initiated
					} // end all static content
					
					var n,dynconCt = _ar_DYNAMICCONTENT.length,dContentattribute,dCurlang,dUrlstringattribute,dContento,dPubdate,dSechredcaps,dTodolistalt,dCtaimgalt,dCtaimg,dFigcaption,dCaptionimgalt,dCaptionimg,dSechthree,dSecpma,dSecpsa,dSecpmlrb,dSecpmlrc,dSecpmd,dSecpdryz,dAsidea,dAsideb,dAsidec,dAsideimgaalt,dAsideimga,dAsided,dAsidee,dAsidef,dAsideimgbalt,dAsideimgb,dAsideg,dAsideh,dAsidei,dAsideimgcalt,dAsideimgc,dLikea,dLikeb,dLikec,dWorkinprogress;
					
					var XX,nonpareilconCt = _ar_NONPAREILCONTENT.length,npContentattribute,npCurlang,npUrlattribute,npXaxis,npYaxis,npZaxis,npCubeback,npCubefront,npCuberight,npCubeleft,npCubebottom,npCubetop,npLgdfsrox,npLblfsrox,npLgdfsroy,npLblfsroy,npLgdfsopnclo,npOpenitvalue,npCloseitvalue,npFbzhxa,npFbzhxb,npFbzp1,npFbzp2,npBbzli1,npBbzli2,npBbzli3,npBbzli4,npFlp1,npFlp2,npBlp3,npBlp4,npBlp5,npTranhxa,npTranlr,npTranhxb,npTranpa,npTranhxc,npTranovni,npDvcpa,npDvcpb,npDvcpc,npDvcpd,npDvchxa,npDvchxb,npDvchxc,npDvchxd,npDvcpanoramic,npDvcportrait,npDvchxe,npDvchxf,npDvchxg,npDvchxh,npDvchxi,npDvchxj,npDvchxk,npDvchxl,npDvcmh2,npDvcmh3,npDvcoh2,npDvcoh3,npMvlo,npMvrg,npMvtm,npMvbp;
					
					
					var rightconCt = _ar_RIGHTEOUSCONTENT.length;
					var f,rContentattribute,rCurlang,rActivepage,rRighteouswords;
					
					var w,statconCt = _ar_STATICCONTENT.length,sContentattribute,sCurlang,sCourtesygreeting,sBcapable,sBincapable,sCloseone,sSilkylinks,sLabelhome,sLabelinspire,sLabelenhance,sLabeltransform,sLabeltransition,sLabelgallery,sLabeldevice,sLabeluntang,sLabelinteract,sLabelactivities,sLabelevents,sLabelcommunity,sLabeldownloads,sLabelreferences,sLabelabout,sLabelproducts,sLabelservices,sLabelhow,sLabelsolutions,sLabelcontact,sLabelsitemap,sLabelsection,sLabelaside,sLabelrightcolumn,sLabelfooter,sDz1a,sDz1b,sDz1c,sDz1d,sDz2a,sDz2b,sJbza,sJbzb,sJbzc,sJbzd,sFslaunch,sFsrestore,sCnva,sCnvb,sCnvc,sCnvd,sFindlabel,sWatchlabel,sClearlabel,sTrylabel,sAcclabel,sLatlabel,sLonlabel,sGloe,sGloa,sGlob,sGloc,sGlod,sDraglabel,sFda,sFdb,sFdc,sFdd,sOpnlabel,sClslabel,sFha,sFhb,sFhc,sFhd,sRetexphotelfour4,sPartners;
					
					// consequent formatting and presentation loops
					/* _el_loadicon.style.display = "none"; */
					
					// DYNAMIC CONTENT
					for( n = 0; n < dynconCt; n = n + 1 ){
						if( ( _actv_Page === _ar_DYNAMICCONTENT[n].activepage ) || ( _actv_Page === _ar_DYNAMICCONTENT[n].urlstringattribute ) ){
							dContentattribute = _ar_DYNAMICCONTENT[n].contentattribute;
							dUrlstringattribute = _ar_DYNAMICCONTENT[n].urlstringattribute;
							dCurlang = _ar_DYNAMICCONTENT[n].curlang;
							dContento = _ar_DYNAMICCONTENT[n].contento;
							dPubdate = _ar_DYNAMICCONTENT[n].pubdate;
							dSechredcaps = _ar_DYNAMICCONTENT[n].sechredcaps;
							dTodolistalt = _ar_DYNAMICCONTENT[n].todolistalt;
							dCtaimgalt = _ar_DYNAMICCONTENT[n].ctaimgalt;
							dCtaimg = _ar_DYNAMICCONTENT[n].ctaimg;
							dFigcaption = _ar_DYNAMICCONTENT[n].figcaption;
							dCaptionimgalt = _ar_DYNAMICCONTENT[n].captionimgalt;
							dCaptionimg = _ar_DYNAMICCONTENT[n].captionimg;
							dSechthree = _ar_DYNAMICCONTENT[n].sechthree;
							dSecpma = _ar_DYNAMICCONTENT[n].secpma;
							dSecpsa = _ar_DYNAMICCONTENT[n].secpsa;
							dSecpmlrb = _ar_DYNAMICCONTENT[n].secpmlrb;
							dSecpmlrc = _ar_DYNAMICCONTENT[n].secpmlrc;
							dSecpmd = _ar_DYNAMICCONTENT[n].secpmd;
							dSecpdryz = _ar_DYNAMICCONTENT[n].secpdryz;
							dAsidea = _ar_DYNAMICCONTENT[n].asidea;
							dAsideb = _ar_DYNAMICCONTENT[n].asideb;
							dAsidec = _ar_DYNAMICCONTENT[n].asidec;
							dAsideimgaalt = _ar_DYNAMICCONTENT[n].asideimgaalt;
							dAsideimga = _ar_DYNAMICCONTENT[n].asideimga;
							dAsided = _ar_DYNAMICCONTENT[n].asided;
							dAsidee = _ar_DYNAMICCONTENT[n].asidee;
							dAsidef = _ar_DYNAMICCONTENT[n].asidef;
							dAsideimgbalt = _ar_DYNAMICCONTENT[n].asideimgbalt;
							dAsideimgb = _ar_DYNAMICCONTENT[n].asideimgb;
							dAsideg = _ar_DYNAMICCONTENT[n].asideg;
							dAsideh = _ar_DYNAMICCONTENT[n].asideh;
							dAsidei = _ar_DYNAMICCONTENT[n].asidei;
							dAsideimgcalt = _ar_DYNAMICCONTENT[n].asideimgcalt;
							dAsideimgc = _ar_DYNAMICCONTENT[n].asideimgc;
							dLikea = _ar_DYNAMICCONTENT[n].likea;
							dLikeb = _ar_DYNAMICCONTENT[n].likeb;
							dLikec = _ar_DYNAMICCONTENT[n].likec;
							dWorkinprogress = _ar_DYNAMICCONTENT[n].workinprogress;
							
							if( dContentattribute === dCurlang ){
								
								_el_contento.innerHTML += dContento;
								
								if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "enhance.html") || (_actv_Page === "transform.html") || (_actv_Page === "transition.html") || (_actv_Page === "device.html") || (_actv_Page === "interaction.html") || (_actv_Page === "untangle.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") || (_actv_Page === "sitemap.html") || (_actv_Page === "inspiration.html") ){
									if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "enhance.html") || (_actv_Page === "transform.html") || (_actv_Page === "transition.html") || (_actv_Page === "device.html") || (_actv_Page === "interaction.html") || (_actv_Page === "untangle.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") || (_actv_Page === "inspiration.html") ){
										_el_pubdate.innerHTML += dPubdate;
									}
									if( (_actv_Page === "device.html") || (_actv_Page === "enhance.html") ){
										_el_secHredCaps.innerHTML += dSechredcaps;
									}
									if( (_actv_Page === "next.html") ){
										_el_img1.setAttribute( "alt" , dTodolistalt );
										_el_img2.setAttribute( "alt" , dTodolistalt );
										_el_img3.setAttribute( "alt" , dTodolistalt );
										_el_img4.setAttribute( "alt" , dTodolistalt );
										_el_img5.setAttribute( "alt" , dTodolistalt );
										_el_img6.setAttribute( "alt" , dTodolistalt );
										_el_img7.setAttribute( "alt" , dTodolistalt );
										_el_img8.setAttribute( "alt" , dTodolistalt );
									}
									if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "interaction.html") || (_actv_Page === "untangle.html") || (_actv_Page === "contact.html") || (_actv_Page === "inspiration.html") ){
										_el_ctaImg.setAttribute( "alt", dCtaimgalt );
									}
									if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "interaction.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") || (_actv_Page === "inspiration.html") ){
										_el_secHthree.innerHTML += dSechthree;
									}
									if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "enhance.html") || (_actv_Page === "interaction.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") ){
										_el_secPmA.innerHTML += dSecpma;
									}
									if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "enhance.html") || (_actv_Page === "interaction.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") ){
										_el_secPsA.innerHTML += dSecpsa;
									}
									if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "enhance.html") || (_actv_Page === "transform.html") || (_actv_Page === "transition.html") || (_actv_Page === "device.html") || (_actv_Page === "interaction.html") || (_actv_Page === "untangle.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") ){
										_el_secPdryZ.innerHTML += dSecpdryz;
									}
									if( (_actv_Page === "enhance.html") ){
										_el_asideA.innerHTML += dAsidea;
										_el_asideB.innerHTML += dAsideb;
										_el_asideC.innerHTML += dAsidec;
									}
									if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "transform.html") || (_actv_Page === "transition.html") || (_actv_Page === "interaction.html") || (_actv_Page === "untangle.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") || (_actv_Page === "inspiration.html") ){
										_el_asideA.innerHTML += dAsidea;
										_el_asideB.innerHTML += dAsideb;
										_el_asideC.innerHTML += dAsidec;
										_el_asideimgA.setAttribute( "alt" , dAsideimgaalt );
										_el_asideD.innerHTML += dAsided;
										_el_asideE.innerHTML += dAsidee;
										_el_asideF.innerHTML += dAsidef;
										_el_asideimgB.setAttribute( "alt" , dAsideimgaalt );
										_el_asideG.innerHTML += dAsideg;
										_el_asideH.innerHTML += dAsideh;
										_el_asideI.innerHTML += dAsidei;
										_el_asideimgC.setAttribute( "alt" , dAsideimgaalt );
									}
								}
								
								
								
								if( (_actv_Page === "events.html") || (_actv_Page === "references.html") || (_actv_Page === "products.html") || (_actv_Page === "how.html") || (_actv_Page === "solutions.html") || (_actv_Page === "activities.html") || (_actv_Page === "community.html") || (_actv_Page === "downloads.html") || (_actv_Page === "about.html") || (_actv_Page === "services.html") ){
									_el_pubdate.innerHTML += dPubdate;
									_el_figcaption.innerHTML += dFigcaption;
									_el_captionImg.setAttribute( "alt" , dCaptionimgalt );
									_el_secHthree.innerHTML += dSechthree;
									_el_secPmA.innerHTML += dSecpma;
									_el_secPmlrB.innerHTML += dSecpmlrb;
									_el_secPmlrC.innerHTML += dSecpmlrc;
									_el_secPdryZ.innerHTML += dSecpdryz;
									_el_asideA.innerHTML += dAsidea;
									_el_asideB.innerHTML += dAsideb;
									_el_asideC.innerHTML += dAsidec;
									_el_asideimgA.setAttribute( "alt" , dAsideimgaalt );
									_el_asideD.innerHTML += dAsided;
									_el_asideE.innerHTML += dAsidee;
									_el_asideF.innerHTML += dAsidef;
									_el_asideimgB.setAttribute( "alt" , dAsideimgaalt );
									_el_asideG.innerHTML += dAsideg;
									_el_asideH.innerHTML += dAsideh;
									_el_asideI.innerHTML += dAsidei;
									_el_asideimgC.setAttribute( "alt" , dAsideimgaalt );
								}
								if( (_actv_Page === "activities.html") || (_actv_Page === "community.html") || (_actv_Page === "downloads.html") || (_actv_Page === "about.html") || (_actv_Page === "services.html") ){ 
									_el_secPmD.innerHTML += dSecpmd;
								}
								
								_el_likeA.innerHTML += dLikea;
								_el_likeB.innerHTML += dLikeb;
								_el_likeC.innerHTML += dLikec;
								_el_workinProgress.innerHTML += dWorkinprogress;
							}
						}
					} // END DYNAMIC CONTENT
					
					// RIGHTEOUS CONTENT
					for( f = 0; f < rightconCt; f = f + 1 ){
						if( _actv_Page === _ar_RIGHTEOUSCONTENT[f].activepage ){
							rContentattribute = _ar_RIGHTEOUSCONTENT[f].contentattribute;
							rCurlang = _ar_RIGHTEOUSCONTENT[f].curlang;
							rActivepage = _ar_RIGHTEOUSCONTENT[f].activepage;
							rRighteouswords = _ar_RIGHTEOUSCONTENT[f].righteouswords;
							if( rContentattribute === rCurlang ){
								if( rActivepage === "inspiration.html" ){
									_el_righteousWords.innerHTML += rRighteouswords;
								}
							}
							
						} // END if( _actv_Page === _ar_RIGHTEOUSCONTENT[f].activepage )
					} // END RIGHTEOUS CONTENT
					
					// STATIC CONTENT
					for( w = 0; w < statconCt; w = w + 1 ){
						sContentattribute = _ar_STATICCONTENT[w].contentattribute;
						sCurlang = _ar_STATICCONTENT[w].curlang;
						sCourtesygreeting = _ar_STATICCONTENT[w].courtesygreeting;
						sBcapable = _ar_STATICCONTENT[w].bcapable;
						sBincapable = _ar_STATICCONTENT[w].bincapable;
						sCloseone = _ar_STATICCONTENT[w].closeone;
						sSilkylinks = _ar_STATICCONTENT[w].silkylinks;
						sLabelhome = _ar_STATICCONTENT[w].labelhome;
						sLabelinspire = _ar_STATICCONTENT[w].labelinspire;
						sLabelenhance = _ar_STATICCONTENT[w].labelenhance;
						sLabeltransform = _ar_STATICCONTENT[w].labeltransform;
						sLabeltransition = _ar_STATICCONTENT[w].labeltransition;
						sLabelgallery = _ar_STATICCONTENT[w].labelgallery;
						sLabeldevice = _ar_STATICCONTENT[w].labeldevice;
						sLabeluntang = _ar_STATICCONTENT[w].labeluntang;
						sLabelinteract = _ar_STATICCONTENT[w].labelinteract;
						sLabelactivities = _ar_STATICCONTENT[w].labelactivities;
						sLabelevents = _ar_STATICCONTENT[w].labelevents;
						sLabelcommunity = _ar_STATICCONTENT[w].labelcommunity;
						sLabeldownloads = _ar_STATICCONTENT[w].labeldownloads;
						sLabelreferences = _ar_STATICCONTENT[w].labelreferences;
						sLabelabout = _ar_STATICCONTENT[w].labelabout;
						sLabelproducts = _ar_STATICCONTENT[w].labelproducts;
						sLabelservices = _ar_STATICCONTENT[w].labelservices;
						sLabelhow = _ar_STATICCONTENT[w].labelhow;
						sLabelsolutions = _ar_STATICCONTENT[w].labelsolutions;
						sLabelcontact = _ar_STATICCONTENT[w].labelcontact;
						sLabelsitemap = _ar_STATICCONTENT[w].labelsitemap;
						sLabelsection = _ar_STATICCONTENT[w].labelsection;
						sLabelaside = _ar_STATICCONTENT[w].labelaside;
						sLabelrightcolumn = _ar_STATICCONTENT[w].labelrightcolumn;
						sLabelfooter = _ar_STATICCONTENT[w].labelfooter;
						sDz1a = _ar_STATICCONTENT[w].dz1a;
						sDz1b = _ar_STATICCONTENT[w].dz1b;
						sDz1c = _ar_STATICCONTENT[w].dz1c;
						sDz1d = _ar_STATICCONTENT[w].dz1d;
						sDz2a = _ar_STATICCONTENT[w].dz2a;
						sDz2b = _ar_STATICCONTENT[w].dz2b;
						sJbza = _ar_STATICCONTENT[w].jbza;
						sJbzb = _ar_STATICCONTENT[w].jbzb;
						sJbzc = _ar_STATICCONTENT[w].jbzc;
						sJbzd = _ar_STATICCONTENT[w].jbzd;
						sFslaunch = _ar_STATICCONTENT[w].fslaunch;
						sFsrestore = _ar_STATICCONTENT[w].fsrestore;
						sCnva = _ar_STATICCONTENT[w].cnva;
						sCnvb = _ar_STATICCONTENT[w].cnvb;
						sCnvc = _ar_STATICCONTENT[w].cnvc;
						sCnvd = _ar_STATICCONTENT[w].cnvd;
						sFindlabel = _ar_STATICCONTENT[w].findlabel;
						sWatchlabel = _ar_STATICCONTENT[w].watchlabel;
						sClearlabel = _ar_STATICCONTENT[w].clearlabel;
						sTrylabel = _ar_STATICCONTENT[w].trylabel;
						sGloe = _ar_STATICCONTENT[w].gloe;
						sGloa = _ar_STATICCONTENT[w].gloa;
						sGlob = _ar_STATICCONTENT[w].glob;
						sGloc = _ar_STATICCONTENT[w].gloc;
						sGlod = _ar_STATICCONTENT[w].glod;
						sDraglabel = _ar_STATICCONTENT[w].draglabel;
						sFda = _ar_STATICCONTENT[w].fda;
						sFdb = _ar_STATICCONTENT[w].fdb;
						sFdc = _ar_STATICCONTENT[w].fdc;
						sFdd = _ar_STATICCONTENT[w].fdd;
						sOpnlabel = _ar_STATICCONTENT[w].opnlabel;
						sClslabel = _ar_STATICCONTENT[w].clslabel;
						sFha = _ar_STATICCONTENT[w].fha;
						sFhb = _ar_STATICCONTENT[w].fhb;
						sFhc = _ar_STATICCONTENT[w].fhc;
						sFhd = _ar_STATICCONTENT[w].fhd;
						sRetexphotelfour4 = _ar_STATICCONTENT[w].retexphotelfour4;
						sPartners = _ar_STATICCONTENT[w].partners;
						
						if( sContentattribute === sCurlang ){ 
							_el_courtesyGreeting.innerHTML += sCourtesygreeting;
							_el_bCapable.innerHTML += sBcapable;
							_el_bIncapable.innerHTML += sBincapable;
							_el_closeone.innerHTML += sCloseone;
							_el_silky_dL1.innerHTML += sSilkylinks;
							_el_silky_dL2.innerHTML += sSilkylinks;
							_el_silky_dL3.innerHTML += sSilkylinks;
							_el_silky_dL4.innerHTML += sSilkylinks;
							_el_nav_home.innerHTML += sLabelhome;
							_el_foot_home.innerHTML += sLabelhome;
							_el_nav_inspire.innerHTML += sLabelinspire;
							_el_foot_inspire.innerHTML += sLabelinspire;
							_el_nav_enhance.innerHTML += sLabelenhance;
							_el_foot_enhance.innerHTML += sLabelenhance;
							_el_nav_transform.innerHTML += sLabeltransform;
							_el_foot_transform.innerHTML += sLabeltransform;
							_el_nav_transition.innerHTML += sLabeltransition;
							_el_foot_transition.innerHTML += sLabeltransition;
							_el_nav_next.innerHTML += sLabelgallery;
							_el_nav_device.innerHTML += sLabeldevice;
							_el_foot_device.innerHTML += sLabeldevice;
							_el_nav_untang.innerHTML += sLabeluntang;
							_el_foot_untang.innerHTML += sLabeluntang;
							_el_nav_interact.innerHTML += sLabelinteract;
							_el_foot_interact.innerHTML += sLabelinteract;
							_el_nav_activities.innerHTML += sLabelactivities;
							_el_foot_activities.innerHTML += sLabelactivities;
							_el_nav_events.innerHTML += sLabelevents;
							_el_foot_events.innerHTML += sLabelevents;
							_el_nav_community.innerHTML += sLabelcommunity;
							_el_foot_community.innerHTML += sLabelcommunity;
							_el_nav_downloads.innerHTML += sLabeldownloads;
							_el_foot_downloads.innerHTML += sLabeldownloads;
							_el_nav_references.innerHTML += sLabelreferences;
							_el_foot_references.innerHTML += sLabelreferences;
							_el_nav_about.innerHTML += sLabelabout;
							_el_foot_about.innerHTML += sLabelabout;
							_el_nav_products.innerHTML += sLabelproducts;
							_el_foot_products.innerHTML += sLabelproducts;
							_el_nav_services.innerHTML += sLabelservices;
							_el_foot_services.innerHTML += sLabelservices;
							_el_nav_how.innerHTML += sLabelhow;
							_el_foot_how.innerHTML += sLabelhow;
							_el_nav_solutions.innerHTML += sLabelsolutions;
							_el_foot_solutions.innerHTML += sLabelsolutions;
							_el_nav_contact.innerHTML += sLabelcontact;
							_el_foot_contact.innerHTML += sLabelcontact;
							_el_nav_sitemap.innerHTML += sLabelsitemap;
							_el_foot_sitemap.innerHTML += sLabelsitemap;
							_el_sectionContent_dL.innerHTML += sLabelsection;
							_el_asideContent_dL.innerHTML += sLabelaside;
							_el_colRight_dL.innerHTML += sLabelrightcolumn;
							_el_footer_dL.innerHTML += sLabelfooter;
							_el_dz1A.innerHTML += sDz1a;
							_el_dz1B.innerHTML += sDz1b;
							_el_dz1C.innerHTML += sDz1c;
							_el_dz1D.innerHTML += sDz1d;
							_el_dz2A.innerHTML += sDz2a;
							_el_dz2B.innerHTML += sDz2b;
							_el_jbzA.innerHTML += sJbza;
							_el_jbzB.innerHTML += sJbzb;
							_el_jbzC.innerHTML += sJbzc;
							_el_jbzD.innerHTML += sJbzd;
							/*if( _actv_Page !== "next.html"){
								_el_fsLaunch.innerHTML += sFslaunch;
								_el_fsRestore.innerHTML += sFsrestore;
							}*/
							_el_cnvA.innerHTML += sCnva;
							_el_cnvB.innerHTML += sCnvb;
							_el_cnvC.innerHTML += sCnvc;
							_el_cnvD.innerHTML += sCnvd;
							/* _el_gCP_BTN.innerHTML += sFindlabel;
							_el_watPos_BTN.innerHTML += sWatchlabel;
							_el_watPos_CLR_BTN.innerHTML += sClearlabel;
							_el_stat_gCP.innerHTML += sTrylabel;
							_el_stat_watPos.innerHTML += sTrylabel;
							_el_gloE.innerHTML += sGloe;
							_el_gloA.innerHTML += sGloa;
							_el_gloB.innerHTML += sGlob;
							_el_gloC.innerHTML += sGloc;
							_el_gloD.innerHTML += sGlod; */
							_el_Brooke_fxDrg.innerHTML += sDraglabel;
							_el_Franky_fxDrg.innerHTML += sDraglabel;
							_el_Robin_fxDrg.innerHTML += sDraglabel;
							_el_Sanji_fxDrg.innerHTML += sDraglabel;
							_el_Chopper_fxDrg.innerHTML += sDraglabel;
							_el_Usopp_fxDrg.innerHTML += sDraglabel;
							_el_Zoro_fxDrg.innerHTML += sDraglabel;
							_el_Nami_fxDrg.innerHTML += sDraglabel;
							_el_Luffy_fxDrg.innerHTML += sDraglabel;
							_el_fdA.innerHTML += sFda;
							_el_fdB.innerHTML += sFdb;
							_el_fdC.innerHTML += sFdc;
							_el_fdD.innerHTML += sFdd;
							_el_retexpUL1A.innerHTML += sOpnlabel;
							_el_retexpUL2A.innerHTML += sOpnlabel;
							_el_retexpUL3A.innerHTML += sOpnlabel;
							_el_retexpUL4A.innerHTML += sOpnlabel;
							_el_retexpUL1B.innerHTML += sClslabel;
							_el_retexpUL2B.innerHTML += sClslabel;
							_el_retexpUL3B.innerHTML += sClslabel;
							_el_retexpUL4B.innerHTML += sClslabel;
							_el_fha.innerHTML += sFha;
							_el_fhb.innerHTML += sFhb;
							_el_fhc.innerHTML += sFhc;
							_el_fhd.innerHTML += sFhd;
							_el_retexpHotelFour4.innerHTML += sRetexphotelfour4;
							_el_partners.innerHTML += sPartners; 
						} 
						
					} // END STATIC CONTENT
					
					// NONPAREIL CONTENT
					for( XX = 0; XX < nonpareilconCt; XX = XX + 1 ){
						
						if( _actv_Page === _ar_NONPAREILCONTENT[XX].urlattribute ){
							
							npContentattribute = _ar_NONPAREILCONTENT[XX].contentattribute;
							npCurlang = _ar_NONPAREILCONTENT[XX].curlang;
							npUrlattribute = _ar_NONPAREILCONTENT[XX].urlattribute;
							npXaxis = _ar_NONPAREILCONTENT[XX].xaxis; // ENHANCE
							npYaxis = _ar_NONPAREILCONTENT[XX].yaxis;
							npZaxis = _ar_NONPAREILCONTENT[XX].zaxis;
							npCubeback = _ar_NONPAREILCONTENT[XX].cubeback;
							npCubefront = _ar_NONPAREILCONTENT[XX].cubefront;
							npCuberight = _ar_NONPAREILCONTENT[XX].cuberight;
							npCubeleft = _ar_NONPAREILCONTENT[XX].cubeleft;
							npCubebottom = _ar_NONPAREILCONTENT[XX].cubebottom;
							npCubetop = _ar_NONPAREILCONTENT[XX].cubetop;
							npLgdfsrox = _ar_NONPAREILCONTENT[XX].lgdfsrox;
							npLblfsrox = _ar_NONPAREILCONTENT[XX].lblfsrox;
							npLgdfsroy = _ar_NONPAREILCONTENT[XX].lgdfsroy;
							npLblfsroy = _ar_NONPAREILCONTENT[XX].lblfsroy;
							npLgdfsopnclo = _ar_NONPAREILCONTENT[XX].lgdfsopnclo;
							npOpenitvalue = _ar_NONPAREILCONTENT[XX].openitvalue;
							npCloseitvalue = _ar_NONPAREILCONTENT[XX].closeitvalue;
							npFbzhxa = _ar_NONPAREILCONTENT[XX].fbzhxa; // TRANSFORM
							npFbzhxb = _ar_NONPAREILCONTENT[XX].fbzhxb;
							npFbzp1 = _ar_NONPAREILCONTENT[XX].fbzp1;
							npFbzp2 = _ar_NONPAREILCONTENT[XX].fbzp2;
							npBbzli1 = _ar_NONPAREILCONTENT[XX].bbzli1;
							npBbzli2 = _ar_NONPAREILCONTENT[XX].bbzli2;
							npBbzli3 = _ar_NONPAREILCONTENT[XX].bbzli3;
							npBbzli4 = _ar_NONPAREILCONTENT[XX].bbzli4;
							npFlp1 = _ar_NONPAREILCONTENT[XX].flp1;
							npFlp2 = _ar_NONPAREILCONTENT[XX].flp2;
							npBlp3 = _ar_NONPAREILCONTENT[XX].blp3;
							npBlp4 = _ar_NONPAREILCONTENT[XX].blp4;
							npBlp5 = _ar_NONPAREILCONTENT[XX].blp5;
							npTranhxa = _ar_NONPAREILCONTENT[XX].tranhxa; // TRANSITION
							npTranlr = _ar_NONPAREILCONTENT[XX].tranlr;
							npTranhxb = _ar_NONPAREILCONTENT[XX].tranhxb;
							npTranpa = _ar_NONPAREILCONTENT[XX].tranpa;
							npTranhxc = _ar_NONPAREILCONTENT[XX].tranhxc;
							npTranovni = _ar_NONPAREILCONTENT[XX].tranovni;
							npDvcpa = _ar_NONPAREILCONTENT[XX].dvcpa; // DEVICE
							npDvcpb = _ar_NONPAREILCONTENT[XX].dvcpb;
							npDvcpc = _ar_NONPAREILCONTENT[XX].dvcpc;
							npDvcpd = _ar_NONPAREILCONTENT[XX].dvcpd;
							npDvchxa = _ar_NONPAREILCONTENT[XX].dvchxa;
							npDvchxb = _ar_NONPAREILCONTENT[XX].dvchxb;
							npDvchxc = _ar_NONPAREILCONTENT[XX].dvchxc;
							npDvchxd = _ar_NONPAREILCONTENT[XX].dvchxd;
							npDvcpanoramic = _ar_NONPAREILCONTENT[XX].dvcpanoramic;
							npDvcportrait = _ar_NONPAREILCONTENT[XX].dvcportrait;
							npDvchxe = _ar_NONPAREILCONTENT[XX].dvchxe;
							npDvchxf = _ar_NONPAREILCONTENT[XX].dvchxf;
							// npTwoandonehalf = _ar_NONPAREILCONTENT[XX].twoandonehalf;
							npDvchxg = _ar_NONPAREILCONTENT[XX].dvchxg;
							npDvchxh = _ar_NONPAREILCONTENT[XX].dvchxh;
							npDvchxi = _ar_NONPAREILCONTENT[XX].dvchxi;
							npDvchxj = _ar_NONPAREILCONTENT[XX].dvchxj;
							npDvchxk = _ar_NONPAREILCONTENT[XX].dvchxk;
							npDvchxl = _ar_NONPAREILCONTENT[XX].dvchxl;
							npDvcmh2 = _ar_NONPAREILCONTENT[XX].dvcmh2;
							npDvcmh3 = _ar_NONPAREILCONTENT[XX].dvcmh3;
							// npDvcmro = _ar_NONPAREILCONTENT[XX].dvcmro;
							npDvcoh2 = _ar_NONPAREILCONTENT[XX].dvcoh2;
							npDvcoh3 = _ar_NONPAREILCONTENT[XX].dvcoh3;
							// npDvcoro = _ar_NONPAREILCONTENT[XX].dvcoro;
							npMvlo = _ar_NONPAREILCONTENT[XX].mvlo; // UNTANGLE
							npMvrg = _ar_NONPAREILCONTENT[XX].mvrg;
							npMvtm = _ar_NONPAREILCONTENT[XX].mvtm;
							npMvbp = _ar_NONPAREILCONTENT[XX].mvbp;
							
							if( npUrlattribute === "enhance.html" ){ 
								// _el_XAxis.innerHTML += npXaxis;
								// _el_YAxis.innerHTML += npYaxis;
								// _el_ZAxis.innerHTML += npZaxis;
								_el_Back.innerHTML += npCubeback;
								_el_Front.innerHTML += npCubefront;
								_el_Right.innerHTML += npCuberight;
								_el_Left.innerHTML += npCubeleft;
								_el_Bottom.innerHTML += npCubebottom;
								_el_Top.innerHTML += npCubetop;
								_el_lgdFSrox.innerHTML += npLgdfsrox;
								_el_lblFSrox.innerHTML += npLblfsrox;
								_el_lgdFSroy.innerHTML += npLgdfsroy;
								_el_lblFSroy.innerHTML += npLblfsroy;
								_el_lgdFSopnclo.innerHTML += npLgdfsopnclo;
								_el_openIt.value += npOpenitvalue;
								_el_closeIt.value += npCloseitvalue;
							}
							if( npUrlattribute === "transform.html" ){
								_el_fbZhxA.innerHTML += npFbzhxa;
								_el_fbZhxB.innerHTML += npFbzhxb;
								_el_fbZp1.innerHTML += npFbzp1;
								_el_fbZp2.innerHTML += npFbzp2;
								_el_bbZli1.innerHTML += npBbzli1;
								_el_bbZli2.innerHTML += npBbzli2;
								_el_bbZli3.innerHTML += npBbzli3;
								_el_bbZli4.innerHTML += npBbzli4;
								_el_fLp1.innerHTML += npFlp1;
								_el_fLp2.innerHTML += npFlp2;
								_el_bLp3.innerHTML += npBlp3;
								_el_bLp4.innerHTML += npBlp4;
								_el_bLp5.innerHTML += npBlp5;
							}
							if( npUrlattribute === "transition.html" ){
								_el_tranHxA.innerHTML += npTranhxa;
								_el_tranLR.innerHTML += npTranlr;
								_el_tranHxB.innerHTML += npTranhxb;
								_el_tranPa.innerHTML += npTranpa;
								_el_tranHxC.innerHTML += npTranhxc;
								_el_tranOvni.innerHTML += npTranovni;
							}
							if( npUrlattribute === "device.html" ){
								_el_dvcPa.innerHTML += npDvcpa;
								_el_dvcPb.innerHTML += npDvcpb; 
								_el_dvcPc.innerHTML += npDvcpc;
								_el_dvcPd.innerHTML += npDvcpd;
								_el_dvchxA.innerHTML += npDvchxa;
								_el_dvchxB.innerHTML += npDvchxb;
								_el_dvchxC.innerHTML += npDvchxc;
								_el_dvchxD.innerHTML += npDvchxd;
								_el_dvc_panoramic.innerHTML += npDvcpanoramic;
								_el_dvc_portrait.innerHTML += npDvcportrait;
								_el_dvchxE.innerHTML += npDvchxe;
								_el_dvchxF.innerHTML += npDvchxf;
								// _el_dvc_two_one_half.innerHTML += npTwoandonehalf;
								_el_dvchxG.innerHTML += npDvchxg;
								_el_dvchxH.innerHTML += npDvchxh;
								_el_dvchxI.innerHTML += npDvchxi;
								_el_dvchxJ.innerHTML += npDvchxj;
								_el_dvchxK.innerHTML += npDvchxk;
								_el_dvchxL.innerHTML += npDvchxl;
								_el_dvcM_h2.innerHTML += npDvcmh2;
								_el_dvcM_h3.innerHTML += npDvcmh3;
								// _el_dvcM_readOut.innerHTML += npDvcmro;
								_el_dvcO_h2.innerHTML += npDvcoh2;
								_el_dvcO_h3.innerHTML += npDvcoh3;
								// _el_dvcO_readOut.innerHTML += npDvcoro;
							}
							if( npUrlattribute === "untangle.html" ){
								_el_LO.innerHTML += npMvlo;
								_el_RG.innerHTML += npMvrg;
								_el_TM.innerHTML += npMvtm;
								_el_BP.innerHTML += npMvbp;
							}
							
							
						} // END if( _actv_Page === _ar_NONPAREILCONTENT[XX].urlattribute )
					} // END NONPAREIL CONTENT
					
				
				} // END BIG IF...  ACTIVEX RESPONSE
			} // END STATUS 200
			
			else {
				_ActvX_albtrs_KnockedOffline();
				window.alert(_obj_ConfigAll.CannedStrings.ajaxOps.assets.nonFormatString.problems.intro + _actvX_albtrs_Content.status + ". ");
			} 
		}
		function GetVal(pmData, pmTag) { 
			return pmData.getElementsByTagName(pmTag)[0].firstChild.nodeValue;
		}
	} // END function
	
	// XHR ~ Content RESPONSE OBJ
	function _Xhr_Content_xml_Resp() {
		if (this.readyState === 4) {
			if (this.status === 200) {
				if( this.responseXML ){
					
					// DYNAMIC CONTENT
					var all_dynamiccontent = this.responseXML.getElementsByTagName("dynamiccontent"); 
					var dynconLen = all_dynamiccontent.length; 
					var l;
					var dynamic_attr;
					var m;
					
					// RIGHTEOUS CONTENT
					var all_righteouscontent = this.responseXML.getElementsByTagName("righteouscontent");
					var rytconLen = all_righteouscontent.length;
					var g;
					var focus_rightcon;
					var righteous_attr;
					var h;
					
					// STATIC CONTENT
					var all_staticcontent = this.responseXML.getElementsByTagName("staticcontent");
					var statconLen = all_staticcontent.length;
					var o;
					var static_attr;
					var k;
					
					// NONPAREIL CONTENT
					var all_nonpareilcontent = this.responseXML.getElementsByTagName("nonpareilcontent"); 
					var nonpareilconLen = all_nonpareilcontent.length;
					var ZZ;
					var focus_nonpareilcon;
					var nonpareil_attr;
					var url_attr;
					var YY;
					
					// NONPAREIL CONTENT
					for ( YY = 0; YY < nonpareilconLen; YY = YY + 1) {
						
						nonpareil_attr = all_nonpareilcontent[YY].getAttribute("clientele");
						url_attr = all_nonpareilcontent[YY].getAttribute("url");
						
						if( !( _obj_locsto_lang.getItem("defaultLanguage") ) ){
							_stringCheese = "EN";
							if( ( nonpareil_attr === _stringCheese ) && ( url_attr === _actv_Page ) ){
								focus_nonpareilcon = all_nonpareilcontent[YY];
								for ( ZZ = 0; ZZ < nonpareilconLen; ZZ = ZZ + 1) {
									var tempObj_Nonpareilcon = {};
									if( all_nonpareilcontent[ZZ] === focus_nonpareilcon ){
										tempObj_Nonpareilcon.contentattribute = all_nonpareilcontent[ZZ].getAttribute("clientele");
										tempObj_Nonpareilcon.curlang = _stringCheese;
										tempObj_Nonpareilcon.urlattribute = all_nonpareilcontent[ZZ].getAttribute("url");
										
										// ENHANCE
										tempObj_Nonpareilcon.xaxis = GetVal(all_nonpareilcontent[ZZ], "xaxis");
										tempObj_Nonpareilcon.yaxis = GetVal(all_nonpareilcontent[ZZ], "yaxis");
										tempObj_Nonpareilcon.zaxis = GetVal(all_nonpareilcontent[ZZ], "zaxis");
										tempObj_Nonpareilcon.cubeback = GetVal(all_nonpareilcontent[ZZ], "cubeback");
										tempObj_Nonpareilcon.cubefront = GetVal(all_nonpareilcontent[ZZ], "cubefront");
										tempObj_Nonpareilcon.cuberight = GetVal(all_nonpareilcontent[ZZ], "cuberight");
										tempObj_Nonpareilcon.cubeleft = GetVal(all_nonpareilcontent[ZZ], "cubeleft");
										tempObj_Nonpareilcon.cubebottom = GetVal(all_nonpareilcontent[ZZ], "cubebottom");
										tempObj_Nonpareilcon.cubetop = GetVal(all_nonpareilcontent[ZZ], "cubetop");
										tempObj_Nonpareilcon.lgdfsrox = GetVal(all_nonpareilcontent[ZZ], "lgdfsrox");
										tempObj_Nonpareilcon.lblfsrox = GetVal(all_nonpareilcontent[ZZ], "lblfsrox");
										tempObj_Nonpareilcon.lgdfsroy = GetVal(all_nonpareilcontent[ZZ], "lgdfsroy");
										tempObj_Nonpareilcon.lblfsroy = GetVal(all_nonpareilcontent[ZZ], "lblfsroy");
										tempObj_Nonpareilcon.lgdfsopnclo = GetVal(all_nonpareilcontent[ZZ], "lgdfsopnclo");
										tempObj_Nonpareilcon.openitvalue = GetVal(all_nonpareilcontent[ZZ], "openitvalue");
										tempObj_Nonpareilcon.closeitvalue = GetVal(all_nonpareilcontent[ZZ], "closeitvalue");
										// TRANSFORM
										tempObj_Nonpareilcon.fbzhxa = GetVal(all_nonpareilcontent[ZZ], "fbzhxa");
										tempObj_Nonpareilcon.fbzhxb = GetVal(all_nonpareilcontent[ZZ], "fbzhxb");
										tempObj_Nonpareilcon.fbzp1 = GetVal(all_nonpareilcontent[ZZ], "fbzp1");
										tempObj_Nonpareilcon.fbzp2 = GetVal(all_nonpareilcontent[ZZ], "fbzp2");
										tempObj_Nonpareilcon.bbzli1 = GetVal(all_nonpareilcontent[ZZ], "bbzli1");
										tempObj_Nonpareilcon.bbzli2 = GetVal(all_nonpareilcontent[ZZ], "bbzli2");
										tempObj_Nonpareilcon.bbzli3 = GetVal(all_nonpareilcontent[ZZ], "bbzli3");
										tempObj_Nonpareilcon.bbzli4 = GetVal(all_nonpareilcontent[ZZ], "bbzli4");
										tempObj_Nonpareilcon.flp1 = GetVal(all_nonpareilcontent[ZZ], "flp1");
										tempObj_Nonpareilcon.flp2 = GetVal(all_nonpareilcontent[ZZ], "flp2");
										tempObj_Nonpareilcon.blp3 = GetVal(all_nonpareilcontent[ZZ], "blp3");
										tempObj_Nonpareilcon.blp4 = GetVal(all_nonpareilcontent[ZZ], "blp4");
										tempObj_Nonpareilcon.blp5 = GetVal(all_nonpareilcontent[ZZ], "blp5");
										// TRANSITION
										tempObj_Nonpareilcon.tranhxa = GetVal(all_nonpareilcontent[ZZ], "tranhxa");
										tempObj_Nonpareilcon.tranlr = GetVal(all_nonpareilcontent[ZZ], "tranlr");
										tempObj_Nonpareilcon.tranhxb = GetVal(all_nonpareilcontent[ZZ], "tranhxb");
										tempObj_Nonpareilcon.tranpa = GetVal(all_nonpareilcontent[ZZ], "tranpa");
										tempObj_Nonpareilcon.tranhxc = GetVal(all_nonpareilcontent[ZZ], "tranhxc");
										tempObj_Nonpareilcon.tranovni = GetVal(all_nonpareilcontent[ZZ], "tranovni");
										// DEVICE
										tempObj_Nonpareilcon.dvcpa = GetVal(all_nonpareilcontent[ZZ], "dvcpa");
										tempObj_Nonpareilcon.dvcpb = GetVal(all_nonpareilcontent[ZZ], "dvcpb");
										tempObj_Nonpareilcon.dvcpc = GetVal(all_nonpareilcontent[ZZ], "dvcpc");
										tempObj_Nonpareilcon.dvcpd = GetVal(all_nonpareilcontent[ZZ], "dvcpd");
										tempObj_Nonpareilcon.dvchxa = GetVal(all_nonpareilcontent[ZZ], "dvchxa");
										tempObj_Nonpareilcon.dvchxb = GetVal(all_nonpareilcontent[ZZ], "dvchxb");
										tempObj_Nonpareilcon.dvchxc = GetVal(all_nonpareilcontent[ZZ], "dvchxc");
										tempObj_Nonpareilcon.dvchxd = GetVal(all_nonpareilcontent[ZZ], "dvchxd");
										tempObj_Nonpareilcon.dvcpanoramic = GetVal(all_nonpareilcontent[ZZ], "dvcpanoramic");
										tempObj_Nonpareilcon.dvcportrait = GetVal(all_nonpareilcontent[ZZ], "dvcportrait");
										tempObj_Nonpareilcon.dvchxe = GetVal(all_nonpareilcontent[ZZ], "dvchxe");
										tempObj_Nonpareilcon.dvchxf = GetVal(all_nonpareilcontent[ZZ], "dvchxf");
										// tempObj_Nonpareilcon.twoandonehalf = GetVal(all_nonpareilcontent[ZZ], "twoandonehalf");
										tempObj_Nonpareilcon.dvchxg = GetVal(all_nonpareilcontent[ZZ], "dvchxg");
										tempObj_Nonpareilcon.dvchxh = GetVal(all_nonpareilcontent[ZZ], "dvchxh");
										tempObj_Nonpareilcon.dvchxi = GetVal(all_nonpareilcontent[ZZ], "dvchxi");
										tempObj_Nonpareilcon.dvchxj = GetVal(all_nonpareilcontent[ZZ], "dvchxj");
										tempObj_Nonpareilcon.dvchxk = GetVal(all_nonpareilcontent[ZZ], "dvchxk");
										tempObj_Nonpareilcon.dvchxl = GetVal(all_nonpareilcontent[ZZ], "dvchxl");
										tempObj_Nonpareilcon.dvcmh2 = GetVal(all_nonpareilcontent[ZZ], "dvcmh2");
										tempObj_Nonpareilcon.dvcmh3 = GetVal(all_nonpareilcontent[ZZ], "dvcmh3");
										// tempObj_Nonpareilcon.dvcmro= GetVal(all_nonpareilcontent[ZZ], "dvcmro");
										tempObj_Nonpareilcon.dvcoh2 = GetVal(all_nonpareilcontent[ZZ], "dvcoh2");
										tempObj_Nonpareilcon.dvcoh3 = GetVal(all_nonpareilcontent[ZZ], "dvcoh3");
										// tempObj_Nonpareilcon.dvcoro = GetVal(all_nonpareilcontent[ZZ], "dvcoro");
										// UNTANGLE
										tempObj_Nonpareilcon.mvlo = GetVal(all_nonpareilcontent[ZZ], "mvlo");
										tempObj_Nonpareilcon.mvrg = GetVal(all_nonpareilcontent[ZZ], "mvrg");
										tempObj_Nonpareilcon.mvtm = GetVal(all_nonpareilcontent[ZZ], "mvtm");
										tempObj_Nonpareilcon.mvbp = GetVal(all_nonpareilcontent[ZZ], "mvbp");
									}
									_ar_NONPAREILCONTENT[ZZ] = tempObj_Nonpareilcon;
								}
								
							}
						}
						else
						if( !!( _obj_locsto_lang.getItem("defaultLanguage") ) ){
							if( !!( _stringCheese ) ){
								_stringCheese = undefined;
							}
							if( ( nonpareil_attr === _obj_locsto_lang.getItem("defaultLanguage") ) && ( url_attr === _actv_Page ) ){
								focus_nonpareilcon = all_nonpareilcontent[YY];
								for ( ZZ = 0; ZZ < nonpareilconLen; ZZ = ZZ + 1) {
									var tempObj_Nonpareilcon = {};
									if( all_nonpareilcontent[ZZ] === focus_nonpareilcon ){
										tempObj_Nonpareilcon.contentattribute = all_nonpareilcontent[ZZ].getAttribute("clientele");
										tempObj_Nonpareilcon.curlang = _obj_locsto_lang.getItem("defaultLanguage");
										tempObj_Nonpareilcon.urlattribute = all_nonpareilcontent[ZZ].getAttribute("url");
										
										// ENHANCE
										tempObj_Nonpareilcon.xaxis = GetVal(all_nonpareilcontent[ZZ], "xaxis");
										tempObj_Nonpareilcon.yaxis = GetVal(all_nonpareilcontent[ZZ], "yaxis");
										tempObj_Nonpareilcon.zaxis = GetVal(all_nonpareilcontent[ZZ], "zaxis");
										tempObj_Nonpareilcon.cubeback = GetVal(all_nonpareilcontent[ZZ], "cubeback");
										tempObj_Nonpareilcon.cubefront = GetVal(all_nonpareilcontent[ZZ], "cubefront");
										tempObj_Nonpareilcon.cuberight = GetVal(all_nonpareilcontent[ZZ], "cuberight");
										tempObj_Nonpareilcon.cubeleft = GetVal(all_nonpareilcontent[ZZ], "cubeleft");
										tempObj_Nonpareilcon.cubebottom = GetVal(all_nonpareilcontent[ZZ], "cubebottom");
										tempObj_Nonpareilcon.cubetop = GetVal(all_nonpareilcontent[ZZ], "cubetop");
										tempObj_Nonpareilcon.lgdfsrox = GetVal(all_nonpareilcontent[ZZ], "lgdfsrox");
										tempObj_Nonpareilcon.lblfsrox = GetVal(all_nonpareilcontent[ZZ], "lblfsrox");
										tempObj_Nonpareilcon.lgdfsroy = GetVal(all_nonpareilcontent[ZZ], "lgdfsroy");
										tempObj_Nonpareilcon.lblfsroy = GetVal(all_nonpareilcontent[ZZ], "lblfsroy");
										tempObj_Nonpareilcon.lgdfsopnclo = GetVal(all_nonpareilcontent[ZZ], "lgdfsopnclo");
										tempObj_Nonpareilcon.openitvalue = GetVal(all_nonpareilcontent[ZZ], "openitvalue");
										tempObj_Nonpareilcon.closeitvalue = GetVal(all_nonpareilcontent[ZZ], "closeitvalue");
										// TRANSFORM
										tempObj_Nonpareilcon.fbzhxa = GetVal(all_nonpareilcontent[ZZ], "fbzhxa");
										tempObj_Nonpareilcon.fbzhxb = GetVal(all_nonpareilcontent[ZZ], "fbzhxb");
										tempObj_Nonpareilcon.fbzp1 = GetVal(all_nonpareilcontent[ZZ], "fbzp1");
										tempObj_Nonpareilcon.fbzp2 = GetVal(all_nonpareilcontent[ZZ], "fbzp2");
										tempObj_Nonpareilcon.bbzli1 = GetVal(all_nonpareilcontent[ZZ], "bbzli1");
										tempObj_Nonpareilcon.bbzli2 = GetVal(all_nonpareilcontent[ZZ], "bbzli2");
										tempObj_Nonpareilcon.bbzli3 = GetVal(all_nonpareilcontent[ZZ], "bbzli3");
										tempObj_Nonpareilcon.bbzli4 = GetVal(all_nonpareilcontent[ZZ], "bbzli4");
										tempObj_Nonpareilcon.flp1 = GetVal(all_nonpareilcontent[ZZ], "flp1");
										tempObj_Nonpareilcon.flp2 = GetVal(all_nonpareilcontent[ZZ], "flp2");
										tempObj_Nonpareilcon.blp3 = GetVal(all_nonpareilcontent[ZZ], "blp3");
										tempObj_Nonpareilcon.blp4 = GetVal(all_nonpareilcontent[ZZ], "blp4");
										tempObj_Nonpareilcon.blp5 = GetVal(all_nonpareilcontent[ZZ], "blp5");
										// TRANSITION
										tempObj_Nonpareilcon.tranhxa = GetVal(all_nonpareilcontent[ZZ], "tranhxa");
										tempObj_Nonpareilcon.tranlr = GetVal(all_nonpareilcontent[ZZ], "tranlr");
										tempObj_Nonpareilcon.tranhxb = GetVal(all_nonpareilcontent[ZZ], "tranhxb");
										tempObj_Nonpareilcon.tranpa = GetVal(all_nonpareilcontent[ZZ], "tranpa");
										tempObj_Nonpareilcon.tranhxc = GetVal(all_nonpareilcontent[ZZ], "tranhxc");
										tempObj_Nonpareilcon.tranovni = GetVal(all_nonpareilcontent[ZZ], "tranovni");
										// DEVICE
										tempObj_Nonpareilcon.dvcpa = GetVal(all_nonpareilcontent[ZZ], "dvcpa");
										tempObj_Nonpareilcon.dvcpb = GetVal(all_nonpareilcontent[ZZ], "dvcpb");
										tempObj_Nonpareilcon.dvcpc = GetVal(all_nonpareilcontent[ZZ], "dvcpc");
										tempObj_Nonpareilcon.dvcpd = GetVal(all_nonpareilcontent[ZZ], "dvcpd");
										tempObj_Nonpareilcon.dvchxa = GetVal(all_nonpareilcontent[ZZ], "dvchxa");
										tempObj_Nonpareilcon.dvchxb = GetVal(all_nonpareilcontent[ZZ], "dvchxb");
										tempObj_Nonpareilcon.dvchxc = GetVal(all_nonpareilcontent[ZZ], "dvchxc");
										tempObj_Nonpareilcon.dvchxd = GetVal(all_nonpareilcontent[ZZ], "dvchxd");
										tempObj_Nonpareilcon.dvcpanoramic = GetVal(all_nonpareilcontent[ZZ], "dvcpanoramic");
										tempObj_Nonpareilcon.dvcportrait = GetVal(all_nonpareilcontent[ZZ], "dvcportrait");
										tempObj_Nonpareilcon.dvchxe = GetVal(all_nonpareilcontent[ZZ], "dvchxe");
										tempObj_Nonpareilcon.dvchxf = GetVal(all_nonpareilcontent[ZZ], "dvchxf");
										// tempObj_Nonpareilcon.twoandonehalf = GetVal(all_nonpareilcontent[ZZ], "twoandonehalf");
										tempObj_Nonpareilcon.dvchxg = GetVal(all_nonpareilcontent[ZZ], "dvchxg");
										tempObj_Nonpareilcon.dvchxh = GetVal(all_nonpareilcontent[ZZ], "dvchxh");
										tempObj_Nonpareilcon.dvchxi = GetVal(all_nonpareilcontent[ZZ], "dvchxi");
										tempObj_Nonpareilcon.dvchxj = GetVal(all_nonpareilcontent[ZZ], "dvchxj");
										tempObj_Nonpareilcon.dvchxk = GetVal(all_nonpareilcontent[ZZ], "dvchxk");
										tempObj_Nonpareilcon.dvchxl = GetVal(all_nonpareilcontent[ZZ], "dvchxl");
										tempObj_Nonpareilcon.dvcmh2 = GetVal(all_nonpareilcontent[ZZ], "dvcmh2");
										tempObj_Nonpareilcon.dvcmh3 = GetVal(all_nonpareilcontent[ZZ], "dvcmh3");
										// tempObj_Nonpareilcon.dvcmro= GetVal(all_nonpareilcontent[ZZ], "dvcmro");
										tempObj_Nonpareilcon.dvcoh2 = GetVal(all_nonpareilcontent[ZZ], "dvcoh2");
										tempObj_Nonpareilcon.dvcoh3 = GetVal(all_nonpareilcontent[ZZ], "dvcoh3");
										// tempObj_Nonpareilcon.dvcoro = GetVal(all_nonpareilcontent[ZZ], "dvcoro");
										// UNTANGLE
										tempObj_Nonpareilcon.mvlo = GetVal(all_nonpareilcontent[ZZ], "mvlo");
										tempObj_Nonpareilcon.mvrg = GetVal(all_nonpareilcontent[ZZ], "mvrg");
										tempObj_Nonpareilcon.mvtm = GetVal(all_nonpareilcontent[ZZ], "mvtm");
										tempObj_Nonpareilcon.mvbp = GetVal(all_nonpareilcontent[ZZ], "mvbp");
									}
									_ar_NONPAREILCONTENT[ZZ] = tempObj_Nonpareilcon; 
								}
							}
						}
					}
					
					
					// DYNAMIC CONTENT
					for ( m = 0; m < dynconLen; m = m + 1 ) {
						dynamic_attr = all_dynamiccontent[m].getAttribute("clientele");
						if( !( _obj_locsto_lang.getItem("defaultLanguage") ) ){
							_stringCheese = "EN";
							if( dynamic_attr === _stringCheese ){
								
								for ( l = 0; l < dynconLen; l = l + 1) {
									var tempObj_Dyncon = {};
									tempObj_Dyncon.contentattribute = all_dynamiccontent[l].getAttribute("clientele");
									tempObj_Dyncon.urlstringattribute = all_dynamiccontent[l].getAttribute("urlstring");
									tempObj_Dyncon.curlang = _stringCheese;
									tempObj_Dyncon.activepage = GetVal(all_dynamiccontent[l], "activepage");
									tempObj_Dyncon.contento = GetVal(all_dynamiccontent[l], "contento");
									tempObj_Dyncon.pubdate = GetVal(all_dynamiccontent[l], "pubdate");
									tempObj_Dyncon.sechredcaps = GetVal(all_dynamiccontent[l], "sechredcaps");
									tempObj_Dyncon.todolistalt = GetVal(all_dynamiccontent[l], "todolistalt");
									tempObj_Dyncon.ctaimgalt = GetVal(all_dynamiccontent[l], "ctaimgalt");
									tempObj_Dyncon.ctaimg = GetVal(all_dynamiccontent[l], "ctaimg");
									tempObj_Dyncon.figcaption = GetVal(all_dynamiccontent[l], "figcaption");
									tempObj_Dyncon.captionimgalt = GetVal(all_dynamiccontent[l], "captionimgalt");
									tempObj_Dyncon.captionimg = GetVal(all_dynamiccontent[l], "captionimg");
									tempObj_Dyncon.sechthree = GetVal(all_dynamiccontent[l], "sechthree");
									tempObj_Dyncon.secpma = GetVal(all_dynamiccontent[l], "secpma");
									tempObj_Dyncon.secpsa = GetVal(all_dynamiccontent[l], "secpsa");
									tempObj_Dyncon.secpmlrb = GetVal(all_dynamiccontent[l], "secpmlrb");
									tempObj_Dyncon.secpmlrc = GetVal(all_dynamiccontent[l], "secpmlrc");
									tempObj_Dyncon.secpmd = GetVal(all_dynamiccontent[l], "secpmd");
									tempObj_Dyncon.secpdryz = GetVal(all_dynamiccontent[l], "secpdryz");
									tempObj_Dyncon.asidea = GetVal(all_dynamiccontent[l], "asidea");
									tempObj_Dyncon.asideb = GetVal(all_dynamiccontent[l], "asideb");
									tempObj_Dyncon.asidec = GetVal(all_dynamiccontent[l], "asidec");
									tempObj_Dyncon.asideimgaalt = GetVal(all_dynamiccontent[l], "asideimgaalt");
									tempObj_Dyncon.asideimga = GetVal(all_dynamiccontent[l], "asideimga");
									tempObj_Dyncon.asided = GetVal(all_dynamiccontent[l], "asided");
									tempObj_Dyncon.asidee = GetVal(all_dynamiccontent[l], "asidee");
									tempObj_Dyncon.asidef = GetVal(all_dynamiccontent[l], "asidef");
									tempObj_Dyncon.asideimgbalt = GetVal(all_dynamiccontent[l], "asideimgbalt");
									tempObj_Dyncon.asideimgb = GetVal(all_dynamiccontent[l], "asideimgb");
									tempObj_Dyncon.asideg = GetVal(all_dynamiccontent[l], "asideg");
									tempObj_Dyncon.asideh = GetVal(all_dynamiccontent[l], "asideh");
									tempObj_Dyncon.asidei = GetVal(all_dynamiccontent[l], "asidei");
									tempObj_Dyncon.asideimgcalt = GetVal(all_dynamiccontent[l], "asideimgcalt");
									tempObj_Dyncon.asideimgc = GetVal(all_dynamiccontent[l], "asideimgc");
									tempObj_Dyncon.likea = GetVal(all_dynamiccontent[l], "likea");
									tempObj_Dyncon.likeb = GetVal(all_dynamiccontent[l], "likeb");
									tempObj_Dyncon.likec = GetVal(all_dynamiccontent[l], "likec");
									tempObj_Dyncon.workinprogress = GetVal(all_dynamiccontent[l], "workinprogress");
									
									_ar_DYNAMICCONTENT[l] = tempObj_Dyncon;
								}
							}
						} // end if locsto not initiated
						else
						if( !!( _obj_locsto_lang.getItem("defaultLanguage") ) ){
							if( !!( _stringCheese ) ){
								_stringCheese = undefined;
							}
							if( dynamic_attr === _obj_locsto_lang.getItem("defaultLanguage") ){
								
								for ( l = 0; l < dynconLen; l = l + 1) {
									var tempObj_Dyncon = {};
									tempObj_Dyncon.contentattribute = all_dynamiccontent[l].getAttribute("clientele");
									tempObj_Dyncon.urlstringattribute = all_dynamiccontent[l].getAttribute("urlstring");
									tempObj_Dyncon.curlang = _obj_locsto_lang.getItem("defaultLanguage");
									tempObj_Dyncon.activepage = GetVal(all_dynamiccontent[l], "activepage");
									tempObj_Dyncon.contento = GetVal(all_dynamiccontent[l], "contento");
									tempObj_Dyncon.pubdate = GetVal(all_dynamiccontent[l], "pubdate");
									tempObj_Dyncon.sechredcaps = GetVal(all_dynamiccontent[l], "sechredcaps");
									tempObj_Dyncon.todolistalt = GetVal(all_dynamiccontent[l], "todolistalt");
									tempObj_Dyncon.ctaimgalt = GetVal(all_dynamiccontent[l], "ctaimgalt");
									tempObj_Dyncon.ctaimg = GetVal(all_dynamiccontent[l], "ctaimg");
									tempObj_Dyncon.figcaption = GetVal(all_dynamiccontent[l], "figcaption");
									tempObj_Dyncon.captionimgalt = GetVal(all_dynamiccontent[l], "captionimgalt");
									tempObj_Dyncon.captionimg = GetVal(all_dynamiccontent[l], "captionimg");
									tempObj_Dyncon.sechthree = GetVal(all_dynamiccontent[l], "sechthree");
									tempObj_Dyncon.secpma = GetVal(all_dynamiccontent[l], "secpma");
									tempObj_Dyncon.secpsa = GetVal(all_dynamiccontent[l], "secpsa");
									tempObj_Dyncon.secpmlrb = GetVal(all_dynamiccontent[l], "secpmlrb");
									tempObj_Dyncon.secpmlrc = GetVal(all_dynamiccontent[l], "secpmlrc");
									tempObj_Dyncon.secpmd = GetVal(all_dynamiccontent[l], "secpmd");
									tempObj_Dyncon.secpdryz = GetVal(all_dynamiccontent[l], "secpdryz");
									tempObj_Dyncon.asidea = GetVal(all_dynamiccontent[l], "asidea");
									tempObj_Dyncon.asideb = GetVal(all_dynamiccontent[l], "asideb");
									tempObj_Dyncon.asidec = GetVal(all_dynamiccontent[l], "asidec");
									tempObj_Dyncon.asideimgaalt = GetVal(all_dynamiccontent[l], "asideimgaalt");
									tempObj_Dyncon.asideimga = GetVal(all_dynamiccontent[l], "asideimga");
									tempObj_Dyncon.asided = GetVal(all_dynamiccontent[l], "asided");
									tempObj_Dyncon.asidee = GetVal(all_dynamiccontent[l], "asidee");
									tempObj_Dyncon.asidef = GetVal(all_dynamiccontent[l], "asidef");
									tempObj_Dyncon.asideimgbalt = GetVal(all_dynamiccontent[l], "asideimgbalt");
									tempObj_Dyncon.asideimgb = GetVal(all_dynamiccontent[l], "asideimgb");
									tempObj_Dyncon.asideg = GetVal(all_dynamiccontent[l], "asideg");
									tempObj_Dyncon.asideh = GetVal(all_dynamiccontent[l], "asideh");
									tempObj_Dyncon.asidei = GetVal(all_dynamiccontent[l], "asidei");
									tempObj_Dyncon.asideimgcalt = GetVal(all_dynamiccontent[l], "asideimgcalt");
									tempObj_Dyncon.asideimgc = GetVal(all_dynamiccontent[l], "asideimgc");
									tempObj_Dyncon.likea = GetVal(all_dynamiccontent[l], "likea");
									tempObj_Dyncon.likeb = GetVal(all_dynamiccontent[l], "likeb");
									tempObj_Dyncon.likec = GetVal(all_dynamiccontent[l], "likec");
									tempObj_Dyncon.workinprogress = GetVal(all_dynamiccontent[l], "workinprogress");
									
									_ar_DYNAMICCONTENT[l] = tempObj_Dyncon;
								}
							}
						} // end if locsto initiated
					} // END for ( m = 0; m < dynconLen; m = m + 1 )
					
					// RIGHTEOUS CONTENT
					for ( h = 0; h < rytconLen; h = h + 1) {
						righteous_attr = all_righteouscontent[h].getAttribute("clientele");
						
						if( !( _obj_locsto_lang.getItem("defaultLanguage") ) ){
							_stringCheese = "EN";
							
							if( righteous_attr === _stringCheese ){
								focus_rightcon = all_righteouscontent[h];
								for ( g = 0; g < rytconLen; g = g + 1) {
									var tempObj_Righteouscon = {};
									if( all_righteouscontent[g] === focus_rightcon ){
										tempObj_Righteouscon.contentattribute = all_righteouscontent[g].getAttribute("clientele");
										tempObj_Righteouscon.curlang = _stringCheese;
										tempObj_Righteouscon.activepage = GetVal(all_righteouscontent[g], "activepage");
										tempObj_Righteouscon.righteouswords = GetVal(all_righteouscontent[g], "righteouswords");
									}
									_ar_RIGHTEOUSCONTENT[g] = tempObj_Righteouscon;
								}
								
							}
						}
						else
						if( !!( _obj_locsto_lang.getItem("defaultLanguage") ) ){
							if( !!( _stringCheese ) ){
								_stringCheese = undefined;
							}
							
							if( righteous_attr === _obj_locsto_lang.getItem("defaultLanguage") ){
								focus_rightcon = all_righteouscontent[h];
								for ( g = 0; g < rytconLen; g = g + 1) {
									var tempObj_Righteouscon = {};
									if( all_righteouscontent[g] === focus_rightcon ){
										tempObj_Righteouscon.contentattribute = all_righteouscontent[g].getAttribute("clientele");
										tempObj_Righteouscon.curlang = _obj_locsto_lang.getItem("defaultLanguage");
										tempObj_Righteouscon.activepage = GetVal(all_righteouscontent[g], "activepage");
										tempObj_Righteouscon.righteouswords = GetVal(all_righteouscontent[g], "righteouswords");
									}
									_ar_RIGHTEOUSCONTENT[g] = tempObj_Righteouscon; 
								}
							}
						}
					}
					
					// STATIC CONTENT
					for ( k = 0; k < statconLen; k = k + 1 ) {
						static_attr = all_staticcontent[k].getAttribute("clientele");
						
						if( !( _obj_locsto_lang.getItem("defaultLanguage") ) ){
							_stringCheese = "EN";
							if( static_attr === _stringCheese ){
								
								for ( o = 0; o < statconLen; o = o + 1) {
									var tempObj_Statcon = {};
									tempObj_Statcon.contentattribute = all_staticcontent[o].getAttribute("clientele");
									tempObj_Statcon.curlang = _stringCheese;
									tempObj_Statcon.courtesygreeting = GetVal(all_staticcontent[o], "courtesygreeting");
									tempObj_Statcon.bcapable = GetVal(all_staticcontent[o], "bcapable");
									tempObj_Statcon.bincapable = GetVal(all_staticcontent[o], "bincapable");
									tempObj_Statcon.closeone = GetVal(all_staticcontent[o], "closeone");
									tempObj_Statcon.silkylinks = GetVal(all_staticcontent[o], "silkylinks");
									tempObj_Statcon.labelhome = GetVal(all_staticcontent[o], "labelhome");
									tempObj_Statcon.labelinspire = GetVal(all_staticcontent[o], "labelinspire");
									tempObj_Statcon.labelenhance = GetVal(all_staticcontent[o], "labelenhance");
									tempObj_Statcon.labeltransform = GetVal(all_staticcontent[o], "labeltransform");
									tempObj_Statcon.labeltransition = GetVal(all_staticcontent[o], "labeltransition");
									tempObj_Statcon.labelgallery = GetVal(all_staticcontent[o], "labelgallery");
									tempObj_Statcon.labeldevice = GetVal(all_staticcontent[o], "labeldevice");
									tempObj_Statcon.labeluntang = GetVal(all_staticcontent[o], "labeluntang");
									tempObj_Statcon.labelinteract = GetVal(all_staticcontent[o], "labelinteract");
									tempObj_Statcon.labelactivities = GetVal(all_staticcontent[o], "labelactivities");
									tempObj_Statcon.labelevents = GetVal(all_staticcontent[o], "labelevents");
									tempObj_Statcon.labelcommunity = GetVal(all_staticcontent[o], "labelcommunity");
									tempObj_Statcon.labeldownloads = GetVal(all_staticcontent[o], "labeldownloads");
									tempObj_Statcon.labelreferences = GetVal(all_staticcontent[o], "labelreferences");
									tempObj_Statcon.labelabout = GetVal(all_staticcontent[o], "labelabout");
									tempObj_Statcon.labelproducts = GetVal(all_staticcontent[o], "labelproducts");
									tempObj_Statcon.labelservices = GetVal(all_staticcontent[o], "labelservices");
									tempObj_Statcon.labelhow = GetVal(all_staticcontent[o], "labelhow");
									tempObj_Statcon.labelsolutions = GetVal(all_staticcontent[o], "labelsolutions");
									tempObj_Statcon.labelcontact = GetVal(all_staticcontent[o], "labelcontact");
									tempObj_Statcon.labelsitemap = GetVal(all_staticcontent[o], "labelsitemap");
									tempObj_Statcon.labelsection = GetVal(all_staticcontent[o], "labelsection");
									tempObj_Statcon.labelaside = GetVal(all_staticcontent[o], "labelaside");
									tempObj_Statcon.labelrightcolumn = GetVal(all_staticcontent[o], "labelrightcolumn");
									tempObj_Statcon.labelfooter = GetVal(all_staticcontent[o], "labelfooter");
									tempObj_Statcon.dz1a = GetVal(all_staticcontent[o], "dz1a");
									tempObj_Statcon.dz1b = GetVal(all_staticcontent[o], "dz1b");
									tempObj_Statcon.dz1c = GetVal(all_staticcontent[o], "dz1c");
									tempObj_Statcon.dz1d = GetVal(all_staticcontent[o], "dz1d");
									tempObj_Statcon.dz2a = GetVal(all_staticcontent[o], "dz2a");
									tempObj_Statcon.dz2b = GetVal(all_staticcontent[o], "dz2b");
									tempObj_Statcon.jbza = GetVal(all_staticcontent[o], "jbza");
									tempObj_Statcon.jbzb = GetVal(all_staticcontent[o], "jbzb");
									tempObj_Statcon.jbzc = GetVal(all_staticcontent[o], "jbzc");
									tempObj_Statcon.jbzd = GetVal(all_staticcontent[o], "jbzd");
									tempObj_Statcon.fslaunch = GetVal(all_staticcontent[o], "fslaunch");
									tempObj_Statcon.fsrestore = GetVal(all_staticcontent[o], "fsrestore");
									tempObj_Statcon.cnva = GetVal(all_staticcontent[o], "cnva");
									tempObj_Statcon.cnvb = GetVal(all_staticcontent[o], "cnvb");
									tempObj_Statcon.cnvc = GetVal(all_staticcontent[o], "cnvc");
									tempObj_Statcon.cnvd = GetVal(all_staticcontent[o], "cnvd");
									tempObj_Statcon.findlabel = GetVal(all_staticcontent[o], "findlabel");
									tempObj_Statcon.watchlabel = GetVal(all_staticcontent[o], "watchlabel");
									tempObj_Statcon.clearlabel = GetVal(all_staticcontent[o], "clearlabel");
									tempObj_Statcon.trylabel = GetVal(all_staticcontent[o], "trylabel");
									tempObj_Statcon.gloe = GetVal(all_staticcontent[o], "gloe");
									tempObj_Statcon.gloa = GetVal(all_staticcontent[o], "gloa");
									tempObj_Statcon.glob = GetVal(all_staticcontent[o], "glob");
									tempObj_Statcon.gloc = GetVal(all_staticcontent[o], "gloc");
									tempObj_Statcon.glod = GetVal(all_staticcontent[o], "glod");
									tempObj_Statcon.draglabel = GetVal(all_staticcontent[o], "draglabel");
									tempObj_Statcon.fda = GetVal(all_staticcontent[o], "fda");
									tempObj_Statcon.fdb = GetVal(all_staticcontent[o], "fdb");
									tempObj_Statcon.fdc = GetVal(all_staticcontent[o], "fdc");
									tempObj_Statcon.fdd = GetVal(all_staticcontent[o], "fdd");
									tempObj_Statcon.opnlabel = GetVal(all_staticcontent[o], "opnlabel");
									tempObj_Statcon.clslabel = GetVal(all_staticcontent[o], "clslabel");
									tempObj_Statcon.fha = GetVal(all_staticcontent[o], "fha");
									tempObj_Statcon.fhb = GetVal(all_staticcontent[o], "fhb");
									tempObj_Statcon.fhc = GetVal(all_staticcontent[o], "fhc");
									tempObj_Statcon.fhd = GetVal(all_staticcontent[o], "fhd");
									tempObj_Statcon.retexphotelfour4 = GetVal(all_staticcontent[o], "retexphotelfour4");
									tempObj_Statcon.partners = GetVal(all_staticcontent[o], "partners");
									_ar_STATICCONTENT[o] = tempObj_Statcon;
								}
							}
						} // end if local storage not initiated
						else
						if( !!( _obj_locsto_lang.getItem("defaultLanguage") ) ){
							if( !!( _stringCheese ) ){
								_stringCheese = undefined;
							}
							if( static_attr === _obj_locsto_lang.getItem("defaultLanguage") ){
								
								for ( o = 0; o < statconLen; o = o + 1) {
									var tempObj_Statcon = {};
									tempObj_Statcon.contentattribute = all_staticcontent[o].getAttribute("clientele");
									tempObj_Statcon.curlang = _obj_locsto_lang.getItem("defaultLanguage");
									tempObj_Statcon.courtesygreeting = GetVal(all_staticcontent[o], "courtesygreeting");
									tempObj_Statcon.bcapable = GetVal(all_staticcontent[o], "bcapable");
									tempObj_Statcon.bincapable = GetVal(all_staticcontent[o], "bincapable");
									tempObj_Statcon.closeone = GetVal(all_staticcontent[o], "closeone");
									tempObj_Statcon.silkylinks = GetVal(all_staticcontent[o], "silkylinks");
									tempObj_Statcon.labelhome = GetVal(all_staticcontent[o], "labelhome");
									tempObj_Statcon.labelinspire = GetVal(all_staticcontent[o], "labelinspire");
									tempObj_Statcon.labelenhance = GetVal(all_staticcontent[o], "labelenhance");
									tempObj_Statcon.labeltransform = GetVal(all_staticcontent[o], "labeltransform");
									tempObj_Statcon.labeltransition = GetVal(all_staticcontent[o], "labeltransition");
									tempObj_Statcon.labelgallery = GetVal(all_staticcontent[o], "labelgallery");
									tempObj_Statcon.labeldevice = GetVal(all_staticcontent[o], "labeldevice");
									tempObj_Statcon.labeluntang = GetVal(all_staticcontent[o], "labeluntang");
									tempObj_Statcon.labelinteract = GetVal(all_staticcontent[o], "labelinteract");
									tempObj_Statcon.labelactivities = GetVal(all_staticcontent[o], "labelactivities");
									tempObj_Statcon.labelevents = GetVal(all_staticcontent[o], "labelevents");
									tempObj_Statcon.labelcommunity = GetVal(all_staticcontent[o], "labelcommunity");
									tempObj_Statcon.labeldownloads = GetVal(all_staticcontent[o], "labeldownloads");
									tempObj_Statcon.labelreferences = GetVal(all_staticcontent[o], "labelreferences");
									tempObj_Statcon.labelabout = GetVal(all_staticcontent[o], "labelabout");
									tempObj_Statcon.labelproducts = GetVal(all_staticcontent[o], "labelproducts");
									tempObj_Statcon.labelservices = GetVal(all_staticcontent[o], "labelservices");
									tempObj_Statcon.labelhow = GetVal(all_staticcontent[o], "labelhow");
									tempObj_Statcon.labelsolutions = GetVal(all_staticcontent[o], "labelsolutions");
									tempObj_Statcon.labelcontact = GetVal(all_staticcontent[o], "labelcontact");
									tempObj_Statcon.labelsitemap = GetVal(all_staticcontent[o], "labelsitemap");
									tempObj_Statcon.labelsection = GetVal(all_staticcontent[o], "labelsection");
									tempObj_Statcon.labelaside = GetVal(all_staticcontent[o], "labelaside");
									tempObj_Statcon.labelrightcolumn = GetVal(all_staticcontent[o], "labelrightcolumn");
									tempObj_Statcon.labelfooter = GetVal(all_staticcontent[o], "labelfooter");
									tempObj_Statcon.dz1a = GetVal(all_staticcontent[o], "dz1a");
									tempObj_Statcon.dz1b = GetVal(all_staticcontent[o], "dz1b");
									tempObj_Statcon.dz1c = GetVal(all_staticcontent[o], "dz1c");
									tempObj_Statcon.dz1d = GetVal(all_staticcontent[o], "dz1d");
									tempObj_Statcon.dz2a = GetVal(all_staticcontent[o], "dz2a");
									tempObj_Statcon.dz2b = GetVal(all_staticcontent[o], "dz2b");
									tempObj_Statcon.jbza = GetVal(all_staticcontent[o], "jbza");
									tempObj_Statcon.jbzb = GetVal(all_staticcontent[o], "jbzb");
									tempObj_Statcon.jbzc = GetVal(all_staticcontent[o], "jbzc");
									tempObj_Statcon.jbzd = GetVal(all_staticcontent[o], "jbzd");
									tempObj_Statcon.fslaunch = GetVal(all_staticcontent[o], "fslaunch");
									tempObj_Statcon.fsrestore = GetVal(all_staticcontent[o], "fsrestore");
									tempObj_Statcon.cnva = GetVal(all_staticcontent[o], "cnva");
									tempObj_Statcon.cnvb = GetVal(all_staticcontent[o], "cnvb");
									tempObj_Statcon.cnvc = GetVal(all_staticcontent[o], "cnvc");
									tempObj_Statcon.cnvd = GetVal(all_staticcontent[o], "cnvd");
									tempObj_Statcon.findlabel = GetVal(all_staticcontent[o], "findlabel");
									tempObj_Statcon.watchlabel = GetVal(all_staticcontent[o], "watchlabel");
									tempObj_Statcon.clearlabel = GetVal(all_staticcontent[o], "clearlabel");
									tempObj_Statcon.trylabel = GetVal(all_staticcontent[o], "trylabel");
									tempObj_Statcon.gloe = GetVal(all_staticcontent[o], "gloe");
									tempObj_Statcon.gloa = GetVal(all_staticcontent[o], "gloa");
									tempObj_Statcon.glob = GetVal(all_staticcontent[o], "glob");
									tempObj_Statcon.gloc = GetVal(all_staticcontent[o], "gloc");
									tempObj_Statcon.glod = GetVal(all_staticcontent[o], "glod");
									tempObj_Statcon.draglabel = GetVal(all_staticcontent[o], "draglabel");
									tempObj_Statcon.fda = GetVal(all_staticcontent[o], "fda");
									tempObj_Statcon.fdb = GetVal(all_staticcontent[o], "fdb");
									tempObj_Statcon.fdc = GetVal(all_staticcontent[o], "fdc");
									tempObj_Statcon.fdd = GetVal(all_staticcontent[o], "fdd");
									tempObj_Statcon.opnlabel = GetVal(all_staticcontent[o], "opnlabel");
									tempObj_Statcon.clslabel = GetVal(all_staticcontent[o], "clslabel");
									tempObj_Statcon.fha = GetVal(all_staticcontent[o], "fha");
									tempObj_Statcon.fhb = GetVal(all_staticcontent[o], "fhb");
									tempObj_Statcon.fhc = GetVal(all_staticcontent[o], "fhc");
									tempObj_Statcon.fhd = GetVal(all_staticcontent[o], "fhd");
									tempObj_Statcon.retexphotelfour4 = GetVal(all_staticcontent[o], "retexphotelfour4");
									tempObj_Statcon.partners = GetVal(all_staticcontent[o], "partners");
									_ar_STATICCONTENT[o] = tempObj_Statcon;
								}
							}
						} // end if local storage initiated
					} // end all static content
					
					
					var n,dynconCt = _ar_DYNAMICCONTENT.length,dContentattribute,dCurlang,dUrlstringattribute,dContento,dPubdate,dSechredcaps,dTodolistalt,dCtaimgalt,dCtaimg,dFigcaption,dCaptionimgalt,dCaptionimg,dSechthree,dSecpma,dSecpsa,dSecpmlrb,dSecpmlrc,dSecpmd,dSecpdryz,dAsidea,dAsideb,dAsidec,dAsideimgaalt,dAsideimga,dAsided,dAsidee,dAsidef,dAsideimgbalt,dAsideimgb,dAsideg,dAsideh,dAsidei,dAsideimgcalt,dAsideimgc,dLikea,dLikeb,dLikec,dWorkinprogress;
					
					var XX,nonpareilconCt = _ar_NONPAREILCONTENT.length,npContentattribute,npCurlang,npUrlattribute,npXaxis,npYaxis,npZaxis,npCubeback,npCubefront,npCuberight,npCubeleft,npCubebottom,npCubetop,npLgdfsrox,npLblfsrox,npLgdfsroy,npLblfsroy,npLgdfsopnclo,npOpenitvalue,npCloseitvalue,npFbzhxa,npFbzhxb,npFbzp1,npFbzp2,npBbzli1,npBbzli2,npBbzli3,npBbzli4,npFlp1,npFlp2,npBlp3,npBlp4,npBlp5,npTranhxa,npTranlr,npTranhxb,npTranpa,npTranhxc,npTranovni,npDvcpa,npDvcpb,npDvcpc,npDvcpd,npDvchxa,npDvchxb,npDvchxc,npDvchxd,npDvcpanoramic,npDvcportrait,npDvchxe,npDvchxf,npDvchxg,npDvchxh,npDvchxi,npDvchxj,npDvchxk,npDvchxl,npDvcmh2,npDvcmh3,npDvcoh2,npDvcoh3,npMvlo,npMvrg,npMvtm,npMvbp;
					
					
					var rightconCt = _ar_RIGHTEOUSCONTENT.length;
					var f,rContentattribute,rCurlang,rActivepage,rRighteouswords;
					
					var w,statconCt = _ar_STATICCONTENT.length,sContentattribute,sCurlang,sCourtesygreeting,sBcapable,sBincapable,sCloseone,sSilkylinks,sLabelhome,sLabelinspire,sLabelenhance,sLabeltransform,sLabeltransition,sLabelgallery,sLabeldevice,sLabeluntang,sLabelinteract,sLabelactivities,sLabelevents,sLabelcommunity,sLabeldownloads,sLabelreferences,sLabelabout,sLabelproducts,sLabelservices,sLabelhow,sLabelsolutions,sLabelcontact,sLabelsitemap,sLabelsection,sLabelaside,sLabelrightcolumn,sLabelfooter,sDz1a,sDz1b,sDz1c,sDz1d,sDz2a,sDz2b,sJbza,sJbzb,sJbzc,sJbzd,sFslaunch,sFsrestore,sCnva,sCnvb,sCnvc,sCnvd,sFindlabel,sWatchlabel,sClearlabel,sTrylabel,sAcclabel,sLatlabel,sLonlabel,sGloe,sGloa,sGlob,sGloc,sGlod,sDraglabel,sFda,sFdb,sFdc,sFdd,sOpnlabel,sClslabel,sFha,sFhb,sFhc,sFhd,sRetexphotelfour4,sPartners;
					
					// consequent formatting and presentation loops
					_el_loadicon.style.display = "none";
					
					// DYNAMIC CONTENT
					for( n = 0; n < dynconCt; n = n + 1 ){
						if( ( _actv_Page === _ar_DYNAMICCONTENT[n].activepage ) || ( _actv_Page === _ar_DYNAMICCONTENT[n].urlstringattribute ) ){
							dContentattribute = _ar_DYNAMICCONTENT[n].contentattribute;
							dUrlstringattribute = _ar_DYNAMICCONTENT[n].urlstringattribute;
							dCurlang = _ar_DYNAMICCONTENT[n].curlang;
							dContento = _ar_DYNAMICCONTENT[n].contento;
							dPubdate = _ar_DYNAMICCONTENT[n].pubdate;
							dSechredcaps = _ar_DYNAMICCONTENT[n].sechredcaps;
							dTodolistalt = _ar_DYNAMICCONTENT[n].todolistalt;
							dCtaimgalt = _ar_DYNAMICCONTENT[n].ctaimgalt;
							dCtaimg = _ar_DYNAMICCONTENT[n].ctaimg;
							dFigcaption = _ar_DYNAMICCONTENT[n].figcaption;
							dCaptionimgalt = _ar_DYNAMICCONTENT[n].captionimgalt;
							dCaptionimg = _ar_DYNAMICCONTENT[n].captionimg;
							dSechthree = _ar_DYNAMICCONTENT[n].sechthree;
							dSecpma = _ar_DYNAMICCONTENT[n].secpma;
							dSecpsa = _ar_DYNAMICCONTENT[n].secpsa;
							dSecpmlrb = _ar_DYNAMICCONTENT[n].secpmlrb;
							dSecpmlrc = _ar_DYNAMICCONTENT[n].secpmlrc;
							dSecpmd = _ar_DYNAMICCONTENT[n].secpmd;
							dSecpdryz = _ar_DYNAMICCONTENT[n].secpdryz;
							dAsidea = _ar_DYNAMICCONTENT[n].asidea;
							dAsideb = _ar_DYNAMICCONTENT[n].asideb;
							dAsidec = _ar_DYNAMICCONTENT[n].asidec;
							dAsideimgaalt = _ar_DYNAMICCONTENT[n].asideimgaalt;
							dAsideimga = _ar_DYNAMICCONTENT[n].asideimga;
							dAsided = _ar_DYNAMICCONTENT[n].asided;
							dAsidee = _ar_DYNAMICCONTENT[n].asidee;
							dAsidef = _ar_DYNAMICCONTENT[n].asidef;
							dAsideimgbalt = _ar_DYNAMICCONTENT[n].asideimgbalt;
							dAsideimgb = _ar_DYNAMICCONTENT[n].asideimgb;
							dAsideg = _ar_DYNAMICCONTENT[n].asideg;
							dAsideh = _ar_DYNAMICCONTENT[n].asideh;
							dAsidei = _ar_DYNAMICCONTENT[n].asidei;
							dAsideimgcalt = _ar_DYNAMICCONTENT[n].asideimgcalt;
							dAsideimgc = _ar_DYNAMICCONTENT[n].asideimgc;
							dLikea = _ar_DYNAMICCONTENT[n].likea;
							dLikeb = _ar_DYNAMICCONTENT[n].likeb;
							dLikec = _ar_DYNAMICCONTENT[n].likec;
							dWorkinprogress = _ar_DYNAMICCONTENT[n].workinprogress;
							if( dContentattribute === dCurlang ){
								
								_el_contento.innerHTML += dContento;
								
								if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "enhance.html") || (_actv_Page === "transform.html") || (_actv_Page === "transition.html") || (_actv_Page === "device.html") || (_actv_Page === "interaction.html") || (_actv_Page === "untangle.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") || (_actv_Page === "sitemap.html") || (_actv_Page === "inspiration.html") ){
									if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "enhance.html") || (_actv_Page === "transform.html") || (_actv_Page === "transition.html") || (_actv_Page === "device.html") || (_actv_Page === "interaction.html") || (_actv_Page === "untangle.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") || (_actv_Page === "inspiration.html") ){
										_el_pubdate.innerHTML += dPubdate;
									}
									if( (_actv_Page === "device.html") || (_actv_Page === "enhance.html") ){
										_el_secHredCaps.innerHTML += dSechredcaps;
									}
									if( (_actv_Page === "next.html") ){
										_el_img1.setAttribute( "alt" , dTodolistalt );
										_el_img2.setAttribute( "alt" , dTodolistalt );
										_el_img3.setAttribute( "alt" , dTodolistalt );
										_el_img4.setAttribute( "alt" , dTodolistalt );
										_el_img5.setAttribute( "alt" , dTodolistalt );
										_el_img6.setAttribute( "alt" , dTodolistalt );
										_el_img7.setAttribute( "alt" , dTodolistalt );
										_el_img8.setAttribute( "alt" , dTodolistalt );
									}
									if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "interaction.html") || (_actv_Page === "untangle.html") || (_actv_Page === "contact.html") || (_actv_Page === "inspiration.html") ){
										_el_ctaImg.setAttribute( "alt", dCtaimgalt );
									}
									if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "interaction.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") || (_actv_Page === "inspiration.html") ){
										_el_secHthree.innerHTML += dSechthree;
									}
									if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "enhance.html") || (_actv_Page === "interaction.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") ){
										_el_secPmA.innerHTML += dSecpma;
									}
									if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "enhance.html") || (_actv_Page === "interaction.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") ){
										_el_secPsA.innerHTML += dSecpsa;
									}
									if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "enhance.html") || (_actv_Page === "transform.html") || (_actv_Page === "transition.html") || (_actv_Page === "device.html") || (_actv_Page === "interaction.html") || (_actv_Page === "untangle.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") ){
										_el_secPdryZ.innerHTML += dSecpdryz;
									}
									if( (_actv_Page === "enhance.html") ){
										_el_asideA.innerHTML += dAsidea;
										_el_asideB.innerHTML += dAsideb;
										_el_asideC.innerHTML += dAsidec;
									}
									if( ( _actv_Page === CNFG_SVR_IDX ) || (_actv_Page === "transform.html") || (_actv_Page === "transition.html") || (_actv_Page === "interaction.html") || (_actv_Page === "untangle.html") || (_actv_Page === "next.html") || (_actv_Page === "contact.html") || (_actv_Page === "inspiration.html") ){
										_el_asideA.innerHTML += dAsidea;
										_el_asideB.innerHTML += dAsideb;
										_el_asideC.innerHTML += dAsidec;
										_el_asideimgA.setAttribute( "alt" , dAsideimgaalt );
										_el_asideD.innerHTML += dAsided;
										_el_asideE.innerHTML += dAsidee;
										_el_asideF.innerHTML += dAsidef;
										_el_asideimgB.setAttribute( "alt" , dAsideimgaalt );
										_el_asideG.innerHTML += dAsideg;
										_el_asideH.innerHTML += dAsideh;
										_el_asideI.innerHTML += dAsidei;
										_el_asideimgC.setAttribute( "alt" , dAsideimgaalt );
									}
								}
								
								
								
								if( (_actv_Page === "events.html") || (_actv_Page === "references.html") || (_actv_Page === "products.html") || (_actv_Page === "how.html") || (_actv_Page === "solutions.html") || (_actv_Page === "activities.html") || (_actv_Page === "community.html") || (_actv_Page === "downloads.html") || (_actv_Page === "about.html") || (_actv_Page === "services.html") ){
									_el_pubdate.innerHTML += dPubdate;
									_el_figcaption.innerHTML += dFigcaption;
									_el_captionImg.setAttribute( "alt" , dCaptionimgalt );
									_el_secHthree.innerHTML += dSechthree;
									_el_secPmA.innerHTML += dSecpma;
									_el_secPmlrB.innerHTML += dSecpmlrb;
									_el_secPmlrC.innerHTML += dSecpmlrc;
									_el_secPdryZ.innerHTML += dSecpdryz;
									_el_asideA.innerHTML += dAsidea;
									_el_asideB.innerHTML += dAsideb;
									_el_asideC.innerHTML += dAsidec;
									_el_asideimgA.setAttribute( "alt" , dAsideimgaalt );
									_el_asideD.innerHTML += dAsided;
									_el_asideE.innerHTML += dAsidee;
									_el_asideF.innerHTML += dAsidef;
									_el_asideimgB.setAttribute( "alt" , dAsideimgaalt );
									_el_asideG.innerHTML += dAsideg;
									_el_asideH.innerHTML += dAsideh;
									_el_asideI.innerHTML += dAsidei;
									_el_asideimgC.setAttribute( "alt" , dAsideimgaalt );
								}
								if( (_actv_Page === "activities.html") || (_actv_Page === "community.html") || (_actv_Page === "downloads.html") || (_actv_Page === "about.html") || (_actv_Page === "services.html") ){ 
									_el_secPmD.innerHTML += dSecpmd;
								}
								
								_el_likeA.innerHTML += dLikea;
								_el_likeB.innerHTML += dLikeb;
								_el_likeC.innerHTML += dLikec;
								_el_workinProgress.innerHTML += dWorkinprogress;
							}
						}
					} // END DYNAMIC CONTENT
					
					// RIGHTEOUS CONTENT
					for( f = 0; f < rightconCt; f = f + 1 ){
						if( _actv_Page === _ar_RIGHTEOUSCONTENT[f].activepage ){
							rContentattribute = _ar_RIGHTEOUSCONTENT[f].contentattribute;
							rCurlang = _ar_RIGHTEOUSCONTENT[f].curlang;
							rActivepage = _ar_RIGHTEOUSCONTENT[f].activepage;
							rRighteouswords = _ar_RIGHTEOUSCONTENT[f].righteouswords;
							if( rContentattribute === rCurlang ){
								if( rActivepage === "inspiration.html" ){
									_el_righteousWords.innerHTML += rRighteouswords;
								}
							}
							
						} // END if( _actv_Page === _ar_RIGHTEOUSCONTENT[f].activepage )
					} // END RIGHTEOUS CONTENT
					
					// STATIC CONTENT
					for( w = 0; w < statconCt; w = w + 1 ){ 
						sContentattribute = _ar_STATICCONTENT[w].contentattribute;
						sCurlang = _ar_STATICCONTENT[w].curlang;
						sCourtesygreeting = _ar_STATICCONTENT[w].courtesygreeting;
						sBcapable = _ar_STATICCONTENT[w].bcapable;
						sBincapable = _ar_STATICCONTENT[w].bincapable;
						sCloseone = _ar_STATICCONTENT[w].closeone;
						sSilkylinks = _ar_STATICCONTENT[w].silkylinks;
						sLabelhome = _ar_STATICCONTENT[w].labelhome;
						sLabelinspire = _ar_STATICCONTENT[w].labelinspire;
						sLabelenhance = _ar_STATICCONTENT[w].labelenhance;
						sLabeltransform = _ar_STATICCONTENT[w].labeltransform;
						sLabeltransition = _ar_STATICCONTENT[w].labeltransition;
						sLabelgallery = _ar_STATICCONTENT[w].labelgallery;
						sLabeldevice = _ar_STATICCONTENT[w].labeldevice;
						sLabeluntang = _ar_STATICCONTENT[w].labeluntang;
						sLabelinteract = _ar_STATICCONTENT[w].labelinteract;
						sLabelactivities = _ar_STATICCONTENT[w].labelactivities;
						sLabelevents = _ar_STATICCONTENT[w].labelevents;
						sLabelcommunity = _ar_STATICCONTENT[w].labelcommunity;
						sLabeldownloads = _ar_STATICCONTENT[w].labeldownloads;
						sLabelreferences = _ar_STATICCONTENT[w].labelreferences;
						sLabelabout = _ar_STATICCONTENT[w].labelabout;
						sLabelproducts = _ar_STATICCONTENT[w].labelproducts;
						sLabelservices = _ar_STATICCONTENT[w].labelservices;
						sLabelhow = _ar_STATICCONTENT[w].labelhow;
						sLabelsolutions = _ar_STATICCONTENT[w].labelsolutions;
						sLabelcontact = _ar_STATICCONTENT[w].labelcontact;
						sLabelsitemap = _ar_STATICCONTENT[w].labelsitemap;
						sLabelsection = _ar_STATICCONTENT[w].labelsection;
						sLabelaside = _ar_STATICCONTENT[w].labelaside;
						sLabelrightcolumn = _ar_STATICCONTENT[w].labelrightcolumn;
						sLabelfooter = _ar_STATICCONTENT[w].labelfooter;
						sDz1a = _ar_STATICCONTENT[w].dz1a;
						sDz1b = _ar_STATICCONTENT[w].dz1b;
						sDz1c = _ar_STATICCONTENT[w].dz1c;
						sDz1d = _ar_STATICCONTENT[w].dz1d;
						sDz2a = _ar_STATICCONTENT[w].dz2a;
						sDz2b = _ar_STATICCONTENT[w].dz2b;
						sJbza = _ar_STATICCONTENT[w].jbza;
						sJbzb = _ar_STATICCONTENT[w].jbzb;
						sJbzc = _ar_STATICCONTENT[w].jbzc;
						sJbzd = _ar_STATICCONTENT[w].jbzd;
						sFslaunch = _ar_STATICCONTENT[w].fslaunch;
						sFsrestore = _ar_STATICCONTENT[w].fsrestore;
						sCnva = _ar_STATICCONTENT[w].cnva;
						sCnvb = _ar_STATICCONTENT[w].cnvb;
						sCnvc = _ar_STATICCONTENT[w].cnvc;
						sCnvd = _ar_STATICCONTENT[w].cnvd;
						sFindlabel = _ar_STATICCONTENT[w].findlabel;
						sWatchlabel = _ar_STATICCONTENT[w].watchlabel;
						sClearlabel = _ar_STATICCONTENT[w].clearlabel;
						sTrylabel = _ar_STATICCONTENT[w].trylabel;
						sGloe = _ar_STATICCONTENT[w].gloe;
						sGloa = _ar_STATICCONTENT[w].gloa;
						sGlob = _ar_STATICCONTENT[w].glob;
						sGloc = _ar_STATICCONTENT[w].gloc;
						sGlod = _ar_STATICCONTENT[w].glod;
						sDraglabel = _ar_STATICCONTENT[w].draglabel;
						sFda = _ar_STATICCONTENT[w].fda;
						sFdb = _ar_STATICCONTENT[w].fdb;
						sFdc = _ar_STATICCONTENT[w].fdc;
						sFdd = _ar_STATICCONTENT[w].fdd;
						sOpnlabel = _ar_STATICCONTENT[w].opnlabel;
						sClslabel = _ar_STATICCONTENT[w].clslabel;
						sFha = _ar_STATICCONTENT[w].fha;
						sFhb = _ar_STATICCONTENT[w].fhb;
						sFhc = _ar_STATICCONTENT[w].fhc;
						sFhd = _ar_STATICCONTENT[w].fhd;
						sRetexphotelfour4 = _ar_STATICCONTENT[w].retexphotelfour4;
						sPartners = _ar_STATICCONTENT[w].partners;
						
						if( sContentattribute === sCurlang ){
							//console.log( "sContentattribute" , sContentattribute );
							//console.log( "sCurlang " , sCurlang );
							
							_el_courtesyGreeting.innerHTML += sCourtesygreeting;
							_el_bCapable.innerHTML += sBcapable;
							_el_bIncapable.innerHTML += sBincapable;
							_el_closeone.innerHTML += sCloseone;
							_el_silky_dL1.innerHTML += sSilkylinks;
							_el_silky_dL2.innerHTML += sSilkylinks;
							_el_silky_dL3.innerHTML += sSilkylinks;
							_el_silky_dL4.innerHTML += sSilkylinks;
							_el_nav_home.innerHTML += sLabelhome;
							_el_foot_home.innerHTML += sLabelhome;
							_el_nav_inspire.innerHTML += sLabelinspire;
							_el_foot_inspire.innerHTML += sLabelinspire;
							_el_nav_enhance.innerHTML += sLabelenhance;
							_el_foot_enhance.innerHTML += sLabelenhance;
							_el_nav_transform.innerHTML += sLabeltransform;
							_el_foot_transform.innerHTML += sLabeltransform;
							_el_nav_transition.innerHTML += sLabeltransition;
							_el_foot_transition.innerHTML += sLabeltransition;
							_el_nav_next.innerHTML += sLabelgallery;
							_el_nav_device.innerHTML += sLabeldevice;
							_el_foot_device.innerHTML += sLabeldevice;
							_el_nav_untang.innerHTML += sLabeluntang;
							_el_foot_untang.innerHTML += sLabeluntang;
							_el_nav_interact.innerHTML += sLabelinteract;
							_el_foot_interact.innerHTML += sLabelinteract;
							_el_nav_activities.innerHTML += sLabelactivities;
							_el_foot_activities.innerHTML += sLabelactivities;
							_el_nav_events.innerHTML += sLabelevents;
							_el_foot_events.innerHTML += sLabelevents;
							_el_nav_community.innerHTML += sLabelcommunity;
							_el_foot_community.innerHTML += sLabelcommunity;
							_el_nav_downloads.innerHTML += sLabeldownloads;
							_el_foot_downloads.innerHTML += sLabeldownloads;
							_el_nav_references.innerHTML += sLabelreferences;
							_el_foot_references.innerHTML += sLabelreferences;
							_el_nav_about.innerHTML += sLabelabout;
							_el_foot_about.innerHTML += sLabelabout;
							_el_nav_products.innerHTML += sLabelproducts;
							_el_foot_products.innerHTML += sLabelproducts;
							_el_nav_services.innerHTML += sLabelservices;
							_el_foot_services.innerHTML += sLabelservices;
							_el_nav_how.innerHTML += sLabelhow;
							_el_foot_how.innerHTML += sLabelhow;
							_el_nav_solutions.innerHTML += sLabelsolutions;
							_el_foot_solutions.innerHTML += sLabelsolutions;
							_el_nav_contact.innerHTML += sLabelcontact;
							_el_foot_contact.innerHTML += sLabelcontact;
							_el_nav_sitemap.innerHTML += sLabelsitemap;
							_el_foot_sitemap.innerHTML += sLabelsitemap;
							_el_sectionContent_dL.innerHTML += sLabelsection;
							_el_asideContent_dL.innerHTML += sLabelaside;
							_el_colRight_dL.innerHTML += sLabelrightcolumn;
							_el_footer_dL.innerHTML += sLabelfooter;
							_el_dz1A.innerHTML += sDz1a;
							_el_dz1B.innerHTML += sDz1b;
							_el_dz1C.innerHTML += sDz1c;
							_el_dz1D.innerHTML += sDz1d;
							_el_dz2A.innerHTML += sDz2a;
							_el_dz2B.innerHTML += sDz2b;
							_el_jbzA.innerHTML += sJbza;
							_el_jbzB.innerHTML += sJbzb;
							_el_jbzC.innerHTML += sJbzc;
							_el_jbzD.innerHTML += sJbzd;
							if( _actv_Page !== "next.html"){
								_el_fsLaunch.innerHTML += sFslaunch;
								_el_fsRestore.innerHTML += sFsrestore;
							}
							_el_cnvA.innerHTML += sCnva;
							_el_cnvB.innerHTML += sCnvb;
							_el_cnvC.innerHTML += sCnvc;
							_el_cnvD.innerHTML += sCnvd;
							_el_gCP_BTN.innerHTML += sFindlabel;
							_el_watPos_BTN.innerHTML += sWatchlabel;
							_el_watPos_CLR_BTN.innerHTML += sClearlabel;
							_el_stat_gCP.innerHTML += sTrylabel;
							_el_stat_watPos.innerHTML += sTrylabel;
							_el_gloE.innerHTML += sGloe;
							_el_gloA.innerHTML += sGloa;
							_el_gloB.innerHTML += sGlob;
							_el_gloC.innerHTML += sGloc;
							_el_gloD.innerHTML += sGlod;
							_el_Brooke_fxDrg.innerHTML += sDraglabel;
							_el_Franky_fxDrg.innerHTML += sDraglabel;
							_el_Robin_fxDrg.innerHTML += sDraglabel;
							_el_Sanji_fxDrg.innerHTML += sDraglabel;
							_el_Chopper_fxDrg.innerHTML += sDraglabel;
							_el_Usopp_fxDrg.innerHTML += sDraglabel;
							_el_Zoro_fxDrg.innerHTML += sDraglabel;
							_el_Nami_fxDrg.innerHTML += sDraglabel;
							_el_Luffy_fxDrg.innerHTML += sDraglabel;
							_el_fdA.innerHTML += sFda;
							_el_fdB.innerHTML += sFdb;
							_el_fdC.innerHTML += sFdc;
							_el_fdD.innerHTML += sFdd;
							_el_retexpUL1A.innerHTML += sOpnlabel;
							_el_retexpUL2A.innerHTML += sOpnlabel;
							_el_retexpUL3A.innerHTML += sOpnlabel;
							_el_retexpUL4A.innerHTML += sOpnlabel;
							_el_retexpUL1B.innerHTML += sClslabel;
							_el_retexpUL2B.innerHTML += sClslabel;
							_el_retexpUL3B.innerHTML += sClslabel;
							_el_retexpUL4B.innerHTML += sClslabel;
							_el_fha.innerHTML += sFha;
							_el_fhb.innerHTML += sFhb;
							_el_fhc.innerHTML += sFhc;
							_el_fhd.innerHTML += sFhd;
							_el_retexpHotelFour4.innerHTML += sRetexphotelfour4;
							_el_partners.innerHTML += sPartners; //	console.log( "sPartners" , sPartners );
						} 
						
					} // END STATIC CONTENT
					
					// NONPAREIL CONTENT
					for( XX = 0; XX < nonpareilconCt; XX = XX + 1 ){
						
						if( _actv_Page === _ar_NONPAREILCONTENT[XX].urlattribute ){
							
							npContentattribute = _ar_NONPAREILCONTENT[XX].contentattribute;
							npCurlang = _ar_NONPAREILCONTENT[XX].curlang;
							npUrlattribute = _ar_NONPAREILCONTENT[XX].urlattribute;
							npXaxis = _ar_NONPAREILCONTENT[XX].xaxis; // ENHANCE
							npYaxis = _ar_NONPAREILCONTENT[XX].yaxis;
							npZaxis = _ar_NONPAREILCONTENT[XX].zaxis;
							npCubeback = _ar_NONPAREILCONTENT[XX].cubeback;
							npCubefront = _ar_NONPAREILCONTENT[XX].cubefront;
							npCuberight = _ar_NONPAREILCONTENT[XX].cuberight;
							npCubeleft = _ar_NONPAREILCONTENT[XX].cubeleft;
							npCubebottom = _ar_NONPAREILCONTENT[XX].cubebottom;
							npCubetop = _ar_NONPAREILCONTENT[XX].cubetop;
							npLgdfsrox = _ar_NONPAREILCONTENT[XX].lgdfsrox;
							npLblfsrox = _ar_NONPAREILCONTENT[XX].lblfsrox;
							npLgdfsroy = _ar_NONPAREILCONTENT[XX].lgdfsroy;
							npLblfsroy = _ar_NONPAREILCONTENT[XX].lblfsroy;
							npLgdfsopnclo = _ar_NONPAREILCONTENT[XX].lgdfsopnclo;
							npOpenitvalue = _ar_NONPAREILCONTENT[XX].openitvalue;
							npCloseitvalue = _ar_NONPAREILCONTENT[XX].closeitvalue;
							npFbzhxa = _ar_NONPAREILCONTENT[XX].fbzhxa; // TRANSFORM
							npFbzhxb = _ar_NONPAREILCONTENT[XX].fbzhxb;
							npFbzp1 = _ar_NONPAREILCONTENT[XX].fbzp1;
							npFbzp2 = _ar_NONPAREILCONTENT[XX].fbzp2;
							npBbzli1 = _ar_NONPAREILCONTENT[XX].bbzli1;
							npBbzli2 = _ar_NONPAREILCONTENT[XX].bbzli2;
							npBbzli3 = _ar_NONPAREILCONTENT[XX].bbzli3;
							npBbzli4 = _ar_NONPAREILCONTENT[XX].bbzli4;
							npFlp1 = _ar_NONPAREILCONTENT[XX].flp1;
							npFlp2 = _ar_NONPAREILCONTENT[XX].flp2;
							npBlp3 = _ar_NONPAREILCONTENT[XX].blp3;
							npBlp4 = _ar_NONPAREILCONTENT[XX].blp4;
							npBlp5 = _ar_NONPAREILCONTENT[XX].blp5;
							npTranhxa = _ar_NONPAREILCONTENT[XX].tranhxa; // TRANSITION
							npTranlr = _ar_NONPAREILCONTENT[XX].tranlr;
							npTranhxb = _ar_NONPAREILCONTENT[XX].tranhxb;
							npTranpa = _ar_NONPAREILCONTENT[XX].tranpa;
							npTranhxc = _ar_NONPAREILCONTENT[XX].tranhxc;
							npTranovni = _ar_NONPAREILCONTENT[XX].tranovni;
							npDvcpa = _ar_NONPAREILCONTENT[XX].dvcpa; // DEVICE
							npDvcpb = _ar_NONPAREILCONTENT[XX].dvcpb;
							npDvcpc = _ar_NONPAREILCONTENT[XX].dvcpc;
							npDvcpd = _ar_NONPAREILCONTENT[XX].dvcpd;
							npDvchxa = _ar_NONPAREILCONTENT[XX].dvchxa;
							npDvchxb = _ar_NONPAREILCONTENT[XX].dvchxb;
							npDvchxc = _ar_NONPAREILCONTENT[XX].dvchxc;
							npDvchxd = _ar_NONPAREILCONTENT[XX].dvchxd;
							npDvcpanoramic = _ar_NONPAREILCONTENT[XX].dvcpanoramic;
							npDvcportrait = _ar_NONPAREILCONTENT[XX].dvcportrait;
							npDvchxe = _ar_NONPAREILCONTENT[XX].dvchxe;
							npDvchxf = _ar_NONPAREILCONTENT[XX].dvchxf;
							// npTwoandonehalf = _ar_NONPAREILCONTENT[XX].twoandonehalf;
							npDvchxg = _ar_NONPAREILCONTENT[XX].dvchxg;
							npDvchxh = _ar_NONPAREILCONTENT[XX].dvchxh;
							npDvchxi = _ar_NONPAREILCONTENT[XX].dvchxi;
							npDvchxj = _ar_NONPAREILCONTENT[XX].dvchxj;
							npDvchxk = _ar_NONPAREILCONTENT[XX].dvchxk;
							npDvchxl = _ar_NONPAREILCONTENT[XX].dvchxl;
							npDvcmh2 = _ar_NONPAREILCONTENT[XX].dvcmh2;
							npDvcmh3 = _ar_NONPAREILCONTENT[XX].dvcmh3;
							// npDvcmro = _ar_NONPAREILCONTENT[XX].dvcmro;
							npDvcoh2 = _ar_NONPAREILCONTENT[XX].dvcoh2;
							npDvcoh3 = _ar_NONPAREILCONTENT[XX].dvcoh3;
							// npDvcoro = _ar_NONPAREILCONTENT[XX].dvcoro;
							npMvlo = _ar_NONPAREILCONTENT[XX].mvlo; // UNTANGLE
							npMvrg = _ar_NONPAREILCONTENT[XX].mvrg;
							npMvtm = _ar_NONPAREILCONTENT[XX].mvtm;
							npMvbp = _ar_NONPAREILCONTENT[XX].mvbp;
							
							if( npUrlattribute === "enhance.html" ){ 
								// _el_XAxis.innerHTML += npXaxis;
								// _el_YAxis.innerHTML += npYaxis;
								// _el_ZAxis.innerHTML += npZaxis;
								_el_Back.innerHTML += npCubeback;
								_el_Front.innerHTML += npCubefront;
								_el_Right.innerHTML += npCuberight;
								_el_Left.innerHTML += npCubeleft;
								_el_Bottom.innerHTML += npCubebottom;
								_el_Top.innerHTML += npCubetop;
								_el_lgdFSrox.innerHTML += npLgdfsrox;
								_el_lblFSrox.innerHTML += npLblfsrox;
								_el_lgdFSroy.innerHTML += npLgdfsroy;
								_el_lblFSroy.innerHTML += npLblfsroy;
								_el_lgdFSopnclo.innerHTML += npLgdfsopnclo;
								_el_openIt.value += npOpenitvalue;
								_el_closeIt.value += npCloseitvalue;
							}
							if( npUrlattribute === "transform.html" ){
								_el_fbZhxA.innerHTML += npFbzhxa;
								_el_fbZhxB.innerHTML += npFbzhxb;
								_el_fbZp1.innerHTML += npFbzp1;
								_el_fbZp2.innerHTML += npFbzp2;
								_el_bbZli1.innerHTML += npBbzli1;
								_el_bbZli2.innerHTML += npBbzli2;
								_el_bbZli3.innerHTML += npBbzli3;
								_el_bbZli4.innerHTML += npBbzli4;
								_el_fLp1.innerHTML += npFlp1;
								_el_fLp2.innerHTML += npFlp2;
								_el_bLp3.innerHTML += npBlp3;
								_el_bLp4.innerHTML += npBlp4;
								_el_bLp5.innerHTML += npBlp5;
							}
							if( npUrlattribute === "transition.html" ){
								_el_tranHxA.innerHTML += npTranhxa;
								_el_tranLR.innerHTML += npTranlr;
								_el_tranHxB.innerHTML += npTranhxb;
								_el_tranPa.innerHTML += npTranpa;
								_el_tranHxC.innerHTML += npTranhxc;
								_el_tranOvni.innerHTML += npTranovni;
							}
							if( npUrlattribute === "device.html" ){
								_el_dvcPa.innerHTML += npDvcpa;
								_el_dvcPb.innerHTML += npDvcpb; 
								_el_dvcPc.innerHTML += npDvcpc;
								_el_dvcPd.innerHTML += npDvcpd;
								_el_dvchxA.innerHTML += npDvchxa;
								_el_dvchxB.innerHTML += npDvchxb;
								_el_dvchxC.innerHTML += npDvchxc;
								_el_dvchxD.innerHTML += npDvchxd;
								_el_dvc_panoramic.innerHTML += npDvcpanoramic;
								_el_dvc_portrait.innerHTML += npDvcportrait;
								_el_dvchxE.innerHTML += npDvchxe;
								_el_dvchxF.innerHTML += npDvchxf;
								// _el_dvc_two_one_half.innerHTML += npTwoandonehalf;
								_el_dvchxG.innerHTML += npDvchxg;
								_el_dvchxH.innerHTML += npDvchxh;
								_el_dvchxI.innerHTML += npDvchxi;
								_el_dvchxJ.innerHTML += npDvchxj;
								_el_dvchxK.innerHTML += npDvchxk;
								_el_dvchxL.innerHTML += npDvchxl;
								_el_dvcM_h2.innerHTML += npDvcmh2;
								_el_dvcM_h3.innerHTML += npDvcmh3;
								// _el_dvcM_readOut.innerHTML += npDvcmro;
								_el_dvcO_h2.innerHTML += npDvcoh2;
								_el_dvcO_h3.innerHTML += npDvcoh3;
								// _el_dvcO_readOut.innerHTML += npDvcoro;
							}
							if( npUrlattribute === "untangle.html" ){
								_el_LO.innerHTML += npMvlo;
								_el_RG.innerHTML += npMvrg;
								_el_TM.innerHTML += npMvtm;
								_el_BP.innerHTML += npMvbp;
							}
							
							
							
						} // END if( _actv_Page === _ar_NONPAREILCONTENT[XX].urlattribute )
					} // END NONPAREIL CONTENT
					
				} // END BIG IF... IF XML RESPONSE
			} // END STATUS 200
			
			else {
				_Xhr_KnockedOffline();
				window.alert(_obj_ConfigAll.CannedStrings.ajaxOps.assets.nonFormatString.problems.intro + this.status + ". ");
			} 
		}
		function GetVal(pmData, pmTag) { 
			return pmData.getElementsByTagName(pmTag)[0].firstChild.nodeValue;
		}
	} // END function
	
	// END PRETZEL
	
	
	
	
	// INSPIRATION ~ ACTIVE~X ~ COMBO Content Electric ~ APPLICATION CACHE ~ "W3C excluding "iOS4"
	function _ActvX_albtrs_KnockedOffline(){	
	
		_el_loadOps.innerHTML = "";
		_el_loadOps.style.display = "block";
		_el_loadicon.style.display = "none";
		
		if( ( document.documentElement.attachEvent && !!( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1") ) ) ) {
			_el_loadOps.innerHTML = _obj_ConfigAll.CannedStrings.ajaxOps.assets.nonFormatString.lostConn[ document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1") ].fullText;
		} 
		else 
		if( ( document.documentElement.attachEvent && !( document.cookie.replace(/(?:(?:^|.*;\s* )defaultLanguage\s*\=\s*([^;]*).*$)|^.*$/, "$1") ) ) ) {
			_el_loadOps.innerHTML = _obj_ConfigAll.CannedStrings.ajaxOps.assets.nonFormatString.lostConn.EN.fullText;
		}
		
		
	} // END function
	
	// INSPIRATION ~ XHR ~ COMBO Content Electric ~ APPLICATION CACHE ~ "W3C excluding "iOS4"
	function _Xhr_KnockedOffline(){	
	
		_el_loadOps.innerHTML = "";
		_el_loadOps.style.display = "block";
		_el_loadicon.style.display = "none";
		
		if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
			_el_loadOps.innerHTML = _obj_ConfigAll.CannedStrings.ajaxOps.assets.nonFormatString.lostConn[window.localStorage.getItem("defaultLanguage")].fullText;
		}
		else 
		if( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) {
			_el_loadOps.innerHTML = _obj_ConfigAll.CannedStrings.ajaxOps.assets.nonFormatString.lostConn.EN.fullText;
		}
		
		if(document.documentElement.addEventListener){
			
			// The big payback!
			/*_docObj.body.addEventListener( "offline" ,function (){
				_UpdateOnlineStatus("offline");
			},false); */
			
		}
		
	} // END function
	
	// ONLINE\OFFLINE
	// XHR ~ COMBO Content Electric
	function _UpdateOnlineStatus(pm_AnyTxt) {
		if (_navObj.onLine) {
			var condition = "ONLINE";
			condition = condition.toLowerCase();
		}
		else {
			var condition = "OFFLINE";
			condition = condition.toLowerCase();
		}
		_el_colorBlock.setAttribute("class", condition);
		_el_toggleState.innerHTML = "<span class=" + condition + ">" + condition + "</span>";
	} // END function
	
	// XHR ~ COMBO Content Electric 
	// INSPIRATION ~ XHR ~ COMBO Content Electric ~ APPLICATION CACHE ~ "W3C excluding "iOS4"
	function _UpgradeApp() {
		
		if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
			if ( _topGLO.confirm( _obj_ConfigAll.CannedStrings.ajaxOps.assets.nonFormatString.upgradeUrApp[window.localStorage.getItem("defaultLanguage")].fullText ) ) {
				_topGLO.applicationCache.swapCache();
				_topGLO.location.reload();
			}
		}
		else 
		if( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) {
			if ( _topGLO.confirm( _obj_ConfigAll.CannedStrings.ajaxOps.assets.nonFormatString.upgradeUrApp.EN.fullText ) ) {
				_topGLO.applicationCache.swapCache();
				_topGLO.location.reload();
			}
		}
		
	} // END function
	
	
	// XHR ~ Content
	// ONLINE~OFFLINE ~ APPLICATION CACHE ~ XHR
	function _Xhr_Content_xml_Load() {
		
		if( document.documentElement.addEventListener ){
			// PHASE 4
			_AC_StabilizeAndDeploy();
			
			// The big payback!
			/* _docObj.body.addEventListener( "online" ,function (){
				_UpdateOnlineStatus("online");
			},false); */ 
		}
		
		
	} // END function
	
	// ACTIVE~X ~ electric REQUEST OBJ 
	function _ActvX_albTrsReqObj_Truth_xml() { 
		if (window.XMLHttpRequest) {
			_actvX_albtrs_truth = new XMLHttpRequest();
		}
		else
		if (window.ActiveXObject) {
			try {
				_actvX_albtrs_truth = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e) {}
		}
		if (_actvX_albtrs_truth) {
			_actvX_albtrs_truth.onreadystatechange = _ActvX_albTrs_Truth_xml_Resp;
			_actvX_albtrs_truth.open("GET", _url_truth, true);
			_actvX_albtrs_truth.send(null);
		}
		else {
			window.alert(_obj_ConfigAll.CannedStrings.ajaxOps.assets.nonFormatString.dOAajaxReq.intro);
		}
		
		_ActvX_albTrs_Truth_xml_Load();
		
	} // END function
	
	// XHR ~ electric REQUEST OBJ 
	function _XhrReqObj_Truth_xml() { 
		if (window.XMLHttpRequest) {
			_xhr_truth = new XMLHttpRequest();
		}
		else
		if (window.ActiveXObject) {
			try {
				_xhr_truth = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e) {}
		}
		if (!!_xhr_truth) {
			_xhr_truth.onreadystatechange = _Xhr_Truth_xml_Resp;
			_xhr_truth.open("GET", _url_truth, true);
			_xhr_truth.send(null);
			_el_loadicon.style.display = "inline-block"; // "LOADICON..."
			
			_xhr_truth.onload = _Xhr_Truth_xml_Load;
			
		}
		else
		if (!_xhr_truth) { 
			window.alert(_obj_ConfigAll.CannedStrings.ajaxOps.assets.nonFormatString.dOAajaxReq.intro);
		}
	} // END function
	
	// ACTIVE~X ~ electric RESPONSE OBJ
	function _ActvX_albTrs_Truth_xml_Resp(event) { 
		
		if (_actvX_albtrs_truth.readyState === 4) {
			if (_actvX_albtrs_truth.status === 200) { 
				
				if( _actvX_albtrs_truth.responseXML ){
					
					
					var PASSAGEDOC = _actvX_albtrs_truth.responseXML;
					var psg_attr; 
					var passage_trees = PASSAGEDOC.getElementsByTagName("passages");
					var ptLen = passage_trees.length;
					var s; 
					var all_passages;
					var passageLen;
					var t;
					
					for( s = 0; s < ptLen; s = s + 1 ){
						
						psg_attr = passage_trees[s].getAttribute("clientele");
						
						if( !( _GetCookie(DEFAULTLANGUAGE) ) ) {
							
							_stringCheese = "EN";
							
							if( psg_attr === _stringCheese ){
								all_passages = passage_trees[s].getElementsByTagName("passage");
								passageLen = all_passages.length;
								for( t = 0; t < passageLen; t = t + 1 ){
									var tempObj_passages = {};
									tempObj_passages.chapter = GetVal(all_passages[t], "chapter");
									tempObj_passages.verse = GetVal(all_passages[t], "verse");
									tempObj_passages.ordinal = GetVal(all_passages[t], "ordinal");
									tempObj_passages.fullverse = GetVal(all_passages[t], "fullverse");
									tempObj_passages.truthquote = GetVal(all_passages[t], "truthquote");
									_ar_TRUTH[t] = tempObj_passages;
								}
							}
						} 
						else
						if( !!( _GetCookie(DEFAULTLANGUAGE) ) ) {
							
							if( !!( _stringCheese ) ){
								_stringCheese = undefined;
							}
							
							if( psg_attr === _GetCookie(DEFAULTLANGUAGE) ){
								all_passages = passage_trees[s].getElementsByTagName("passage");
								passageLen = all_passages.length;
								for( t = 0; t < passageLen; t = t + 1 ){
									var tempObj_passages = {};
									tempObj_passages.chapter = GetVal(all_passages[t], "chapter");
									tempObj_passages.verse = GetVal(all_passages[t], "verse");
									tempObj_passages.ordinal = GetVal(all_passages[t], "ordinal");
									tempObj_passages.fullverse = GetVal(all_passages[t], "fullverse");
									tempObj_passages.truthquote = GetVal(all_passages[t], "truthquote");
									_ar_TRUTH[t] = tempObj_passages;
								}
							}
						}
					} // END ALL RIGHTEOUS MATERIAL
					
				} // END BIG IF... IF ACTIVEX RESPONSE
			} // END STATUS 200
			else {
				_ActvX_albtrs_KnockedOffline();
				window.alert(_obj_ConfigAll.CannedStrings.ajaxOps.assets.nonFormatString.problems.intro + _actvX_albtrs_truth.status + ". ");
			} 
		}
		
		function GetVal(pmData, pmTag) {
			return pmData.getElementsByTagName(pmTag)[0].firstChild.nodeValue;
		} 
		
	} // END function
	
	// XHR ~ electric RESPONSE OBJ
	function _Xhr_Truth_xml_Resp(event) { 
		
		if (this.readyState === 4) {
			if (this.status === 200) { 
				
				if( this.responseXML ){
					
					var PASSAGEDOC = this.responseXML;
					var psg_attr; 
					var passage_trees = PASSAGEDOC.getElementsByTagName("passages");
					var ptLen = passage_trees.length;
					var s; 
					var all_passages;
					var passageLen;
					var t;
					
					for( s = 0; s < ptLen; s = s + 1 ){
						
						psg_attr = passage_trees[s].getAttribute("clientele");
						
						if(	!( _obj_locsto_lang.getItem("defaultLanguage") ) ) {
						
							_stringCheese = "EN";
							
							if( psg_attr === _stringCheese ){
								all_passages = passage_trees[s].getElementsByTagName("passage");
								passageLen = all_passages.length;
								for( t = 0; t < passageLen; t = t + 1 ){
									var tempObj_passages = {};
									tempObj_passages.chapter = GetVal(all_passages[t], "chapter");
									tempObj_passages.verse = GetVal(all_passages[t], "verse");
									tempObj_passages.ordinal = GetVal(all_passages[t], "ordinal");
									tempObj_passages.fullverse = GetVal(all_passages[t], "fullverse");
									tempObj_passages.truthquote = GetVal(all_passages[t], "truthquote");
									_ar_TRUTH[t] = tempObj_passages;
								}
							}
						}
						else
						if( !!( _obj_locsto_lang.getItem("defaultLanguage") ) ) {
							
							if( !!( _stringCheese ) ){
								_stringCheese = undefined;
							}
							
							if( psg_attr === _obj_locsto_lang.getItem("defaultLanguage") ){
								
								all_passages = passage_trees[s].getElementsByTagName("passage");
								passageLen = all_passages.length;
								for( t = 0; t < passageLen; t = t + 1 ){
									var tempObj_passages = {};
									tempObj_passages.chapter = GetVal(all_passages[t], "chapter");
									tempObj_passages.verse = GetVal(all_passages[t], "verse");
									tempObj_passages.ordinal = GetVal(all_passages[t], "ordinal");
									tempObj_passages.fullverse = GetVal(all_passages[t], "fullverse");
									tempObj_passages.truthquote = GetVal(all_passages[t], "truthquote");
									_ar_TRUTH[t] = tempObj_passages;
								}
							}
						}
					} // END ALL RIGHTEOUS MATERIAL
				}
			} // END STATUS 200			
			else {
				_Xhr_KnockedOffline();
				window.alert(_obj_ConfigAll.CannedStrings.ajaxOps.assets.nonFormatString.problems.intro + this.status + ". ");
			} 
		}
		
		function GetVal(pmData, pmTag) {
			return pmData.getElementsByTagName(pmTag)[0].firstChild.nodeValue;
		} 
		
	} // END function
	
	
	// ACTIVE~X ~ electric
	function _ActvX_albTrs_Truth_xml_Load() { 	
		// logDetails.log( "Thank you, Yehovah! " ); 
		_ActvX_albTrs_CitationDeploy(); 
	} // END function
	
	// XHR ~ electric
	function _Xhr_Truth_xml_Load() { 
		// logDetails.log( "Thank you, Yehovah! " ); 
		
		if(document.documentElement.addEventListener){
			_Xhr_Content_xml_Load();
		}
		else
		if(document.documentElement.attachEvent){
			_Xhr_Content_xml_Load();
		} // NO change...
		
		// PHASE 2
		_Xhr_CitationDeploy();
		
	} // END function
	
	// ACTIVE~X ~ COMBO Content Electric 
	function _ActvX_albTrs_CitationDeploy(){
		var allCitations = _docObj.getElementsByTagName("cite");
		var citeLen = allCitations.length;
		var u;
		for (u = 0; u < citeLen; u = u + 1) {
			donLuchoHAKI.Utils.evt_u.AddEventoHandler( allCitations[u], "click", _ActvX_albTrs_Truth_xml_tgtDisplay );
		}
	} // END function
	
	// XHR ~ COMBO Content Electric 
	function _Xhr_CitationDeploy(){
		var allCitations = _docObj.getElementsByTagName("cite");
		var citeLen = allCitations.length;
		var u;
		for (u = 0; u < citeLen; u = u + 1) {
			donLuchoHAKI.Utils.evt_u.AddEventoHandler( allCitations[u], "click", _Xhr_Truth_xml_tgtDisplay );
		}
	} // END function
	
	// ACTIVE~X ~ electric
	function _ActvX_albTrs_Truth_xml_tgtDisplay(leEvt) { // TARGETED CITATION DISPLAY
		var electricQuoteContent = _docObj.getElementsByTagName("cite");
		var truthLen = electricQuoteContent.length;
		var v;
		for (v = 0; v < truthLen; v = v + 1) {
			electricQuoteContent[v].className = "verseCallOut electricStateChanged";
		}
		
		var _actvX_albtrsElectricTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget(leEvt);
		_actvX_albtrsElectricTrgt.className = "verseCallOut electricStateChanged electricSelection";
		var passageInstance = null;
		var citationLen = _ar_TRUTH.length;
		var w;
		for (w = 0; w < citationLen; w = w + 1) {
			if (_actvX_albtrsElectricTrgt.id === _ar_TRUTH[w].ordinal) {
				passageInstance = _ar_TRUTH[w];
			}
		}
		if (!!passageInstance) {		
			var el_electricQuote = _docObj.getElementById("electricQuote");
			var theMsg = "<span id='closeBox' class='electricCloseX electricClosePos'>x</span>" + passageInstance.truthquote;
			theMsg += "<strong id='strongBox' class='electricStrong'>" + passageInstance.chapter + ": " + passageInstance.verse + "</strong>";
			
			el_electricQuote.innerHTML = theMsg;
			
			el_electricQuote.style.display = "block";
			el_electricQuote.style.position = "absolute";
			el_electricQuote.style.zIndex = "5";
			el_electricQuote.style.visibility = "visible";
			el_electricQuote.style.backgroundColor = "#68BEBE";
			
			var nastyScrollTop = window.document.documentElement.scrollTop;
			var nastyScrollLeft = window.document.documentElement.scrollLeft;
			var nastyRect = _actvX_albtrsElectricTrgt.getBoundingClientRect();
			var nastyTopToApply = nastyScrollTop + ( parseInt( nastyRect.top, 10 ) );  
			var nastyLftToApply = nastyScrollLeft + ( parseInt( nastyRect.left, 10 ) );	
			el_electricQuote.style.left = ( nastyLftToApply - 5 ) + "px";
			el_electricQuote.style.top = ( nastyTopToApply - 80 ) + "px";
			
			donLuchoHAKI.Utils.evt_u.AddEventoHandler(_docObj.getElementById("closeBox"), "click", function() {
				_docObj.getElementById("electricQuote").style.visibility = "hidden";
			});
		}
	} // END function
	
	// XHR ~ electric
	function _Xhr_Truth_xml_tgtDisplay(leEvt) { // TARGETED CITATION DISPLAY
		var electricQuoteContent = _docObj.getElementsByTagName("cite");
		var truthLen = electricQuoteContent.length;
		var v;
		for (v = 0; v < truthLen; v = v + 1) {
			electricQuoteContent[v].className = "verseCallOut electricStateChanged";
		}
		
		var _xhrElectricTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget(leEvt);
		_xhrElectricTrgt.className = "verseCallOut electricStateChanged electricSelection";
		var passageInstance = null;
		var citationLen = _ar_TRUTH.length;
		var w;
		for (w = 0; w < citationLen; w = w + 1) {
			if (_xhrElectricTrgt.id === _ar_TRUTH[w].ordinal) {
				passageInstance = _ar_TRUTH[w];
			}
		}
		if (!!passageInstance) {		
			var el_electricQuote = _docObj.getElementById("electricQuote");
			var theMsg = "<span id='closeBox' class='electricCloseX electricClosePos'>x</span>" + passageInstance.truthquote;
			theMsg += "<strong id='strongBox' class='electricStrong'>" + passageInstance.chapter + ": " + passageInstance.verse + "</strong>";
			
			el_electricQuote.innerHTML = theMsg;
			el_electricQuote.style.cssText = "top:" + (_xhrElectricTrgt.offsetTop - 80) + "px; left:" + (_xhrElectricTrgt.offsetLeft - 5) + "px; visibility:visible;";
			
			donLuchoHAKI.Utils.evt_u.AddEventoHandler(_docObj.getElementById("closeBox"), "click", function() {
				_docObj.getElementById("electricQuote").style.visibility = "hidden";
			});
		}
	} // END function
	
	// INSPIRATION ~ XHR ~ COMBO Content Electric ~ APPLICATION CACHE ~ "W3C excluding "iOS4"
	function _AC_StabilizeAndDeploy(){
		
		_AC = _topGLO.applicationCache;
		
		donLuchoHAKI.Utils.evt_u.AddEventoHandler(_AC, "cached", function() {
			_el_loadicon.style.display = "none";
			//_PrintMsg("<p class='AC'>Application cached</p>"); 
		});
		
		donLuchoHAKI.Utils.evt_u.AddEventoHandler(_AC, "checking", function() {
			_el_loadicon.style.display = "inline-block";
			//_PrintMsg("<p class='AC'>Checking for update</p>");
		});
		
		donLuchoHAKI.Utils.evt_u.AddEventoHandler(_AC, "downloading", function() {
			_el_loadicon.style.display = "inline-block";
			//_PrintMsg("<p class='AC'>Downloading update</p>"); 
		});
		
		donLuchoHAKI.Utils.evt_u.AddEventoHandler(_AC, "error", function() {
			
			_Xhr_KnockedOffline();
			
			_Xhr_Truth_xml_Load();
			//_PrintMsg("<p class='dERP'>There has been an error fetching the manifest</p>"); 
		});
		
		donLuchoHAKI.Utils.evt_u.AddEventoHandler(_AC, "noupdate", function() {
			_el_loadicon.style.display = "none";
			_el_loadOps.style.display = "none";
			
			_Xhr_Truth_xml_Load(); 
			//_PrintMsg("<p class='AC'>Using latest version</p>");
		});
		
		donLuchoHAKI.Utils.evt_u.AddEventoHandler(_AC, "obsolete", function() {
			_el_loadicon.style.display = "none";
			//_PrintMsg("<p class='AC'>obsolete</p>");
		});
		
		donLuchoHAKI.Utils.evt_u.AddEventoHandler(_AC, "progress", function() {
			_el_loadicon.style.display = "inline-block";
			// _PrintMsg("<p class='AC'>Downloaded file</p>"); 
		});
		
		donLuchoHAKI.Utils.evt_u.AddEventoHandler(_AC, "updateready", function() {
			_el_loadicon.style.display = "none";
			// _PrintMsg("<p class='AC_UDR'>There is an update ready</p>"); 
			_el_loadOps.style.display = "block";
		});
		
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_dwnldAC, "click", _UpgradeApp);
		// PHASE 1
	} // END function
	
	// INSPIRATION ~ XHR ~ COMBO Content Electric ~ APPLICATION CACHE ~ "W3C excluding "iOS4"
	function _LastSpike_wThreeC(){
		// CONTENT ~ _xhr_truth ~ XHR REQUEST OBJ 
		_XhrReqObj_Content_xml(); 
		// INSPIRATION ~ _xhr_truth ~ XHR REQUEST OBJ 
		if( _actv_Page === "inspiration.html" ) { 
			_XhrReqObj_Truth_xml();
		} 		
	} // END function
	
	// INSPIRATION ~ XHR ~ COMBO Content Electric ~ APPLICATION CACHE ~ "W3C excluding "iOS4"
	function _LastSpike_IE(){
		// CONTENT ~ _actvX_albtrs_truth ~ ACTIVE~X REQUEST OBJ 
		_ActvX_albTrsReqObj_Content_xml(); 
		// INSPIRATION ~ _actvX_albtrs_truth ~ ACTIVE~X REQUEST OBJ 
		if( _actv_Page === "inspiration.html" ) { 
			_ActvX_albTrsReqObj_Truth_xml();
		} 
	} // END function
	
	
	// ~ Carousel functionality ~ image gallery
	function _DecideFive( pmNum ) {
		
		if( (pmNum % 5) !== 0 ){
			// return pmNum = parseInt(( ((pmNum/5)*5)-5),10);
			return pmNum = parseInt(( ((pmNum/5)*5)-pmNum),10);
		}
		else {
			return pmNum = parseInt(( (pmNum/5)*5 ),10);
		}
		
		//return (pmNum % 5) >= 2.5 ? parseInt(( ((pmNum/5)*5)+5),10) : parseInt(( (pmNum/5)*5 ),10);
	}
	
	// ~ Carousel functionality ~ image gallery
	function _CaroResizer() {	
		for ( _I = 0; _I < _all_imgs.length; _I = _I + 1 ) { 
			if ( _all_imgs[ _I ].className === "caroImg" ) { 
				_all_imgs[ _I ].style.width = _el_crutch.offsetWidth + "px"; 
				_arPush_ImgELs.push( _all_imgs[ _I ] );
				_arPush_ImgWidths.push( _all_imgs[ _I ].width );
			}
		} 
		
		_ar_ImgCt = _arPush_ImgELs.length; 
		_el_AvgWideCan.style.width = (_ar_ImgCt * (_arPush_ImgWidths[ _ar_ImgCt - 1 ]) ) + "px"; 
		_singleImgWidth = _arPush_ImgWidths[ _ar_ImgCt - 1 ]; // typeof ~= number
		
		_el_AverageOut.style.width = _singleImgWidth + "px"; // newly uncommented
		_el_AverageIn.style.width = _singleImgWidth + "px"; 
		
		if( !_startVal ){
			_startVal = 0; // console.log( "_startVal" , _startVal ); // 0
			_el_AvgWideCan.style.marginLeft = _startVal + "px"; // 0px -500px -1000px -1500px 
		}
		else 
		if( !!_startVal ){
			_startVal = parseInt( _el_AvgWideCan.style.marginLeft, 10 ); // console.log( "_startVal" , _startVal ); // 0
			_el_AvgWideCan.style.marginLeft = _startVal + "px"; // 0px -500px -1000px -1500px 
		}
	} // END function() 
	
	// ~ Carousel functionality ~ image gallery
	var _RunCarousel = function( leEvt ) {
		var evtTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		
		if (_chopper === "iPod" || _chopper === "iPhone") { 
			var dataAttrib = evtTrgt.getAttribute( "data-dir" ); 
		} 
		else {
			var dataAttrib = evtTrgt.dataset.dir; 
		}
		// END if "chopper" condition\CONTROL ~ squash ios4 bug
		
		if( dataAttrib === "prev" ){
			if( _curImg <= 1 ) {
				_curImg = _ar_ImgCt;
				_endVal = _singleImgWidth * (_curImg - 1);
				
				dataAttrib === "next";
			}
			else {
				_curImg = _curImg - 1;
				_endVal = _singleImgWidth * (_curImg - 1);
			}
		}
		else
		if( dataAttrib === "next" ){ 
			if( _curImg >= _ar_ImgCt ) {
				_curImg = 1;
				_endVal = _singleImgWidth * (_curImg - 1);
				
				dataAttrib === "prev";
			} 
			else {
				_curImg = _curImg + 1;
				_endVal = _singleImgWidth * (_curImg - 1);
			}
		}		
		if( _endVal !== 0 ) {
			_endVal = -_endVal;
		}
		else 
		if( _endVal === 0 ) {
			_endVal = _endVal;
		} 
		_Anim_marginLeft_w3c( _el_AvgWideCan , _endVal ); 		
	}; // END function 
	
	// ~ Carousel functionality ~ image gallery
	function _Anim_marginLeft_w3c( _pmAvgMultiple, _pmEndV ) {
		
		_mrBeepersMS = donLuchoHAKI.rafUtils.get_bpms(); 
		_RAF = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "requestAnimationFrame" , donLuchoHAKI.RetELs.glow() ); 
		_CAF = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "CancelAnimationFrame" , donLuchoHAKI.RetELs.glow() ); 
		
		_pmAvgMultiple = _el_AvgWideCan; // console.log( "_pmAvgMultiple PREVIOUSLY" , _pmAvgMultiple );
		_pmEndV = _endVal; // console.log( "_pmEndV" , _pmEndV );
		
		if( parseInt( _pmAvgMultiple.style.marginLeft, 10 ) > _pmEndV ){	
			if( parseInt( _pmAvgMultiple.style.marginLeft, 10 ) !== _pmEndV ) { 
				
				_pmAvgMultiple.style.marginLeft = parseInt( _pmAvgMultiple.style.marginLeft, 10 ) - ( parseInt( _arPush_ImgWidths[ _ar_ImgCt - 1 ] / 10 ) , 5 )  + "px"; 
				
				if( window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame ) { 
					_loopTimer_caro = (function( self ) {
						return window[ _RAF ]( _Anim_marginLeft_w3c ); 
					})( _loopTimer_caro ); 
				}
				else { 
					_loopTimer_caro = (function( self ) {
						return setTimeout( _Anim_marginLeft_w3c , _mrBeepersMS ); 
					})( _loopTimer_caro ); 
				}
			}
		}
		else
		if( parseInt( _pmAvgMultiple.style.marginLeft, 10 ) < _pmEndV ){	
			if( parseInt( _pmAvgMultiple.style.marginLeft, 10 ) !== _pmEndV ) { 
				
				_pmAvgMultiple.style.marginLeft = parseInt( _pmAvgMultiple.style.marginLeft, 10 ) + ( parseInt( _arPush_ImgWidths[ _ar_ImgCt - 1 ] / 10 ) , 10 )  + "px"; 
				
				if( window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame ) { 
					_loopTimer_caro = (function( self ) {
						return window[ _RAF ]( _Anim_marginLeft_w3c ); 
					})( _loopTimer_caro ); 
				}
				
				else { 
					_loopTimer_caro = (function( self ) {
						return setTimeout( _Anim_marginLeft_w3c , _mrBeepersMS ); 
					})( _loopTimer_caro ); 
				}
			}
		}
		else 
		if( parseInt( _pmAvgMultiple.style.marginLeft, 10 ) === _pmEndV ) { 
		
			if( window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame ) { 
				_loopTimer_caro = ( function( self ) {
					return window[ _CAF ]( _loopTimer_caro );
				})( _loopTimer_caro ); 
			}
			else { 
				_loopTimer_caro = ( function( self ) {
					return clearTimeout( _loopTimer_caro ); 
				})( _loopTimer_caro );
			}
			_pmAvgMultiple.style.marginLeft = parseInt( _pmAvgMultiple.style.marginLeft, 10 ) + "px"; 
		}
	} // END function
	// END ~ Carousel functionality ~ image gallery
	
	// ~ Page Visibility functionality
	function ConfirmProp() { 
		var sameProp = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "hidden" , donLuchoHAKI.RetELs.docObj() ); // hidden
		if( !sameProp ) {
			return false;
		}
		return document[ sameProp ];
	} 
	// ~ Page Visibility functionality
	function ChangeReadOut( leEvt ) { // console.log( leEvt );
		var evtTgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); // console.log( evtTgt ); 
		if( ConfirmProp() ) {
			// sessionStorage.thisTitle = _peeVeeLite_configObject.altTitle;
			_title_sessto.setItem( "thisTitle" , _peeVeeLite_configObject.altTitle );
			evtTgt.title = _peeVeeLite_configObject.altTitle;
			// console.log(evtTgt.title); / console.log(sessionStorage.thisTitle);
		} 
		else {
			// sessionStorage.thisTitle = _peeVeeLite_configObject.thisTitle;
			_title_sessto.setItem( "thisTitle" , _peeVeeLite_configObject.thisTitle );
			evtTgt.title = _peeVeeLite_configObject.thisTitle;
			// console.log(evtTgt.title); // console.log(sessionStorage.thisTitle);
		}
	}
	
	// ~ Fullscreen functionality
	function _LaunchFullscreen() {
		
		_el_fsLaunch.style.display = "none"; 
		_el_fsRestore.style.display = "block"; 
		
		if ( !!_requestFullScreen ) { 
		
			if ( _docEL[ _requestFullScreen ] ) { 
				_docEL[ _requestFullScreen ]() ; 
			} 
			
			var fullScreenElement = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "fullScreenElement" , _docObj );
			// console.log( "fullScreenElement" , fullScreenElement );
			
			var fullScreenEnabled = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "fullScreenEnabled" , _docObj );
			// console.log( "fullScreenEnabled" , fullScreenEnabled );
			
			if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
			
				if ( !!( fullScreenElement && fullScreenEnabled ) ) {
					_el_pro.innerHTML += "<p>" + _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.phrase[ window.localStorage.getItem("defaultLanguage") ].fullText + fullScreenElement + "</p>";
					_el_pro.innerHTML += "<p>" + _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.phrase[ window.localStorage.getItem("defaultLanguage") ].fullText + fullScreenEnabled + "</p>";
				}
				else
				if ( !( fullScreenElement && fullScreenEnabled ) ) { 
					if ( _docObj.webkitCurrentFullScreenElement && _docObj.webkitIsFullScreen ) { 
						_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.wcfse[ window.localStorage.getItem("defaultLanguage") ].fullText;
						_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.WIFs[ window.localStorage.getItem("defaultLanguage") ].fullText;
					}
					else
					if ( _docObj.webkitCurrentFullScreenElement && _docObj.webkitDisplayingFullscreen ) { 
						_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.wcfse[ window.localStorage.getItem("defaultLanguage") ].fullText;
						_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.WDF[ window.localStorage.getItem("defaultLanguage") ].fullText;
					}
				}
			
			}
			else 
			if( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) {
			
				if ( !!( fullScreenElement && fullScreenEnabled ) ) {
					_el_pro.innerHTML += "<p>" + _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.phrase.EN.fullText + fullScreenElement + "</p>";
					_el_pro.innerHTML += "<p>" + _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.phrase.EN.fullText + fullScreenEnabled + "</p>";
				}
				else
				if ( !( fullScreenElement && fullScreenEnabled ) ) { 
					if ( _docObj.webkitCurrentFullScreenElement && _docObj.webkitIsFullScreen ) { 
						_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.wcfse.EN.fullText;
						_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.WIFs.EN.fullText;
					}
					else
					if ( _docObj.webkitCurrentFullScreenElement && _docObj.webkitDisplayingFullscreen ) { 
						_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.wcfse.EN.fullText;
						_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.WDF.EN.fullText;
					}
				}
			
			}
			
		} // END if ( !!_requestFullScreen )
		
	} // END function
	
	// ~ Fullscreen functionality
	function _CancelFullscreen(){
		
		_el_fsLaunch.style.display = "block"; 
		_el_fsRestore.style.display = "none"; 
		
		if ( _docObj[ _cancelFullScreen ] ) { 
			_docObj[ _cancelFullScreen ]() ; 
		}
		
	} // END function
	
	// ~ SELECTED LI functionality ~ 'custom_select' -- incl. IE and W3C for LOAD AND RESIZE
	function _RunUnfrmNavLis() {
		// ~ SELECTED LI functionality ~ 'custom_select'
		var linkhref_Page;
		var str_LIclass;
		var newString;	
		for( S = 0, H = 0; S < ar_slctdClsLIsMAX, H < ar_slctdClsAsMAX; S = S + 1, H = H + 1 ){ 
			
			if( S === 0 ) {
				ar_slctdCls_LIs[S].setAttribute( "class" , "navfirst" );
			}
			if( S === ar_slctdClsLIsMAX - 1 ) {
				ar_slctdCls_LIs[S].setAttribute( "class" , "navlast" );
			}
			
			// if(_docEL.addEventListener) {console.log( "LI" , ar_slctdCls_LIs[S] ); }
			
			linkhref_Page = ar_slctdCls_childAs[H].getAttribute("href").substring(ar_slctdCls_childAs[H].getAttribute("href").lastIndexOf('/') + 1); 
			
			// VIEWPORT SIZE
			if( linkhref_Page === "" ){
				
				if( document.documentElement.addEventListener ) {
					
					// console.log( "A", ar_slctdCls_childAs[H] );
					
					if( window.document.documentElement.clientWidth > 800 ){
						if( ar_slctdCls_childAs[H].id === "colRight_dL" ){
							ar_slctdCls_childAs[H].style.display = "none";
							ar_slctdCls_LIs[S].style.display = "none";
						}
						if( ar_slctdCls_childAs[H].id === "footer_dL" ){
							ar_slctdCls_childAs[H].style.display = "none";
							ar_slctdCls_LIs[S].style.display = "none";
						} 
					}
					else
					if( (_docObj.body.offsetWidth > 383) && (window.document.documentElement.clientWidth <= 800 ) ){
						
						if( ar_slctdCls_childAs[H].id === "footer_dL" ){
							ar_slctdCls_childAs[H].style.display = "inline";
							ar_slctdCls_LIs[S].style.display = "inline";
						}
						if( ar_slctdCls_childAs[H].id === "colRight_dL" ){
							ar_slctdCls_childAs[H].style.display = "none";
							ar_slctdCls_LIs[S].style.display = "none";
						}
					}
					else
					if( _docObj.body.offsetWidth <= 383 ){
						if( ar_slctdCls_childAs[H].id === "footer_dL" ){
							ar_slctdCls_childAs[H].style.display = "inline";
							ar_slctdCls_LIs[S].style.display = "inline";
						}
						if( ar_slctdCls_childAs[H].id === "colRight_dL" ){
							ar_slctdCls_childAs[H].style.display = "inline";
							ar_slctdCls_LIs[S].style.display = "inline";
						}
					}
				}
				else
				if( document.documentElement.attachEvent ) {
					if( ar_slctdCls_childAs[H].id === "colRight_dL" || ar_slctdCls_childAs[H].id === "footer_dL" ){
						ar_slctdCls_childAs[H].style.display = "none";
						ar_slctdCls_LIs[S].style.display = "none";
					}
				}
				
			}
			
			// SELECTED ATTRIBUTE AND CLASS
			if( linkhref_Page !== "" ){
				
				// if(_docEL.addEventListener) {console.log( "A", ar_slctdCls_childAs[H] );} 
				// INDEX LEFTY RIGHTY NEXT
				
				if ( (ar_slctdCls_childAs[H].className !== "downTown") && (linkhref_Page !== "") ) { 
					
					// SELECTED CLASS
					if( _actv_Page === linkhref_Page ){
						str_LIclass = ar_slctdCls_childAs[H].parentNode.className;						
						// if(_docEL.addEventListener) {console.log( "active LI" , ar_slctdCls_childAs[H].parentNode );console.log( "str_LIclass" , str_LIclass );}						
						newString = str_LIclass + " " + "custom_selected";
						donLuchoHAKI.Utils.evt_u.AddEventoHandler( ar_slctdCls_childAs[H] , "click" , _PrevDeffo ); 
						ar_slctdCls_childAs[H].parentNode.setAttribute( "class" , newString );						
						// if(_docEL.addEventListener) {	console.log( "str_LIclass" , newString );}
					}
					
					// NEXT
				} 
			}
			if( document.documentElement.attachEvent && linkhref_Page === "next.html" ){
				ar_slctdCls_LIs[S].style.display = "none";
				ar_slctdCls_childAs[H].style.display = "none";
			}
			else
			if( document.documentElement.attachEvent && linkhref_Page === "enhance.html" ){
				ar_slctdCls_LIs[S].style.display = "none";
				ar_slctdCls_childAs[H].style.display = "none";
			}
			else
			if( document.documentElement.attachEvent && linkhref_Page === "device.html" ){
				ar_slctdCls_LIs[S].style.display = "none";
				ar_slctdCls_childAs[H].style.display = "none";
			}
			
		} // END forszzzzzzz
		
	} // END function
	
	
	// ~ ELEVATOR LINK functionality ~ upTown\downTown
	function _UpUpAndAway( evtTrgt ) { 
		
		_mrBeepersMS = donLuchoHAKI.rafUtils.get_bpms();
		_RAF = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "requestAnimationFrame" , donLuchoHAKI.RetELs.glow() );
		_CAF = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "CancelAnimationFrame" , donLuchoHAKI.RetELs.glow() );
		
		_tgtELId_elev = evtTrgt; // silky sectionContent asideContent colRight partners
		_tgtEl_elev = _docObj.getElementById(_tgtELId_elev); 
		
		if(document.documentElement.addEventListener) {
			var currentY_elev = window.pageYOffset;				
			var targetY_elev = donLuchoHAKI.PixelsArePixelsArePixels.GetOffsetTop( _tgtEl_elev ); 
		}
		else 
		if(document.documentElement.attachEvent) {			
			var currentY_elev = window.document.documentElement.scrollTop; 
			var targetYRect_elev = _tgtEl_elev.getBoundingClientRect();
			var targetY_elev = currentY_elev + ( parseInt( targetYRect_elev.top , 10 ) );
		} 
		
		// new CORE CONTROL STRUCTURE
		/**/
		if( window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame ){			
			_loopTimer_elev = window[ _RAF ]( function() {
				return _UpUpAndAway( _tgtELId_elev );
			});			
		}
		else{
			_loopTimer_elev = window.setTimeout( function() {
				return _UpUpAndAway( _tgtELId_elev );
			} , _mrBeepersMS );
		}
		
		// new CORE CONTROL STRUCTURE
		if( currentY_elev > targetY_elev ) {
			_scrollY_elev = currentY_elev - _silky_cmptdDist;
			window.scroll( 0 , _scrollY_elev );
		}
		else { 
			if( window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame ){
				_loopTimer_elev = (function( self ) {
					return window[ _CAF ]( _loopTimer_elev );
				})(_loopTimer_elev);
			}
			else {				
				_loopTimer_elev = (function( self ) {
					return clearTimeout( _loopTimer_elev );
				})(_loopTimer_elev);
			} 
		} // END new CORE CONTROL STRUCTURE
		/**/
	} // END function
	
	// ~ ELEVATOR LINK functionality ~ upTown\downTown
	function _GetDownOnIt( evtTrgt ) {
		
		_mrBeepersMS = donLuchoHAKI.rafUtils.get_bpms();
		_RAF = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "requestAnimationFrame" , donLuchoHAKI.RetELs.glow() );
		_CAF = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "CancelAnimationFrame" , donLuchoHAKI.RetELs.glow() );
		
		_tgtELId_elev = evtTrgt; // silky sectionContent asideContent colRight partners	
		_tgtEl_elev = _docObj.getElementById(_tgtELId_elev); 
		
		
		if(document.documentElement.addEventListener) {
			
			var bodyHeight_elev = donLuchoHAKI.PixelsArePixelsArePixels.bodHeight(); // _docObj.body.offsetHeight
			
			var yHeight_elev = window.innerHeight; 
			var currentY_elev = window.pageYOffset; 
			var yPos_elev = currentY_elev + yHeight_elev; 
			
			var targetY_elev = donLuchoHAKI.PixelsArePixelsArePixels.GetOffsetTop( _tgtEl_elev ); 			
		}
		else 
		if(document.documentElement.attachEvent) {
			
			// ! quirks but strict thus resort calculation to viewport and !body
			var bodyHeight_elev = donLuchoHAKI.PixelsArePixelsArePixels.glowHeight(); // ! window.document.documentElement.clientHeight;
			
			var yHeight_elev = window.document.documentElement.clientHeight; // CONSTANT
			var currentY_elev = window.document.documentElement.scrollTop; // VARIABLE	
			var yPos_elev = currentY_elev + yHeight_elev; // VARIABLE 
			
			var targetYRect_elev = _tgtEl_elev.getBoundingClientRect();	
			var targetY_elev = currentY_elev + ( parseInt( targetYRect_elev.top , 10 ) );
		} 
		
		/**/
		// new CORE CONTROL STRUCTURE
		
		if( window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame ){			
			_loopTimer_elev = window[ _RAF ]( function() {
				return _GetDownOnIt( _tgtELId_elev );
			});			
		}
		else{			
			_loopTimer_elev = window.setTimeout( function() {
				return _GetDownOnIt( _tgtELId_elev );
			} , _mrBeepersMS );			
		}
		
		// new CORE CONTROL STRUCTURE 
		if( currentY_elev > targetY_elev ) { /* if( yPos_elev > bodyHeight_elev ) { */
			if( window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame ){	
				_loopTimer_elev = (function( self ) {
					return window[ _CAF ]( _loopTimer_elev );
				})(_loopTimer_elev);
			}
			else{				
				_loopTimer_elev = (function( self ) {
					return clearTimeout( _loopTimer_elev );
				})(_loopTimer_elev);
			} 
		}
		else {
			if( currentY_elev < targetY_elev - _silky_cmptdDist ) {
				_scrollY_elev = currentY_elev + _silky_cmptdDist;
				window.scroll( 0 , _scrollY_elev );
			}
			else {
				if( window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame ){
					
					_loopTimer_elev = (function( self ) {
						return window[ _CAF ]( _loopTimer_elev );
					})(_loopTimer_elev);
				}
				else{					
					_loopTimer_elev = (function( self ) {
						return clearTimeout( _loopTimer_elev );
					})(_loopTimer_elev);
				} 
			}
			
		} // END new CORE CONTROL STRUCTURE
		/**/
		
	} // END function
	
	// ~ Geolocation functionality
	function _Geolo_Profile() { 
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_gCP_BTN , "click" , _geoOneShot ); 
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_watPos_BTN , "click" , _geoContinuous );
	} // END function
	
	// ~ Geolocation functionality
	function _geoOneShot() { 
	
		var success = function (_objPos) { 					
			
			var statClass = _el_stat_gCP.className;
			var newString = statClass + " " + "success";
			
			if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
				_el_stat_gCP.innerHTML = _obj_ConfigAll.CannedStrings.geoloOps.assets.nonFormatString.gotcha[window.localStorage.getItem("defaultLanguage")].fullText;
			}
			else 
			if( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) {
				_el_stat_gCP.innerHTML = _obj_ConfigAll.CannedStrings.geoloOps.assets.nonFormatString.gotcha.EN.fullText;
			}
			
			_el_stat_gCP.className = newString; 
			
			var _latitude = _objPos.coords.latitude;
			var _longitude = _objPos.coords.longitude;
			
			_el_gCP_lat.innerHTML = _latitude;
			_el_gCP_long.innerHTML = _longitude;
		};
		
		function error(_objPosErr) {
			
			if( typeof _objPosErr === "string" ) {
				_el_stat_gCP.innerHTML = _objPosErr;
			}
			else {
				if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
					_el_stat_gCP.innerHTML = _obj_ConfigAll.CannedStrings.geoloOps.assets.nonFormatString.choke[window.localStorage.getItem("defaultLanguage")].fullText;
				}
				else 
				if( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) {
					_el_stat_gCP.innerHTML = _obj_ConfigAll.CannedStrings.geoloOps.assets.nonFormatString.choke.EN.fullText;
				}
			}
			
			_el_stat_gCP.className = 'fail'; 
			// console.log(arguments);
		}
		
		window.navigator.geolocation.getCurrentPosition(success,error); 
		
	} // END function
	
	// ~ Geolocation functionality
	function _clear_wP() {
		window.alert("fin!");
		if (watchId > 0){
			window.navigator.geolocation.clearPosition(watchId);
		}
		
	} // END function
	
	// ~ Geolocation functionality
	function _geoContinuous() { 
		
		var success = function (_objPos) { 
			
			var statClass = _el_stat_watPos.className;
			var newString = statClass + " " + "success";
			
			if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
				_el_stat_watPos.innerHTML = _obj_ConfigAll.CannedStrings.geoloOps.assets.nonFormatString.gotcha[window.localStorage.getItem("defaultLanguage")].fullText;
			}
			else 
			if( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) {
				_el_stat_watPos.innerHTML = _obj_ConfigAll.CannedStrings.geoloOps.assets.nonFormatString.gotcha.EN.fullText;
			}
			
			
			_el_stat_watPos.className = newString; 
			
			var _accuracy = _objPos.coords.accuracy;
			var _latitude = _objPos.coords.latitude;
			var _longitude = _objPos.coords.longitude;
			
			_el_watPos_acc.innerHTML = _accuracy;
			_el_watPos_lat.innerHTML = _latitude;
			_el_watPos_long.innerHTML = _longitude;
		}; 
		
		function error(_objPosErr) {
			
			if( typeof _objPosErr === "string" ) {
				_el_stat_watPos.innerHTML = _objPosErr;
			}
			else {
				
				if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
					_el_stat_watPos.innerHTML = _obj_ConfigAll.CannedStrings.geoloOps.assets.nonFormatString.choke[window.localStorage.getItem("defaultLanguage")].fullText;
				}
				else 
				if( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) {
					_el_stat_watPos.innerHTML = _obj_ConfigAll.CannedStrings.geoloOps.assets.nonFormatString.choke.EN.fullText;
				}
			}
			
			_el_stat_watPos.className = 'fail'; 
			// console.log(arguments);
		}
		
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_watPos_CLR_BTN , "click" , _clear_wP ); 
		
		var watchId = window.navigator.geolocation.watchPosition(success,error); 
	} // END function
	
	// ~ CANVAS functionality
	function _Cnv_Ctx() {
		return _el_cnv.getContext("2d");
	} // END function
	
	// ~ CANVAS functionality
	function _Cnv_IsAndroid() {
		return _navObj.userAgent.toLowerCase().indexOf("android") >= 0;
	} // END function
	
	// ~ CANVAS functionality
	function _Cnv_RescaleRadius(pm_radius) {
	// Radius on Chrome for Android varies in [80, 160]. We want to rescale this range to [5, 50]. First get normalized value in [0, 1], then, scale out to [5, 50].
		var lePixAmt = (pm_radius - 80) / 80;
		return 10 * lePixAmt + 5; // return 40 * lePixAmt + 5
	} // END function
	
	// ~ CANVAS functionality
	function _Cnv_DRY_Paint_init( pmCnv, pmCtx){	
		
		if(document.documentElement.attachEvent){ 
			var time = new Date().getTime();
			// pmCtx.fillStyle = "#ff0000"; 
		}
		
		pmCtx.fillStyle = "#ff0000"; 
		
	} // END function
	
	// ~ CANVAS functionality
	function _Cnv_TouchHandler( event ) { 
		_Cnv_DrawTouches(event.changedTouches, event.type);
		_PrevDeffo( event );
	} // END function
	
	// ~ CANVAS functionality
	function _Cnv_MouseHandler( event ) {
		if(document.documentElement.addEventListener){
			if ( event.type === "mousedown" && event.button === 0 ) {
				_mousePressed = true;
			}
			if (_mousePressed && event.button === 0) {
				var fakeTouch = { 
					identifier : 10 , 
					pageX : event.pageX , 
					pageY : event.pageY 
				};  
				// _el_logDetails.innerHTML = "id: " + fakeTouch.identifier + "; pageX " + fakeTouch.pageX + "; pageY: " + fakeTouch.pageY + " " ;
				if (event.type === "mousedown" ) {
					var eventType = "touchstart";
				}
				else 
				if ( event.type === "mousemove" ) {
					var eventType = "touchmove";
				}
				else 
				if ( event.type === "mouseup" ) {
					var eventType = "touchend";
					_mousePressed = false;
				}
				//var eventType = event.type === "mousedown" ? "touchstart" : event.type === "mouseup" ? "touchend" : "touchmove";	
				_Cnv_DrawTouches( [ fakeTouch ] , eventType );
				_PrevDeffo( event );
			}
		} 
		else 
		if(document.documentElement.attachEvent){
			if ( event.type === "mousedown" ) {
				_mousePressed = true;
			}
			if (_mousePressed ) { 
				var fakeTouch = { 
					identifier : 10 , 
					pageX : event.clientX , 
					pageY : event.clientY 
				}; 
				// _el_logDetails.innerHTML = "id: " + fakeTouch.identifier + "; pageX " + fakeTouch.pageX + "; pageY: " + fakeTouch.pageY + " " ;
				if (event.type === "mousedown" ) {
					var eventType = "touchstart";
				}
				else 
				if ( event.type === "mousemove" ) {
					var eventType = "touchmove";
				}
				else 
				if ( event.type === "mouseup" ) {
					var eventType = "touchend";
					_mousePressed = false;
				}
				//var eventType = event.type === "mousedown" ? "touchstart" : event.type === "mouseup" ? "touchend" : "touchmove";	
				_Cnv_DrawTouches( [ fakeTouch ] , eventType );
				_PrevDeffo( event );
			}
		}
	} // END function
	
	function _Cnv_DrawTouches( pm_touches, pm_eventType ) {
		
		if( !!_Cnv_Ctx() ){	
			var ctx = _Cnv_Ctx();
			_Cnv_DRY_Paint( ctx, pm_touches, pm_eventType );
		}
		
	} // END function
	
	// ~ CANVAS functionality
	function _Cnv_DRY_Paint( le_ctx, le_touches, le_eventType ) {
		
		var L;
		var tchesLen = le_touches.length;
		
		for ( L = 0; L < tchesLen; L = L + 1 ) { 
			
			var touch = le_touches[L];
			
			if ( !( touch.identifier in _obj_touchMap ) ) {
				_obj_touchMap[ touch.identifier ] = _nextCount;
				_nextCount = _nextCount + 1;
			}
			// if(document.documentElement.addEventListener) { console.log("_obj_touchMap" , _obj_touchMap ); }	
			// _el_logDetails.innerHTML = "_nextCount: " + _nextCount + "; id: " + le_touches[L].identifier + "; pageX " + le_touches[L].pageX + "; pageY: " + le_touches[L].pageY + ". " ;
			
			// new
			le_ctx.save();
			if(document.documentElement.attachEvent){ 
				var time = new Date().getTime();
			} // END new
			
			le_ctx.beginPath();
			
			_radiusSupported = (touch.webkitRadiusX > 1); 
			if (_radiusSupported) {
				var radius = touch.webkitRadiusX;
			}
			else {
				var radius = 15;
			}
			if ( _Cnv_IsAndroid() ) {
				radius = _Cnv_RescaleRadius( radius );
			}
			if (radius > 100) {
				if(document.documentElement.addEventListener){ /*console.error( "Got large webkitRadiusX: " + touch.webkitRadiusX );*/ }
				radius = 15; // 100
			}
			if (_pointMode) { 
				radius = 1 / _scale;
			} 
			if (le_eventType === "touchend") {
				radius = radius + 1;
			}
			// _el_logDetails.innerHTML += "radius: " + radius + ". " ;
			// _el_logDetails.innerHTML += "_scale: " + _scale + ". " ;	
			if(document.documentElement.addEventListener) {
				var win_offY = window.pageYOffset;
				var cnv_offTop = donLuchoHAKI.PixelsArePixelsArePixels.GetOffsetTop( _el_cnv ); 
				var win_offX = window.pageXOffset;
				var cnv_offLeft = donLuchoHAKI.PixelsArePixelsArePixels.GetOffsetLeft( _el_cnv ); 
			}
			else 
			if(document.documentElement.attachEvent) { 
				var cnv_gcbRect = _el_cnv.getBoundingClientRect();				
				var win_offY = window.document.documentElement.scrollTop; 
				var cnv_offTop = ( parseInt( cnv_gcbRect.top , 10 ) );
				var win_offX = window.document.documentElement.scrollLeft; 
				var cnv_offLeft = ( parseInt( cnv_gcbRect.left , 10 ) );
			} 
			// _el_logDetails.innerHTML = "touch.pageX: " + touch.pageX + ". ";
			// _el_logDetails.innerHTML += "cnv_offLeft: " + cnv_offLeft + ". ";
			// _el_logDetails.innerHTML += "touch.pageY: " + touch.pageY + ". ";
			// _el_logDetails.innerHTML += "cnv_offTop: " + cnv_offTop + ". ";
			/* if(document.documentElement.addEventListener){ 
				// For debugging, output the radius we receive.
				le_ctx.font = "20pt Arial"; // "20pt Arial"
				le_ctx.fillStyle = "#000000";
				le_ctx.clearRect(0, 0, 100, 100);
				le_ctx.fillText( "radius: " + radius, 50, 70); 
			} */	
			var xc = ( (touch.pageX - cnv_offLeft) * _scale  );
			var yc = ( (touch.pageY - cnv_offTop) * _scale  ); 
			le_ctx.arc( xc , yc , radius * _scale , _startCircle , _fullCircle , false );	
			le_ctx.closePath(); 
			
			// Fill circle on start/move
			if ( le_eventType !== "touchend" ) { 
			
				if( _pointMode ) { 	var opacity = 1; }
				else { var opacity = 0.1; } 
				var hue = _obj_touchMap[ touch.identifier ] + 180; // ( _obj_touchMap[ touch.identifier ] * 30 ) % 256
				var saturation = 100;
				var luminosity = 40;
				if ( _enableForce && touch.webkitForce ) { luminosity = Math.round( touch.webkitForce / 0.4 * 50 + 20 ); }
				
				// BLACK ~ hsla( 0, 0%, 0%, 1) // WHITE ~ hsla( 360, 100%, 100%, 1) 
				if(document.documentElement.addEventListener) {
					le_ctx.fillStyle = 'hsla(' + hue + ', ' + saturation + '%, ' + luminosity + '%, ' + opacity + ')';
				}
				else 
				if(document.documentElement.attachEvent) { 
					le_ctx.fillStyle =  "rgba(0,204,204," + opacity + ")";
				}
				
				le_ctx.fill();
			} 
			// Outline circle on start/end
			if ( le_eventType !== "touchmove" ) {
				if ( le_eventType === "touchstart" ) {
					le_ctx.strokeStyle = "#FFFAF0"; // whitish-orange
				}
				else {
					le_ctx.strokeStyle = "#FFCC33"; // orange
				} 
				le_ctx.lineWidth = 2;
				le_ctx.stroke();
			} // END if ( le_eventType !== "touchmove" )	
			
		} // END GINORMOUS for loop 
		
	} // END function
	
	// ~ CANVAS functionality
	function _Cnv_Resizer() {
		// ~ CANVAS functionality
		var glow = donLuchoHAKI.RetELs.glow();
		if ( !_scale && !!glow.devicePixelRatio ) {
			_scale = donLuchoHAKI.DeviceCapabilities.html5_Actual_DevicePixelRatio();
		} 
		else 
		if ( !_scale && !glow.devicePixelRatio ) {
			_scale = 1;
		}
		
		// ~ CANVAS functionality
		var str_Width = _el_cnvMemb.offsetWidth;
		var str_Height = _el_cnvMemb.offsetHeight; 
		_el_cnv.width = str_Width * _scale;
		_el_cnv.style.width = str_Width + "px";		
		_el_cnv.height = str_Height * _scale;
		_el_cnv.style.height = str_Height + "px"; 
		
	} // END function
	
	// ~ PageFlip Functionality
	function _PageLeft( leEvt ) { 
		
		_evtTrgt_pflip = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt );
		_nm_bgYPOS_JbzPg = _nm_bgYPOS_JbzPg + _nm_Height_JbzPg; // update Y position 
		
		if( _nm_bgYPOS_JbzPg === ( _nm_negator * _nm_el_bgMAX ) ){
			// _nm_bgYPOS_JbzPg = 0;
			_nm_bgYPOS_JbzPg = ( _nm_negator * (_nm_el_bgMAX - _nm_Height_JbzPg) )
		} 
		else 
		if( _nm_bgYPOS_JbzPg === ( _nm_Height_JbzPg ) ){ 
			// _nm_bgYPOS_JbzPg = 0;
			_nm_bgYPOS_JbzPg = ( _nm_negator * ( _nm_rowCt * _nm_Height_JbzPg ) );
		}
		
		// 0 189 378 567 756 945 [ 1134 ] 
		
		_el_leftpage = _docObj.getElementById( _evtTrgt_pflip.id ); 
		_el_rightpage = _docObj.getElementById( "rightpage" );
		_el_bookspine = _docObj.getElementById( "bookspine" ); 
				
		function AnimFlipRight( objPmOne, objPmTwo, objPmThree ) { 	
			
			_mrBeepersMS = donLuchoHAKI.rafUtils.get_bpms(); 
			objPmOne.style.backgroundPosition  = "" + _nm_negator * ( parseInt( _SetLConn() ,10 ) ) + "px" + " " + _nm_bgYPOS_JbzPg + "px";
			objPmThree.style.backgroundPosition = "-263px 0px";
			
			var Step = function( frm_TmStmp ) { 
				var frm_Diff = frm_TmStmp - frm_start;
				
				objPmThree.style.backgroundPosition = "-143px 0px";
				objPmTwo.style.backgroundPosition = "" + _nm_negator * ( parseInt( _SetRConn() ,10 ) ) + "px" + " " + _nm_bgYPOS_JbzPg + "px";
				// objPmTwo.style.backgroundPosition = _nm_Width_JbzPg + "px " + _nm_bgYPOS_JbzPg + "px";
				
				frm_start = frm_TmStmp;
				_loopTimer_pFlip = setTimeout( Step , _mrBeepersMS );
			}; // END Step()
			
			_loopTimer_pFlip = setTimeout( Step , _mrBeepersMS );
		} // END function 
		
		var frm_start = new Date().getTime();
		AnimFlipRight( _el_leftpage, _el_rightpage, _el_bookspine );
		
	} // END function
	
	// ~ PageFlip Functionality
	function _PageRight( leEvt ) {
		
		_evtTrgt_pflip = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt );
		_nm_bgYPOS_JbzPg = _nm_bgYPOS_JbzPg - _nm_Height_JbzPg; // update Y position // note: minus page height 
		
		if( Math.abs( _nm_bgYPOS_JbzPg ) === _nm_el_bgMAX ){
			_nm_bgYPOS_JbzPg = 0;
		} 
		
		_el_rightpage = _docObj.getElementById( _evtTrgt_pflip.id );
		_el_leftpage = _docObj.getElementById( "leftpage" );
		_el_bookspine = _docObj.getElementById( "bookspine" ); 
		
		function AnimFlipLeft( objPmOne, objPmTwo, objPmThree ) {
			_mrBeepersMS = donLuchoHAKI.rafUtils.get_bpms(); 
						
			// objPmTwo.style.backgroundPosition = _nm_Width_JbzPg + "px " + _nm_bgYPOS_JbzPg + "px";	
			objPmTwo.style.backgroundPosition = "" + _nm_negator * ( parseInt( _SetRConn() ,10 ) ) + "px" + " " + _nm_bgYPOS_JbzPg + "px";			
			objPmThree.style.backgroundPosition = "-23px 0px"; 
			
			var Step = function( frm_TmStmp ) { 
				var frm_Diff = frm_TmStmp - frm_start; 
				
				objPmThree.style.backgroundPosition = "-143px 0px"; 
				objPmOne.style.backgroundPosition  = "" + _nm_negator * ( parseInt( _SetLConn() ,10 ) ) + "px" + " " + _nm_bgYPOS_JbzPg + "px";	
				
				frm_start = frm_TmStmp;
				_loopTimer_pFlip = setTimeout( Step , _mrBeepersMS );
				
			}; // END Step()
			_loopTimer_pFlip = setTimeout( Step , _mrBeepersMS );
			
		} // END function
		
		var frm_start = new Date().getTime(); 
		AnimFlipLeft( _el_leftpage, _el_rightpage, _el_bookspine ); 

	} // END function
	
	// ~ PageFlip Functionality
	function _JabezBG_Ops() { 
		_el_leftpage = _docObj.getElementById( "leftpage" );
		_el_rightpage = _docObj.getElementById( "rightpage" );	
		/* ~~~~~~~~~~~~~~~~~~~~~ _el_coverwrapper   ~~~~~~~~~~~~~~~~~~~~~~ */
		if(document.documentElement.addEventListener){ 
			_coverWidth = parseInt( _GetComputedStyle( _el_coverwrapper, "width" ) , 10 );
		}
		else
		if(document.documentElement.attachEvent){ 
			var coverRect = _el_coverwrapper.getBoundingClientRect();
			var coverX = parseInt( coverRect.left, 10) ; // 34
			var coverY = parseInt( coverRect.top, 10) ; // 1509
			var endcoverX = parseInt( coverRect.right, 10) ; // 258
			var endcoverY = parseInt( coverRect.bottom, 10) ; // 1719
			_coverWidth = endcoverX - coverX; // var coverHeight = endcoverY - coverY;
		} 
		var coverCtx = ( _nm_BGcoverWidth - _coverWidth ) / 2;
		_cvrWrprLeftqty = _nm_negator * ( coverCtx );		
		_el_coverwrapper.style.backgroundColor = "transparent";
		
		if( (_str_host === 'donlucho.github.io') || ( _str_host === 'artavia.gitlab.io') ){	
			_el_coverwrapper.style.backgroundImage = "url(" + _obj_ConfigAll.AssetURLs.flipBookOps.assets.frame.pathLVL0_IO + _obj_ConfigAll.AssetURLs.flipBookOps.assets.frame.asset + ")";
		} 
		else {
			_el_coverwrapper.style.backgroundImage = "url(" + _obj_ConfigAll.AssetURLs.flipBookOps.assets.frame.pathLVL0 + _obj_ConfigAll.AssetURLs.flipBookOps.assets.frame.asset + ")";
		}
		
		
		_el_coverwrapper.style.backgroundPosition = "" + _cvrWrprLeftqty + "px " + "0px";
		_el_coverwrapper.style.backgroundRepeat = "no-repeat";			
		/* ~~~~~~~~~~~~~~~~~~~~~ _el_bookspine   ~~~~~~~~~~~~~~~~~~~~~~ */
		_el_bookspine = _docObj.getElementById( "bookspine" );
		if(document.documentElement.addEventListener){ 
			_spineWidth = parseInt( _GetComputedStyle( _el_bookspine, "width" ) , 10 );
		}
		else
		if(document.documentElement.attachEvent){ 
			var spineRect = _el_bookspine.getBoundingClientRect();
			var spineX = parseInt( spineRect.left, 10) ; // 34
			var spineY = parseInt( spineRect.top, 10) ; // 1509
			var endspineX = parseInt( spineRect.right, 10) ; // 152
			var endspineY = parseInt( spineRect.bottom, 10) ; // 1719
			_spineWidth = endspineX - spineX; // var spineHeight = endspineY - spineY; 	
		}
		_el_bookspine.style.backgroundColor = "transparent"; // "#EBC793"
		
		if( (_str_host === 'donlucho.github.io') || ( _str_host === 'artavia.gitlab.io') ){
			_el_bookspine.style.backgroundImage = "url(" + _obj_ConfigAll.AssetURLs.flipBookOps.assets.threeD.pathLVL0_IO + _obj_ConfigAll.AssetURLs.flipBookOps.assets.threeD.asset + " )";
		} else {
			_el_bookspine.style.backgroundImage = "url(" + _obj_ConfigAll.AssetURLs.flipBookOps.assets.threeD.pathLVL0 + _obj_ConfigAll.AssetURLs.flipBookOps.assets.threeD.asset + " )";
		}
		
		
		_el_bookspine.style.backgroundPosition = "-143px 0px"; // "-23px 0px" "-143px 0px" "-263px 0px"
		_el_bookspine.style.backgroundRepeat = "no-repeat";
		
		var spineCtx = ( _coverWidth - _spineWidth ) / 2;
		_spineLeft = ( spineCtx ); 
		_el_bookspine.style.left = _spineLeft + "px";
		
		/* ~~~~~~~~~~~~~~~~~~~~~ _el_leftpage   ~~~~~~~~~~~~~~~~~~~~~~ */
		if(document.documentElement.addEventListener){ 
			_leftPageWidth = parseInt( _GetComputedStyle( _el_leftpage, "width" ) , 10 );
		}
		else
		if(document.documentElement.attachEvent){ 
			var leftpageRect = _el_leftpage.getBoundingClientRect();
			var lpX = parseInt( leftpageRect.left , 10);
			var lpY = parseInt( leftpageRect.top , 10);
			var endlpX = parseInt( leftpageRect.right , 10);
			var endlpY = parseInt( leftpageRect.bottom , 10);
			_leftPageWidth = endlpX - lpX;
		}
		
		if( (_str_host === 'donlucho.github.io') || ( _str_host === 'artavia.gitlab.io') ){
			_el_leftpage.style.backgroundImage = "url(" + _obj_ConfigAll.AssetURLs.flipBookOps.assets.tenPage.pathLVL0_IO + _obj_ConfigAll.AssetURLs.flipBookOps.assets.tenPage.asset + " )";
		} else {
			_el_leftpage.style.backgroundImage = "url(" + _obj_ConfigAll.AssetURLs.flipBookOps.assets.tenPage.pathLVL0 + _obj_ConfigAll.AssetURLs.flipBookOps.assets.tenPage.asset + " )";
		}
		
		_leftConLeft = _SetLConn(); // 5
		
		var lLeftqty = _nm_negator * ( parseInt( _leftConLeft ,10 ) );	
		var lTopqty = _nm_negator * ( _leftConTop );		
		_el_leftpage.style.backgroundPosition  = lLeftqty + "px" + " " + lTopqty + "px";
		_el_leftpage.style.backgroundRepeat = "no-repeat";
		/* ~~~~~~~~~~~~~~~~~~~~~ _el_rightpage   ~~~~~~~~~~~~~~~~~~~~~~ */
		if(document.documentElement.addEventListener){ 
			_rightPageWidth = parseInt( _GetComputedStyle( _el_rightpage, "width" ) , 10 );
		}
		else
		if(document.documentElement.attachEvent){ 
			var rightpageRect = _el_rightpage.getBoundingClientRect();
			var rpX = parseInt( rightpageRect.left , 10);
			var rpY = parseInt( rightpageRect.top , 10);
			var endrpX = parseInt( rightpageRect.right , 10);
			var endrpY = parseInt( rightpageRect.bottom , 10);
			_rightPageWidth = endrpX - rpX;
		}
		_rightPageLeftX = _leftPageWidth;
		_el_rightpage.style.left = _rightPageLeftX + "px";
		
		if( (_str_host === 'donlucho.github.io') || ( _str_host === 'artavia.gitlab.io') ){
			_el_rightpage.style.backgroundImage = "url(" + _obj_ConfigAll.AssetURLs.flipBookOps.assets.tenPage.pathLVL0_IO + _obj_ConfigAll.AssetURLs.flipBookOps.assets.tenPage.asset + " )";
		} else {
			_el_rightpage.style.backgroundImage = "url(" + _obj_ConfigAll.AssetURLs.flipBookOps.assets.tenPage.pathLVL0 + _obj_ConfigAll.AssetURLs.flipBookOps.assets.tenPage.asset + " )";
		}
		
		
		_rightConLeft = _SetRConn(); // 179
		
		var rLeftqty = _nm_negator * ( parseInt( _rightConLeft ,10 ) );
		var rTopqty = _nm_negator * ( _rightConTop );
		_el_rightpage.style.backgroundPosition = rLeftqty + "px" + " " + rTopqty + "px";
		_el_rightpage.style.backgroundRepeat = "no-repeat";	
	} // END function
	
	// ~ PageFlip Functionality
	function _SetLConn(){
		if( document.documentElement.addEventListener ){ 
			if( ( document.documentElement.clientWidth >= 1600 ) ){
				return 5;
			}
			else
			if( ( document.documentElement.clientWidth >= 1500 ) && ( document.documentElement.clientWidth < 1600 ) ){
				return 5;
			}
			else
			if( ( document.documentElement.clientWidth >= 1400 ) && ( document.documentElement.clientWidth < 1500 ) ){
				return 5;
			}
			else
			if( ( document.documentElement.clientWidth >= 1300 ) && ( document.documentElement.clientWidth < 1400 ) ){
				return 7;
			}
			else
			if( ( document.documentElement.clientWidth >= 1200 ) && ( document.documentElement.clientWidth < 1300 ) ){
				return 16;
			}
			else
			if( ( document.documentElement.clientWidth >= 1100 ) && ( document.documentElement.clientWidth < 1200 ) ){
				return 20;
			}
			else
			if( ( document.documentElement.clientWidth >= 1000 ) && ( document.documentElement.clientWidth < 1100 ) ){
				return 26;
			}
			else
			if( ( document.documentElement.clientWidth >= 900 ) && ( document.documentElement.clientWidth < 1000 ) ){
				return 30;
			}
			else
			if( (document.documentElement.clientWidth >= 800 ) && (document.documentElement.clientWidth < 900 ) ){
				return 36;
			}
		}
		else
		if( document.documentElement.attachEvent ){
			return 5;
		}
	} // END function
	
	// ~ PageFlip Functionality
	function _SetRConn() {
		if( document.documentElement.addEventListener ){ 
			if( ( document.documentElement.clientWidth >= 1600 ) ){
				return 179;
			}
			else
			if( ( document.documentElement.clientWidth >= 1500 ) && ( document.documentElement.clientWidth < 1600 ) ){
				return 185;
			}
			else
			if( ( document.documentElement.clientWidth >= 1400 ) && ( document.documentElement.clientWidth < 1500 ) ){
				return 192;
			}
			else
			if( ( document.documentElement.clientWidth >= 1300 ) && ( document.documentElement.clientWidth < 1400 ) ){
				return 197;
			}
			else
			if( ( document.documentElement.clientWidth >= 1200 ) && ( document.documentElement.clientWidth < 1300 ) ){
				return 190;
			}
			else
			if( ( document.documentElement.clientWidth >= 1100 ) && ( document.documentElement.clientWidth < 1200 ) ){
				return 194;
			}
			else
			if( ( document.documentElement.clientWidth >= 1000 ) && ( document.documentElement.clientWidth < 1100 ) ){
				return 200;
			}
			else
			if( ( document.documentElement.clientWidth >= 900 ) && ( document.documentElement.clientWidth < 1000 ) ){
				return 204;
			}
			else
			if( (document.documentElement.clientWidth >= 800 ) && (document.documentElement.clientWidth < 900 ) ){
				return 210;
			}
		}
		else
		if( document.documentElement.attachEvent ){
			return 179;
		}
	} // END function
	
	// EXTRA ~ Axis functionality ~ just x\y poles... no Z
	function _CenterPoles(){
	
		var LessonConRect = _el_LessonCon.getBoundingClientRect();
		var LcX = parseInt( LessonConRect.left, 10);
		var LcY = parseInt( LessonConRect.top, 10);
		var endLcX = parseInt( LessonConRect.right, 10);
		var endLcY = parseInt( LessonConRect.bottom, 10);
		_lcWidth = endLcX - LcX;
		_lcHeight = endLcY - LcY;
		
		// _el_View.style.top = _lcHeight/2 + "px";
		// _el_View.style.top = ( (_lcHeight/2 + _lcHeight/4) /2 ) + "px";
		_el_View.style.top = _lcHeight/4 + "px";
		
		_el_View.style.left = ( 0.6 * _lcWidth ) + "px";
		
	} // END function
	
	// EXTRA ~ Axis functionality ~ just x\y poles... no Z
	function _OpenBox() { 
		_el_Top.style[ transform ] = "translateY(-100px) rotateX(0deg)"; // OK	
		_el_Bottom.style[ transform ] = "translateY(100px) rotateX(0deg)"; // OK
	} // END function // eg. - "-webkit-transform 1s"
	
	// EXTRA ~ Axis functionality ~ just x\y poles... no Z
	function _CloseBox() { 
		_el_Top.style[ transform ] = "translateY(-100px) rotateX(-90deg)"; // OK
		_el_Bottom.style[ transform ] = "translateY(0px) translateZ(100px) rotateX(90deg)"; // OK
	} // END function // eg. - "-webkit-transform 1s"
	
	// EXTRA ~ Axis functionality ~ just x\y poles... no Z
	function _Set_poles_XY( leEvt ) { 
		_el_View.style[ perspective ] = "800px"; 
		_el_View.style[ transformStyle ] = "preserve-3d";
		_el_View.style[ transition ] = "" + css_transform + " 0.3515625s"; // eg. - "-webkit-transform 1s"
		
		var evtTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt );
		if( evtTrgt.id === "xSlide" ){
			_el_xSlide = _docObj.querySelector( "#" + evtTrgt.id );
			_xVal = _el_xSlide.value;
		}
		else 
		if( evtTrgt.id === "ySlide" ){
			_el_ySlide = _docObj.querySelector( "#" + evtTrgt.id );
			_yVal = _el_ySlide.value;
		}		
		_el_View.style[ transform ] = "rotateX(" + _xVal + "deg) rotateY("+ _yVal + "deg)";
		
		// _el_Top.style[ transformOrigin ] = "50% 100%";
		_el_Top.style[ transition ] = "" + css_transform + " 0.3515625s";
		_el_Top.style[ transform ] = "translateY(-100px)"; 
		
		// _el_Bottom.style[ transformOrigin ] = "50% 100%";
		_el_Bottom.style[ transition ] = "" + css_transform + " 0.3515625s"; 
		_el_Bottom.style[ transform ] = "translateY(100px) translateZ(0px) rotateX(0deg)"; //OK
		
		_el_Ball.style[ transform ] = "translateZ(50px) rotateX(90deg) translateY(10px)";
		_el_Left.style[ transform ] = "translateZ(50px) translateX(-50px) rotateY(-90deg)";	
		_el_Right.style[ transform ] = "translateZ(50px) translateX(50px) rotateY(90deg)"; 
		_el_Front.style[ transform ] = "translateZ(100px)";
		_el_YAxis.style[ transform ] = "rotate(90deg)";
		_el_ZAxis.style[ transform ] = "rotateY(-90deg)"; // eg. - "-webkit-transform 1s"
	} // END function
	
	// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html
	function _SetGardenPoint(){
		if( _actv_Page === "device.html" ) {
			_el_dvc_garden.style.height = _el_dvc_garden.offsetWidth + "px"; // _el_dvc_garden + "px"
		} 
	} // END function
	
	// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html
	function _CompassResizer(){	
		if( _actv_Page === "device.html" ) {
			
			if( document.documentElement.addEventListener ){
				
				if( ( (_chopperPoint === "iOS4") || ( _chopperPoint === "Newer_Android_Device" ) ) && ( donLuchoHAKI.PixelsArePixelsArePixels.glowWidth() <= 320 ) ){
				}
				else {
					_docObj.getElementById( "compass" ).style.height = _docObj.getElementById( "compass" ).offsetWidth + "px";
					_docObj.getElementById( "spinner" ).style.height = _docObj.getElementById( "spinner" ).offsetWidth + "px";
					_docObj.getElementById( "pin" ).style.height = _docObj.getElementById( "pin" ).offsetWidth + "px";
				}
			}
			
		}
	} // END function
	
	// TOUCH ~ untangle.html
	function _TS_untngl( leEvt ) { 		
		var evtTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		var ar_CHGDtouches = leEvt.changedTouches, _tch_fingerCount = ar_CHGDtouches.length, c; 
		if ( _tch_fingerCount === 1 ) {
			for( c = 0; c < _tch_fingerCount; c = c + 1 ) { 
				if ( evtTrgt.tagName.toLowerCase() === "p" ) { // if (ar_CHGDtouches[c].id === "sectionContent"){}				
					_tch_startX = ar_CHGDtouches[ c ].pageX;
					_tch_startY = ar_CHGDtouches[ c ].pageY;					
					var hack1 = ar_CHGDtouches[ c ].target.parentNode; 
					_hack1_ID = hack1.id; // console.log( "_TE_untngl() - target.parentNode:" , hack1 );	// console.log( "_TS_untngl() - _hack1_ID:" , _hack1_ID ); // console.log( "_tch_startX" , _tch_startX ); // console.log( "_tch_startY" , _tch_startY );
				}
				else 
				if ( evtTrgt.tagName.toLowerCase() === "div" ) {
					var hack1 = ar_CHGDtouches[ c ].target; 
					_hack1_ID = hack1.id; // console.log( "_TS_untngl() - target:" , hack1 ); // console.log( "_TS_untngl() - _hack1_ID:" , _hack1_ID ); // console.log( "_tch_startX" , _tch_startX ); // console.log( "_tch_startY" , _tch_startY );
				} 
			} // end for()
		}// end if()
		donLuchoHAKI.Utils.evt_u.RetValFalprevDef( leEvt );
	} // END function
	
	// TOUCH ~ untangle.html
	function _TM_untngl( leEvt ) { // lenCHGD
		var evtTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		var ar_CHGDtouches = leEvt.changedTouches, lenCHGD = ar_CHGDtouches.length, c; 
		if ( lenCHGD === 1 ) {
			for( c = 0; c < lenCHGD; c = c + 1 ) { 
				if ( lenCHGD === 1 && evtTrgt.tagName.toLowerCase() === "p" ) { 
					_tch_lastX = ar_CHGDtouches[ c ].pageX;
					_tch_lastY = ar_CHGDtouches[ c ].pageY; 
					var hack1 = ar_CHGDtouches[ c ].target.parentNode; 
					_hack1_ID = hack1.id; // console.log( "_TM_untngl() - _hack1_ID:" , _hack1_ID ); // console.log( "_tch_lastX" , _tch_lastX ); // console.log( "_tch_lastY" , _tch_lastY );
				}
				else 
				if ( lenCHGD === 1 && evtTrgt.tagName.toLowerCase() === "div" ) {
					var hack1 = ar_CHGDtouches[ c ].target; 
					_hack1_ID = hack1.id; // console.log( "_TM_untngl() - _hack1_ID:" , _hack1_ID ); // console.log( "_tch_lastX" , _tch_lastX ); // console.log( "_tch_lastY" , _tch_lastY );
				}
			} // end for()
		} // end if()
		donLuchoHAKI.Utils.evt_u.RetValFalprevDef( leEvt );
	} // END function
	
	// TOUCH ~ untangle.html
	function _TE_untngl( leEvt ) { 	
		var evtTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		var ar_CHGDtouches = leEvt.changedTouches, _tch_fingerCount = ar_CHGDtouches.length, c; // console.log( "_TE_untngl() _tch_startX" , _tch_startX ); // console.log( "_TE_untngl() _hack1_ID" , _hack1_ID ); // console.log( "_TE_untngl() _tch_startY" , _tch_startY ); 
		
		if ( _tch_fingerCount === 1 && _tch_lastX !== 0 ) { 
			for( c = 0; c < _tch_fingerCount; c = c + 1 ) { 
				if ( _tch_fingerCount === 1 && evtTrgt.tagName.toLowerCase() === "p" ) { 				
					
					_tch_lastX = ar_CHGDtouches[ c ].pageX;
					_tch_lastY = ar_CHGDtouches[ c ].pageY; 
					_tch_swipeLength = Math.round( Math.sqrt( Math.pow( _tch_lastX - _tch_startX, 2 ) + Math.pow( _tch_lastY - _tch_startY, 2 ) ) ); 
					
					if ( _tch_swipeLength >= 72 ) { 
						// _minLength -- the shortest distance the user may swipe  // console.log ("_TE_untngl() _tch_swipeLength" , _tch_swipeLength );
						_AngleCalc();
						_GetSwipeDirection();
						_ApplyColorCoat();
					} // console.log ("_TE_untngl() _tch_fingerCount " , _tch_fingerCount );  // console.log( "_TE_untngl() _tch_lastX" , _tch_lastX ); // console.log( "_TE_untngl() _tch_lastY" , _tch_lastY ); 
				}
				else 
				if ( _tch_fingerCount === 1 && evtTrgt.tagName.toLowerCase() === "div" ) { 
					_tch_lastX = ar_CHGDtouches[ c ].pageX;
					_tch_lastY = ar_CHGDtouches[ c ].pageY; 
					_tch_swipeLength = Math.round( Math.sqrt( Math.pow( _tch_lastX - _tch_startX, 2 ) + Math.pow( _tch_lastY - _tch_startY, 2 ) ) ); 	
					if ( _tch_swipeLength >= 72 ) { 
						// 72 EQ. to the shortest distance the user may swipe  // console.log ("_TE_untngl() _tch_swipeLength" , _tch_swipeLength );
						_AngleCalc();
						_GetSwipeDirection();
						_ApplyColorCoat();
					} // console.log ("_TE_untngl() _tch_fingerCount " , _tch_fingerCount ); // console.log( "_TE_untngl() _tch_lastX" , _tch_lastX ); // console.log( "_TE_untngl() _tch_lastY" , _tch_lastY ); 
				}
			} // end for()
		} // end if() 		
		donLuchoHAKI.Utils.evt_u.RetValFalprevDef( leEvt ); // console.log( "_tch_lastX post reset" , _tch_lastX ); // console.log( "_tch_lastY post reset" , _tch_lastY ); 		
	} // END function
	
	// TOUCH ~ untangle.html
	function _AngleCalc() { 
		var difX = _tch_startX - _tch_lastX;
		var difY = _tch_lastY - _tch_startY;	
		//the distance - rounded - in pixels
		var hypotenuseZ = Math.round( Math.sqrt( Math.pow( difX, 2 ) + Math.pow( difY, 2 ) ) ); 
		//angle in radians ( Cartesian system )
		var angleRadians = Math.atan2( difY, difX ); 
		//angle in degrees
		_tch_swipeDegrees = Math.round( angleRadians * 180 / Math.PI ); 
		if ( _tch_swipeDegrees < 0 ) {
			_tch_swipeDegrees = 360 - Math.abs( _tch_swipeDegrees );
		} 
	} // END function
	
	// TOUCH ~ untangle.html
	function _GetSwipeDirection() { 
		if ( ( _tch_swipeDegrees <= 45 ) && ( _tch_swipeDegrees >= 0 ) ) {
			_tch_swipeDirection = "left";
		}
		else
		if ( ( _tch_swipeDegrees <= 360 ) && ( _tch_swipeDegrees >= 315 ) ) {
			_tch_swipeDirection = "left";
		}
		else
		if ( ( _tch_swipeDegrees >= 135 ) && ( _tch_swipeDegrees <= 225 ) ) {
			_tch_swipeDirection = "right";
		}
		else
		if ( ( _tch_swipeDegrees > 45 ) && ( _tch_swipeDegrees < 135 ) ) {
			_tch_swipeDirection = "down";
		}
		else {
			_tch_swipeDirection = "up";
		}
	} // END function
	
	// TOUCH ~ untangle.html
	function _ApplyColorCoat() { 
		if ( _tch_swipeDirection === "left" ) {
			_docObj.getElementById( _hack1_ID ).style.backgroundColor = "orange";
		}
		else
		if ( _tch_swipeDirection === "right" ) {
			_docObj.getElementById( _hack1_ID ).style.backgroundColor = "green";
		}
		else
		if ( _tch_swipeDirection === "up" ) {
			_docObj.getElementById( _hack1_ID ).style.backgroundColor = "maroon";
		}
		else
		if ( _tch_swipeDirection === "down" ) {
			_docObj.getElementById( _hack1_ID ).style.backgroundColor = "purple";
		}
	} // END function
	
	// TOUCH ~ interaction.html
	function _DeployKuma() {
		
		_el_gesture.style.width = _threeU + "px";
		_el_gesture.style.height = _threeU + "px";
		_el_gesture.style.left = ( ( _el_gesture.parentElement.offsetWidth - _el_gesture.offsetWidth) / 2 ) + "px";
		_el_gesture.style.top = ( ( _el_gesture.parentElement.offsetHeight - _el_gesture.offsetHeight) / 2 ) + "px";
		
		_el_green.style.width = _oneU + "px";
		_el_green.style.height = _oneU + "px";
		_el_green.style.top = "10px";
		_el_solid.style.width = _ninetyTwoPerU + "px";
		_el_solid.style.height = _ninetyTwoPerU + "px";
		_el_solid.style.top = "10px";
		
		_el_blue.style.width = _oneU + "px";
		_el_blue.style.height = _oneU + "px";
		_el_blue.style.top = ( _el_green.offsetTop + _el_green.offsetHeight + 10 ) + "px";
		_el_dashed.style.width = _ninetyTwoPerU + "px";
		_el_dashed.style.height = _ninetyTwoPerU + "px";
		_el_dashed.style.top = ( _el_solid.offsetTop + _el_solid.offsetHeight + 10 ) + "px";
		
		_el_red.style.width = _oneU + "px";
		_el_red.style.height = _oneU + "px";
		_el_red.style.top = ( _el_blue.offsetHeight + 10 + _el_green.offsetTop + _el_green.offsetHeight + 10 ) + "px";
		_el_dotted.style.width = _ninetyTwoPerU + "px";
		_el_dotted.style.height = _ninetyTwoPerU + "px";
		_el_dotted.style.top = ( _el_dashed.offsetHeight + 12 + _el_solid.offsetTop + _el_solid.offsetHeight + 12 ) + "px";
		
		_el_kuma.style.height = parseInt( (_el_gesture.offsetHeight + _el_green.offsetHeight + _el_blue.offsetHeight + _el_red.offsetHeight) , 10) + "px";
	} // END function
	
	
	// TOUCH ~ interaction.html
	function _TS_interact( evt ) { 	
		var ar_CHGDtouches = evt.changedTouches, lenCHGD = ar_CHGDtouches.length, b;
		for( b = 0; b < lenCHGD; b = b + 1 ) { 
			var lechangedTouch = ar_CHGDtouches[ b ];
			var color, border;
			var target = lechangedTouch.target; 
			if( target.className.indexOf( "gestureCOL" ) >= 0 ) { 
				color = lechangedTouch.target.id;
			}
			else 
			if( target.className.indexOf( "gestureBRDR" ) >= 0) { 
				border = lechangedTouch.target.id;
			}
			else 
			if( ( target.className.indexOf( "gestureINTFC" ) >= 0 ) && (!_tchDragging) ) { 
				var draggingX = ( lechangedTouch.pageX - ( donLuchoHAKI.PixelsArePixelsArePixels.GetOffsetLeft( lechangedTouch.target ) ) ); // console.log( "draggingX" , draggingX );
				var draggingY = ( lechangedTouch.pageY - ( donLuchoHAKI.PixelsArePixelsArePixels.GetOffsetTop( lechangedTouch.target ) ) ); // console.log( "draggingY" , draggingY );
				_tchDragging = [ draggingX , draggingY ];
			}
			if (color) {
				_el_gesture.style.backgroundColor = color;
			}
			else
			if(border) {
				_el_gesture.style.border = "3px " + border + " #000000";
			}
		}
	} // END function
	
	// TOUCH ~ interaction.html
	function _TM_interact( leEvt ) { 
		var ar_CHGDtouches = leEvt.changedTouches, lenCHGD = ar_CHGDtouches.length, b;
		for( b = 0; b < lenCHGD; b = b + 1 ) { 
			var lechangedTouch = ar_CHGDtouches[ b ];
			if ( _tchDragging && !_tchSizing && ( lechangedTouch.target.className.indexOf( "gestureINTFC" ) >= 0 ) ) {
				// alert( lechangedTouch.target.id );
				lechangedTouch.target.style.left = leEvt.pageX - _tchDragging[ 0 ];
				lechangedTouch.target.style.top = leEvt.pageY - _tchDragging[ 1 ];
			}
		}
		donLuchoHAKI.Utils.evt_u.RetValFalprevDef( leEvt );
	} // END function
	
	// TOUCH ~ interaction.html
	function _TE_interact( leEvt ) { 
		var ar_CHGDtouches = leEvt.changedTouches, lenCHGD = ar_CHGDtouches.length, b;
		for( b = 0; b < lenCHGD; b = b + 1 ) { 
			var lechangedTouch = ar_CHGDtouches[ b ];
			var target = lechangedTouch.target; 
			if( target.className.indexOf( "gestureINTFC" ) >= 0 ) { 
				if( leEvt.targetTouches.length === 1 ) { 
					var draggingX = ( leEvt.targetTouches[0].pageX - ( donLuchoHAKI.PixelsArePixelsArePixels.GetOffsetLeft( leEvt.targetTouches[0].id ) ) ); // console.log( "draggingX" , draggingX );
					var draggingY = ( leEvt.targetTouches[0].pageY - ( donLuchoHAKI.PixelsArePixelsArePixels.GetOffsetTop( leEvt.targetTouches[0].id ) ) ); // console.log( "draggingY" , draggingY );
					_tchDragging = [ draggingX , draggingY ];
				}

				else 
				if( !leEvt.targetTouches.length ) {
					_tchDragging = false;
				}
			}
		} // END b
		var color = "grey", border = "none";
		var ar_ALLtouches = leEvt.touches, lenALL = ar_ALLtouches.len, e;
		for( e = 0; e < lenALL; e = e + 1 ) { 
			var letouch = ar_ALLtouches[ e ];
			var target = letouch.target; 
			if( target.className.indexOf( "gestureCOL" ) >= 0 ) { 
				color = target.id;
			} 
			if( target.className.indexOf( "gestureBRDR" ) >= 0) { 
				border = target.id;
			} 
		} // END e 
		_el_gesture.style.backgroundColor = color;
		_el_gesture.style.border = "3px " + border + " #000000";
	} // END function
	
	// TOUCH ~ interaction.html
	function _GS_interact( leEvt ) {
		var node = leEvt.target;
		_tchSizing = [ donLuchoHAKI.PixelsArePixelsArePixels.GetOffsetWidth( node ) , donLuchoHAKI.PixelsArePixelsArePixels.GetOffsetHeight( node ) ];
	} // END function
	
	// TOUCH ~ interaction.html
	function _GC_interact( leEvt ) {
		if( _tchSizing ) {
			var node = leEvt.target;
			node.style.width = Math.min( _tchSizing[0] * leEvt.scale, 300) + "px";
			node.style.height = Math.min( _tchSizing[1] * leEvt.scale, 300) + "px";
			node.style[ transform ] = "rotate(" + ( ( _tchRotating + leEvt.rotation ) % 360 ) + "deg)";
		}
	} // END function
	
	// TOUCH ~ interaction.html
	function _GE_interact( leEvt ) {
		_tchSizing = false;
		_tchRotating = (_tchRotating + leEvt.rotation ) % 360;
	} // END function
	
	// TRANSFORM ~ two sided business card
	function _AdjustBizCard() {
		
		if( _actv_Page === "transform.html" ) {
			
			if( _docObj.documentElement.addEventListener ){
				
				_el_bizCardOne.style[ transition ] = "1.40625s all";
				_el_bizCardOne.style[ transformStyle ] = "preserve-3d"; 
				
				_el_frontBiz.style[ backfaceVisibility ] = "hidden";
				_el_frontBiz.style[ transform ] = "rotateX(180deg) scaleY(-1)"; 	
				
				_el_frontBiz.style[ backfaceVisibility ] = "hidden";
				_el_frontBiz.style[ transform ] = "rotateX(0deg)";
				
				if( ( _docObj.documentElement.clientWidth > 1024 ) || ( _docObj.documentElement.clientWidth <= 800 ) ){
					_el_backBiz.style.height = _el_frontBiz.offsetHeight + "px";
					_el_backBiz.style.width = _el_frontBiz.offsetWidth + "px";
					_el_backBiz.style.left = ( parseInt( ( (_GetComputedStyle( _el_backBiz, "right" )) - ( .25 *(_GetComputedStyle( _el_backBiz, "right" ))) ), 10 ) ) + "px";	
				}
				else
				if( ( _docObj.documentElement.clientWidth > 800 ) && ( _docObj.documentElement.clientWidth <= 1024 )){
					_el_backBiz.style.height = _el_frontBiz.offsetHeight + "px";
					_el_backBiz.style.width = _el_frontBiz.offsetWidth + "px";
					_el_backBiz.style.left = ( parseInt( ( _GetComputedStyle( _el_backBiz, "right" ) ), 10 ) ) + "px"; 
				}
			}
			
			else
			if( _docObj.documentElement.attachEvent ){
				
				/* 
				_el_backBiz.style.height = _el_frontBiz.offsetHeight + "px";
				// _el_backBiz.style.top = -( _el_frontBiz.offsetHeight ) + "px";
				
				_el_backBiz.style.width = _el_frontBiz.offsetWidth + "px";
				
				// _el_backBiz.style.left = ( parseInt( ( _GetComputedStyle( _el_backBiz, "right" ) / 2 ), 10 ) ) + "px";
				_el_backBiz.style.left = ( parseInt( ( _GetComputedStyle( _el_backBiz, "right" ) ), 10 ) ) + "px";
				
				 */
			}
			
		}
		
	} // END function
	
	// TRANSITION ~ ZOOM card 
	function _AdjustZoomFlipper() {
		
		if( _actv_Page === "transition.html" ) {
			if( _docObj.documentElement.addEventListener ){
				// -- _ZoomZoneSetUp() --------------- 
				_el_zoomWrapper.style.height = (parseInt( _fFHeight , 10 ) + "px"); // 1.375 1.125
				_el_zoomWrapper.style.width = (parseInt( _fFWidth , 10 ) + "px");
				
				_el_zoomCardOne.style.marginTop = ( .125 * (parseInt( _fFHeight , 10 ) + "px") ) + "px";
				_el_zoomCardOne.style.height = (parseInt( _fFHeight , 10 ) + "px"); // 1.375 1.125
				_el_zoomCardOne.style.width = (parseInt( _fFWidth , 10 ) + "px");
				
				_el_pBOne.setAttribute( "width" , (parseInt( _fFWidth , 10 ) + "px") );
				_el_pBOne.setAttribute( "height" , (parseInt( _fFHeight , 10 ) + "px") );
				
				_el_pFOne.setAttribute( "width" , (parseInt( _fFWidth , 10 ) + "px") );
				_el_pFOne.setAttribute( "height" , (parseInt( _fFHeight , 10 ) + "px") );	
				
				_el_zFOne.style.height = (parseInt( _fFHeight , 10 ) + "px");
				_el_zFOne.style.width = (parseInt( _fFWidth , 10 ) + "px");
				
				_el_zBOne.style.height = (parseInt( _fFHeight , 10 ) + "px"); ////
				_el_zBOne.style.width = (parseInt( _fFWidth , 10 ) + "px");
				
				// -- end _ZoomZoneSetUp() ---------------
			}
		}
		
	} // END function
	
	// TRANSFORM ~ law
	function _AdjustLawFlipper() {
		
		if( _actv_Page === "transform.html" ) {
			
			if( _docObj.documentElement.addEventListener ){
				
				///// if( ( _docObj.documentElement.clientWidth > 1024 ) || ( _docObj.documentElement.clientWidth <= 800 ) ){} else if( ( _docObj.documentElement.clientWidth > 800 ) && ( _docObj.documentElement.clientWidth <= 1024 )) {}
				
				
				// TRANSFORM ~ law
				_el_backLaw.style.width = _el_frontLaw.offsetWidth + "px";
				_el_backLaw.style.height = _el_frontLaw.offsetHeight + "px";
				
				// if VP GT 1ST mq...
				///// _el_lawCardOne.style[ transformOrigin ] = "37% 50% 0"; // "50% 50% 0"
				///// _el_lawCardOne.style[ transformOrigin ] = "50% 50% 0"; // "50% 50% 0"	
				
				// TRANSFORM ~ law
				// if VP GT 1ST mq...
				///// _el_backLaw.style.left = ( ( _GetComputedStyle( _el_frontLaw.parentElement, "padding-left" ) + _GetComputedStyle( _el_frontLaw.parentElement, "margin-left" ) ) + ( _el_frontLaw.offsetWidth / 2 ) ) + "px";
				
				// TRANSFORM ~ law
				// UNSURE OF PURPOSE
				///// _el_backLaw.style.left = ( parseInt( ( _GetComputedStyle( _el_backLaw, "right" ) / 2 ), 10 ) ) + "px";
				
			}
			
			else
			if( _docObj.documentElement.attachEvent ){
				/*
				
				// TRANSFORM ~ law
				_el_backLaw.style.width = _el_frontLaw.offsetWidth + "px";
				_el_backLaw.style.height = _el_frontLaw.offsetHeight + "px";
				
				// UNSURE OF PURPOSE
				// _el_backLaw.style.left = ( parseInt( ( _GetComputedStyle( _el_backLaw, "right" ) / 2 ), 10 ) ) + "px";
				
				// if VP GT 1ST mq...
				// _el_backLaw.style.left = ( ( _GetComputedStyle( _el_frontLaw.parentElement, "padding-left" ) + _GetComputedStyle( _el_frontLaw.parentElement, "margin-left" ) ) + ( _el_frontLaw.offsetWidth / 2 ) ) + "px";
				
				*/
			}
			
		}
		
	} // END function
	
	
	
	/*################  INIT function  ######################*/
	function _LOAD() { 
		
		// ALL BROWSERS
		// ~ DATE OBJECT(YEAR) functionality
		// var dblQt = "\u0022"; // "
		var vrtBr = "\u007C"; // |
		var dshCh1 = "\u002D"; // - 
		var year = _dtObj.getFullYear();
		var liteStr = "2008 " + dshCh1 + " " + year + "";
		
		if( ( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) || ( document.documentElement.attachEvent && !!( _GetCookie(DEFAULTLANGUAGE) ) ) ) { 
			if( document.documentElement.attachEvent && !!( _GetCookie(DEFAULTLANGUAGE) ) ) {
				var xtraStr = _obj_ConfigAll.CannedStrings.dateObjOps.assets.nonFormatString.dtObjPrezzo[_GetCookie(DEFAULTLANGUAGE)].fullText + vrtBr + " " + liteStr + "";
			}
			else
			if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
				var xtraStr = _obj_ConfigAll.CannedStrings.dateObjOps.assets.nonFormatString.dtObjPrezzo[window.localStorage.getItem("defaultLanguage")].fullText + vrtBr + " " + liteStr + "";
			}
		}
		else { 
			if( ( document.documentElement.attachEvent && !( _GetCookie(DEFAULTLANGUAGE) ) ) || ( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) ) {
				var xtraStr = _obj_ConfigAll.CannedStrings.dateObjOps.assets.nonFormatString.dtObjPrezzo.EN.fullText + vrtBr + " " + liteStr + "";
			}
		}
		
		_el_placedLow.innerHTML = xtraStr;
		
		// 1 ~ WINDOW RESIZE METRICS functionality ~ TESTING PURPOSES
		////_el_logDetails.innerHTML = _ViewportWidth + _DeviceWidth + _PageWidth; 
			
		_RunUnfrmNavLis();
		
		/*###########  ~ CROSSOVER functionality ###########*/ 
		// EXTRA ~ deviceorientation functionality || devicemovement functionality ~ device.html
		if( _actv_Page === "device.html" ) {
			
			if( document.documentElement.addEventListener ){
				_SetGardenPoint(); 
				_CompassResizer();
			}
			
		}
		
		// ~ ELEVATOR LINK functionality ~ upTown\downTown
		// _scrollY_elev = 0;
		
		for( _E = 0; _E < _all_anchors.length; _E = _E + 1 ) { 
			
			if( _all_anchors[ _E ].className === "upTown" || _all_anchors[ _E ].className === "downTown" ) {
			
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _all_anchors[ _E ] , "mousedown" , function ( leEvt ) {
					var leTarget_elev = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
					if (leTarget_elev.className === "upTown" && leTarget_elev.nodeName.toLowerCase() === "a") {
						_tgtELId_elev = leTarget_elev.id.slice( 0 , leTarget_elev.id.length - 4 ); 
						_tgtEl_elev = _docObj.getElementById( _tgtELId_elev );
						_UpUpAndAway( _tgtELId_elev );
					}
				} );	
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _all_anchors[ _E ] , "mousedown" , function ( leEvt ) {
					var leTarget_elev = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
					if (leTarget_elev.className === "downTown" && leTarget_elev.nodeName.toLowerCase() === "a") {
						_tgtELId_elev = leTarget_elev.id.slice( 0 , leTarget_elev.id.length - 3 ); 
						_tgtEl_elev = _docObj.getElementById( _tgtELId_elev );
						_GetDownOnIt( _tgtELId_elev );
					}
				} ); 
				
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _all_anchors[ _E ] , "click" , _PrevDeffo ); 
				
			} // END if( _all_anchors[ _E ].className === "upTown" || _all_anchors[ _E ].className === "downTown" )
		} // END for
		
		_Cnv_Resizer();
		
		// ~ CANVAS functionality
		var ctx = _Cnv_Ctx();		
		// ~ CANVAS functionality
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_cnv , "touchstart" , _Cnv_TouchHandler );
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_cnv , "touchmove" , _Cnv_TouchHandler );
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_cnv , "touchend" , _Cnv_TouchHandler ); 		
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_cnv , "mousedown" , _Cnv_MouseHandler );
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_cnv , "mousemove" , _Cnv_MouseHandler );
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_cnv , "mouseup" , _Cnv_MouseHandler ); 
		
		// ~ CANVAS functionality
		_Cnv_DRY_Paint_init( _el_cnv , ctx);
		
		// ~ PageFlip Functionality
		_JabezBG_Ops();
		
		// ~ PageFlip Functionality
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _docObj.getElementById( "leftpage" ), "click", _PageLeft );
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _docObj.getElementById( "rightpage" ), "click", _PageRight ); 
		
		// END ~ ALL BROWSERS
		
		if(document.documentElement.addEventListener) { 
			
			// The big payback!
			_docObj.body.addEventListener( "offline" ,function (){
				_UpdateOnlineStatus("offline");
			},false); 
			
			_docObj.body.addEventListener( "online" ,function (){
				_UpdateOnlineStatus("online");
			},false); 
			
			// ~ Loadicon Behaviors functionality ~ Inject Keyframe Rule
			_el_loadicon.style[ _js_animation ] = "spin 1.406s linear infinite";	
			
			var KF_STR = "" +
			_keyframes + " spin { "
				+ "from {" 
					+ _css_transform + ":rotate( 0deg );" 
					+ " opacity: 0.4; " 
				+" }" 
				+ "50% {" 
					+ _css_transform + ":rotate( 180deg );" 
					+ " opacity: 1.0;" 
				+" }" 			
				+ "to {" 
					+ _css_transform + ":rotate( 360deg );" 
					+ " opacity: 0.4;" 
				+ " }" 		
			+ "}";
			
			var leFrag = _docObj.createDocumentFragment();
			var leStyle = _docObj.createElement( "style" );
			leStyle.type = "text/css";
			// leStyle.setAttribute( "media", "all,screen,projection" );
			leStyle.setAttribute( "media", "screen" );
			leStyle.appendChild( _docObj.createTextNode( KF_STR ) ); // console.log( leStyle );
			leFrag.appendChild( leStyle );
			_leHead.appendChild( leFrag ); // console.log( _leHead );


			
			
			// INSPIRATION ~ XHR ~ COMBO ~ AC ~ "excluding "iOS4" ~ FIRST WAVE  ~ url adjustments
			if( _ar_urlPath.length === _nm_lenThresh ){

				// console.log( 'carving a path to XHR assets' );
				// console.log( '_ar_urlPath.length' , _ar_urlPath.length );
				// console.log( '_url_content : ...' );
				// console.log( 'pathLVL0' , _obj_ConfigAll.AssetURLs.xhrOps.assets.content.pathLVL0 );
				// console.log( 'asset' , _obj_ConfigAll.AssetURLs.xhrOps.assets.content.asset );
				
				if( (_str_host === 'donlucho.github.io') || ( _str_host === 'artavia.gitlab.io') ){	
					_url_content = "" + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.pathLVL0_IO + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.asset + "";
				} else {
					_url_content = "" + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.pathLVL0 + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.asset + "";
				} 

				// console.log( '_url_content' , _url_content );
			}
			else
			if( _ar_urlPath.length > _nm_lenThresh ){
				var z;
				var xhr_tackOn = _ar_urlPath.length - _nm_lenThresh;
				var xhrPrefixer = "";
				for( z = 0; z < xhr_tackOn; z = z + 1 ){
					xhrPrefixer = _str_Pullback + xhrPrefixer;
				}

				// console.log( 'carving a path to XHR assets' );
				// console.log( '_ar_urlPath.length' , _ar_urlPath.length );
				// console.log( '_url_content : ...' );
				// console.log( 'pathLVL0' , _obj_ConfigAll.AssetURLs.xhrOps.assets.content.pathLVL0 );
				// console.log( 'asset' , _obj_ConfigAll.AssetURLs.xhrOps.assets.content.asset );
				// console.log( 'xhr_tackOn' , xhr_tackOn );
				// console.log( 'xhrPrefixer' , xhrPrefixer );

				if( (_str_host === 'donlucho.github.io') || ( _str_host === 'artavia.gitlab.io') ){
					_url_content = "" + xhrPrefixer + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.pathLVL0_IO + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.asset + "";
				} else {
					_url_content = "" + xhrPrefixer + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.pathLVL0 + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.asset + "";
				} 

				// console.log( '_url_content' , _url_content );
			}
			
			// INSPIRATION ~ XHR ~ COMBO Content Electric ~ APPLICATION CACHE ~ "W3C excluding "iOS4"
			if( ( !!(window.navigator.cookieEnabled)) && ( !!( window.localStorage ) ) ){ 
				
				if( !!( "localStorage" in window ) ) {
					
					// WEB STORAGE || COOKIE FUNC.
					_obj_locsto_lang = window.localStorage || {}; // LOCAL STORAGE ~ ASSIGNMENT
					
					// WEB STORAGE || COOKIE FUNC.
					if( !( _obj_locsto_lang.getItem("defaultLanguage") ) ) {
						_el_logDetails.innerHTML = _obj_ConfigAll.CannedStrings.linguaOps.assets.nonFormatString.deffoLingo.intro;
					}
					
					// LANGUAGE SWITCHER FUNC.
					for (r = 0; r < _anchoMax; r = r + 1) {
						if (_all_anchors[r].className === _classOBJ.d ) { 
							donLuchoHAKI.Utils.evt_u.AddEventoHandler(_all_anchors[r], "click", donLuchoHAKI.Utils.evt_u.RetValFalprevDef );
							_all_anchors[r].innerHTML = _textOBJ.a;
						}
					} // END LANGUAGE SWITCHER FUNC.
					
					// LANGUAGE SWITCHER FUNC.
					for (_oneShort = 0; _oneShort < _LIMax; _oneShort = _oneShort + 1) {
						if ( _allLIs[_oneShort].className === _classOBJ.a ) {
							donLuchoHAKI.Utils.evt_u.AddEventoHandler( _allLIs[_oneShort], "click", _ChangeSrcLangAndBack );
						}
					} // END LANGUAGE SWITCHER FUNC.
					
					// absolute final step
					_LastSpike_wThreeC();
				}
			}
			else 
			if(!window.navigator.cookieEnabled){
				_el_logDetails.innerHTML = "<p>" + _obj_ConfigAll.CannedStrings.linguaOps.assets.nonFormatString.adjustUrStng.intro + "</p>";
			} // END if(!window.navigator.cookieEnabled)	
			// END INSPIRATION ~ XHR ~ COMBO Content Electric ~ APPLICATION CACHE ~ "W3C excluding "iOS4"
			
			// NEXT!!!
			
			// universal HIDE URL BAR functionality
			if (_chopper === "iPod" || _chopper === "iPhone") { 
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( window, "load", function() {
					if (document.documentElement.addEventListener) {
						if (window.mozRequestAnimationFrame) {
							window.mozRequestAnimationFrame(_HideURLbar);
						}
						else if (window.webkitRequestAnimationFrame) {
							window.webkitRequestAnimationFrame(_HideURLbar);
						}
						else {
							window.setTimeout(_HideURLbar, 0);
						}
					}
				} );
			} // END if "chopper" condition ~ HIDE URL bar functionality
			
			// ~ Carousel functionality ~ image gallery
			/*###########  ~ Carousel functionality ~ image gallery ###########*/ 
			if ( _str_href.match(/next/)) {
			
				////_el_logDetails.innerHTML = "#crutch offsetWidth: " + _el_crutch.offsetWidth + "px | ";
				
				_el_AvgBtnCon.style.display = "block";
				
				for( _B = 0; _B < _btnCt; _B = _B + 1 ){
					if( _all_btns[ _B ].className === "caroButton" ) {
						donLuchoHAKI.Utils.evt_u.AddEventoHandler( _all_btns[ _B ], "click", _RunCarousel );	
					}
				} 
				
				_CaroResizer();
			} // EMD if ( _str_href.match(/next/)) 
			
			// ~ Page Visibility functionality
			if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
				if( !!_stoProp ) {
					_el_pro.style.display = "block";
					_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.localStorageOps.assets.formattedString.pro[ _obj_locsto_lang.getItem("defaultLanguage") ].fullText;
					
					// sessionStorage.thisTitle = _peeVeeLite_configObject.thisTitle;
					_title_sessto.setItem("thisTitle", _peeVeeLite_configObject.thisTitle);
					
					if ( !!_firstProp ) { 
						_el_pro.style.display = "block";
						_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.pageVisibilityOps.assets.formattedString.pro[ _obj_locsto_lang.getItem("defaultLanguage") ].fullText;
						
						var V; 
						var visAr = donLuchoHAKI.Utils.aR_u.htmlFive.visibilitychange.valueOf(); // ["visibilitychange", "mozvisibilitychange", "msvisibilitychange", "webkitvisibilitychange"]
						var visMax = visAr.length;
						for ( V = 0; V < visMax; V = V + 1) {
							// LUCHORAMA ~ undefined 
							if( document[ visAr[ V ] ] !== "undefined" ) { // _docObj.addEventListener( visAr[ V ] , ChangeReadOut, false);
								donLuchoHAKI.Utils.evt_u.AddEventoHandler( donLuchoHAKI.RetELs.docObj(), visAr[ V ], ChangeReadOut );
							}
						}
					}
					else { 
						// ~ Page Visibility functionality
						_el_anti.style.display = "block";
						_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.pageVisibilityOps.assets.formattedString.anti[ _obj_locsto_lang.getItem("defaultLanguage") ].fullText;
					}

				}
				else { 
					// ~ Page Visibility functionality
					_el_anti.style.display = "block";
					_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.localStorageOps.assets.formattedString.anti[ _obj_locsto_lang.getItem("defaultLanguage") ].fullText;
				}
			}
			else 
			if( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) {	
				if( !!_stoProp ) { 
					_el_pro.style.display = "block";
					_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.localStorageOps.assets.formattedString.pro.EN.fullText;
					
					// sessionStorage.thisTitle = _peeVeeLite_configObject.thisTitle;
					_title_sessto.setItem("thisTitle", _peeVeeLite_configObject.thisTitle);
					
					if ( !!_firstProp ) { 
						_el_pro.style.display = "block";
						_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.pageVisibilityOps.assets.formattedString.pro.EN.fullText;
						
						var V; 
						var visAr = donLuchoHAKI.Utils.aR_u.htmlFive.visibilitychange.valueOf(); // ["visibilitychange", "mozvisibilitychange", "msvisibilitychange", "webkitvisibilitychange"]
						var visMax = visAr.length;
						for ( V = 0; V < visMax; V = V + 1) {
							// LUCHORAMA ~ undefined 
							if( document[ visAr[ V ] ] !== "undefined" ) { // _docObj.addEventListener( visAr[ V ] , ChangeReadOut, false);
								donLuchoHAKI.Utils.evt_u.AddEventoHandler( donLuchoHAKI.RetELs.docObj(), visAr[ V ], ChangeReadOut );
							}
						}
					}
					else { 
						// ~ Page Visibility functionality
						_el_anti.style.display = "block";
						_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.pageVisibilityOps.assets.formattedString.anti.EN.fullText;
					}

				}
				else { 
					// ~ Page Visibility functionality
					_el_anti.style.display = "block";
					_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.localStorageOps.assets.formattedString.anti.EN.fullText;
				}
			}
			
			
			// ~ Toolbar functionality ~ Only for MozillaFF at the moment
			if( _chopper === "Firefox" && ( _ar_urlPath.length === _nm_lenThresh )) { 
				var urlFRAG = _docObj.createDocumentFragment();
				var ctxMX = _ctxmnu_AR.length;
				var M; 
				var betaObj; 
				for( M = 0; M < ctxMX; M = M + 1 ) { 
					if( typeof _ctxmnu_AR[M] === "object" && _ctxmnu_AR[M][ "eL" ] === "menuitem" ) { 
						// console.log( "menuitem" , _ctxmnu_AR[M] ); 
						// console.log( "menuitem" , _ctxmnu_AR[M][ "eL" ] );
						betaObj = _docObj.createElement( _ctxmnu_AR[M][ "eL" ] );
						betaObj.setAttribute( "label" , _ctxmnu_AR[M][ "label" ] );
						betaObj.setAttribute( "onclick" , _ctxmnu_AR[M][ "onclick" ] );
						betaObj.setAttribute( "icon" , _ctxmnu_AR[M][ "icon" ] );
						betaObj.appendChild( _docObj.createTextNode( M ) ); 
					} 
					urlFRAG.appendChild( betaObj );
				}
				_el_TBar_API.appendChild( urlFRAG );
			}
			else
			if( _chopper === "Firefox" && ( _ar_urlPath.length > _nm_lenThresh )) { 
				var W;
				var udf_nm_tackOn = _ar_urlPath.length - _nm_lenThresh;
				var menuPrefixer = "";
				for( W = 0; W < udf_nm_tackOn; W = W + 1 ){
					menuPrefixer = _str_Pullback + menuPrefixer;
				}
				var urlFRAG = _docObj.createDocumentFragment();
				var ctxMX = _ctxmnu_AR.length;
				var M; 
				var betaObj; 
				for( M = 0; M < ctxMX; M = M + 1 ) { 
					if( typeof _ctxmnu_AR[M] === "object" && _ctxmnu_AR[M][ "eL" ] === "menuitem" ) { 
						// console.log( "menuitem" , _ctxmnu_AR[M] ); 
						// console.log( "menuitem" , _ctxmnu_AR[M][ "eL" ] );
						betaObj = _docObj.createElement( _ctxmnu_AR[M][ "eL" ] );
						betaObj.setAttribute( "label" , _ctxmnu_AR[M][ "label" ] );
						betaObj.setAttribute( "onclick" , _ctxmnu_AR[M][ "onclick" ] ); 
						betaObj.setAttribute( "icon" , menuPrefixer + _ctxmnu_AR[M][ "icon" ] ); 
						betaObj.appendChild( _docObj.createTextNode( M ) ); 
					} 
					urlFRAG.appendChild( betaObj );
				}
				_el_TBar_API.appendChild( urlFRAG );
			} 
			// END if "chopper" condition ~ MozFF Toolbar functionality
			// ~ END ~ Toolbar functionality ~ Only for MozillaFF at the moment
			
			// ~ Fullscreen functionality
			if ( !_str_href.match(/next/) ) {
				
				// ~ Fullscreen functionality
				_requestFullScreen = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "requestFullScreen" , _docEL ); 
				
				if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
				
					if ( !!_requestFullScreen ) {
						
						_cancelFullScreen = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "cancelFullScreen" , _docObj ); 
						
						_el_fsBtnCon.style.display = "block";
						_el_fsLaunch.style.display = "block"; 
						_el_pro.style.display = "block";
						
						_el_pro.innerHTML += "<p>" + _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.pro[ _obj_locsto_lang.getItem("defaultLanguage") ].fullText + _requestFullScreen + _obj_ConfigAll.CannedStrings.genericPhrasesOps.assets.formattedString.whatIs_and[ _obj_locsto_lang.getItem("defaultLanguage") ].fullText + _cancelFullScreen + "</p>";						
						/*
						_el_pro.innerHTML += "<p>" + _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.pro[ _obj_locsto_lang.getItem("defaultLanguage") ].fullText + _requestFullScreen + _obj_ConfigAll.CannedStrings.genericPhrasesOps.assets.formattedString.whatIs_and[ _obj_locsto_lang.getItem("defaultLanguage") ].fullText + _cancelFullScreen + "</p>";
						*/
						
						// ~ Fullscreen functionality
						var F;
						_ar_TrgrAs_fs = _el_fsBtnCon.getElementsByTagName("a");
						_trgrMax_fs = _ar_TrgrAs_fs.length;
						
						for( F = 0; F < _trgrMax_fs; F = F + 1 ){
							if( _ar_TrgrAs_fs[ F ].className === "fs" && _ar_TrgrAs_fs[ F ].nodeName.toLowerCase() === "a" ){							
								if( _ar_TrgrAs_fs[ F ].id === _el_fsLaunch.id ){
									donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_TrgrAs_fs[ F ] , "mousedown" , _LaunchFullscreen ); 
									donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_TrgrAs_fs[ F ] , "click" , _PrevDeffo ); 
								}
								else
								if( _ar_TrgrAs_fs[ F ].id === _el_fsRestore.id ){
									donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_TrgrAs_fs[ F ] , "mousedown" , _CancelFullscreen ); 
									donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_TrgrAs_fs[ F ] , "click" , _PrevDeffo ); 
								}
							}// END if
						} // END for
						
					}
					else
					if ( !_requestFullScreen ) { 
						// console.log( "no can do fullscreen" );
						_el_anti.style.display = "block";
						
						if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
							_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.anti[ _obj_locsto_lang.getItem("defaultLanguage") ].fullText;
						}
					}
				
				}
				else 
				if( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) {
					if ( !!_requestFullScreen ) {
						
						_cancelFullScreen = donLuchoHAKI.ReturnProps.RetPropertyRoutine( "cancelFullScreen" , _docObj ); 
						
						_el_fsBtnCon.style.display = "block";
						_el_fsLaunch.style.display = "block"; 
						_el_pro.style.display = "block";
						_el_pro.innerHTML += "<p>" + _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.pro.EN.fullText + _requestFullScreen + _obj_ConfigAll.CannedStrings.genericPhrasesOps.assets.formattedString.whatIs_and.EN.fullText + _cancelFullScreen + "</p>";
						
						// ~ Fullscreen functionality
						var F;
						_ar_TrgrAs_fs = _el_fsBtnCon.getElementsByTagName("a");
						_trgrMax_fs = _ar_TrgrAs_fs.length;
						
						for( F = 0; F < _trgrMax_fs; F = F + 1 ){
							if( _ar_TrgrAs_fs[ F ].className === "fs" && _ar_TrgrAs_fs[ F ].nodeName.toLowerCase() === "a" ){							
								if( _ar_TrgrAs_fs[ F ].id === _el_fsLaunch.id ){
									donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_TrgrAs_fs[ F ] , "mousedown" , _LaunchFullscreen ); 
									donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_TrgrAs_fs[ F ] , "click" , _PrevDeffo ); 
								}
								else
								if( _ar_TrgrAs_fs[ F ].id === _el_fsRestore.id ){
									donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_TrgrAs_fs[ F ] , "mousedown" , _CancelFullscreen ); 
									donLuchoHAKI.Utils.evt_u.AddEventoHandler( _ar_TrgrAs_fs[ F ] , "click" , _PrevDeffo ); 
								}
							}// END if
						} // END for
						
					}
					else
					if ( !_requestFullScreen ) { 
						
						// console.log( "no can do fullscreen" );
						
						_el_anti.style.display = "block"; 
						
						if( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) {
							_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.fullScreenOps.assets.formattedString.anti.EN.fullText;
						}
					}
				
				}
				
			} // END if ( !_str_href.match(/next/) )
			
			// ~ Geolocation functionality ~ undry
			_wboolyIf_Geo = donLuchoHAKI.html5_Geolocation_API.If_GeolocationAPI();
			
			if( !!_wboolyIf_Geo ){
				
				_el_geolo.style.display = "block";
				
				if( document.documentElement.addEventListener && ( !!( window.localStorage.getItem("defaultLanguage") ) ) ) {
					_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.geoloOps.assets.formattedString.pro[ _obj_locsto_lang.getItem("defaultLanguage") ].fullText;
				}
				else 
				if( document.documentElement.addEventListener && ( !( window.localStorage.getItem("defaultLanguage") ) ) ) {
					_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.geoloOps.assets.formattedString.pro.EN.fullText;
				}
				
				_el_pro.style.display = "block";
				
				if( !!_wboolyIf_Geo && (_chopper === "Safari_5") ){
					_el_geolo.style.display = "none";
				}
				else
				if( !!_wboolyIf_Geo && (_chopper !== "Safari_5") ){
					_el_geolo.style.display = "block";
					_Geolo_Profile();
				}
			}
			// else {
			else
			if( !_wboolyIf_Geo ){
				
				if( ( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) || ( document.documentElement.attachEvent && !!( _GetCookie(DEFAULTLANGUAGE) ) ) ) {
				
					if( document.documentElement.addEventListener && !!( window.localStorage.getItem("defaultLanguage") ) ) {
						if( !_wboolyIf_Geo && document.documentElement.addEventListener) {
							_el_anti.style.display = "block";
							_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.geoloOps.assets.formattedString.anti[window.localStorage.getItem("defaultLanguage")].fullText;
						}
						else
						if( !_wboolyIf_Geo && document.documentElement.attachEvent ){
							_el_anti.style.display = "block";
							_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.geoloOps.assets.formattedString.antiAlt[window.localStorage.getItem("defaultLanguage")].fullText;
						}
					}
					else 
					if( document.documentElement.attachEvent && !!( _GetCookie(DEFAULTLANGUAGE) ) ) {
						if( !_wboolyIf_Geo && document.documentElement.addEventListener) {
							_el_anti.style.display = "block";
							_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.geoloOps.assets.formattedString.anti[_GetCookie(DEFAULTLANGUAGE)].fullText;
						}
						else
						if( !_wboolyIf_Geo && document.documentElement.attachEvent ){
							_el_anti.style.display = "block";
							_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.geoloOps.assets.formattedString.antiAlt[_GetCookie(DEFAULTLANGUAGE)].fullText;
						}
					}
					
				}
				else
				if( ( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) || ( document.documentElement.attachEvent && !( _GetCookie(DEFAULTLANGUAGE) ) ) ) {
				
					if( ( document.documentElement.addEventListener && !( window.localStorage.getItem("defaultLanguage") ) ) ) {
						if( !_wboolyIf_Geo && document.documentElement.addEventListener) {
							_el_anti.style.display = "block";
							_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.geoloOps.assets.formattedString.anti.EN.fullText;
						}
					}
					else
					if( ( document.documentElement.attachEvent && !( _GetCookie(DEFAULTLANGUAGE) ) ) ) {
						if( !_wboolyIf_Geo && document.documentElement.attachEvent ){
							_el_anti.style.display = "block";
							_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.geoloOps.assets.formattedString.antiAlt.EN.fullText;
						}
					}
				}
				
			}
			
			// ~ CANVAS functionality
			// _Cnv_DRY_Paint_init( _el_cnv , ctx);
			
			// TOUCH ~ untangle.html
			if( _actv_Page === "untangle.html" ) {
				for ( d = 0; d < _allDvMAX; d = d + 1 ) { 
					if( _allDIVs[ d ].className === "CTA" ) { 				
						donLuchoHAKI.Utils.evt_u.AddEventoHandler( _allDIVs[ d ], "touchstart" , _TS_untngl ); 
						donLuchoHAKI.Utils.evt_u.AddEventoHandler( _allDIVs[ d ], "touchmove" , _TM_untngl );
						donLuchoHAKI.Utils.evt_u.AddEventoHandler( _allDIVs[ d ], "touchend" , _TE_untngl ); 
					} // END if( _allDIVs[ d ].className === "CTA" )
				} // END for ( d = 0; d < _allDvMAX; d = d + 1 ) 
			} 
			
			// TOUCH ~ interaction.html
			if( _actv_Page === "interaction.html" ) { 
				
				_DeployKuma();	
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( donLuchoHAKI.RetELs.docObj(), "touchstart" , _TS_interact );
				for ( a = 0; a < _sparMax; a = a + 1 ) { // node = _sparDIVs[ a ].id; // console.log( node ); 
					donLuchoHAKI.Utils.evt_u.AddEventoHandler( _$xID( _sparDIVs[ a ].id ), "touchmove" , _TM_interact );
				} 
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( donLuchoHAKI.RetELs.docObj(), "touchend" , _TE_interact );
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_gesture, "gesturestart" , _GS_interact );
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_gesture, "gesturechange" , _GC_interact );
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_gesture, "gestureend" , _GE_interact ); 
			}
			
			// EXTRA ~ Axis functionality ~ just x\y poles... no Z
			if( document.documentElement.addEventListener && _actv_Page === "enhance.html" ){ 
				
				_CenterPoles();
				
				_el_View.style[ perspective ] = "800px"; 
				_el_View.style[ transformStyle ] = "preserve-3d";
				_el_View.style[ transition ] = "" + css_transform + " 0.3515625s"; // eg. - "-webkit-transform 1s"
				
				if( !_el_xSlide.defaultValue ) { 
					var defX = _el_xSlide.defaultValue = -40; //30
				} 
				if( !_el_ySlide.defaultValue ) { 
					var defY = _el_ySlide.defaultValue = 40;//-45
				} 
				_xVal = defX; 
				_yVal = defY; 
				
				_el_View.style[ transform ] = "rotateX(" + _xVal + "deg) rotateY("+ _yVal + "deg)";
				
				_el_Top.style[ transformOrigin ] = "50% 100%";
				_el_Top.style[ transition ] = "" + css_transform + " 0.3515625s";
				_el_Top.style[ transform ] = "translateY(-100px)"; 
				
				_el_Bottom.style[ transformOrigin ] = "50% 100%";
				_el_Bottom.style[ transition ] = "" + css_transform + " 0.3515625s";	
				
				_el_Bottom.style[ transform ] = "translateY(100px)"; 
				//
				_el_Ball.style[ transform ] = "translateZ(50px) rotateX(90deg) translateY(10px)";
				_el_Left.style[ transform ] = "translateZ(50px) translateX(-50px) rotateY(-90deg)";	
				_el_Right.style[ transform ] = "translateZ(50px) translateX(50px) rotateY(90deg)"; 
				_el_Front.style[ transform ] = "translateZ(100px)";
				_el_YAxis.style[ transform ] = "rotate(90deg)";
				_el_ZAxis.style[ transform ] = "rotateY(-90deg)"; // eg. - "-webkit-transform 1s"
				
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_openIt , "click" , _OpenBox ); 
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_closeIt , "click" , _CloseBox );
				
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_xSlide , "change" , _Set_poles_XY );
				donLuchoHAKI.Utils.evt_u.AddEventoHandler( _el_ySlide , "change" , _Set_poles_XY );	
			}
			
			// ~ next...
			
		} // END if addEventListener
		else
		if(document.documentElement.attachEvent) {
			
			// ~ Online Offline functionality
			_el_toggleState.style.display = "none";
			_el_colorBlock.style.display = "none";
			
			// if ( !_str_href.match(/next/) ) {
			if ( _actv_Page === "next.html" ) {
				// ~ Carousel NON AVAILABILITY ~ image gallery
				_el_caroLink.style.display = "none";
			}
			
			// ~ Geolocation functionality ~ undry
			_wboolyIf_Geo = donLuchoHAKI.html5_Geolocation_API.If_GeolocationAPI();
			if( !!( _GetCookie(DEFAULTLANGUAGE) ) ){
				if( !!_wboolyIf_Geo ){
					_el_pro.style.display = "block";
					_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.geoloOps.assets.formattedString.pro[_GetCookie(DEFAULTLANGUAGE)].fullText;
					
					_Geolo_Profile();
				}
				else
				if( !_wboolyIf_Geo ){
					_el_geolo.style.display = "none";
					_el_anti.style.display = "block";
					_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.geoloOps.assets.formattedString.antiAlt[_GetCookie(DEFAULTLANGUAGE)].fullText;
				}
			}
			else
			if( !( _GetCookie(DEFAULTLANGUAGE) ) ){
				if( !!_wboolyIf_Geo ){
					_el_pro.style.display = "block";
					_el_pro.innerHTML += _obj_ConfigAll.CannedStrings.geoloOps.assets.formattedString.pro.EN.fullText;
					
					_Geolo_Profile();
				}
				else
				if( !_wboolyIf_Geo ){
					_el_geolo.style.display = "none";
					_el_anti.style.display = "block";
					_el_anti.innerHTML += _obj_ConfigAll.CannedStrings.geoloOps.assets.formattedString.antiAlt.EN.fullText;
				}
			}
			
			// ~ CANVAS functionality
			// _Cnv_DRY_Paint_init( _el_cnv , ctx);
			
			// ~ next...
			// INSPIRATION ~ XHR ~ COMBO ~ AC ~ "excluding "iOS4" ~ FIRST WAVE  ~ url adjustments
			if( _ar_urlPath.length === _nm_lenThresh ){
				
				if( (_str_host === 'donlucho.github.io') || ( _str_host === 'artavia.gitlab.io') ){
					_url_content = "" + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.pathLVL0_IO + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.asset + "";
				} else {
					_url_content = "" + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.pathLVL0 + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.asset + "";
				} 

			}
			else
			if( _ar_urlPath.length > _nm_lenThresh ){
				var z;
				var xhr_tackOn = _ar_urlPath.length - _nm_lenThresh;
				var xhrPrefixer = "";
				for( z = 0; z < xhr_tackOn; z = z + 1 ){
					xhrPrefixer = _str_Pullback + xhrPrefixer;
				}
				

				if( (_str_host === 'donlucho.github.io') || ( _str_host === 'artavia.gitlab.io') ){
					_url_content = "" + xhrPrefixer + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.pathLVL0_IO + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.asset + "";
				} else {
					_url_content = "" + xhrPrefixer + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.pathLVL0 + _obj_ConfigAll.AssetURLs.xhrOps.assets.content.asset + "";
				} 

			}
			
			// INSPIRATION ~ XHR ~ COMBO Content Electric ~ APPLICATION CACHE ~ "W3C excluding "iOS4"
			if( ( !!(window.navigator.cookieEnabled)) && ( !( window.localStorage ) ) ){ 				
				
				if( !( "localStorage" in window ) ) {
					
					// WEB STORAGE || COOKIE FUNC.
					if( !( _GetCookie(DEFAULTLANGUAGE) ) ) {
						_el_logDetails.innerHTML = _obj_ConfigAll.CannedStrings.linguaOps.assets.nonFormatString.deffoLingo.intro;
					}
					
					// LANGUAGE SWITCHER FUNC.
					for (r = 0; r < _anchoMax; r = r + 1) {
						if (_all_anchors[r].className === _classOBJ.d ) { 
							donLuchoHAKI.Utils.evt_u.AddEventoHandler(_all_anchors[r], "click", donLuchoHAKI.Utils.evt_u.RetValFalprevDef );
							_all_anchors[r].innerHTML = _textOBJ.a;
						}
					} // END LANGUAGE SWITCHER FUNC.
					
					// LANGUAGE SWITCHER FUNC.
					for (_oneShort = 0; _oneShort < _LIMax; _oneShort = _oneShort + 1) {
						if ( _allLIs[_oneShort].className === _classOBJ.a ) {
							donLuchoHAKI.Utils.evt_u.AddEventoHandler( _allLIs[_oneShort], "click", _ChangeSrcLangAndBack );
						}
					} // END LANGUAGE SWITCHER FUNC.
					
					// absolute final step
					_LastSpike_IE();
				}
			}
			else 
			if(!window.navigator.cookieEnabled){
				_el_logDetails.innerHTML = "<p>" + _obj_ConfigAll.CannedStrings.linguaOps.assets.nonFormatString.adjustUrStng.intro + "</p>";
			} // END if(!window.navigator.cookieEnabled)
			// END INSPIRATION ~ XHR ~ COMBO Content Electric ~ APPLICATION CACHE ~ "W3C excluding "iOS4"
			
			// NEXT!!!
		}

		// alert("load");
		// console.log( "It\'s on, bebe! ~ load ");
		
		// ~ CANVAS functionality
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( _docObj , "keyup" , function ( leEvt ) { 
			/* versus leEvt.keyCode === ETC. */
			if(_docEL.addEventListener) {
				
				if( leEvt.which === 27 ) { 
					var canvas = _el_cnv;
					canvas.getContext("2d").clearRect( 0, 0, canvas.width, canvas.height );
				}
				if( leEvt.which === 80 ) {
					_pointMode = !_pointMode;
				}
				if( leEvt.which === 70) {
					_enableForce = !_enableForce;
				}
			}
			else
			if(_docEL.attachEvent) {
				if( leEvt.keyCode === 27 ) { 
					var canvas = _el_cnv;
					canvas.getContext("2d").clearRect( 0, 0, canvas.width, canvas.height );
				}
				if( leEvt.keyCode === 80 ) {
					_pointMode = !_pointMode;
				}
				if( leEvt.keyCode === 70) {
					_enableForce = !_enableForce;
				}
			}
		} );  // END event handler ()
		
		// ORIENTATIONCHANGE functionality
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( donLuchoHAKI.RetELs.glow(), "orientationchange", function() {
			
			if( _actv_Page === "device.html" ) {
				if( document.documentElement.addEventListener ){
					_SetGardenPoint(); 
					_CompassResizer();
				}
			}
			
			else
			if( _actv_Page === "transform.html" ) {
				if( document.documentElement.addEventListener ){
					
					// TRANSFORM ~ two sided business card
					_AdjustBizCard();
					
					// TRANSFORM ~ law
					_AdjustLawFlipper();
					
				}
			}
			
			else
			if( _actv_Page === "transition.html" ) {
				if(document.documentElement.addEventListener ) {
					// TRANSITION ~ ZOOM card 
					_AdjustZoomFlipper();
				}
			}
			
		} ); 
		
		// RESIZE functionality
		donLuchoHAKI.Utils.evt_u.AddEventoHandler( window, "resize", function() { 
			
			/*###########  ~ CROSSOVER functionality ###########*/
			// TOUCH ~ interaction.html
			if( _actv_Page === "interaction.html" ) { 
				_DeployKuma();
			}
			
			/*###########  ~ CANVAS functionality ###########*/ 
			_Cnv_Resizer();
			
			// ~ PageFlip Functionality
			_JabezBG_Ops(); 
			
			if(document.documentElement.attachEvent ) {
				// 1 ~ WINDOW RESIZE METRICS functionality ~ TESTING PURPOSES
				// _el_logDetails.innerHTML = window.document.documentElement.clientWidth + "px";
				////_el_logDetails.innerHTML = "#sectionContent offsetWidth: " + _docObj.getElementById("sectionContent").offsetWidth + "px";
			}
			
			else
			if(document.documentElement.addEventListener ) { 
				
				// ~ SELECTED LI functionality ~ 'custom_select' -- incl. IE and W3C for LOAD AND RESIZE
				_RunUnfrmNavLis();
				
				/*###########  ~ Carousel functionality ~ image gallery ###########*/	
				if( ! _str_href.match(/next/) ) {
					// 1 ~ WINDOW RESIZE METRICS functionality ~ TESTING PURPOSES
					////_el_logDetails.innerHTML = "#sectionContent offsetWidth: " + _docObj.getElementById("sectionContent").offsetWidth + "px";
					// window.innerWidth + "px" // "#body offsetWidth: " + _docObj.body.offsetWidth + "px"
				}
				else
				if( !! _str_href.match(/next/) ) {
					// 2 ~ Carousel functionality ~ image gallery
					////_el_logDetails.innerHTML = "#crutch offsetWidth: " + _el_crutch.offsetWidth + "px | ";
					_CaroResizer();	
				}
				
				// EXTRA ~ Axis functionality ~ just x\y poles... no Z
				if( !! _str_href.match(/enhance/) ) {
					_CenterPoles();
				}
				
				// TRANSITION ~ ZOOM card 
				if( _actv_Page === "transition.html" ) { 
					_AdjustZoomFlipper();
				}
				else
				if( _actv_Page === "transform.html" ) {
					
					// TRANSFORM ~ two sided business card
					_AdjustBizCard();
					
					// TRANSFORM ~ law
					_AdjustLawFlipper();
					
				}
				else
				if( _actv_Page === "device.html" ) {
					_SetGardenPoint(); 
					_CompassResizer();
				}
				
			}
			
		} );
		
	} // END function _LOAD()
	// END private section
	
	
	return { 
		StormOfD : function() { 
		
			// ~ ELEVATOR LINK functionality ~ upTown\downTown
			_scrollY_elev = 0; _loopTimer_elev = null;
			
			// ~ PageFlip Functionality
			_loopTimer_pFlip = null;
			
			/*###########  ~ Carousel functionality ~ image gallery ###########*/ 
			if(document.documentElement.addEventListener){ 
				// ~ Carousel functionality ~ image gallery
				/*###########  ~ Carousel functionality ~ image gallery ###########*/ 
				if ( _str_href.match(/next/)) {
					// ~ Carousel functionality ~ image gallery
					_curImg = 1; _startVal = null; _loopTimer_caro = null;
				}
			}
			
			/*###########  ~ CANVAS functionality ###########*/ 
			if(document.documentElement.attachEvent){ 
				// ~ CANVAS functionality
				_plusNewDate = new Date().getTime();
			}
			
			// INSPIRATION ~ XHR ~ COMBO ~ AC ~ "excluding "iOS4" ~ FIRST WAVE 
			_stringCheese = undefined;
			
			return _LOAD(); 
		} 
	}; // END public section ~ return values
}) (); 

donLuchoHAKI.Utils.evt_u.AddEventoHandler( donLuchoHAKI.RetELs.glow(), "load", donLuchoHAKI.RetELs.glow().FinishTheGame.StormOfD() ); // FIN. #2

// finito all